﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Linq;

namespace Business
{
    public class ControlPDFReportesEstadisticos
    {
        //Crear directorio 
        private static string CrearDirectorioControl(string nombre)
        {
            try
            {

                //Verificar si existe el directorio principal del repositorio de documentos
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio de la obra existe
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio para el grupo documental existe, sino crearlo
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre)));


                return string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs//" + nombre);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("No fue posible crear el repositorio para crear el archivo. ", ex.Message));
            }
        }


        //Crear celdas
        public static void Celda(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas para tablas
        public static void CeldaParaTablas(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, BaseColor colorrelleno, float espacio, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                celda.BackgroundColor = colorrelleno;
                celda.Padding = espacio;
                celda.UseAscender = true;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas vacias
        public static void CeldaVacio(PdfPTable tabla, int cantidad)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                celda.BorderWidth = 0;
                tabla.AddCell(celda);
            }

        }



        //para crear el Header y Footer
        public partial class HeaderFooter : PdfPageEventHelper
        {
            PdfContentByte cb;
            PdfTemplate headerTemplate;
            BaseFont bf = null;
            float tamanofuente = 8f;

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                }
                catch (DocumentException de) { }
                catch (System.IO.IOException ioe) { }
            }

            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                //Encabezado de Página
                Rectangle pageSize = doc.PageSize;
                PdfPTable footerTbl = new PdfPTable(1);
                BaseColor blau = new BaseColor(0, 61, 122);

                float[] TablaAncho = new float[] { 100 };
                footerTbl.SetWidths(TablaAncho);
                footerTbl.TotalWidth = 550;
                Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                PdfPCell cell;
                CeldaParaTablas(footerTbl, FuenteCelda, " ", 1, 0, 1, blau, 0, Element.ALIGN_TOP, Element.ALIGN_TOP);


                string pagina = "Página " + writer.PageNumber + " de ";
                //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
                {
                    float margenbottom = 11f;
                    float margenright = 280f;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, tamanofuente);
                    cb.SetTextMatrix(doc.PageSize.GetBottom(margenright), doc.PageSize.GetBottom(margenbottom));
                    cb.ShowText(pagina);
                    cb.EndText();
                    float len = bf.GetWidthPoint(pagina, tamanofuente);
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));
                }

                PdfPTable footerTbl2 = new PdfPTable(2);


                float[] TablaAncho2 = new float[] { 100, 100 };
                footerTbl2.SetWidths(TablaAncho2);
                footerTbl2.TotalWidth = doc.Right - doc.Left;
                Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                string dia = String.Format("{0:dd}", DateTime.Now);
                string anio = String.Format("{0:yyyy}", DateTime.Now);
                string hora = String.Format("{0:HH:mm}", DateTime.Now);
                string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
                string fechahora = dia + " de " + mestexto + " de " + anio + " " + hora;
                Chunk chkFecha = new Chunk(fechahora, FuenteCelda);
                Phrase phrfecha = new Phrase(chkFecha);

                //Phrase promad = new Phrase("Promad Soluciones Computarizadas " + anio, FuenteCelda2);
                Phrase promad = new Phrase("", FuenteCelda2);
                cell = new PdfPCell(promad);
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);

                cell = new PdfPCell(phrfecha);
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);
                footerTbl2.WriteSelectedRows(0, -1, 5, writer.PageSize.GetBottom(doc.BottomMargin)+6, writer.DirectContent);
            }

            private static string ObtenerMesTexto(string mes)
            {
                string mestexto = "";
                switch (mes)
                {
                    case "01":
                        mestexto = "Enero";
                        break;
                    case "02":
                        mestexto = "Febrero";
                        break;
                    case "03":
                        mestexto = "Marzo";
                        break;
                    case "04":
                        mestexto = "Abril";
                        break;
                    case "05":
                        mestexto = "Mayo";
                        break;
                    case "06":
                        mestexto = "Junio";
                        break;
                    case "07":
                        mestexto = "Julio";
                        break;
                    case "08":
                        mestexto = "Agosto";
                        break;
                    case "09":
                        mestexto = "Septiembre";
                        break;
                    case "10":
                        mestexto = "Octubre";
                        break;
                    case "11":
                        mestexto = "Noviembre";
                        break;
                    case "12":
                        mestexto = "Diciembre";
                        break;
                    default:
                        mestexto = " ";
                        break;
                }
                return mestexto;
            }


            //Necesario para incluir pagina x de y al header
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);
                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, tamanofuente);
                headerTemplate.SetTextMatrix(0, 0);
                
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

            }
        }

        public static void CeldaConChunk(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, int bordo, int horizontal, int vertical)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk(valor, FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = horizontal;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }


        public static void CeldaConChunk2(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, int bordo, int horizontal, int vertical, string titulo2, string valor2, string titulo3, string valor3)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk(valor, FuenteValor);
            Chunk chunk3 = new Chunk(titulo2, FuenteTitulo);
            Chunk chunk4 = new Chunk(valor2, FuenteValor);
            Chunk chunk5 = new Chunk(titulo3, FuenteTitulo);
            Chunk chunk6 = new Chunk(valor3, FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            elements.Add(chunk3);
            elements.Add(chunk4);
            elements.Add(chunk5);
            elements.Add(chunk6);
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = horizontal;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }
        /// <summary>
        /// 
        /// 
        public static object[] ReporteTituloDetenidosporLesion(List<Entity.DetenidosPorTipoLesion> detenidos, string[] filtros, object[] data)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Comparativo de lesionados" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.comparativodelesionados"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloDetenidosporLesion(documentoPdf, FuenteTitulo, FuenteTituloTabla, detenidos, blau,filtros,data);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloDetenidosporLesion(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.DetenidosPorTipoLesion> detenidos, BaseColor blau, string[] fitros, object[] data)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;



            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));

            }



            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            if (fitros[3] == "0" && fitros[5] != "0")
            {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
            }
            tabla.AddCell(cellWithRowspan);

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Detenidos por tipo de lesión", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 8f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);


            documentoPdf.Add(tabla);
            BaseColor colorrelleno = new BaseColor(192, 224, 255);
            BaseColor colorrelleno2 = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.BLACK);




            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;
            //comparativodelesionadosporunidads
            List<string> tipolesion = new List<string>();

            foreach (var item in detenidos)
            {
                tipolesion.Add(item.Tipolesion);

            }

            PdfPTable tablay = new PdfPTable(3);//Numero de columnas de la tabla            
            tablay.WidthPercentage = 100; //Porcentaje ancho
            tablay.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            CeldaParaTablas(tablay, FuenteTituloTabla2, "Lesión", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Total", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            double totallesionados = 0;
            totallesionados = detenidos.Sum(x => x.Cantidad);
            foreach (var item in tipolesion.Distinct())

            {
                if (i % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                i++;
                CeldaParaTablas(tablay, FuenteNormal, item, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteNormal, detenidos.Where(t => t.Tipolesion == item).Sum(q => q.Cantidad).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteNormal, Math.Round(((detenidos.Where(t => t.Tipolesion == item).Sum(q => q.Cantidad) * 100) / totallesionados), 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            }

            Font FuenteNegritaq = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablam2 = new PdfPTable(1);
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;
            Celda(tablam2, FuenteNegritaq, "Municipio:" + data[0].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(tablam2, FuenteNegritaq, $"Del {fitros[0]} al {fitros[1]}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            if (fitros[3] != "0")
            {
                var contratosub = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(fitros[3]));
                var instit = ControlInstitucion.ObtenerPorId(contratosub.InstitucionId);
                Celda(tablam2, FuenteNegritaq, "Institución:" + instit.Nombre, 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            }
            CeldaVacio(tablam2, 1);

            PdfPTable tablax = new PdfPTable(3);//Numero de columnas de la tabla            
            tablax.WidthPercentage = 100; //Porcentaje ancho
            tablax.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical
            CeldaParaTablas(tablax, FuenteNormal, "", 1, 0, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablax, FuenteTituloTabla2, totallesionados.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablax, FuenteTituloTabla2, "100", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            documentoPdf.Add(tablam2);
            documentoPdf.Add(tablax);

            documentoPdf.Add(tablay);

            PdfPTable tablaz = new PdfPTable(3);//Numero de columnas de la tabla            
            tablax.WidthPercentage = 100; //Porcentaje ancho
            tablax.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical
            CeldaParaTablas(tablaz, FuenteNormal, "Detenidos por tipo de lesión", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, totallesionados.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaVacio(tablaz, 6);
            documentoPdf.Add(tablaz);

            PdfPTable tabla1 = new PdfPTable(1);
            CeldaParaTablas(tabla1, FuenteNormal, "Información de todos los exámenes médicos practicados a los detenidos", 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            documentoPdf.Add(tabla1);






        }

        /// 
        /// 
        public static object[] ReporteDetenidosPorlesion(List<Entity.ReporteEstadisticoPorLesion> comparativodelesionadosporunidads, object[] data, DateTime fechaInicio, DateTime fechaFin, bool utilizaHora, string[] filtros)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Lesionados por unidad" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("02.lesionadosporunidad"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloReporteDetenidosPorlesion(documentoPdf, FuenteTitulo, FuenteTituloTabla, comparativodelesionadosporunidads, blau, data, fechaInicio, fechaFin, utilizaHora, filtros);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloReporteDetenidosPorlesion(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.ReporteEstadisticoPorLesion> comparativodelesionadosporunidads, BaseColor blau, object[] data, DateTime fechaInicio, DateTime fechaFin, bool utilizaHora, string[] filtros)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;

            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));
            }
            
            PdfPCell cellWithRowspan = new PdfPCell(image,true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;

            if (filtros[3] == "0" && filtros[7] != "0")
            {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
            }

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Detenidos por tipo de lesión", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 1);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            Font FuenteNegritaq = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablam2 = new PdfPTable(1);
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;

            string fechainicial = "";
            string fechafinal = "";

            if (!Convert.ToBoolean(filtros[6]))
            {
                fechainicial = Convert.ToDateTime(filtros[0]).ToShortDateString();
                fechafinal = Convert.ToDateTime(filtros[1]).ToShortDateString();

            }
            else
            {
                var date = Convert.ToDateTime(filtros[0]);
                var h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                var m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                var s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechainicial = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
                date = Convert.ToDateTime(filtros[1]);
                h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechafinal = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
            }

            Celda(tablam2, FuenteNegritaq, "Municipio: " + data[0].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(tablam2, FuenteNegritaq, $"Del {fechainicial} al {fechafinal}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(tablam2, FuenteNegritaq, "Institución: " + data[1].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            CeldaVacio(tablam2, 1);

            documentoPdf.Add(tabla);
            documentoPdf.Add(tablam2);

            BaseColor colorrelleno = new BaseColor(192, 224, 255);
            BaseColor colorrelleno2 = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 11f, Font.BOLD, BaseColor.BLACK);

            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;
            //comparativodelesionadosporunidads
            List<string> tipolesion = new List<string>();

            //if (utilizaHora)
            //    comparativodelesionadosporunidads = comparativodelesionadosporunidads.Where(x => x.Fecha >= fechaInicio && x.Fecha <= fechaFin.AddMinutes(1).AddSeconds(-1)).ToList();
            //else
            //    comparativodelesionadosporunidads = comparativodelesionadosporunidads.Where(x => x.Fecha.Date >= fechaInicio.Date && x.Fecha.Date <= fechaFin.Date).ToList();

            foreach (var item in comparativodelesionadosporunidads)
            {
                tipolesion.Add(item.Lesion);
            }




            int totalregistros = 0;
            totalregistros = comparativodelesionadosporunidads.Count();

            PdfPTable tablay = new PdfPTable(3);//Numero de columnas de la tabla            
            tablay.WidthPercentage = 100; //Porcentaje ancho
            tablay.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical
            CeldaVacio(tablay, 1);
            CeldaParaTablas(tablay, FuenteTituloTabla2, totalregistros.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "100.00%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaVacio(tablay, 3);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Lesión", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Total", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //CeldaParaTablas(tablay, FuenteTituloTabla2, "Detenidos por lesión", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            //CeldaParaTablas(tablay, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            //CeldaParaTablas(tablay, FuenteTituloTabla2, "Detenidos reales", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            //CeldaParaTablas(tablay, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            int k = 1;
            
            foreach (var item in tipolesion.Distinct())
            {
                if (k % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                List<Entity.ReporteEstadisticoDetenidoPorTipoLesionCL> lesionadosLesion = new List<Entity.ReporteEstadisticoDetenidoPorTipoLesionCL>();

                k++;
           int detenidos = 0;

                detenidos = comparativodelesionadosporunidads.Where(x => x.Lesion == item).Count();

                double porcentaje = ((double)detenidos * 100) / (double)totalregistros;
                CeldaParaTablas(tablay, FuenteNormal,item , 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, detenidos.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_CENTER);
                CeldaParaTablas(tablay, FuenteNormal, Math.Round(porcentaje,2).ToString()+"%", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_CENTER);

                //CeldaParaTablas(tablay, FuenteNormal, totalLesionadosReales.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
                //CeldaParaTablas(tablay, FuenteNormal, porcentajeLesionadosReales.ToString("p2"), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            }




            List<int> detenidos2 = new List<int>();

            foreach (var item in comparativodelesionadosporunidads)
            {
                detenidos2.Add(item.DetenidoId);
            }
            PdfPTable tablam3 = new PdfPTable(1);
            tablam3.WidthPercentage = 100; //Porcentaje ancho
            tablam3.HorizontalAlignment = Element.ALIGN_RIGHT;
            Celda(tablam3, FuenteNegritaq, "Total: " + totalregistros.ToString(), 0, 0, 1, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            Celda(tablam3, FuenteNegritaq, "Detenidos reales: " + detenidos2.Distinct().Count().ToString(), 0, 0, 1, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);


            documentoPdf.Add(tablay);
            
            documentoPdf.Add(tablam3);
        }

        /// 
        /// 
        /// ReporteComparativoLesionados
        public static object[] ReporteComparativoLesionadosCL(List<Entity.ReporteEstadisticoDetenidoPorTipoLesionCL> comparativodelesionadosporunidads, object[] data, DateTime fechaInicio, DateTime fechaFin, bool utilizaHora, string[] filtros)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Lesionados por unidad" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("02.lesionadosporunidad"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloReporteComparativoLesionadosCL(documentoPdf, FuenteTitulo, FuenteTituloTabla, comparativodelesionadosporunidads, blau, data, fechaInicio, fechaFin, utilizaHora, filtros);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloReporteComparativoLesionadosCL(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.ReporteEstadisticoDetenidoPorTipoLesionCL> comparativodelesionadosporunidads, BaseColor blau, object[] data, DateTime fechaInicio, DateTime fechaFin, bool utilizaHora, string[] filtros)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;

            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));
            }

            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            if(filtros[3]=="0" && filtros[7]!="0")
            {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
            }
            

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Comparativo de lesionados por unidad", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            Font FuenteNegritaq = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablam2 = new PdfPTable(1);
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;

            string fechainicial = "";
            string fechafinal = "";

            if (!Convert.ToBoolean(filtros[6]))
            {
                fechainicial = Convert.ToDateTime(filtros[0]).ToShortDateString();
                fechafinal = Convert.ToDateTime(filtros[1]).ToShortDateString();

            }
            else
            {
                var date = Convert.ToDateTime(filtros[0]);
                var h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                var m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                var s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechainicial = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
                date = Convert.ToDateTime(filtros[1]);
                h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechafinal = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
            }

            Celda(tablam2, FuenteNegritaq, "Municipio: " + data[0].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(tablam2, FuenteNegritaq, $"Del {fechainicial} al {fechafinal}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(tablam2, FuenteNegritaq, "Institución: " + data[1].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            CeldaVacio(tablam2, 1);

            documentoPdf.Add(tabla);
            documentoPdf.Add(tablam2);

            BaseColor colorrelleno = new BaseColor(192, 224, 255);
            BaseColor colorrelleno2 = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 11f, Font.BOLD, BaseColor.BLACK);

            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;
            //comparativodelesionadosporunidads
            List<string> tipolesion = new List<string>();

            if (utilizaHora)
                comparativodelesionadosporunidads = comparativodelesionadosporunidads.Where(x => x.Fecha >= fechaInicio && x.Fecha <= fechaFin.AddMinutes(1).AddSeconds(-1)).ToList();
            else
                comparativodelesionadosporunidads = comparativodelesionadosporunidads.Where(x => x.Fecha.Date >= fechaInicio.Date && x.Fecha.Date <= fechaFin.Date).ToList();

            foreach (var item in comparativodelesionadosporunidads)
            {
                tipolesion.Add(item.Lesion);
            }


            //
            PdfPTable tablaTotales = new PdfPTable(2 + tipolesion.Distinct().Count() );//Numero de columnas de la tabla            
            tablaTotales.WidthPercentage = 100; //Porcentaje ancho
            tablaTotales.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical
            CeldaVacio(tablaTotales, 2 + tipolesion.Distinct().Count() );
            CeldaVacio(tablaTotales, 2);

            List<int> totalesMismaLesion = new List<int>();
            foreach (var lesion in tipolesion.Distinct())
            {
                var lesionSum = comparativodelesionadosporunidads.Where(x => x.Lesion == lesion).Count();
                totalesMismaLesion.Add(lesionSum);
                CeldaParaTablas(tablaTotales, FuenteNormal, lesionSum.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            }

            var totalTotalLesionados = totalesMismaLesion.Select(x => x).Sum();
            //CeldaParaTablas(tablaTotales, FuenteNormal, totalTotalLesionados.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            //CeldaParaTablas(tablaTotales, FuenteNormal, totalTotalLesionados > 0 ? "100.00 %" : "0", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);

            var totalTotalLesionadosReales = comparativodelesionadosporunidads.Select(x => x.DetenidoId).Distinct().Count();
            //CeldaParaTablas(tablaTotales, FuenteNormal, totalTotalLesionadosReales.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            //CeldaParaTablas(tablaTotales, FuenteNormal, totalTotalLesionadosReales > 0 ? "100.00 %" : "0", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            //


            var agrupados = comparativodelesionadosporunidads.GroupBy(x => new { x.Unidad, x.Fecha, x.DetenidoId });

            PdfPTable tablay = new PdfPTable(2 + tipolesion.Distinct().Count() );//Numero de columnas de la tabla            
            tablay.WidthPercentage = 100; //Porcentaje ancho
            tablay.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            CeldaParaTablas(tablay, FuenteTituloTabla2, "Unidad", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Fecha", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            foreach (var item in tipolesion.Distinct())
            {
                CeldaParaTablas(tablay, FuenteTituloTabla2, item, 1, 0, 1, colorrelleno, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            }
            //CeldaParaTablas(tablay, FuenteTituloTabla2, "Detenidos por lesión", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            //CeldaParaTablas(tablay, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            //CeldaParaTablas(tablay, FuenteTituloTabla2, "Detenidos reales", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            //CeldaParaTablas(tablay, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            int k = 1;

            foreach (var item in agrupados)
            {
                if (k % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                List<Entity.ReporteEstadisticoDetenidoPorTipoLesionCL> lesionadosLesion = new List<Entity.ReporteEstadisticoDetenidoPorTipoLesionCL>();

                k++;
                CeldaParaTablas(tablay, FuenteNormal, item.FirstOrDefault(x => x.Unidad == item.Key.Unidad).Unidad ?? "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, item.FirstOrDefault(x => x.Unidad == item.Key.Unidad).Fecha.ToString("dd/MM/yyyy HH:mm:ss") ?? "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_CENTER);
                foreach (var str in tipolesion.Distinct())
                {
                    if (item.Any(x => x.Lesion == str))
                    {
                        lesionadosLesion = lesionadosLesion.Concat(item.Where(x => x.Lesion == str)).ToList();
                        var count = item.Where(x => x.Lesion == str).Count();
                        CeldaParaTablas(tablay, FuenteNormal, count.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
                    }
                    else
                    {
                        CeldaParaTablas(tablay, FuenteNormal, "0", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
                    }
                }
                var totalLesionadosLesion = lesionadosLesion.Count();
                var totalLesionadosReales = lesionadosLesion.Select(x => x.DetenidoId).Distinct().Count();

                var porcentajeLesionadosLesion = totalLesionadosLesion == 0 ? 0 : totalLesionadosLesion / (double)totalTotalLesionados;
                var porcentajeLesionadosReales = totalLesionadosReales == 0 ? 0 : totalLesionadosReales / (double)totalTotalLesionadosReales;

                //CeldaParaTablas(tablay, FuenteNormal, totalLesionadosLesion.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
                //CeldaParaTablas(tablay, FuenteNormal, porcentajeLesionadosLesion.ToString("p2"), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
                //CeldaParaTablas(tablay, FuenteNormal, totalLesionadosReales.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
                //CeldaParaTablas(tablay, FuenteNormal, porcentajeLesionadosReales.ToString("p2"), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            }

            PdfPTable tablaPorcentajes = new PdfPTable(2 + tipolesion.Distinct().Count() );
            tablaPorcentajes.WidthPercentage = 100;
            tablaPorcentajes.HorizontalAlignment = Element.ALIGN_CENTER;
            CeldaVacio(tablaPorcentajes, 2);

            foreach (var lesion in tipolesion.Distinct())
            {
                var lesionSum = comparativodelesionadosporunidads.Where(x => x.Lesion == lesion).Count();
                var porcentajeLesion = lesionSum == 0 ? 0 : lesionSum / (double)totalTotalLesionados;

                CeldaParaTablas(tablaPorcentajes, FuenteNormal, porcentajeLesion.ToString("p2"), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            }
           // CeldaParaTablas(tablaPorcentajes, FuenteNormal, totalTotalLesionados > 0 ? "100.00 %" : "0", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            CeldaVacio(tablaPorcentajes, 2);
            //CeldaParaTablas(tablaPorcentajes, FuenteNormal, totalTotalLesionados > 0 ? "100.00 %" : "0", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            PdfPTable tablam3 = new PdfPTable(1);
            tablam3.WidthPercentage = 100; //Porcentaje ancho
            tablam3.HorizontalAlignment = Element.ALIGN_RIGHT;
            Celda(tablam3, FuenteNegritaq, totalTotalLesionados.ToString(), 0, 0, 1, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            Celda(tablam3, FuenteNegritaq, "100%", 0, 0, 1, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            Celda(tablam3, FuenteNegritaq, "Detenidos reales: " + totalTotalLesionadosReales, 0, 0, 1, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);


            documentoPdf.Add(tablay);
            documentoPdf.Add(tablaTotales);
            documentoPdf.Add(tablaPorcentajes);
            documentoPdf.Add(tablam3);
        }



        /// ReporteComparativoLesionados
        public static object[] ReporteComparativoLesionados(List<Entity.Comparativodelesionadosporunidad> comparativodelesionadosporunidads, object[] data, DateTime fechaInicio, DateTime fechaFin, bool utilizaHora,string[] filtros)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Comparativo de lesionados" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.comparativodelesionados"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloReporteComparativoLesionados(documentoPdf, FuenteTitulo, FuenteTituloTabla, comparativodelesionadosporunidads, blau, data, fechaInicio, fechaFin, utilizaHora,filtros);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloReporteComparativoLesionados(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.Comparativodelesionadosporunidad> comparativodelesionadosporunidads, BaseColor blau, object[] data, DateTime fechaInicio, DateTime fechaFin, bool utilizaHora,string[] filtros)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;

            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));
            }

            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            if(filtros[6] !="0" && filtros[3]=="0")
            {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
                
            }
            

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Comparativo de lesionados por unidad", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 1);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            Font FuenteNegritaq = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablam2 = new PdfPTable(1);
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;
            Celda(tablam2, FuenteNegritaq, "Municipio:" + data[0].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(tablam2, FuenteNegritaq, $"Del {filtros[0]} al {filtros[1]}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(tablam2, FuenteNegritaq, "Institución:" + data[1].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            
            CeldaVacio(tablam2, 1);

            documentoPdf.Add(tabla);
            documentoPdf.Add(tablam2);

            BaseColor colorrelleno = new BaseColor(192, 224, 255);
            BaseColor colorrelleno2 = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 11f, Font.BOLD, BaseColor.BLACK);

            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;
            //comparativodelesionadosporunidads
            List<string> tipolesion = new List<string>();

            if (utilizaHora)
                comparativodelesionadosporunidads = comparativodelesionadosporunidads.Where(x => x.Fecha >= fechaInicio && x.Fecha <= fechaFin.AddMinutes(1).AddSeconds(-1)).ToList();
            else
                comparativodelesionadosporunidads = comparativodelesionadosporunidads.Where(x => x.Fecha.Date >= fechaInicio.Date && x.Fecha.Date <= fechaFin.Date).ToList();

            foreach (var item in comparativodelesionadosporunidads)
            {
                tipolesion.Add(item.Tipolesion);
            }


            //
            PdfPTable tablaTotales = new PdfPTable(2 + tipolesion.Distinct().Count() + 4);//Numero de columnas de la tabla            
            tablaTotales.WidthPercentage = 100; //Porcentaje ancho
            tablaTotales.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical
            CeldaVacio(tablaTotales, 2 + tipolesion.Distinct().Count() + 4);
            CeldaVacio(tablaTotales, 2);

            List<int> totalesMismaLesion = new List<int>();
            foreach (var lesion in tipolesion.Distinct())
            {
                var lesionSum = comparativodelesionadosporunidads.Where(x => x.Tipolesion == lesion).Select(x => x.Cantidad).Sum();
                totalesMismaLesion.Add(lesionSum);
                CeldaParaTablas(tablaTotales, FuenteNormal, lesionSum.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            }

            var totalTotalLesionados = totalesMismaLesion.Select(x => x).Sum();
            CeldaParaTablas(tablaTotales, FuenteNormal, totalTotalLesionados.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            CeldaParaTablas(tablaTotales, FuenteNormal, totalTotalLesionados > 0 ? "100.00 %" : "0", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);

            var totalTotalLesionadosReales = comparativodelesionadosporunidads.Select(x => x.DetenidoId).Distinct().Count();
            CeldaParaTablas(tablaTotales, FuenteNormal, totalTotalLesionadosReales.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            CeldaParaTablas(tablaTotales, FuenteNormal, totalTotalLesionadosReales > 0 ? "100.00 %" : "0", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            //


            var agrupados = comparativodelesionadosporunidads.GroupBy(x => new { x.UnidadId, x.Fecha, x.DetenidoId });

            PdfPTable tablay = new PdfPTable(2 + tipolesion.Distinct().Count() + 4);//Numero de columnas de la tabla            
            tablay.WidthPercentage = 100; //Porcentaje ancho
            tablay.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            CeldaParaTablas(tablay, FuenteTituloTabla2, "Unidad", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Fecha", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            foreach (var item in tipolesion.Distinct())
            {
                CeldaParaTablas(tablay, FuenteTituloTabla2, item, 1, 0, 1, colorrelleno, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            }
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Detenidos por lesión", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Detenidos reales", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            int k = 1;

            foreach (var item in agrupados)
            {
                if (k % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                List<Entity.Comparativodelesionadosporunidad> lesionadosLesion = new List<Entity.Comparativodelesionadosporunidad>();

                k++;
                var fecha = "";
                if(item.FirstOrDefault(x => x.UnidadId == item.Key.UnidadId)!=null)
                {
                    var f = item.FirstOrDefault(x => x.UnidadId == item.Key.UnidadId).Fecha;
                    var hora = f.Hour.ToString();
                    if (f.Hour < 10) hora = "0" + hora;
                    var min = f.Minute.ToString();
                    if (f.Minute < 10) min = "0" + min;
                    var seg = f.Second.ToString();
                    if (f.Second < 10) seg = "0" + seg;
                    fecha = f.ToShortDateString() + " " + hora + ":" + min + ":" + seg;
                }
                
                
                CeldaParaTablas(tablay, FuenteNormal, item.FirstOrDefault(x => x.UnidadId == item.Key.UnidadId).Unidad ?? "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, fecha, 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_CENTER);
                foreach (var str in tipolesion.Distinct())
                {
                    if (item.Any(x => x.Tipolesion == str))
                    {
                        lesionadosLesion = lesionadosLesion.Concat(item.Where(x => x.Tipolesion == str)).ToList();
                        var count = item.Where(x => x.Tipolesion == str).Select(x => x.Cantidad).Sum();
                        CeldaParaTablas(tablay, FuenteNormal, count.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
                    }
                    else
                    {
                        CeldaParaTablas(tablay, FuenteNormal, "0", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
                    }
                }
                var totalLesionadosLesion = lesionadosLesion.Select(x => x.Cantidad).Sum();
                var totalLesionadosReales = lesionadosLesion.Select(x => x.DetenidoId).Distinct().Count();

                var porcentajeLesionadosLesion = totalLesionadosLesion == 0 ? 0 : totalLesionadosLesion / (double)totalTotalLesionados;
                var porcentajeLesionadosReales = totalLesionadosReales == 0 ? 0 : totalLesionadosReales / (double)totalTotalLesionadosReales;

                CeldaParaTablas(tablay, FuenteNormal, totalLesionadosLesion.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, porcentajeLesionadosLesion.ToString("p2"), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, totalLesionadosReales.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, porcentajeLesionadosReales.ToString("p2"), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            }

            PdfPTable tablaPorcentajes = new PdfPTable(2 + tipolesion.Distinct().Count() + 4);
            tablaPorcentajes.WidthPercentage = 100;
            tablaPorcentajes.HorizontalAlignment = Element.ALIGN_CENTER;
            CeldaVacio(tablaPorcentajes, 2);

            foreach (var lesion in tipolesion.Distinct())
            {
                var lesionSum = comparativodelesionadosporunidads.Where(x => x.Tipolesion == lesion).Select(x => x.Cantidad).Sum();
                var porcentajeLesion = lesionSum == 0 ? 0 : lesionSum / (double)totalTotalLesionados;

                CeldaParaTablas(tablaPorcentajes, FuenteNormal, porcentajeLesion.ToString("p2"), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            }
            CeldaParaTablas(tablaPorcentajes, FuenteNormal, totalTotalLesionados > 0 ? "100.00 %" : "0", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);
            CeldaVacio(tablaPorcentajes, 3);

            documentoPdf.Add(tablay);
            documentoPdf.Add(tablaTotales);
            documentoPdf.Add(tablaPorcentajes);
        }


        /// <returns></returns>
        public static object[] ReporteDescuentosagravanteJC(List<Entity.Descuentosagravantesjuezcalificador> descuentosagravantesjuezcalificador, string[] filtros)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "DescuentosAgravantesJuezCalificador" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.DescuentosAgravantesJuezCalificador"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloDescuentosagravanteJC(documentoPdf, FuenteTitulo, FuenteTituloTabla, descuentosagravantesjuezcalificador, blau, filtros);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloDescuentosagravanteJC(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.Descuentosagravantesjuezcalificador> descuentosagravantesjuezcalificador, BaseColor blau, string[] filtros)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;



            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";
            PdfPCell cellWithRowspan = new PdfPCell();
            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }
            iTextSharp.text.Image image = null;
            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            if (logotipo == "~/Content/img/logo.png")
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
                image.ScaleAbsoluteHeight(81f);
                image.ScaleAbsoluteWidth(200f);
                cellWithRowspan = new PdfPCell(image);
            }
            else
            {
                

                try
                {
                    image = iTextSharp.text.Image.GetInstance(pathfoto);
                    cellWithRowspan = new PdfPCell(image,true);
                }
                catch (Exception)
                {
                    image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));
                    image.ScaleAbsoluteHeight(79f);
                    image.ScaleAbsoluteWidth(200f);
                    cellWithRowspan = new PdfPCell(image);
                }
            }
            

            

            
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 4;
            cellWithRowspan.Border = 0;
            if(filtros[3] =="0" && filtros[6] !="0" )
            {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
            }

            

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Descuentos y agravantes por juez calificador", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 9f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);


            documentoPdf.Add(tabla);

            Font FuenteNegritaq = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablam2 = new PdfPTable(1);//Numero de columnas de la tabla            
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;

            string fechainicial = "";
            string fechafinal = "";

            if (!Convert.ToBoolean(filtros[5]))
            {
                fechainicial = Convert.ToDateTime(filtros[0]).ToShortDateString();
                fechafinal = Convert.ToDateTime(filtros[1]).ToShortDateString();

            }
            else
            {
                var date = Convert.ToDateTime(filtros[0]);
                var h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                var m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                var s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechainicial = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
                date = Convert.ToDateTime(filtros[1]);
                h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechafinal = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
            }

            Celda(tablam2, FuenteNegritaq, $"Del {fechainicial} al {fechafinal}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            if (filtros[3] != "0")
            {
                var contratosub = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(filtros[3]));
                var instit = ControlInstitucion.ObtenerPorId(contratosub.InstitucionId);
                var domicilio = ControlDomicilio.ObtenerPorId(instit.DomicilioId);
                var municipio = ControlMunicipio.Obtener(domicilio.MunicipioId ?? 0);
                var mun = "No especificado";
                if (municipio != null) mun = municipio.Nombre;
                Celda(tablam2, FuenteNegritaq, "Institución: " + instit.Nombre, 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
                Celda(tablam2, FuenteNegritaq, "Municipio: " + mun, 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            }
            CeldaVacio(tablam2, 1);
            documentoPdf.Add(tablam2);



            BaseColor colorrelleno = new BaseColor(192, 224, 255);
            BaseColor colorrelleno2 = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 11f, Font.BOLD, BaseColor.BLACK);




            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;

            PdfPTable tablay = new PdfPTable(8);//Numero de columnas de la tabla  
            float[] w1 = new float[] {30,60,45,25,30,25,25,80 };
            tablay.SetWidths(w1);
            tablay.WidthPercentage = 100; //Porcentaje ancho
            tablay.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            CeldaParaTablas(tablay, FuenteTituloTabla2, "No. remisión", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Detenido", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Fecha calificación", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Total", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Descuento", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Agravante", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Total a pagar", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Razón", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            foreach (var item in descuentosagravantesjuezcalificador)
            {
                if (i % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                i++;
                CeldaParaTablas(tablay, FuenteNormal, item.Remision, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteNormal, item.Detenido, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteNormal, item.Fechacalificacion.ToString("dd/MM/yyyy HH:mm:ss"), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteNormal, item.Total.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteNormal, item.Descuento.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteNormal, item.Agravante.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteNormal, item.Totalapagar.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteNormal, item.Razon, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);


            }
            documentoPdf.Add(tablay);


            ///
            //foreach (int k in Ciudades.Distinct())
            //{

            //    var detalle = descuentosagravantesjuezcalificador.FirstOrDefault(x => x.MunicipioId == k);
            //    PdfPTable tablachunk = new PdfPTable(1);
            //    tablachunk.WidthPercentage = 100;
            //    tablachunk.HorizontalAlignment = Element.ALIGN_LEFT;
            //    CeldaParaTablas(tablachunk, FuenteTituloTabla2, detalle.Municipio.ToString(), 1, 0, 1, colorrelleno2, espacio, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            //    //CeldaConChunk(tablachunk, FuenteNegrita, FuenteNormal, "", detalle.Motivodetencion.ToString(), 0, 0, 0);
            //    CeldaVacio(tablachunk, 1);
            //    documentoPdf.Add(tablachunk);


            //    PdfPTable tablay = new PdfPTable(3);//Numero de columnas de la tabla            
            //    tablay.WidthPercentage = 100; //Porcentaje ancho
            //    tablay.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            //    CeldaParaTablas(tablay, FuenteTituloTabla2, "Colonia", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            //    CeldaParaTablas(tablay, FuenteTituloTabla2, "Total", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            //    CeldaParaTablas(tablay, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);

            //    int cantidaddedetenidos = 0;
            //    cantidaddedetenidos = descuentosagravantesjuezcalificador.Where(t => t.MunicipioId == k).Sum(s => s.Cantidad);
            //    foreach (var det in descuentosagravantesjuezcalificador.Where(x => x.MunicipioId == k))
            //    {
            //        if (i % 2 != 0)
            //            color = colorgris;
            //        else color = colorblanco;

            //        i++;

            //        CeldaParaTablas(tablay, FuenteNormal, det.Colonia, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            //        CeldaParaTablas(tablay, FuenteNormal, det.Cantidad.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            //        CeldaParaTablas(tablay, FuenteNormal, Math.Round(((det.Cantidad * 100.0) / cantidaddedetenidos), 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);

            //    }

            //    if (i % 2 != 0)
            //        color = colorgris;
            //    else color = colorblanco;


            //    CeldaParaTablas(tablay, FuenteNormal, "Remisiones", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            //    CeldaParaTablas(tablay, FuenteNormal, cantidaddedetenidos.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            //    CeldaParaTablas(tablay, FuenteNormal, "100.00", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);

            //    CeldaVacio(tablay, 3);
            //    documentoPdf.Add(tablay);

            //}






        }



        ///

        public static object[] ReporteDetenidosporcolonia(List<Entity.Detenidosporcolonia> detenidosporcolonias, string[] filtros,object[] data)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "DetenidosPorColonia" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.DetenidosPorColonia"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloDDetenidosporColonia(documentoPdf, FuenteTitulo, FuenteTituloTabla, detenidosporcolonias, blau, filtros,data);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloDDetenidosporColonia(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.Detenidosporcolonia> detenidosporcolonias, BaseColor blau, string[] filtros,object[] data)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;



            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }
            PdfPCell cellWithRowspan = new PdfPCell();
            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            if (logotipo== "~/Content/img/logo.png")
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
                image.ScaleAbsoluteHeight(87f);
                image.ScaleAbsoluteWidth(200f);
                cellWithRowspan = new PdfPCell(image);
            }
            else
            {
                try
                {
                    image = iTextSharp.text.Image.GetInstance(pathfoto);
                    cellWithRowspan = new PdfPCell(image,true);
                }
                catch (Exception)
                {
                    image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));
                    image.ScaleAbsoluteHeight(87f);
                    image.ScaleAbsoluteWidth(200f);
                    cellWithRowspan = new PdfPCell(image);
                }
            }
            



            
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 3;
            cellWithRowspan.Border = 0;
            if(filtros[3]=="0" && filtros[5]!="0")
            {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
            }
            

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Detenidos por colonia", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 2);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);


            documentoPdf.Add(tabla);

            Font FuenteNegritaq = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablam2 = new PdfPTable(1);//Numero de columnas de la tabla            
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;

            string fechainicial = "";
            string fechafinal = "";

            if (!Convert.ToBoolean(filtros[4]))
            {
                fechainicial = Convert.ToDateTime(filtros[0]).ToShortDateString();
                fechafinal = Convert.ToDateTime(filtros[1]).ToShortDateString();

            }
            else
            {
                var date = Convert.ToDateTime(filtros[0]);
                var h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                var m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                var s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechainicial = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
                date = Convert.ToDateTime(filtros[1]);
                h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechafinal = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
            }

            Celda(tablam2, FuenteNegritaq, "Municipio:" + data[0].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(tablam2, FuenteNegritaq, $"Del {fechainicial} al {fechafinal}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            if (filtros[3] != "0")
            {
                var contratosub = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(filtros[3]));
                var instit = ControlInstitucion.ObtenerPorId(contratosub.InstitucionId);
                Celda(tablam2, FuenteNegritaq, "Institución:" + instit.Nombre, 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            }
            CeldaVacio(tablam2, 1);
            documentoPdf.Add(tablam2);

            BaseColor colorrelleno = new BaseColor(192, 224, 255);
            BaseColor colorrelleno2 = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 11f, Font.BOLD, BaseColor.BLACK);




            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;

            List<Int32> Ciudades = new List<int>();

            foreach (var item in detenidosporcolonias)
            {
                Ciudades.Add(item.MunicipioId);

            }


            ///
            foreach (int k in Ciudades.Distinct())
            {

                var detalle = detenidosporcolonias.FirstOrDefault(x => x.MunicipioId == k);
                PdfPTable tablachunk = new PdfPTable(1);
                tablachunk.WidthPercentage = 100;
                tablachunk.HorizontalAlignment = Element.ALIGN_LEFT;
                CeldaParaTablas(tablachunk, FuenteTituloTabla2, detalle.Municipio.ToString(), 1, 0, 1, colorrelleno2, espacio, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
                //CeldaConChunk(tablachunk, FuenteNegrita, FuenteNormal, "", detalle.Motivodetencion.ToString(), 0, 0, 0);
                CeldaVacio(tablachunk, 1);
                documentoPdf.Add(tablachunk);


                PdfPTable tablay = new PdfPTable(3);//Numero de columnas de la tabla            
                tablay.WidthPercentage = 100; //Porcentaje ancho
                tablay.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

                CeldaParaTablas(tablay, FuenteTituloTabla2, "Colonia", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Total", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);

                int cantidaddedetenidos = 0;
                cantidaddedetenidos = detenidosporcolonias.Where(t => t.MunicipioId == k).Sum(s => s.Cantidad);
                foreach (var det in detenidosporcolonias.Where(x => x.MunicipioId == k))
                {
                    if (i % 2 != 0)
                        color = colorgris;
                    else color = colorblanco;

                    i++;

                    CeldaParaTablas(tablay, FuenteNormal, det.Colonia, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                    CeldaParaTablas(tablay, FuenteNormal, det.Cantidad.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
                    CeldaParaTablas(tablay, FuenteNormal, Math.Round(((det.Cantidad * 100.0) / cantidaddedetenidos), 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);

                }

                if (i % 2 != 0)
                    color = colorgris;
                else color = colorblanco;


                CeldaParaTablas(tablay, FuenteNormal, "Remisiones", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteNormal, cantidaddedetenidos.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteNormal, "100.00", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);



                CeldaVacio(tablay,6);
                documentoPdf.Add(tablay);

            }






        }
        ///trazabilidad
        ///

        public static object[] ReporteTrazabilidad(List<Entity.ReporteEstadisticoTrazabilidad> reporteEstadisticoTrazabilidad, string[] filtros)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            var margen2 = iTextSharp.text.Utilities.MillimetersToPoints(4); //5 milimetros para los margenes
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen2);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Trazabilidaddeprocesos" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.Trazabilidadeprocesos"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloTrazabilidad(documentoPdf, FuenteTitulo, FuenteTituloTabla, reporteEstadisticoTrazabilidad, blau, filtros);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloTrazabilidad(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.ReporteEstadisticoTrazabilidad> reporteEstadisticoTrazabilidads, BaseColor blau, string[] filtros)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;



            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));

            }



            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            if (filtros[2] == "0" && filtros[4] != "0")
            {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
            }

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Trazabilidad de procesos", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 9f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);


            documentoPdf.Add(tabla);

            Font FuenteNegritaq = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablam2 = new PdfPTable(1);//Numero de columnas de la tabla            
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;


            if (filtros[2] != "0")
            {
                var contratosub = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(filtros[2]));
                var instit = ControlInstitucion.ObtenerPorId(contratosub.InstitucionId);
                var domicilio = ControlDomicilio.ObtenerPorId(instit.DomicilioId);
                var municipio = ControlMunicipio.Obtener(domicilio.MunicipioId ?? 0);
                var mun = "No especificado";
                if (municipio != null) mun = municipio.Nombre;
                string fechainicial = "";
                string fechafinal = "";

                if (!Convert.ToBoolean(filtros[3]))
                {
                    fechainicial = Convert.ToDateTime(filtros[0]).ToShortDateString();
                    fechafinal = Convert.ToDateTime(filtros[1]).ToShortDateString();

                }
                else
                {
                    var date = Convert.ToDateTime(filtros[0]);
                    var h = date.Hour.ToString();
                    if (date.Hour < 10) h = "0" + h;
                    var m = date.Minute.ToString();
                    if (date.Minute < 10) m = "0" + m;
                    var s = date.Second.ToString();
                    if (date.Second < 10) s = "0" + s;
                     fechainicial = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
                     
                    date = Convert.ToDateTime(filtros[1]);
                     h = date.Hour.ToString();
                    if (date.Hour < 10) h = "0" + h;
                     m = date.Minute.ToString();
                    if (date.Minute < 10) m = "0" + m;
                     s = date.Second.ToString();
                    if (date.Second < 10) s = "0" + s;
                     fechafinal = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
                    
                    
                }
                Celda(tablam2, FuenteNegritaq, "Municipio: " + mun, 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
                Celda(tablam2, FuenteNegritaq, $"Del {fechainicial} al {fechafinal}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
                Celda(tablam2, FuenteNegritaq, "Institución: " + instit.Nombre, 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
                
            }
            CeldaVacio(tablam2, 1);
            documentoPdf.Add(tablam2);

            BaseColor colorrelleno = new BaseColor(192, 224, 255);
            BaseColor colorrelleno2 = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.BLACK);

            


            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;


            ///

            ///
            float[] w1 = new float[] { 42, 25, 30, 33, 32, 20, 38 };
            PdfPTable tablay = new PdfPTable(7);//Numero de columnas de la tabla            
            tablay.WidthPercentage = 100; //Porcentaje ancho
            
            tablay.SetWidths(w1);
            //tablay.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical
            

            CeldaParaTablas(tablay, FuenteTituloTabla2, "Detenido", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "No. remisión", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Proceso", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Subproceso", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Usuario", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Estatus", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Fecha y hora", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            int k = 1;
            foreach (var item in reporteEstadisticoTrazabilidads)
            {

           
             

                if (k % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                k++;
                CeldaParaTablas(tablay, FuenteNormal, item.Nombre.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, item.Remision.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, item.Proceso, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, item.Subproceso.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, item.Usuario, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, item.Estatus.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, item.Fechayhora.ToString("dd/MM/yyyy HH:mm:ss"), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);

            }
            //CeldaVacio(tablay, 7);
            documentoPdf.Add(tablay);

           



        }



        ///
        /// 
        /// 
        public static object[] ReporteDetalledetenidoporrangoedad(List<Entity.Detenidosporedad> detenidosporedad, string[] filtros)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "DetenidosPorrangoEdad" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.DetenidosPorrangoedad"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloDetalledeteniodosporrangoEdad(documentoPdf, FuenteTitulo, FuenteTituloTabla, detenidosporedad, blau, filtros);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloDetalledeteniodosporrangoEdad(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.Detenidosporedad> detenidosporedad, BaseColor blau, string[] filtros)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;



            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));

            }



            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            if(filtros[3]=="0" && filtros[5]!="0")
            {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
            }
            

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Detenidos por rango de edad", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 9f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);


            documentoPdf.Add(tabla);

            Font FuenteNegritaq = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablam2 = new PdfPTable(1);//Numero de columnas de la tabla            
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;
            string fechainicial = "";
            string fechafinal = "";

            if (!Convert.ToBoolean(filtros[4]))
            {
                fechainicial = Convert.ToDateTime(filtros[0]).ToShortDateString();
                fechafinal = Convert.ToDateTime(filtros[1]).ToShortDateString();

            }
            else
            {
                var date = Convert.ToDateTime(filtros[0]);
                var h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                var m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                var s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechainicial = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
                date = Convert.ToDateTime(filtros[1]);
                h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechafinal = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
            }


            Celda(tablam2, FuenteNegritaq, $"Del {fechainicial} al {fechafinal}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            if (filtros[3] != "0")
            {
                var contratosub = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(filtros[3]));
                var instit = ControlInstitucion.ObtenerPorId(contratosub.InstitucionId);
                var domicilio = ControlDomicilio.ObtenerPorId(instit.DomicilioId);
                var municipio = ControlMunicipio.Obtener(domicilio.MunicipioId ?? 0);
                var mun = "No especificado";
                if (municipio != null) mun = municipio.Nombre;
                Celda(tablam2, FuenteNegritaq, "Institución: " + instit.Nombre, 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
                Celda(tablam2, FuenteNegritaq, "Municipio: " + mun, 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            }
            CeldaVacio(tablam2, 1);
            documentoPdf.Add(tablam2);

            BaseColor colorrelleno = new BaseColor(192, 224, 255);
            BaseColor colorrelleno2 = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.BLACK);

            List<int> Edades = new List<int>();

            foreach (var item in detenidosporedad)
            {
                Edades.Add(item.Edad);

            }

            var sexos = ControlCatalogo.ObtenerTodo(4);
            var masculinoId = sexos.FirstOrDefault(x => x.Nombre.ToUpper() == "Masculino".ToUpper()).Id;
            var femeninoId = sexos.FirstOrDefault(x => x.Nombre.ToUpper() == "Femenino".ToUpper()).Id;

            if (detenidosporedad.Where(x => Edades.Contains(x.Edad)).Count() > 1)
            {
                var detenidosporedadAux = new List<Entity.Detenidosporedad>();
                foreach (var edad in Edades.Distinct())
                {
                    var detenidos = detenidosporedad.Where(x => x.Edad == edad);
                    if (detenidos.Count() > 1)
                    {
                        var detenidoMasculino = detenidos.FirstOrDefault(x => x.Sexoid == masculinoId);
                        var detenidoFemenino = detenidos.FirstOrDefault(x => x.Sexoid == femeninoId);
                        if (detenidoMasculino != null)
                        {
                            foreach (var item in detenidos)
                            {
                                if (detenidoMasculino != item && item.Sexoid == masculinoId) detenidoMasculino.cantidad += item.cantidad;
                            }
                            detenidosporedadAux.Add(detenidoMasculino);
                        }
                        if (detenidoFemenino != null)
                        {
                            foreach (var item in detenidos)
                            {
                                if (detenidoFemenino != item && item.Sexoid == femeninoId) detenidoFemenino.cantidad += item.cantidad;
                            }
                            detenidosporedadAux.Add(detenidoFemenino);
                        }
                    }
                    else
                    {
                        detenidosporedadAux.Add(detenidos.First());
                    }
                }

                detenidosporedad = detenidosporedadAux;
            }

            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;

            int totalmujere = 0;
            int totalhombres = 0;

            totalhombres = detenidosporedad.Where(y => y.Sexo.ToUpper() == "Masculino".ToUpper()).Sum(x => x.cantidad);
            totalmujere = detenidosporedad.Where(y => y.Sexo.ToUpper() == "Femenino".ToUpper()).Sum(x => x.cantidad);
            int Totaldetenidos = totalhombres + totalmujere;
            double rporcentajehombres;
            double rporcentajemujeres;
            rporcentajehombres = (totalhombres * 100.0) / Totaldetenidos;
            rporcentajemujeres = (totalmujere * 100.0) / Totaldetenidos;

            ///
            PdfPTable tabla1 = new PdfPTable(7);//Numero de columnas de la tabla            
            tabla1.WidthPercentage = 100; //Porcentaje ancho
            tabla1.HorizontalAlignment = Element.ALIGN_RIGHT;//Alineación vertical
            CeldaParaTablas(tabla1, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla1, FuenteTituloTabla2, totalhombres.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla1, FuenteTituloTabla2, double.IsNaN(rporcentajehombres) ? "0" : Math.Round(rporcentajehombres, 2).ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla1, FuenteTituloTabla2, totalmujere.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla1, FuenteTituloTabla2, double.IsNaN(rporcentajemujeres) ? "0" : Math.Round(rporcentajemujeres, 2).ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla1, FuenteTituloTabla2, Totaldetenidos.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla1, FuenteTituloTabla2, "100", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaVacio(tabla1, 7);
            documentoPdf.Add(tabla1);
            ///
            PdfPTable tablay = new PdfPTable(7);//Numero de columnas de la tabla            
            tablay.WidthPercentage = 100; //Porcentaje ancho
            tablay.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            CeldaParaTablas(tablay, FuenteTituloTabla2, "Edad", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Masculino", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Femenino", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Detenidos", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            int k = 1;
            foreach (int Edad in Edades.Distinct())

            {
                var detenidoshombre = detenidosporedad.FirstOrDefault(x => x.Edad == Edad && x.Sexo.ToUpper() == "Masculino".ToUpper());
                var detenidosmujer = detenidosporedad.FirstOrDefault(x => x.Edad == Edad && x.Sexo.ToUpper() == "Femenino".ToUpper());
                int homdet = 0;
                int mujdet = 0;
                homdet = detenidoshombre != null ? detenidoshombre.cantidad : 0;
                mujdet = detenidosmujer != null ? detenidosmujer.cantidad : 0;
                int _totaldetenidos = homdet + mujdet;
                double porcentajehombre = (homdet * 100.0) / _totaldetenidos;
                double porcentajemujer = (mujdet * 100.0) / _totaldetenidos;
                double porcentajedetenidosedad = (_totaldetenidos * 100.0) / Totaldetenidos;

                if (k % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                k++;
                CeldaParaTablas(tablay, FuenteNormal, Edad.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, homdet.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, double.IsNaN(porcentajehombre) ? "0" : Math.Round(porcentajehombre, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, mujdet.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, double.IsNaN(porcentajemujer) ? "0" : Math.Round(porcentajemujer, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, _totaldetenidos.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, double.IsNaN(porcentajedetenidosedad) ? "0" : Math.Round(porcentajedetenidosedad, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);

            }
            CeldaVacio(tablay, 7);
            documentoPdf.Add(tablay);

            PdfPTable tabla2 = new PdfPTable(7);//Numero de columnas de la tabla            
            tablay.WidthPercentage = 100; //Porcentaje ancho
            tablay.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            CeldaParaTablas(tabla2, FuenteTituloTabla2, "Clasificación", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla2, FuenteTituloTabla2, "Masculino", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla2, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla2, FuenteTituloTabla2, "Femenino", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla2, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla2, FuenteTituloTabla2, "Total", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla2, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            int totalmujere2 = 0;
            int totalhombres2 = 0;

            totalhombres2 = detenidosporedad.Where(y => y.Sexo.ToUpper() == "Masculino".ToUpper() && y.Edad >= 18).Sum(x => x.cantidad);
            totalmujere2 = detenidosporedad.Where(y => y.Sexo.ToUpper() == "Femenino".ToUpper() && y.Edad >= 18).Sum(x => x.cantidad);
            int Totaldetenidos2 = totalhombres2 + totalmujere2;
            double rporcentajehombres2;
            double rporcentajemujeres2;
            rporcentajehombres2 = (totalhombres2 * 100.0) / Totaldetenidos2;
            rporcentajemujeres2 = (totalmujere2 * 100.0) / Totaldetenidos2;
            double porcentajedetenidosedad2 = (Totaldetenidos2 * 100.0) / Totaldetenidos;

            CeldaParaTablas(tabla2, FuenteNormal, "MAYOR DE EDAD", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, totalhombres2.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(rporcentajehombres2) ? "0" : Math.Round(rporcentajehombres2, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, totalmujere2.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(rporcentajemujeres2) ? "0" : Math.Round(rporcentajemujeres2, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, Totaldetenidos2.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(porcentajedetenidosedad2) ? "0" : Math.Round(porcentajedetenidosedad2, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);



            int totalmujere3 = 0;
            int totalhombres3 = 0;

            totalhombres3 = detenidosporedad.Where(y => y.Sexo.ToUpper() == "Masculino".ToUpper() && y.Edad >= 1 && y.Edad < 18).Sum(x => x.cantidad);
            totalmujere3 = detenidosporedad.Where(y => y.Sexo.ToUpper() == "Femenino".ToUpper() && y.Edad >= 1 && y.Edad < 18).Sum(x => x.cantidad);
            int Totaldetenidos3 = totalhombres3 + totalmujere3;
            double rporcentajehombres3 = 0.0;
            double rporcentajemujeres3 = 0.0;
            rporcentajehombres3 = (totalhombres3 * 100.0) / Totaldetenidos3;
            rporcentajemujeres3 = (totalmujere3 * 100.0) / Totaldetenidos3;
            if (double.IsNaN(rporcentajehombres3))
            {
                rporcentajehombres3 = 0;
            }

            if (double.IsNaN(rporcentajemujeres3))
            {
                rporcentajemujeres3 = 0;
            }
            double porcentajedetenidosedad3 = (Totaldetenidos3 * 100.0) / Totaldetenidos;

            CeldaParaTablas(tabla2, FuenteNormal, "MENORES DE EDAD", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, totalhombres3.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(rporcentajehombres3) ? "0" : Math.Round(rporcentajehombres3, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, totalmujere3.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(rporcentajemujeres3) ? "0" : Math.Round(rporcentajemujeres3, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, Totaldetenidos3.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(porcentajedetenidosedad3) ? "0" : Math.Round(porcentajedetenidosedad3, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);



            int totalmujere4 = 0;
            int totalhombres4 = 0;

            totalhombres4 = detenidosporedad.Where(y => y.Sexo.ToUpper() == "Masculino".ToUpper() && y.Edad == 0).Sum(x => x.cantidad);
            totalmujere4 = detenidosporedad.Where(y => y.Sexo.ToUpper() == "Femenino".ToUpper() && y.Edad == 0).Sum(x => x.cantidad);
            int Totaldetenidos4 = totalhombres4 + totalmujere4;
            double rporcentajehombres4;
            double rporcentajemujeres4;
            rporcentajehombres4 = (totalhombres4 * 100.0) / Totaldetenidos4;
            rporcentajemujeres4 = (totalmujere4 * 100.0) / Totaldetenidos4;
            if (double.IsNaN(rporcentajehombres4))
            {
                rporcentajehombres4 = 0;
            }
            if (double.IsNaN(rporcentajemujeres4))
            {
                rporcentajemujeres4 = 0;
            }

            double porcentajedetenidosedad4 = (Totaldetenidos4 * 100.0) / Totaldetenidos;

            CeldaParaTablas(tabla2, FuenteNormal, "NO ESPECIFICADA", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, totalhombres4.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(rporcentajehombres4) ? "0" : Math.Round(rporcentajehombres4, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, totalmujere4.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(rporcentajemujeres4) ? "0" : Math.Round(rporcentajemujeres4, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, Totaldetenidos4.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(porcentajedetenidosedad4) ? "0" : Math.Round(porcentajedetenidosedad4, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);

            CeldaParaTablas(tabla2, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, (totalhombres4 + totalhombres3 + totalhombres2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN((Convert.ToDouble((totalhombres4 + totalhombres3 + totalhombres2)) * 100) / Totaldetenidos) ? "0" : Math.Round((Convert.ToDouble((totalhombres4 + totalhombres3 + totalhombres2)) * 100) / Totaldetenidos, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, (totalmujere4 + totalmujere3 + totalmujere2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN((Convert.ToDouble((totalmujere4 + totalmujere3 + totalmujere2) * 100) / Totaldetenidos)) ? "0" : Math.Round((Convert.ToDouble((totalmujere4 + totalmujere3 + totalmujere2) * 100) / Totaldetenidos), 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, Totaldetenidos.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, Math.Round(100.0, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            documentoPdf.Add(tabla2);



        }


        /// <summary>
        public static object[] ReporteDetalledetenidoporrangoremision(List<Entity.ReporteEstadisticoInformeDetencion> reporteEstadisticoInformes,string[] filtros,object[] data)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "InformeDetencion" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.Informedetencion"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloDetalledeteniodosporrangoremision(documentoPdf, FuenteTitulo, FuenteTituloTabla, reporteEstadisticoInformes, blau,filtros,data);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloDetalledeteniodosporrangoremision(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.ReporteEstadisticoInformeDetencion> reporteEstadisticoInformes, BaseColor blau, string[] filtros, object[] data)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;



            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));

            }



            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            if (filtros[2] =="0" && filtros[6]!="0")
                {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
            }
            

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Informe de detención", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 11f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 18f, Font.BOLD, blau);
            Font FuenteNegrita2 = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            documentoPdf.Add(tabla);
            BaseColor colorrelleno = new BaseColor(192, 224, 255);
            BaseColor colorrelleno2 = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.BLACK);

            List<int> Edades = new List<int>();
            Font FuenteNegritaq = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablam2 = new PdfPTable(1);
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;
            Celda(tablam2, FuenteNegritaq, "Municipio:" + data[0].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            string fechainicial = "";
            string fechafinal = "";

            if (!Convert.ToBoolean(filtros[5]))
            {
                fechainicial = Convert.ToDateTime(filtros[0]).ToShortDateString();
                fechafinal = Convert.ToDateTime(filtros[1]).ToShortDateString();

            }
            else
            {
                var date = Convert.ToDateTime(filtros[0]);
                var h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                var m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                var s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechainicial = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
                date = Convert.ToDateTime(filtros[1]);
                h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechafinal = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
            }

            Celda(tablam2, FuenteNegritaq, $"Del {fechainicial} al {fechafinal}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            if (filtros[3] != "0")
            {
                var contratosub = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(filtros[3]));
                var instit = new Entity.Institucion();
                if(contratosub !=null)
                {
                    instit = ControlInstitucion.ObtenerPorId(contratosub.InstitucionId);
                }
                else
                {
                    instit = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                }
                 
                Celda(tablam2, FuenteNegritaq, "Institución:" + instit.Nombre, 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            }
            CeldaVacio(tablam2, 1);
            documentoPdf.Add(tablam2);


            foreach (var item in reporteEstadisticoInformes)

            {

                PdfPTable tablaencabeadoinforme = new PdfPTable(1);//Numero de columnas de la tabla


                float[] w1 = new float[] { 20, 30, 30, 20 };
                float[] w2 = new float[] { 20, 30, 30, 20 };
                float[] w3 = new float[] { 100 };
                tablaencabeadoinforme.WidthPercentage = 100; //Porcentaje ancho
                tablaencabeadoinforme.HorizontalAlignment = Element.ALIGN_LEFT;
                tablaencabeadoinforme.SetWidths(w3);
                Celda(tablaencabeadoinforme, FuenteNegrita, "DATOS DEL DETENIDO", 0, 0, 1, 1, 0);
                var detalle = ControlDetalleDetencion.ObtenerPorExpediente(item.Remision.ToString());
                var info = ControlInformacionDeDetencion.ObtenerPorInternoId(detalle.DetenidoId);
                var evento = ControlEvento.ObtenerById(info.IdEvento);
                var motivo = "";
                var detenidoevento = ControlDetenidoEvento.ObtenerPorId(info.IdDetenido_Evento);
                if(detenidoevento!=null)
                {
                    var mot = ControlWSAMotivo.ObtenerPorId(detenidoevento.MotivoId);
                    if (mot != null) motivo = mot.NombreMotivoLlamada;
                }
                

                documentoPdf.Add(tablaencabeadoinforme);
                PdfPTable tablainfo = new PdfPTable(4);
                tablainfo.WidthPercentage = 100; //Porcentaje ancho
                tablainfo.HorizontalAlignment = Element.ALIGN_LEFT;
                tablainfo.SetWidths(w1);
                CeldaVacio(tablainfo, 1);
                CeldaConChunk(tablainfo, FuenteNegrita2, FuenteNormal, "Remisión: ", item.Remision.ToString(), 0, 0, 1);
                CeldaVacio(tablainfo, 2);
                CeldaVacio(tablainfo, 1);
                CeldaConChunk(tablainfo, FuenteNegrita2, FuenteNormal, "Registro: ", item.Fecharegistro.ToString("dd/MM/yyyy HH:mm:ss"), 0, 0, 1);
                CeldaVacio(tablainfo, 2);
                CeldaVacio(tablainfo, 1);
                CeldaConChunk(tablainfo, FuenteNegrita2, FuenteNormal, "Detención: ", item.Fechadetencion.ToString("dd/MM/yyyy HH:mm:ss"), 0, 0, 1);
                CeldaVacio(tablainfo, 2);

                documentoPdf.Add(tablainfo);
                PdfPTable tablenamalias = new PdfPTable(4);
                tablenamalias.WidthPercentage = 100; //Porcentaje ancho
                tablenamalias.HorizontalAlignment = Element.ALIGN_LEFT;
                tablenamalias.SetWidths(w2);
                CeldaVacio(tablenamalias, 1);
                CeldaConChunk(tablenamalias, FuenteNegrita2, FuenteNormal, "Nombre: ", item.Nombre.ToString(), 0, 0, 1);
                CeldaConChunk(tablenamalias, FuenteNegrita2, FuenteNormal, "Alias: ", item.Alias.ToString(), 0, 0, 1);
                CeldaVacio(tablenamalias, 1);
                documentoPdf.Add(tablenamalias);
                float[] w4 = new float[] { 20, 30, 30, 20 };
                PdfPTable tabledatodet1 = new PdfPTable(4);
                tabledatodet1.WidthPercentage = 100; //Porcentaje ancho
                tabledatodet1.HorizontalAlignment = Element.ALIGN_LEFT;
                tabledatodet1.SetWidths(w4);
                CeldaVacio(tabledatodet1, 1);



                if (item.Fechanacimiento != DateTime.MinValue)

                {
                    int edad = 0;
                    edad = DateTime.Now.Year - item.Fechanacimiento.Year;
                    if (DateTime.Now.DayOfYear < item.Fechanacimiento.DayOfYear)
                        edad = edad - 1;
                    item.Edad = item.Edad != 0 ? item.Edad : edad;
                }
                var edades = "";
                if (item.Edad != 0) edades = item.Edad.ToString();

                CeldaConChunk2(tabledatodet1, FuenteNegrita2, FuenteNormal, "Fecha Nac.: ", (item.Fechanacimiento != DateTime.MinValue) ? item.Fechanacimiento.ToShortDateString() : "", 0, 0, 1, " Edad: ", edades + " años", " Sexo: ", item.Sexo.ToString());
                //CeldaConChunk(tabledatodet1, FuenteNegrita2, FuenteNormal, "Edad: ", item.Edad.ToString()+" años", 0, 0, 1);
                //CeldaConChunk(tabledatodet1, FuenteNegrita2, FuenteNormal, "Sexo: ", item.Sexo.ToString(), 0, 0, 1);
                CeldaConChunk(tabledatodet1, FuenteNegrita2, FuenteNormal, "Edo. Civil: ", (!string.IsNullOrEmpty(item.Estadocivil)) ? item.Estadocivil : "Sin dato", 0, 0, 1);
                CeldaVacio(tabledatodet1, 1);

                CeldaVacio(tabledatodet1, 1);
                var nacionalidad = "";
                if (!string.IsNullOrEmpty(item.Nacionalidad)) nacionalidad = item.Nacionalidad;
                CeldaConChunk(tabledatodet1, FuenteNegrita2, FuenteNormal, "Nacionalidad: ", nacionalidad, 0, 0, 1);
                CeldaConChunk2(tabledatodet1, FuenteNegrita2, FuenteNormal, "Estatura: ", item.Estatura != 0 ? item.Estatura.ToString() + " mts." : "", 0, 0, 1, " Peso: ", item.peso != 0 ? item.peso.ToString() + " kgs." : "", "", "");


                CeldaVacio(tabledatodet1, 1);

                CeldaVacio(tabledatodet1, 1);
                CeldaConChunk(tabledatodet1, FuenteNegrita2, FuenteNormal, "Domicilio: ", (!string.IsNullOrEmpty(item.Domicilio)) ? item.Domicilio.ToString() : "Sin dato", 0, 0, 1);
                CeldaConChunk(tabledatodet1, FuenteNegrita2, FuenteNormal, "Ocupación: ", item.Ocupacion.ToString(), 0, 0, 1);

                CeldaVacio(tabledatodet1, 1);

                CeldaVacio(tabledatodet1, 1);
                CeldaConChunk(tabledatodet1, FuenteNegrita2, FuenteNormal, "Colonia: ", (!string.IsNullOrEmpty(item.Colonia)) ? item.Colonia.ToString() : "Sin dato", 0, 0, 1);
                CeldaConChunk2(tabledatodet1, FuenteNegrita2, FuenteNormal, "Escolaridad: ", item.Escolaridad, 0, 0, 1, " Sueldo: ", item.Sueldo != 0 ? "$" + item.Sueldo.ToString() + " " : "$0.00", "", "");


                CeldaVacio(tabledatodet1, 1);

                CeldaVacio(tabledatodet1, 1);
                CeldaConChunk2(tabledatodet1, FuenteNegrita2, FuenteNormal, "Ciudad: ", (!string.IsNullOrEmpty(item.Ciudad)) ? item.Ciudad : "Sin dato", 0, 0, 1, " Estado: ", (!string.IsNullOrEmpty(item.Estado)) ? item.Estado : "Sin dato", "", "");
                CeldaVacio(tabledatodet1, 1);

                CeldaVacio(tabledatodet1, 1);

                CeldaVacio(tabledatodet1, 4);
                documentoPdf.Add(tabledatodet1);
                PdfPTable encabezadoremision = new PdfPTable(1);
                encabezadoremision.WidthPercentage = 100; //Porcentaje ancho
                encabezadoremision.HorizontalAlignment = Element.ALIGN_LEFT;
                encabezadoremision.SetWidths(w3);
                Celda(encabezadoremision, FuenteNegrita, "REMISIÓN", 0, 0, 1, 1, 0);
                CeldaVacio(encabezadoremision, 1);
                documentoPdf.Add(encabezadoremision);

                float[] w5 = new float[] { 20, 80 };
                PdfPTable tablainforemision = new PdfPTable(2);
                tablainforemision.WidthPercentage = 100; //Porcentaje ancho
                tablainforemision.HorizontalAlignment = Element.ALIGN_LEFT;
                tablainforemision.SetWidths(w5);
                CeldaVacio(tablainforemision, 1);
                CeldaConChunk(tablainforemision, FuenteNegrita2, FuenteNormal, "No. Evento: ", item.Noevento.ToString(), 0, 0, 1);
                documentoPdf.Add(tablainforemision);

                tablainforemision = new PdfPTable(2);
                tablainforemision.WidthPercentage = 100; //Porcentaje ancho
                tablainforemision.HorizontalAlignment = Element.ALIGN_LEFT;
                tablainforemision.SetWidths(w5);
                CeldaVacio(tablainforemision, 1);
                CeldaConChunk(tablainforemision, FuenteNegrita2, FuenteNormal, "Motivo: ", motivo, 0, 0, 1);
                CeldaVacio(tablainforemision, 1);
                CeldaConChunk(tablainforemision, FuenteNegrita2, FuenteNormal, "Descripción: ", item.Eventodescripcion.ToString(), 0, 0, 1);
                documentoPdf.Add(tablainforemision);

                tablainforemision = new PdfPTable(2);
                tablainforemision.WidthPercentage = 100; //Porcentaje ancho
                tablainforemision.HorizontalAlignment = Element.ALIGN_LEFT;
                tablainforemision.SetWidths(w5);
                CeldaVacio(tablainforemision, 1);
                CeldaConChunk(tablainforemision, FuenteNegrita2, FuenteNormal, "Lugar de detención: ", item.Lugardetencion.ToString(), 0, 0, 1);
                documentoPdf.Add(tablainforemision);


                tablainforemision = new PdfPTable(2);
                tablainforemision.WidthPercentage = 100; //Porcentaje ancho
                tablainforemision.HorizontalAlignment = Element.ALIGN_LEFT;
                tablainforemision.SetWidths(w5);
                CeldaVacio(tablainforemision, 1);
                CeldaConChunk(tablainforemision, FuenteNegrita2, FuenteNormal, "Colonia: ", item.Coloniadetencion.ToString(), 0, 0, 1);

                documentoPdf.Add(tablainforemision);


                float[] w6 = new float[] { 20, 30, 30, 20 };
                PdfPTable tablaunidad = new PdfPTable(4);
                tablaunidad.WidthPercentage = 100; //Porcentaje ancho
                tablaunidad.HorizontalAlignment = Element.ALIGN_LEFT;
                tablaunidad.SetWidths(w6);

                CeldaVacio(tablaunidad, 1);
                CeldaConChunk(tablaunidad, FuenteNegrita2, FuenteNormal, "Cve. Unidad: ", "", 0, 0, 1);
                CeldaConChunk(tablaunidad, FuenteNegrita2, FuenteNormal, "Cve. Agente: ", "", 0, 0, 1);
                CeldaVacio(tablaunidad, 1);

                CeldaVacio(tablaunidad, 1);
                CeldaConChunk(tablaunidad, FuenteNegrita2, FuenteNormal, "", item.Claveunidad, 0, 0, 1);
                CeldaConChunk(tablaunidad, FuenteNegrita2, FuenteNormal, "", item.Claveagente, 0, 0, 1);
                CeldaVacio(tablaunidad, 1);

                CeldaVacio(tablaunidad, 1);
                CeldaConChunk(tablaunidad, FuenteNegrita2, FuenteNormal, "Nombre agente: ", "", 0, 0, 1);
                CeldaConChunk(tablaunidad, FuenteNegrita2, FuenteNormal, "Firma", "", 0, 0, 1);
                CeldaVacio(tablaunidad, 1);

                CeldaVacio(tablaunidad, 1);
                CeldaConChunk(tablaunidad, FuenteNegrita2, FuenteNormal, "", item.Nombreagente, 0, 0, 1);
                CeldaConChunk(tablaunidad, FuenteNegrita2, FuenteNormal, "", "___________________", 0, 0, 1);
                CeldaVacio(tablaunidad, 1);

                documentoPdf.Add(tablaunidad);
                documentoPdf.NewPage();

            }




            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;





        }


        ////
        /// <summary>
        /// <summary>
        public static object[] ReporteTituloDetalledeteniodosDescriptivoporrangoremision(List<Entity.DescriptivoDetenidos> reporteEstadisticoInformes,string[] filtros,object[] data)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Descriptivodetenidos" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.Descriptivodetenidos"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloDetalledeteniodosDescriptivoporrangoremision(documentoPdf, FuenteTitulo, FuenteTituloTabla, reporteEstadisticoInformes, blau,filtros,data);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloDetalledeteniodosDescriptivoporrangoremision(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.DescriptivoDetenidos> reporteEstadisticoInformes, BaseColor blau,string[] filtros,object[] data)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;



            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));

            }



            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            if(filtros[2] =="0" && filtros[6] !="0")
            {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
            }
            

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Descriptivo de detenidos", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 11f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 18f, Font.BOLD, blau);
            Font FuenteNegrita2 = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            documentoPdf.Add(tabla);
            Font FuenteNegritaq = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablam2 = new PdfPTable(1);
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;
            Celda(tablam2, FuenteNegritaq, "Municipio:" + data[0].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            string fechainicial = "";
            string fechafinal = "";

            if (!Convert.ToBoolean(filtros[5]))
            {
                fechainicial = Convert.ToDateTime(filtros[0]).ToShortDateString();
                fechafinal = Convert.ToDateTime(filtros[1]).ToShortDateString();

            }
            else
            {
                var date = Convert.ToDateTime(filtros[0]);
                var h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                var m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                var s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechainicial = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
                date = Convert.ToDateTime(filtros[1]);
                h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechafinal = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
            }

            Celda(tablam2, FuenteNegritaq, $"Del {fechainicial} al {fechafinal}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(tablam2, FuenteNegritaq, "Institución:" + data[1].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            if (filtros[3] != "0")

                CeldaVacio(tablam2, 1);
            documentoPdf.Add(tablam2);
            BaseColor colorrelleno = new BaseColor(192, 224, 255);
            BaseColor colorrelleno2 = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.BLACK);

            List<int> Edades = new List<int>();



            foreach (var item in reporteEstadisticoInformes)

            {

                PdfPTable tablaencabeadoinforme = new PdfPTable(1);//Numero de columnas de la tabla


                float[] w1 = new float[] { 20, 30, 30, 20 };
                float[] w2 = new float[] { 20, 30, 30, 20 };
                float[] w3 = new float[] { 100 };
                tablaencabeadoinforme.WidthPercentage = 100; //Porcentaje ancho
                tablaencabeadoinforme.HorizontalAlignment = Element.ALIGN_LEFT;
                tablaencabeadoinforme.SetWidths(w3);
                Celda(tablaencabeadoinforme, FuenteNegrita, "DATOS DEL DETENIDO", 0, 0, 1, 1, 0);
                var detalle = ControlDetalleDetencion.ObtenerPorId(item.DetalleDetencionId);
                var info = ControlInformacionDeDetencion.ObtenerPorInternoId(detalle.DetenidoId);
                var evento = ControlEvento.ObtenerById(info.IdEvento);
                var motivoe = "";
                var detenidoevendo = ControlDetenidoEvento.ObtenerPorId(info.IdDetenido_Evento);
                if(detenidoevendo!=null)
                {
                    var mote = ControlWSAMotivo.ObtenerPorId(detenidoevendo.MotivoId);
                    if (mote != null) motivoe = mote.NombreMotivoLlamada;
                }
                

                documentoPdf.Add(tablaencabeadoinforme);
                PdfPTable tablainfo = new PdfPTable(4);
                tablainfo.WidthPercentage = 100; //Porcentaje ancho
                tablainfo.HorizontalAlignment = Element.ALIGN_LEFT;
                tablainfo.SetWidths(w1);
                CeldaVacio(tablainfo, 1);
                CeldaConChunk(tablainfo, FuenteNegrita2, FuenteNormal, "Remisión: ", item.Remision.ToString(), 0, 0, 1);
                CeldaVacio(tablainfo, 2);
                CeldaVacio(tablainfo, 1);
                CeldaConChunk(tablainfo, FuenteNegrita2, FuenteNormal, "Registro: ", item.Fecharegistro.ToString("dd/MM/yyyy HH:mm:ss"), 0, 0, 1);
                CeldaVacio(tablainfo, 2);
                CeldaVacio(tablainfo, 1);
                CeldaConChunk(tablainfo, FuenteNegrita2, FuenteNormal, "Detención: ", item.Fechadetencion.ToString("dd/MM/yyyy HH:mm:ss"), 0, 0, 1);
                CeldaVacio(tablainfo, 2);

                documentoPdf.Add(tablainfo);
                PdfPTable tablenamalias = new PdfPTable(4);
                tablenamalias.WidthPercentage = 100; //Porcentaje ancho
                tablenamalias.HorizontalAlignment = Element.ALIGN_LEFT;
                tablenamalias.SetWidths(w2);
                CeldaVacio(tablenamalias, 1);
                CeldaConChunk(tablenamalias, FuenteNegrita2, FuenteNormal, "Nombre: ", item.Nombre.ToString(), 0, 0, 1);
                CeldaConChunk(tablenamalias, FuenteNegrita2, FuenteNormal, "Alias: ", item.Alias.ToString(), 0, 0, 1);
                CeldaVacio(tablenamalias, 1);
                documentoPdf.Add(tablenamalias);
                float[] w4 = new float[] { 20, 30, 30, 20 };
                PdfPTable tabledatodet1 = new PdfPTable(4);
                tabledatodet1.WidthPercentage = 100; //Porcentaje ancho
                tabledatodet1.HorizontalAlignment = Element.ALIGN_LEFT;
                tabledatodet1.SetWidths(w4);
                CeldaVacio(tabledatodet1, 1);



                if (item.Fechanacimiento != DateTime.MinValue)

                {
                    int edad = 0;
                    edad = DateTime.Now.Year - item.Fechanacimiento.Year;
                    if (DateTime.Now.DayOfYear < item.Fechanacimiento.DayOfYear)
                        edad = edad - 1;
                    item.Edad = item.Edad != 0 ? item.Edad : edad;
                }
                var edades = "";
                if (item.Edad != 0) edades = item.Edad.ToString();

                CeldaConChunk2(tabledatodet1, FuenteNegrita2, FuenteNormal, "Fecha Nac.: ", (item.Fechanacimiento != DateTime.MinValue) ? item.Fechanacimiento.ToShortDateString() : "", 0, 0, 1, " Edad: ", edades + " años", " Sexo: ", item.Sexo.ToString());
                //CeldaConChunk(tabledatodet1, FuenteNegrita2, FuenteNormal, "Edad: ", item.Edad.ToString()+" años", 0, 0, 1);
                //CeldaConChunk(tabledatodet1, FuenteNegrita2, FuenteNormal, "Sexo: ", item.Sexo.ToString(), 0, 0, 1);
                CeldaConChunk(tabledatodet1, FuenteNegrita2, FuenteNormal, "Edo. Civil: ", (!string.IsNullOrEmpty(item.Estadocivil)) ? item.Estadocivil : "sin dato", 0, 0, 1);
                CeldaVacio(tabledatodet1, 1);

                CeldaVacio(tabledatodet1, 1);
                var nacionalidad = "";
                if (!string.IsNullOrEmpty(item.Nacionalidad)) nacionalidad = item.Nacionalidad;
                CeldaConChunk(tabledatodet1, FuenteNegrita2, FuenteNormal, "Nacionalidad: ", nacionalidad, 0, 0, 1);
                CeldaConChunk2(tabledatodet1, FuenteNegrita2, FuenteNormal, "Estatura: ", item.Estatura != 0 ? item.Estatura.ToString() + " mts." : "", 0, 0, 1, " Peso: ", item.peso != 0 ? item.peso.ToString() + " kgs." : "", "", "");


                CeldaVacio(tabledatodet1, 1);

                CeldaVacio(tabledatodet1, 1);
                CeldaConChunk(tabledatodet1, FuenteNegrita2, FuenteNormal, "Domicilio: ", (!string.IsNullOrEmpty(item.Domicilio)) ? item.Domicilio.ToString() : "", 0, 0, 1);
                CeldaConChunk(tabledatodet1, FuenteNegrita2, FuenteNormal, "Ocupación: ", item.Ocupacion.ToString(), 0, 0, 1);

                CeldaVacio(tabledatodet1, 1);

                CeldaVacio(tabledatodet1, 1);
                CeldaConChunk(tabledatodet1, FuenteNegrita2, FuenteNormal, "Colonia: ", (!string.IsNullOrEmpty(item.Colonia)) ? item.Colonia.ToString() : "", 0, 0, 1);
                CeldaConChunk2(tabledatodet1, FuenteNegrita2, FuenteNormal, "Escolaridad: ", item.Escolaridad, 0, 0, 1, " Sueldo: ", item.Sueldo != 0 ? item.peso.ToString() + " " : "", "", "");


                CeldaVacio(tabledatodet1, 1);

                CeldaVacio(tabledatodet1, 1);
                CeldaConChunk2(tabledatodet1, FuenteNegrita2, FuenteNormal, "Ciudad: ", (!string.IsNullOrEmpty(item.Ciudad)) ? item.Ciudad : "", 0, 0, 1, " Estado: ", (!string.IsNullOrEmpty(item.Estado)) ? item.Estado : "", "", "");
                CeldaVacio(tabledatodet1, 1);

                CeldaVacio(tabledatodet1, 1);

                CeldaVacio(tabledatodet1, 4);
                documentoPdf.Add(tabledatodet1);
                PdfPTable encabezadoremision = new PdfPTable(1);
                encabezadoremision.WidthPercentage = 100; //Porcentaje ancho
                encabezadoremision.HorizontalAlignment = Element.ALIGN_LEFT;
                encabezadoremision.SetWidths(w3);
                Celda(encabezadoremision, FuenteNegrita, "REMISIÓN", 0, 0, 1, 1, 0);
                CeldaVacio(encabezadoremision, 1);
                documentoPdf.Add(encabezadoremision);

                float[] w5 = new float[] { 20, 80 };
                PdfPTable tablainforemision = new PdfPTable(2);
                tablainforemision.WidthPercentage = 100; //Porcentaje ancho
                tablainforemision.HorizontalAlignment = Element.ALIGN_LEFT;
                tablainforemision.SetWidths(w5);
                CeldaVacio(tablainforemision, 1);
                CeldaConChunk(tablainforemision, FuenteNegrita2, FuenteNormal, "No. Evento: ", item.Noevento.ToString(), 0, 0, 1);

                documentoPdf.Add(tablainforemision);

                tablainforemision = new PdfPTable(2);
                tablainforemision.WidthPercentage = 100; //Porcentaje ancho
                tablainforemision.HorizontalAlignment = Element.ALIGN_LEFT;
                tablainforemision.SetWidths(w5);
                CeldaVacio(tablainforemision, 1);
                CeldaConChunk(tablainforemision, FuenteNegrita2, FuenteNormal, "Motivo: ", motivoe, 0, 0, 1);
                CeldaVacio(tablainforemision, 1);
                CeldaConChunk(tablainforemision, FuenteNegrita2, FuenteNormal, "Descripción: ", item.Eventodescripcion.ToString(), 0, 0, 1);
                documentoPdf.Add(tablainforemision);

                tablainforemision = new PdfPTable(2);
                tablainforemision.WidthPercentage = 100; //Porcentaje ancho
                tablainforemision.HorizontalAlignment = Element.ALIGN_LEFT;
                tablainforemision.SetWidths(w5);
                CeldaVacio(tablainforemision, 1);
                CeldaConChunk(tablainforemision, FuenteNegrita2, FuenteNormal, "Lugar de detención: ", item.Lugardetencion.ToString(), 0, 0, 1);
                documentoPdf.Add(tablainforemision);


                tablainforemision = new PdfPTable(2);
                tablainforemision.WidthPercentage = 100; //Porcentaje ancho
                tablainforemision.HorizontalAlignment = Element.ALIGN_LEFT;
                tablainforemision.SetWidths(w5);
                CeldaVacio(tablainforemision, 1);
                CeldaConChunk(tablainforemision, FuenteNegrita2, FuenteNormal, "Colonia: ", item.Coloniadetencion.ToString(), 0, 0, 1);

                documentoPdf.Add(tablainforemision);


                float[] w6 = new float[] { 20, 30, 30, 20 };
                PdfPTable tablaunidad = new PdfPTable(4);
                tablaunidad.WidthPercentage = 100; //Porcentaje ancho
                tablaunidad.HorizontalAlignment = Element.ALIGN_LEFT;
                tablaunidad.SetWidths(w6);
                CeldaVacio(tablaunidad, 1);
                CeldaConChunk(tablaunidad, FuenteNegrita2, FuenteNormal, "Cve. Unidad: ", "", 0, 0, 1);
                CeldaConChunk(tablaunidad, FuenteNegrita2, FuenteNormal, "Cve. Agente: ", "", 0, 0, 1);
                CeldaVacio(tablaunidad, 1);

                CeldaVacio(tablaunidad, 1);
                CeldaConChunk(tablaunidad, FuenteNegrita2, FuenteNormal, "", item.Claveunidad, 0, 0, 1);
                CeldaConChunk(tablaunidad, FuenteNegrita2, FuenteNormal, "", item.Claveagente, 0, 0, 1);
                CeldaVacio(tablaunidad, 1);

                CeldaVacio(tablaunidad, 1);
                CeldaConChunk(tablaunidad, FuenteNegrita2, FuenteNormal, "Nombre agente: ", "", 0, 0, 1);
                CeldaConChunk(tablaunidad, FuenteNegrita2, FuenteNormal, "Firma", "", 0, 0, 1);
                CeldaVacio(tablaunidad, 1);


                CeldaVacio(tablaunidad, 1);
                CeldaConChunk(tablaunidad, FuenteNegrita2, FuenteNormal, "", item.Nombreagente, 0, 0, 1);
                CeldaConChunk(tablaunidad, FuenteNegrita2, FuenteNormal, "", "____________________", 0, 0, 1);
                CeldaVacio(tablaunidad, 1);
                documentoPdf.Add(tablaunidad);

                PdfPTable EncabezadoSituacion = new PdfPTable(1);
                EncabezadoSituacion.WidthPercentage = 100; //Porcentaje ancho
                EncabezadoSituacion.HorizontalAlignment = Element.ALIGN_LEFT;
                EncabezadoSituacion.SetWidths(w3);
                Celda(EncabezadoSituacion, FuenteNegrita, "SITUACIÓN", 0, 0, 1, 1, 0);
                CeldaVacio(EncabezadoSituacion, 1);

                var s = new Entity.MotivoDetencionInterno();
                s.InternoId = item.DetalleDetencionId;
                var mot = ControlMotivoDetencionInterno.GetListaMotivosDetencionByInternoId(s);

                var articulo = "";
                var motivo = "";

                if (mot != null || mot.Count > 0)
                {
                    var ins = 0;
                    foreach (var G in mot)
                    {
                        var T = ControlMotivoDetencion.ObtenerPorId(G.MotivoDetencionId);
                        if (ins == 0)
                        {
                            if (!string.IsNullOrEmpty(T.Articulo)) articulo = T.Articulo.ToString();
                            motivo = T.Motivo;
                        }
                        else
                        {
                            motivo = motivo + "," + T.Motivo;
                            if (!string.IsNullOrEmpty(T.Articulo) && !string.IsNullOrEmpty(articulo))
                            {
                                articulo = articulo + ", " + T.Articulo;
                            }
                            else
                            {
                                articulo = T.Articulo;
                            }
                        }
                        ins = ins + 1;
                    }

                }
                documentoPdf.Add(EncabezadoSituacion);

                float[] w7 = new float[] { 20, 30, 50 };
                PdfPTable tablasituacion = new PdfPTable(3);
                tablasituacion.WidthPercentage = 100; //Porcentaje ancho
                tablasituacion.HorizontalAlignment = Element.ALIGN_LEFT;
                tablasituacion.SetWidths(w7);
                CeldaVacio(tablasituacion, 1);
                CeldaConChunk(tablasituacion, FuenteNegrita2, FuenteNormal, "Artículo: ", articulo, 0, 0, 1);
                CeldaConChunk(tablasituacion, FuenteNegrita2, FuenteNormal, "Motivo: ", motivo, 0, 0, 1);
                documentoPdf.Add(tablasituacion);
                var autorizacion = "";
                var salida = ControlSalidaEfectuadaJuez.ObtenerByDetalleDetencionId(item.DetalleDetencionId);
                var tipo = "";
                if (salida != null && salida.Activo == 1)
                {

                    autorizacion = salida.Fundamento;
                    var tiposalida = ControlCatalogo.Obtener(salida.TiposalidaId, Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));
                    if (tiposalida != null)
                    {
                        tipo = tiposalida.Nombre;
                    }

                }
                PdfPTable EncabezadoAutorizacion = new PdfPTable(1);
                EncabezadoAutorizacion.WidthPercentage = 100; //Porcentaje ancho
                EncabezadoAutorizacion.HorizontalAlignment = Element.ALIGN_LEFT;
                EncabezadoAutorizacion.SetWidths(w3);
                Celda(EncabezadoAutorizacion, FuenteNegrita, "AUTORIZACIÓN DE SALIDA ", 0, 0, 1, 1, 0);
                CeldaVacio(EncabezadoAutorizacion, 1);
                documentoPdf.Add(EncabezadoAutorizacion);

                float[] w8 = new float[] { 20, 40, 40 };
                PdfPTable tablaautorizacion = new PdfPTable(3);
                tablaautorizacion.WidthPercentage = 100; //Porcentaje ancho
                tablaautorizacion.HorizontalAlignment = Element.ALIGN_LEFT;
                tablaautorizacion.SetWidths(w8);
                CeldaVacio(tablaautorizacion, 1);
                CeldaConChunk(tablaautorizacion, FuenteNegrita2, FuenteNormal, "Fundamento: ", autorizacion, 0, 0, 1);
                CeldaConChunk(tablaautorizacion, FuenteNegrita2, FuenteNormal, "Tipo de salida: ", tipo, 0, 0, 1);

                documentoPdf.Add(tablaautorizacion);

                documentoPdf.NewPage();

            }




            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;






        }


        ////
        /// </summary>
        /// <param name="detenidosporedad"></param>
        /// <returns></returns>
        public static object[] ReporteDetalledetenidoporedad(List<Entity.Detenidosporedad> detenidosporedad, string[] filtros,object[] data)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "DetenidosPorEdad" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.DetenidosPoredad"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloDetalledeteniodosporEdad(documentoPdf, FuenteTitulo, FuenteTituloTabla, detenidosporedad, blau, filtros, data);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloDetalledeteniodosporEdad(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.Detenidosporedad> detenidosporedad, BaseColor blau, string[] filtros, object[] data)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;



            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));

            }



            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            if(filtros[3]=="0" && filtros[5] !="0")
            {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
            }
            

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Detenidos por edad", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);


            documentoPdf.Add(tabla);

            Font FuenteNegritaq = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablam2 = new PdfPTable(1);//Numero de columnas de la tabla            
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;
            string fechainicial = "";
            string fechafinal = "";

            if (!Convert.ToBoolean(filtros[4]))
            {
                fechainicial = Convert.ToDateTime(filtros[0]).ToShortDateString();
                fechafinal = Convert.ToDateTime(filtros[1]).ToShortDateString();

            }
            else
            {
                var date = Convert.ToDateTime(filtros[0]);
                var h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                var m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                var s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechainicial = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
                date = Convert.ToDateTime(filtros[1]);
                h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechafinal = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
            }

            Celda(tablam2, FuenteNegritaq, "Municipio:" + data[0].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(tablam2, FuenteNegritaq, $"Del {fechainicial} al {fechafinal}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            if (filtros[3] != "0")
            {
                var contratosub = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(filtros[3]));
                var instit = ControlInstitucion.ObtenerPorId(contratosub.InstitucionId);
                Celda(tablam2, FuenteNegritaq, "Institución:" + instit.Nombre, 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            }
            CeldaVacio(tablam2, 1);
            documentoPdf.Add(tablam2);


            BaseColor colorrelleno = new BaseColor(192, 224, 255);
            BaseColor colorrelleno2 = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);

            List<int> Edades = new List<int>();

            foreach (var item in detenidosporedad)
            {
                Edades.Add(item.Edad);

            }

            var sexos = ControlCatalogo.ObtenerTodo(4);
            var masculinoId = sexos.FirstOrDefault(x => x.Nombre.ToUpper() == "Masculino".ToUpper()).Id;
            var femeninoId = sexos.FirstOrDefault(x => x.Nombre.ToUpper() == "Femenino".ToUpper()).Id;

            if (detenidosporedad.Where(x => Edades.Contains(x.Edad)).Count() > 1)
            {
                var detenidosporedadAux = new List<Entity.Detenidosporedad>();
                foreach (var edad in Edades.Distinct())
                {
                    var detenidos = detenidosporedad.Where(x => x.Edad == edad);
                    if (detenidos.Count() > 1)
                    {
                        var detenidoMasculino = detenidos.FirstOrDefault(x => x.Sexoid == masculinoId);
                        var detenidoFemenino = detenidos.FirstOrDefault(x => x.Sexoid == femeninoId);
                        if (detenidoMasculino != null)
                        {
                            foreach (var item in detenidos)
                            {
                                if (detenidoMasculino != item && item.Sexoid == masculinoId) detenidoMasculino.cantidad += item.cantidad;
                            }
                            detenidosporedadAux.Add(detenidoMasculino);
                        }
                        if (detenidoFemenino != null)
                        {
                            foreach (var item in detenidos)
                            {
                                if (detenidoFemenino != item && item.Sexoid == femeninoId) detenidoFemenino.cantidad += item.cantidad;
                            }
                            detenidosporedadAux.Add(detenidoFemenino);
                        }
                    }
                    else
                    {
                        detenidosporedadAux.Add(detenidos.First());
                    }
                }

                detenidosporedad = detenidosporedadAux;
            }

            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;

            int totalmujere = 0;
            int totalhombres = 0;

            totalhombres = detenidosporedad.Where(y => y.Sexo.ToUpper() == "Masculino".ToUpper()).Sum(x => x.cantidad);
            totalmujere = detenidosporedad.Where(y => y.Sexo.ToUpper() == "Femenino".ToUpper()).Sum(x => x.cantidad);
            int Totaldetenidos = totalhombres + totalmujere;
            double rporcentajehombres;
            double rporcentajemujeres;
            rporcentajehombres = (totalhombres * 100.0) / Totaldetenidos;
            rporcentajemujeres = (totalmujere * 100.0) / Totaldetenidos;

            ///
            PdfPTable tabla1 = new PdfPTable(7);//Numero de columnas de la tabla            
            tabla1.WidthPercentage = 100; //Porcentaje ancho
            tabla1.HorizontalAlignment = Element.ALIGN_RIGHT;//Alineación vertical
            CeldaParaTablas(tabla1, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla1, FuenteTituloTabla2, totalhombres.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla1, FuenteTituloTabla2, double.IsNaN(rporcentajehombres) ? "0" : Math.Round(rporcentajehombres, 2).ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla1, FuenteTituloTabla2, totalmujere.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla1, FuenteTituloTabla2, double.IsNaN(rporcentajemujeres) ? "0" : Math.Round(rporcentajemujeres, 2).ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla1, FuenteTituloTabla2, Totaldetenidos.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla1, FuenteTituloTabla2, "100", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaVacio(tabla1, 7);
            documentoPdf.Add(tabla1);
            ///
            PdfPTable tablay = new PdfPTable(7);//Numero de columnas de la tabla            
            tablay.WidthPercentage = 100; //Porcentaje ancho
            tablay.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            CeldaParaTablas(tablay, FuenteTituloTabla2, "Edad", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Masculino", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Femenino", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "Detenidos", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablay, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            int k = 1;
            foreach (int Edad in Edades.Distinct())

            {
                var detenidoshombre = detenidosporedad.FirstOrDefault(x => x.Edad == Edad && x.Sexo.ToUpper() == "Masculino".ToUpper());
                var detenidosmujer = detenidosporedad.FirstOrDefault(x => x.Edad == Edad && x.Sexo.ToUpper() == "Femenino".ToUpper());
                int homdet = 0;
                int mujdet = 0;
                homdet = detenidoshombre != null ? detenidoshombre.cantidad : 0;
                mujdet = detenidosmujer != null ? detenidosmujer.cantidad : 0;
                int _totaldetenidos = homdet + mujdet;
                double porcentajehombre = (homdet * 100.0) / _totaldetenidos;
                double porcentajemujer = (mujdet * 100.0) / _totaldetenidos;
                double porcentajedetenidosedad = (_totaldetenidos * 100.0) / Totaldetenidos;

                if (k % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                k++;
                CeldaParaTablas(tablay, FuenteNormal, Edad.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, homdet.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, double.IsNaN(porcentajehombre) ? "0" : Math.Round(porcentajehombre, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, mujdet.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, double.IsNaN(porcentajemujer) ? "0" : Math.Round(porcentajemujer, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, _totaldetenidos.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablay, FuenteNormal, double.IsNaN(porcentajedetenidosedad) ? "0" : Math.Round(porcentajedetenidosedad, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);

            }
            CeldaVacio(tablay, 7);
            documentoPdf.Add(tablay);

            PdfPTable tabla2 = new PdfPTable(7);//Numero de columnas de la tabla            
            tablay.WidthPercentage = 100; //Porcentaje ancho
            tablay.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            CeldaParaTablas(tabla2, FuenteTituloTabla2, "Clasificación", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla2, FuenteTituloTabla2, "Masculino", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla2, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla2, FuenteTituloTabla2, "Femenino", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla2, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla2, FuenteTituloTabla2, "Total", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla2, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            int totalmujere2 = 0;
            int totalhombres2 = 0;
            
            totalhombres2 = detenidosporedad.Where(y => y.Sexo.ToUpper() == "Masculino".ToUpper() && y.Edad >= 18).Sum(x => x.cantidad);
            totalmujere2 = detenidosporedad.Where(y => y.Sexo.ToUpper() == "Femenino".ToUpper() && y.Edad >= 18).Sum(x => x.cantidad);
            int Totaldetenidos2 = totalhombres2 + totalmujere2;
            double rporcentajehombres2;
            double rporcentajemujeres2;
            rporcentajehombres2 = (totalhombres2 * 100.0) / Totaldetenidos2;
            rporcentajemujeres2 = (totalmujere2 * 100.0) / Totaldetenidos2;
            double porcentajedetenidosedad2 = (Totaldetenidos2 * 100.0) / Totaldetenidos;

            CeldaParaTablas(tabla2, FuenteNormal, "MAYOR DE EDAD", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, totalhombres2.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(rporcentajehombres2) ? "0" : Math.Round(rporcentajehombres2, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, totalmujere2.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(rporcentajemujeres2) ? "0" : Math.Round(rporcentajemujeres2, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, Totaldetenidos2.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(porcentajedetenidosedad2) ? "0" : Math.Round(porcentajedetenidosedad2, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);



            int totalmujere3 = 0;
            int totalhombres3 = 0;

            totalhombres3 = detenidosporedad.Where(y => y.Sexo.ToUpper() == "Masculino".ToUpper() && y.Edad >0 && y.Edad < 18).Sum(x => x.cantidad);
            totalmujere3 = detenidosporedad.Where(y => y.Sexo.ToUpper() == "Femenino".ToUpper() && y.Edad > 0 && y.Edad < 18).Sum(x => x.cantidad);
            
            
            int Totaldetenidos3 = totalhombres3 + totalmujere3;
            double rporcentajehombres3 = 0.0;
            double rporcentajemujeres3 = 0.0;
            rporcentajehombres3 = (totalhombres3 * 100.0) / Totaldetenidos3;
            rporcentajemujeres3 = (totalmujere3 * 100.0) / Totaldetenidos3;
            if (double.IsNaN(rporcentajehombres3))
            {
                rporcentajehombres3 = 0;
            }

            if (double.IsNaN(rporcentajemujeres3))
            {
                rporcentajemujeres3 = 0;
            }
            double porcentajedetenidosedad3 = (Totaldetenidos3 * 100.0) / Totaldetenidos;

            CeldaParaTablas(tabla2, FuenteNormal, "MENORES DE EDAD", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, totalhombres3.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(rporcentajehombres3) ? "0" : Math.Round(rporcentajehombres3, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, totalmujere3.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(rporcentajemujeres3) ? "0" : Math.Round(rporcentajemujeres3, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, Totaldetenidos3.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(porcentajedetenidosedad3) ? "0" : Math.Round(porcentajedetenidosedad3, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);



            int totalmujere4 = 0;
            int totalhombres4 = 0;

            totalhombres4 = detenidosporedad.Where(y => y.Sexo.ToUpper() == "Masculino".ToUpper() && y.Edad == 0).Sum(x => x.cantidad);
            totalmujere4 = detenidosporedad.Where(y => y.Sexo.ToUpper() == "Femenino".ToUpper() && y.Edad == 0).Sum(x => x.cantidad);
            int Totaldetenidos4 = totalhombres4 + totalmujere4;
            double rporcentajehombres4;
            double rporcentajemujeres4;
            rporcentajehombres4 = (totalhombres4 * 100.0) / Totaldetenidos4;
            rporcentajemujeres4 = (totalmujere4 * 100.0) / Totaldetenidos4;
            if (double.IsNaN(rporcentajehombres4))
            {
                rporcentajehombres4 = 0;
            }
            if (double.IsNaN(rporcentajemujeres4))
            {
                rporcentajemujeres4 = 0;
            }

            double porcentajedetenidosedad4 = (Totaldetenidos4 * 100.0) / Totaldetenidos;

            CeldaParaTablas(tabla2, FuenteNormal, "NO ESPECIFICADA", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, totalhombres4.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(rporcentajehombres4) ? "0" : Math.Round(rporcentajehombres4, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, totalmujere4.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(rporcentajemujeres4) ? "0" : Math.Round(rporcentajemujeres4, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, Totaldetenidos4.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN(porcentajedetenidosedad4) ? "0" : Math.Round(porcentajedetenidosedad4, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);

            CeldaParaTablas(tabla2, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, (totalhombres4 + totalhombres3 + totalhombres2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN((Convert.ToDouble((totalhombres4 + totalhombres3 + totalhombres2)) * 100) / Totaldetenidos) ? "0" : Math.Round((Convert.ToDouble((totalhombres4 + totalhombres3 + totalhombres2)) * 100) / Totaldetenidos, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, (totalmujere4 + totalmujere3 + totalmujere2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, double.IsNaN((Convert.ToDouble((totalmujere4 + totalmujere3 + totalmujere2) * 100) / Totaldetenidos)) ? "0" : Math.Round((Convert.ToDouble((totalmujere4 + totalmujere3 + totalmujere2) * 100) / Totaldetenidos), 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, Totaldetenidos.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla2, FuenteNormal, Math.Round(100.0, 2).ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            documentoPdf.Add(tabla2);



        }
        //////

        public static object[] ReporteRelaciondetenidoporSalidaAutorizada(List<Entity.RelacionDetenidosPorSalidaAutorizada> relacionDetenidos, string[] filtros, object[] data)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Relaciondetenidossalidaautorizada" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.Relaciondetenidosporsalidaautorizadan"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloRelaciondeteniodosporSalidaAutorizada(documentoPdf, FuenteTitulo, FuenteTituloTabla, relacionDetenidos, blau,filtros,data);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }
        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }

            return edad;
        }
        private static void TituloRelaciondeteniodosporSalidaAutorizada(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.RelacionDetenidosPorSalidaAutorizada> relacionDetenidos, BaseColor blau, string[] filtros, object[] data)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;



            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));

            }



            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;

            if (filtros[3] == "0" && filtros[5] != "0")
            {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
            }

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Relación de detenidos por salida autorizada", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 9f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);


            documentoPdf.Add(tabla);
            Font FuenteNegritaq = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablam2 = new PdfPTable(1);//Numero de columnas de la tabla            
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;
            Celda(tablam2, FuenteNegritaq, "Municipio:" + data[0].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            string fechainicial = "";
            string fechafinal = "";
            if (!Convert.ToBoolean(filtros[4]))
            {
                fechainicial = Convert.ToDateTime(filtros[0]).ToShortDateString();
                fechafinal = Convert.ToDateTime(filtros[1]).ToShortDateString();
            }
            else
            {
                var date = Convert.ToDateTime(filtros[0]);
                var h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                var m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                var s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechainicial = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
                date = Convert.ToDateTime(filtros[1]);
                h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechafinal = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
            }

            Celda(tablam2, FuenteNegritaq, $"Del {fechainicial} al {fechafinal}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            if (filtros[3] != "0")
            {
                var contratosub = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(filtros[3]));
                var instit = ControlInstitucion.ObtenerPorId(contratosub.InstitucionId);
                Celda(tablam2, FuenteNegritaq, "Institución:" + instit.Nombre, 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            }
            CeldaVacio(tablam2, 1);
            documentoPdf.Add(tablam2);
            BaseColor colorrelleno = new BaseColor(192, 224, 255);
            BaseColor colorrelleno2 = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.BLACK);

            List<int> tipodslidaids = new List<int>();

            foreach (var item in relacionDetenidos)
            {
                tipodslidaids.Add(item.TiposalidaId);
            }


            foreach (int item in tipodslidaids.Distinct())
            {
                var detalle = relacionDetenidos.FirstOrDefault(x => x.TiposalidaId == item);
                PdfPTable tablachunk = new PdfPTable(1);
                tablachunk.WidthPercentage = 100;
                tablachunk.HorizontalAlignment = Element.ALIGN_LEFT;
                CeldaParaTablas(tablachunk, FuenteTituloTabla2, detalle.Tiposalida.ToString(), 1, 0, 1, colorrelleno2, espacio, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
                //CeldaConChunk(tablachunk, FuenteNegrita, FuenteNormal, "", detalle.Motivodetencion.ToString(), 0, 0, 0);
                CeldaVacio(tablachunk, 1);
                documentoPdf.Add(tablachunk);



                PdfPTable tablay = new PdfPTable(11);//Numero de columnas de la tabla            
                tablay.WidthPercentage = 100; //Porcentaje ancho
                tablay.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical
                tablay.SetWidths(new float[] { 10, 14, 6, 14, 8, 14, 10, 6, 6, 14, 12 });

                CeldaParaTablas(tablay, FuenteTituloTabla2, "No. remisión", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Detenido", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Edad", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Fecha registro", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Unidad", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Fecha calificación", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Solo arresto", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Multa", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Horas", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Fecha cumplimiento", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Motivo(s) calificación", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

                BaseColor colorgris = new BaseColor(246, 243, 242);
                BaseColor color = colorgris;
                int i = 1;

                foreach (var detalledet in relacionDetenidos.Where(x => x.TiposalidaId == item))
                {
                    if (i % 2 != 0)
                        color = colorgris;
                    else color = colorblanco;
                    var general = ControlGeneral.ObtenerPorDetenidoId(detalledet.DetenidoId);

                    int edad = 0;
                    if (general != null)
                    {
                        if (general.FechaNacimineto != DateTime.MinValue)
                            edad = CalcularEdad(general.FechaNacimineto);
                        else if (general.Edaddetenido != 0)
                            edad = general.Edaddetenido;
                        else
                        {
                            var infoDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(detalledet.DetenidoId);
                            var detenidoEvento = ControlDetenidoEvento.ObtenerPorId(infoDetencion.IdDetenido_Evento);

                            if (detenidoEvento != null)
                            {
                                edad = detenidoEvento.Edad;
                            }
                        }
                    }
                    string[] internod = new string[2] {
                            detalledet.DetenidoId.ToString(), "true"
                        };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(detalledet.DetenidoId).LastOrDefault();
                    edad = detalleDetencion.DetalledetencionEdad;
                    var edades = "0";
                    if(detalledet.Edad !=0)
                    {
                        edades = detalledet.Edad.ToString();
                    }
                    else
                    {
                        edades = edad.ToString();
                    }

                    i++;
                    var date = detalledet.Fecharegistro.ToShortDateString();
                    var h = detalledet.Fecharegistro.Hour.ToString();
                    if (detalledet.Fecharegistro.Hour < 10) h = "0" + h;
                    var m = detalledet.Fecharegistro.Minute.ToString();
                    if (detalledet.Fecharegistro.Minute < 10) m = "0" + m;
                    var s = detalledet.Fecharegistro.Second.ToString();
                    if (detalledet.Fecharegistro.Second < 10) s = "0" + s;
                    var fecha1 = date + " " + h + ":" + m + ":" + s;


                     date = detalledet.Fechacalificacion.ToShortDateString();
                     h = detalledet.Fechacalificacion.Hour.ToString();
                    if (detalledet.Fechacalificacion.Hour < 10) h = "0" + h;
                     m = detalledet.Fechacalificacion.Minute.ToString();
                    if (detalledet.Fechacalificacion.Minute < 10) m = "0" + m;
                     s = detalledet.Fechacalificacion.Second.ToString();
                    if (detalledet.Fechacalificacion.Second < 10) s = "0" + s;
                    var fecha2 = date + " " + h + ":" + m + ":" + s;

                    date = detalledet.Fechasalida.ToShortDateString();
                    h = detalledet.Fechasalida.Hour.ToString();
                    if (detalledet.Fechasalida.Hour < 10) h = "0" + h;
                    m = detalledet.Fechasalida.Minute.ToString();
                    if (detalledet.Fechasalida.Minute < 10) m = "0" + m;
                    s = detalledet.Fechasalida.Second.ToString();
                    if (detalledet.Fechasalida.Second < 10) s = "0" + s;
                    var fecha3 = date + " " + h + ":" + m + ":" + s;

                    CeldaParaTablas(tablay, FuenteNormal, detalledet.Remision, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, detalledet.Nombre, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, edades, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, fecha1, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, (!string.IsNullOrEmpty(detalledet.Unidad)) ? detalledet.Unidad.ToString() : "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, fecha2, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, detalledet.Soloarresto ? "Si" : "No", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, detalledet.Totalapagar.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, detalledet.Totalhoras.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, fecha3, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, (!string.IsNullOrEmpty(detalledet.Motivodetencion)) ? detalledet.Motivodetencion.ToString() : "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);

                    //CeldaParaTablas(tablay, FuenteNormal, detalledet.Detenido, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    //CeldaParaTablas(tablay, FuenteNormal, detalledet.Alias, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    //CeldaParaTablas(tablay, FuenteNormal, detalledet.Domicilio, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);


                }
                
                CeldaVacio(tablay, 11);
                documentoPdf.Add(tablay);


            }




            //Titulo "Datos del detenido"
            PdfPTable tablam = new PdfPTable(9);//Numero de columnas de la tabla            
            tablam.WidthPercentage = 100; //Porcentaje ancho
            tablam.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical





        }



        /// <summary>
        public static object[] ReporteRelaciondetenidoporCalificacion(List<Entity.RelacionDetenidoCalificacion> relacionDetenidos,string[] filtros,object[] data)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Relaciondetenidoscalificacion" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.Relaciondetenidosporcalificacion"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloRelaciondeteniodosporCalificacion(documentoPdf, FuenteTitulo, FuenteTituloTabla, relacionDetenidos, blau,filtros,data);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloRelaciondeteniodosporCalificacion(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.RelacionDetenidoCalificacion> relacionDetenidos, BaseColor blau,string[] filtros,object[] data)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;



            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));

            }



            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 3;
            cellWithRowspan.Border = 0;
            if (filtros[3] == "0" && filtros[5] != "0")
            {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
            }
            

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Relación de detenidos por calificación", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);


            documentoPdf.Add(tabla);
            Font FuenteNegritaq = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablam2 = new PdfPTable(1);
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;
            Celda(tablam2, FuenteNegritaq, "Municipio:" + data[0].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            string fechainicial = "";
            string fechafinal = "";

            if (!Convert.ToBoolean(filtros[4]))
            {
                fechainicial = Convert.ToDateTime(filtros[0]).ToShortDateString();
                fechafinal = Convert.ToDateTime(filtros[1]).ToShortDateString();

            }
            else
            {
                var date = Convert.ToDateTime(filtros[0]);
                var h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                var m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                var s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechainicial = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
                date = Convert.ToDateTime(filtros[1]);
                h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechafinal = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
            }


            Celda(tablam2, FuenteNegritaq, $"Del {fechainicial} al {fechafinal}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(tablam2, FuenteNegritaq, "Institución:" + data[1].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            
            CeldaVacio(tablam2, 1);
            documentoPdf.Add(tablam2);
            BaseColor colorrelleno = new BaseColor(192, 224, 255);
            BaseColor colorrelleno2 = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.BLACK);

            List<int> situacionids = new List<int>();

            foreach (var item in relacionDetenidos)
            {
                situacionids.Add(item.SituacionId);
            }


            foreach (int item in situacionids.Distinct())
            {
                var detalle = relacionDetenidos.FirstOrDefault(x => x.SituacionId == item);
                PdfPTable tablachunk = new PdfPTable(1);
                tablachunk.WidthPercentage = 100;
                tablachunk.HorizontalAlignment = Element.ALIGN_LEFT;
                CeldaParaTablas(tablachunk, FuenteTituloTabla2, detalle.Situacion.ToString(), 1, 0, 1, colorrelleno2, espacio, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
                //CeldaConChunk(tablachunk, FuenteNegrita, FuenteNormal, "", detalle.Motivodetencion.ToString(), 0, 0, 0);
                CeldaVacio(tablachunk, 1);
                documentoPdf.Add(tablachunk);



                PdfPTable tablay = new PdfPTable(9);//Numero de columnas de la tabla            
                tablay.WidthPercentage = 100; //Porcentaje ancho
                tablay.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

                CeldaParaTablas(tablay, FuenteTituloTabla2, "No. remisión", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Detenido", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Edad", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Unidad", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Fecha detención", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Fecha registro", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "horas", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Fecha cumplimiento", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Motivo(s) calificación", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

                BaseColor colorgris = new BaseColor(246, 243, 242);
                BaseColor color = colorgris;
                int i = 1;

                foreach (var detalledet in relacionDetenidos.Where(x => x.SituacionId == item))
                {
                    if (i % 2 != 0)
                        color = colorgris;
                    else color = colorblanco;

                    i++;
                    var date1 = detalledet.Fechadetencion.ToShortDateString();
                    var h= detalledet.Fechadetencion.Hour.ToString();
                    if (detalledet.Fechadetencion.Hour < 10) h = "0" + h;
                    var m = detalledet.Fechadetencion.Minute.ToString();
                    if (detalledet.Fechadetencion.Minute < 10) m = "0" + m;
                    var s = detalledet.Fechadetencion.Second.ToString();
                    if (detalledet.Fechadetencion.Second < 10) s = "0" + s;
                    var fecha1 = date1 + " " + h + ":" + m + ":" + s;

                    date1 = detalledet.Fecharegistro.ToShortDateString();
                     h = detalledet.Fecharegistro.Hour.ToString();
                    if (detalledet.Fecharegistro.Hour < 10) h = "0" + h;
                     m = detalledet.Fecharegistro.Minute.ToString();
                    if (detalledet.Fecharegistro.Minute < 10) m = "0" + m;
                     s = detalledet.Fecharegistro.Second.ToString();
                    if (detalledet.Fecharegistro.Second < 10) s = "0" + s;
                    var fecha2= date1 + " " + h + ":" + m + ":" + s;

                    date1 = detalledet.Fechasalida.ToShortDateString();
                    h = detalledet.Fechasalida.Hour.ToString();
                    if (detalledet.Fechasalida.Hour < 10) h = "0" + h;
                    m = detalledet.Fechasalida.Minute.ToString();
                    if (detalledet.Fechasalida.Minute < 10) m = "0" + m;
                    s = detalledet.Fechasalida.Second.ToString();
                    if (detalledet.Fechasalida.Second < 10) s = "0" + s;
                    var fecha3= date1 + " " + h + ":" + m + ":" + s;
                    
                    CeldaParaTablas(tablay, FuenteNormal, detalledet.Remision, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, detalledet.Nombre, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, detalledet.Edad.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, (!string.IsNullOrEmpty(detalledet.Unidad)) ? detalledet.Unidad.ToString() : "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, fecha1, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, fecha2, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, detalledet.Totalhoras.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, fecha3, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, (!string.IsNullOrEmpty(detalledet.Motivodetencion)) ? detalledet.Motivodetencion.ToString() : "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);

                    //CeldaParaTablas(tablay, FuenteNormal, detalledet.Detenido, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    //CeldaParaTablas(tablay, FuenteNormal, detalledet.Alias, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    //CeldaParaTablas(tablay, FuenteNormal, detalledet.Domicilio, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);


                }
                CeldaVacio(tablay, 9);
                documentoPdf.Add(tablay);


            }




            //Titulo "Datos del detenido"
            PdfPTable tablam = new PdfPTable(9);//Numero de columnas de la tabla            
            tablam.WidthPercentage = 100; //Porcentaje ancho
            tablam.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical





        }


        /// <summary>
        public static object[] ReporteDetalledetenidopormotivos(List<Entity.Detalledetenidopormotivo> detalledetenidopormotivos, string[] filtros, object[] data)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Detalledetenidospormotivos" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.Detalledetenidospormotivos"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloDetalledeteniodosporMotivos(documentoPdf, FuenteTitulo, FuenteTituloTabla, detalledetenidopormotivos, blau, filtros,data);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloDetalledeteniodosporMotivos(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.Detalledetenidopormotivo> detalledetenidopormotivos, BaseColor blau, string[] filtros, object[] data)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;



            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }
            PdfPCell cellWithRowspan = new PdfPCell();

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            if(logotipo== "~/Content/img/logo.png")
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
                image.ScaleAbsoluteHeight(88f);
                image.ScaleAbsoluteWidth(150f);
                cellWithRowspan = new PdfPCell(image);
            }
            else
            {
                try
                {
                    image = iTextSharp.text.Image.GetInstance(pathfoto);
                    cellWithRowspan = new PdfPCell(image,true);
                }
                catch (Exception)
                {
                    image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));
                    image.ScaleAbsoluteHeight(88f);
                    image.ScaleAbsoluteWidth(150f);
                    cellWithRowspan = new PdfPCell(image);
                }
            }

            

            

            
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 53;
            cellWithRowspan.Border = 0;
            if(filtros[3]=="0" && filtros[5] !="0")
            {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
            }

            

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Detalle de detenidos por motivo de calificación", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 2);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);


            documentoPdf.Add(tabla);

            Font FuenteNegritaq = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablam2 = new PdfPTable(1);//Numero de columnas de la tabla            
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;
            Celda(tablam2, FuenteNegritaq, "Municipio:" + data[0].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            string fechainicial = "";
            string fechafinal = "";

            if (!Convert.ToBoolean(filtros[4]))
            {
                fechainicial = Convert.ToDateTime(filtros[0]).ToShortDateString();
               
                fechafinal = Convert.ToDateTime(filtros[1]).ToShortDateString();

            }
            else 
            {
                var date = Convert.ToDateTime(filtros[0]);
                var h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                var m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                var s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechainicial = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
                date = Convert.ToDateTime(filtros[1]);
                h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechafinal = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
            }


            Celda(tablam2, FuenteNegritaq, $"Del {fechainicial} al {fechafinal}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            if (filtros[3] != "0")
            {
                var contratosub = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(filtros[3]));
                var instit = ControlInstitucion.ObtenerPorId(contratosub.InstitucionId);
                Celda(tablam2, FuenteNegritaq, "Institución:" + instit.Nombre, 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            }
            CeldaVacio(tablam2, 1);
            documentoPdf.Add(tablam2);

            BaseColor colorrelleno = new BaseColor(192, 224, 255);
            BaseColor colorrelleno2 = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.BLACK);

            List<int> MotivoDetencion = new List<int>();

            foreach (var item in detalledetenidopormotivos)
            {
                MotivoDetencion.Add(item.MotivodetencionId);
            }


            foreach (int item in MotivoDetencion.Distinct())
            {
                var detalle = detalledetenidopormotivos.FirstOrDefault(x => x.MotivodetencionId == item);
                PdfPTable tablachunk = new PdfPTable(1);
                tablachunk.WidthPercentage = 100;
                tablachunk.HorizontalAlignment = Element.ALIGN_LEFT;
                CeldaParaTablas(tablachunk, FuenteTituloTabla2, detalle.Motivodetencion.ToString(), 1, 0, 1, colorrelleno2, espacio, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
                //CeldaConChunk(tablachunk, FuenteNegrita, FuenteNormal, "", detalle.Motivodetencion.ToString(), 0, 0, 0);
                CeldaVacio(tablachunk, 1);
                documentoPdf.Add(tablachunk);


                int[] width = new int[] { 20,30,25,25 };

                PdfPTable tablay = new PdfPTable(4);//Numero de columnas de la tabla            
                tablay.WidthPercentage = 100; //Porcentaje ancho
                tablay.SetWidths(width);
                tablay.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

                CeldaParaTablas(tablay, FuenteTituloTabla2, "Remisión", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Detenido", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Alias", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tablay, FuenteTituloTabla2, "Domicilio", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                BaseColor colorgris = new BaseColor(246, 243, 242);
                BaseColor color = colorgris;
                int i = 1;

                foreach (var detalledet in detalledetenidopormotivos.Where(x => x.MotivodetencionId == item))
                {
                    if (i % 2 != 0)
                        color = colorgris;
                    else color = colorblanco;

                    i++;
                    CeldaParaTablas(tablay, FuenteNormal, detalledet.Remision, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, detalledet.Detenido, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, detalledet.Alias, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablay, FuenteNormal, detalledet.Domicilio, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);


                }
                CeldaVacio(tablay, 2);
                documentoPdf.Add(tablay);


            }




            //Titulo "Datos del detenido"
            PdfPTable tablam = new PdfPTable(9);//Numero de columnas de la tabla            
            tablam.WidthPercentage = 100; //Porcentaje ancho
            tablam.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical


            //Titulos de las columnas de las tablas
            CeldaParaTablas(tablam, FuenteTituloTabla, "Remisión", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla, "Detenido", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla, "Registrado por", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla, "Motivo(s) detención", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla, "Calificado por", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla, "Motivo(s) calificación", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla, "Folio llamada", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla, "Llamada recibida por", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla, "Motivo llamada", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);



        }

        public static object[] ReporteDetenidospormes(List<Entity.Comparativomotivos> comparativomotivos, string[] filtros,object[] data)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Detenidos_por_mes" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.Detenidospormes"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloDetenidosporMes(documentoPdf, FuenteTitulo, FuenteTituloTabla, comparativomotivos, blau, filtros,data);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloDetenidosporMes(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.Comparativomotivos> comparativomotivos, BaseColor blau, string[] filtros,object[] data)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;



            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));

            }



            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            if (filtros[3] == "0" && filtros[5] != "0")
            {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
            }
           

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Detenidos por mes", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 9f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);

            Font FuenteNegritaq = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            
            PdfPTable tablam2 = new PdfPTable(1);
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;
            string fechainicial = "";
            string fechafinal = "";
            if (!Convert.ToBoolean(filtros[4]))
            {
                fechainicial = Convert.ToDateTime(filtros[0]).ToShortDateString();
                fechafinal = Convert.ToDateTime(filtros[1]).ToShortDateString();

            }
            else
            {
                var date = Convert.ToDateTime(filtros[0]);
                var h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                var m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                var s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechainicial = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
                date = Convert.ToDateTime(filtros[1]);
                h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechafinal = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
            }
            Celda(tablam2, FuenteNegritaq, "Municipio:" + data[0].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(tablam2, FuenteNegritaq, $"Del {fechainicial} al {fechafinal}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            if (filtros[3] != "0")
            {
                var contratosub = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(filtros[3]));
                var instit = ControlInstitucion.ObtenerPorId(contratosub.InstitucionId);
                Celda(tablam2, FuenteNegritaq, "Institución:" + instit.Nombre, 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            }
            CeldaVacio(tablam2, 1); ;
            PdfPTable tablaEncabezado = new PdfPTable(15);
            tablaEncabezado.WidthPercentage = 100;
            tablaEncabezado.HorizontalAlignment = Element.ALIGN_RIGHT;
            tablaEncabezado.SetWidths(new float[] { 25, 8, 10, 10, 7, 8, 8, 8, 9, 14, 12, 14, 14, 7, 7 });

            //Titulo "Datos del detenido"
            PdfPTable tablam = new PdfPTable(15);//Numero de columnas de la tabla            
            tablam.WidthPercentage = 100; //Porcentaje ancho
            tablam.SetWidths(new float[] { 24, 8, 11, 10, 8, 8, 8, 8, 9, 15, 12, 14, 14, 7, 7 });
            tablam.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            BaseColor colorrelleno = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.BLACK);
            //Titulos de las columnas de las tablas
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Motivo detención", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Enero", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Febrero", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Marzo", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Abril", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Mayo", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Junio", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Julio", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Agosto", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Septiembre", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Octubre", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Noviembre", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Diciembre", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Total", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //renglones 
            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;
            int brincapagina = 19;
            //celdas, contenido de la tabla  
            List<string> motivos = new List<string>();
            List<string> remisiones = new List<string>();
            foreach (var item in comparativomotivos)
            {
                motivos.Add(item.Motivos);
                remisiones.Add(item.Remision);
            }
            int detenidosreales = remisiones.Distinct().Count();
            int totalenero = 0;
            int totalfebrero = 0;
            int totalmarzo = 0;
            int totalabril = 0;
            int totalmayo = 0;
            int totaljunio = 0;
            int totaljulio = 0;
            int totalagosto = 0;
            int totalseptiembre = 0;
            int totalOctubre = 0;
            int totalNoviembre = 0;
            int totalDiciembre = 0;


            foreach (var item in motivos.Distinct())
            {
                if (i % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                i++;
                brincapagina--;
                var enero = comparativomotivos.Where(x => x.Fecha.Month == 1 && x.Motivos == item).Count();
                totalenero += enero;
                var febrero = comparativomotivos.Where(x => x.Fecha.Month == 2 && x.Motivos == item).Count();
                totalfebrero += febrero;
                var marzo = comparativomotivos.Where(x => x.Fecha.Month == 3 && x.Motivos == item).Count();
                totalmarzo += marzo;
                var abril = comparativomotivos.Where(x => x.Fecha.Month == 4 && x.Motivos == item).Count();
                totalabril += abril;
                var mayo = comparativomotivos.Where(x => x.Fecha.Month == 5 && x.Motivos == item).Count();
                totalmayo += mayo;
                var junio = comparativomotivos.Where(x => x.Fecha.Month == 6 && x.Motivos == item).Count();
                totaljunio += junio;
                var julio = comparativomotivos.Where(x => x.Fecha.Month == 7 && x.Motivos == item).Count();
                totaljulio += julio;
                var Agosto = comparativomotivos.Where(x => x.Fecha.Month == 8 && x.Motivos == item).Count();
                totalagosto += Agosto;
                var septiembre = comparativomotivos.Where(x => x.Fecha.Month == 9 && x.Motivos == item).Count();
                totalseptiembre += septiembre;
                var octubre = comparativomotivos.Where(x => x.Fecha.Month == 10 && x.Motivos == item).Count();
                totalOctubre += octubre;
                var noviembre = comparativomotivos.Where(x => x.Fecha.Month == 11 && x.Motivos == item).Count();
                totalNoviembre += noviembre;
                var diciembre = comparativomotivos.Where(x => x.Fecha.Month == 12 && x.Motivos == item).Count();
                totalDiciembre += diciembre;



                int totallinea = enero + febrero + marzo + abril + mayo + junio + julio + Agosto + septiembre + octubre + noviembre + diciembre;
                CeldaParaTablas(tablam, FuenteNormal, item, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, enero.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, febrero.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, marzo.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, abril.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, mayo.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, junio.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, julio.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, Agosto.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, septiembre.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, octubre.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, noviembre.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, diciembre.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, totallinea.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                decimal porcentaje = Math.Round((Convert.ToDecimal(totallinea) * 100) / comparativomotivos.Count(), 2);
                CeldaParaTablas(tablam, FuenteNormal, porcentaje.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);

            }

            //Tabla Encabezado (donde aparecen los totales de los dias)
            CeldaVacio(tablaEncabezado, 1);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalenero.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalfebrero.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalmarzo.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalabril.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalmayo.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totaljunio.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totaljulio.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalagosto.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalseptiembre.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalOctubre.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalNoviembre.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalDiciembre.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, (totalenero + totalfebrero + totalmarzo + totalabril + totalmayo + totaljunio + totaljulio + totalagosto + totalseptiembre + totalOctubre + totalNoviembre + totalDiciembre).ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, "100.00 %", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaVacio(tablaEncabezado, 15);


            documentoPdf.Add(tabla);
            documentoPdf.Add(tablam2);
            documentoPdf.Add(tablaEncabezado);
            CeldaVacio(tablam, 15);
            documentoPdf.Add(tablam);
            PdfPTable tablares = new PdfPTable(15);//Numero de columnas de la tabla            
            tablares.WidthPercentage = 100; //Porcentaje ancho
            tablares.SetWidths(new float[] { 25, 8, 10, 10, 8, 7, 8, 8, 9, 14, 12, 14, 7, 7, 7 });
            CeldaVacio(tablares, 12);
            CeldaParaTablas(tablares, FuenteNegrita, "Detenidos reales: " + detenidosreales, 3, 0, 1, colorblanco, 0, 0, 0);
            documentoPdf.Add(tablares);
        }


        public static object[] ReporteResultadosQuimicospordiadelasemana(List<Entity.Comparativomotivos> comparativomotivos,List<Entity.ResultadosQuimicos>resultados,List<Entity.CertificadoQuimico> ListadoQuimicos, string[] filtros, object[] data)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Detenidosporresultadosquimicos" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.Detenidosporresultadosquimicos"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloResultadosQuimicospordiadelasemana(documentoPdf, FuenteTitulo, FuenteTituloTabla, comparativomotivos,resultados, ListadoQuimicos, blau, filtros,data);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloResultadosQuimicospordiadelasemana(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.Comparativomotivos> comparativomotivos,List<Entity.ResultadosQuimicos>resultados,List<Entity.CertificadoQuimico> ListadoQuimicos, BaseColor blau, string[] filtros, object[] data)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;



            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));

            }



            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            if (filtros[3] == "0" && filtros[5] != "0")
            {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
            }

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Detenidos por resultados químicos ", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 9f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);

            PdfPTable tablaEncabezado = new PdfPTable(10);
            tablaEncabezado.WidthPercentage = 100;
            tablaEncabezado.HorizontalAlignment = Element.ALIGN_RIGHT;
            tablaEncabezado.SetWidths(new float[] { 25, 9, 9, 9, 9, 9, 9, 9, 5, 7 });

            //Titulo "Datos del detenido"
            PdfPTable tablam = new PdfPTable(10);//Numero de columnas de la tabla            
            tablam.WidthPercentage = 100; //Porcentaje ancho
            tablam.SetWidths(new float[] { 25, 9, 9, 9, 9, 9, 9, 9, 5, 7 });
            tablam.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            //PdfPTable tablaDelegacionUsuario = new PdfPTable(1);
            //float[] width = new float[] { 220 };
            //tablaDelegacionUsuario.SetWidths(width);
            //tablaDelegacionUsuario.DefaultCell.Border = Rectangle.NO_BORDER;
            //CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Municipio:" + data[0].ToString(), "", 0, 0, 0);
            //CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, $"Del {Convert.ToDateTime(filtros[0])} al {Convert.ToDateTime(filtros[1])}", "", 0, 0, 0);
            //if( filtros[3] !="0")
            //{
            //    var contratosub = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(filtros[3]));
            //    var instit = ControlInstitucion.ObtenerPorId(contratosub.InstitucionId);
            //    CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Insitución:" + instit.Nombre, "", 0, 0, 0);

            //}
            //CeldaVacio(tablaDelegacionUsuario, 2);
            Font FuenteNegritaq = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablam2 = new PdfPTable(1);
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;
            string fechainicial = "";
            string fechafinal = "";
            if (!Convert.ToBoolean(filtros[4]))
            {
                fechainicial = Convert.ToDateTime(filtros[0]).ToShortDateString();
                fechafinal = Convert.ToDateTime(filtros[1]).ToShortDateString();

            }
            else
            {
                var date = Convert.ToDateTime(filtros[0]);
                var h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                var m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                var s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechainicial = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
                date = Convert.ToDateTime(filtros[1]);
                h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechafinal = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
            }

            Celda(tablam2, FuenteNegritaq, "Municipio:" + data[0].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(tablam2, FuenteNegritaq, $"Del {fechainicial} al {fechafinal}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            if (filtros[3] != "0")
            {
                var contratosub = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(filtros[3]));
                var instit = ControlInstitucion.ObtenerPorId(contratosub.InstitucionId);
                Celda(tablam2, FuenteNegritaq, "Institución:" + instit.Nombre, 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            }
            CeldaVacio(tablam2, 1);
            BaseColor colorrelleno = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            //Titulos de las columnas de las tablas
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Resultado", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Domingo", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Lunes", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Martes", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Miercoles", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Jueves", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Viernes", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Sabado", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Total", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //renglones 
            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;
            int brincapagina = 19;
            //celdas, contenido de la tabla  
            List<string> motivos = new List<string>();
            List<int> remisiones = new List<int>();
            foreach (var item in comparativomotivos)
            {
                motivos.Add(item.Motivos);
                
            }
            List<string> Resquimicos = new List<string>();

            foreach (var itemx in resultados)
            {
                Resquimicos.Add(itemx.Resultado);
            }

          

            int detenidosreales = remisiones.Distinct().Count();
            int totaldomingo = 0;
            int totallunes = 0;
            int totalmartes = 0;
            int totalmiercoles = 0;
            int totaljueves = 0;
            int totalviernes = 0;
            int totalsabado = 0;

            //var ListadoQuimicos = ControlCertificadoQuimico.ObTenerTodo();
            //ListadoQuimicos = ListadoQuimicos.Where(x => x.Fecha_proceso >= Convert.ToDateTime(filtros[0]) && x.Fecha_proceso <= Convert.ToDateTime(filtros[1])).ToList();
           int  Totalresultados2 =0;
            foreach (var item in Resquimicos.Distinct())
            {
                switch (item)
                {
                    case "Etanol":
                        Totalresultados2 += ListadoQuimicos.Where(x => x.EtanolId == 2).Count();
                        foreach (var itemr in ListadoQuimicos.Where(x => x.EtanolId == 2))
                        {
                            remisiones.Add(itemr.DetenidoId);
                        }
                        break;
                    case "Benzodiazepina":
                        Totalresultados2 += ListadoQuimicos.Where(x => x.BenzodiapinaId == 2).Count();
                        foreach (var itemr in ListadoQuimicos.Where(x => x.BenzodiapinaId == 2))
                        {
                            remisiones.Add(itemr.DetenidoId);
                        }
                        break;
                    case "Anfetamina":
                        Totalresultados2 += ListadoQuimicos.Where(x => x.AnfetaminaId == 2).Count();
                        foreach (var itemr in ListadoQuimicos.Where(x => x.AnfetaminaId == 2))
                        {
                            remisiones.Add(itemr.DetenidoId);
                        }
                        break;
                    case "Cannabis":
                        Totalresultados2 += ListadoQuimicos.Where(x => x.CannabisId == 2).Count();
                        foreach (var itemr in ListadoQuimicos.Where(x => x.CannabisId == 2))
                        {
                            remisiones.Add(itemr.DetenidoId);
                        }
                        break;
                    case "Cocaína":
                        Totalresultados2 += ListadoQuimicos.Where(x => x.CocainaId == 2).Count();
                        foreach (var itemr in ListadoQuimicos.Where(x => x.CocainaId == 2))
                        {
                            remisiones.Add(itemr.DetenidoId);
                        }
                        break;
                    case "Éxtasis":
                        Totalresultados2 += ListadoQuimicos.Where(x => x.ExtasisId == 2).Count();
                        foreach (var itemr in ListadoQuimicos.Where(x => x.ExtasisId == 2))
                        {
                            remisiones.Add(itemr.DetenidoId);
                        }
                        break;
                    default:
                        break;
                }
            }
            detenidosreales = remisiones.Distinct().Count();
                foreach (var item in Resquimicos.Distinct())
            {
                if (i % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                i++;
                brincapagina--;
                var domingo = obtenerpordiaResultadoQuimico(ListadoQuimicos, DayOfWeek.Sunday, item).Count;
                totaldomingo += domingo;
                var lunes = obtenerpordiaResultadoQuimico(ListadoQuimicos, DayOfWeek.Monday, item).Count;
                totallunes += lunes;
                var martes = obtenerpordiaResultadoQuimico(ListadoQuimicos, DayOfWeek.Tuesday, item).Count;
                totalmartes += martes;
                var miercoles = obtenerpordiaResultadoQuimico(ListadoQuimicos, DayOfWeek.Wednesday, item).Count;
                totalmiercoles += miercoles;
                var jueve = obtenerpordiaResultadoQuimico(ListadoQuimicos, DayOfWeek.Thursday, item).Count;
                totaljueves += jueve;
                var viernes = obtenerpordiaResultadoQuimico(ListadoQuimicos, DayOfWeek.Friday, item).Count;
                totalviernes += viernes;
                var sabado = obtenerpordiaResultadoQuimico(ListadoQuimicos, DayOfWeek.Saturday, item).Count;
                totalsabado += sabado;
                int totallinea = domingo + lunes + martes + miercoles + jueve + viernes + sabado;
                CeldaParaTablas(tablam, FuenteNormal, item, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, domingo.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, lunes.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, martes.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, miercoles.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, jueve.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, viernes.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, sabado.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, totallinea.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                decimal porcentaje = Math.Round((Convert.ToDecimal(totallinea) * 100) / Totalresultados2, 2);
                CeldaParaTablas(tablam, FuenteNormal, porcentaje.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);

            }

            //Tabla Encabezado (donde aparecen los totales de los dias)
            CeldaVacio(tablaEncabezado, 1);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totaldomingo.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totallunes.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalmartes.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalmiercoles.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totaljueves.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalviernes.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalsabado.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, (totaldomingo + totallunes + totalmartes + totalmiercoles + totaljueves + totalviernes + totalsabado).ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, "100.00 %", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaVacio(tablaEncabezado, 10);


            documentoPdf.Add(tabla);
            documentoPdf.Add(tablam2);
            documentoPdf.Add(tablaEncabezado);
            CeldaVacio(tablam, 10);
            documentoPdf.Add(tablam);

            PdfPTable tablares = new PdfPTable(15);//Numero de columnas de la tabla            
            tablares.WidthPercentage = 100; //Porcentaje ancho
            tablares.SetWidths(new float[] { 25, 8, 10, 10, 8, 7, 8, 8, 9, 14, 12, 14, 7, 7, 7 });
            CeldaVacio(tablares, 12);
            CeldaParaTablas(tablares, FuenteNegritaq, "Total: " + Totalresultados2, 3, 0, 1, colorblanco, 0, Element.ALIGN_RIGHT, 0);
            CeldaVacio(tablares, 12);
            CeldaParaTablas(tablares, FuenteNegritaq, "Detenidos reales: " + detenidosreales, 3, 0, 1, colorblanco, 0, Element.ALIGN_RIGHT, 0);
            documentoPdf.Add(tablares);

        }



        /// <returns></returns>
        public static object[] ReporteDetenidospordiadelasemana(List<Entity.Comparativomotivos> comparativomotivos, string[] filtros, object[] data)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Detenidos_por_dia_de_la_semana" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.Detenidospordiadelasemana"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloDetenidospordiadelasemana(documentoPdf, FuenteTitulo, FuenteTituloTabla, comparativomotivos, blau, filtros,data);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloDetenidospordiadelasemana(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.Comparativomotivos> comparativomotivos, BaseColor blau, string[] filtros,object[] data)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;



            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));

            }



            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            if(filtros[3]=="0" && filtros[5]!="0")
            {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
            }
            

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Detenidos por día de la semana", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 9f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);

            PdfPTable tablaEncabezado = new PdfPTable(10);
            tablaEncabezado.WidthPercentage = 100;
            tablaEncabezado.HorizontalAlignment = Element.ALIGN_RIGHT;
            tablaEncabezado.SetWidths(new float[] { 25, 9, 9, 9, 9, 9, 9, 9, 5, 7 });

            //Titulo "Datos del detenido"
            PdfPTable tablam = new PdfPTable(10);//Numero de columnas de la tabla            
            tablam.WidthPercentage = 100; //Porcentaje ancho
            tablam.SetWidths(new float[] { 25, 9, 9, 9, 9, 9, 9, 9, 5, 7 });
            tablam.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            //PdfPTable tablaDelegacionUsuario = new PdfPTable(1);
            //float[] width = new float[] { 220 };
            //tablaDelegacionUsuario.SetWidths(width);
            //tablaDelegacionUsuario.DefaultCell.Border = Rectangle.NO_BORDER;
            //CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Municipio:" + data[0].ToString(), "", 0, 0, 0);
            //CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, $"Del {Convert.ToDateTime(filtros[0])} al {Convert.ToDateTime(filtros[1])}", "", 0, 0, 0);
            //if( filtros[3] !="0")
            //{
            //    var contratosub = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(filtros[3]));
            //    var instit = ControlInstitucion.ObtenerPorId(contratosub.InstitucionId);
            //    CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Insitución:" + instit.Nombre, "", 0, 0, 0);

            //}
            //CeldaVacio(tablaDelegacionUsuario, 2);
            Font FuenteNegritaq = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablam2 = new PdfPTable(1);
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;
            string fechainicial = "";
            string fechafinal = "";
            if (!Convert.ToBoolean(filtros[4]))
            {
                fechainicial = Convert.ToDateTime(filtros[0]).ToShortDateString();
                fechafinal = Convert.ToDateTime(filtros[1]).ToShortDateString();

            }
            else
            {
                var date = Convert.ToDateTime(filtros[0]);
                var h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                var m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                var s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechainicial = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
                date = Convert.ToDateTime(filtros[1]);
                h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechafinal = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
            }

            Celda(tablam2, FuenteNegritaq, "Municipio:" + data[0].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(tablam2, FuenteNegritaq, $"Del {fechainicial} al {fechafinal}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            if (filtros[3] != "0")
            {
                var contratosub = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(filtros[3]));
                var instit = ControlInstitucion.ObtenerPorId(contratosub.InstitucionId);
                Celda(tablam2, FuenteNegritaq, "Institución:" + instit.Nombre, 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            }
            CeldaVacio(tablam2, 1);
            BaseColor colorrelleno = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.BLACK);
            //Titulos de las columnas de las tablas
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Motivo detención", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Domingo", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Lunes", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Martes", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Miercoles", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Jueves", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Viernes", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Sabado", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Total", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //renglones 
            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;
            int brincapagina = 19;
            //celdas, contenido de la tabla  
            List<string> motivos = new List<string>();
            List<string> remisiones = new List<string>();
            foreach (var item in comparativomotivos)
            {
                motivos.Add(item.Motivos);
                remisiones.Add(item.Remision);
            }
            int detenidosreales = remisiones.Distinct().Count();
            int totaldomingo = 0;
            int totallunes = 0;
            int totalmartes = 0;
            int totalmiercoles = 0;
            int totaljueves = 0;
            int totalviernes = 0;
            int totalsabado = 0;

            foreach (var item in motivos.Distinct())
            {
                if (i % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                i++;
                brincapagina--;
                var domingo = obtenerpordia(comparativomotivos, DayOfWeek.Sunday, item).Count;
                totaldomingo += domingo;
                var lunes = obtenerpordia(comparativomotivos, DayOfWeek.Monday, item).Count;
                totallunes += lunes;
                var martes = obtenerpordia(comparativomotivos, DayOfWeek.Tuesday, item).Count;
                totalmartes += martes;
                var miercoles = obtenerpordia(comparativomotivos, DayOfWeek.Wednesday, item).Count;
                totalmiercoles += miercoles;
                var jueve = obtenerpordia(comparativomotivos, DayOfWeek.Thursday, item).Count;
                totaljueves += jueve;
                var viernes = obtenerpordia(comparativomotivos, DayOfWeek.Friday, item).Count;
                totalviernes += viernes;
                var sabado = obtenerpordia(comparativomotivos, DayOfWeek.Saturday, item).Count;
                totalsabado += sabado;
                int totallinea = domingo + lunes + martes + miercoles + jueve + viernes + sabado;
                CeldaParaTablas(tablam, FuenteNormal, item, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, domingo.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, lunes.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, martes.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, miercoles.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, jueve.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, viernes.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, sabado.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, totallinea.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                decimal porcentaje = Math.Round((Convert.ToDecimal(totallinea) * 100) / comparativomotivos.Count(), 2);
                CeldaParaTablas(tablam, FuenteNormal, porcentaje.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);

            }

            //Tabla Encabezado (donde aparecen los totales de los dias)
            CeldaVacio(tablaEncabezado, 1);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totaldomingo.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totallunes.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalmartes.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalmiercoles.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totaljueves.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalviernes.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, totalsabado.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, (totaldomingo + totallunes + totalmartes + totalmiercoles + totaljueves + totalviernes + totalsabado).ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, "100.00 %", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaVacio(tablaEncabezado, 10);


            documentoPdf.Add(tabla);
            documentoPdf.Add(tablam2);
            documentoPdf.Add(tablaEncabezado);
            CeldaVacio(tablam, 10);
            documentoPdf.Add(tablam);

            PdfPTable tablares = new PdfPTable(15);//Numero de columnas de la tabla            
            tablares.WidthPercentage = 100; //Porcentaje ancho
            tablares.SetWidths(new float[] { 25, 8, 10, 10, 8, 7, 8, 8, 9, 14, 12, 14, 7, 7, 7 });
            CeldaVacio(tablares, 12);
            CeldaParaTablas(tablares, FuenteNegrita, "Detenidos reales: " + detenidosreales, 3, 0, 1, colorblanco, 0, 0, 0);
            documentoPdf.Add(tablares);

        }


        static List<Entity.CertificadoQuimico> obtenerpordiaResultadoQuimico(List<Entity.CertificadoQuimico> detenciones, DayOfWeek dia, string resultado)
        {
            switch (resultado)
            {
                case "Etanol":
                    detenciones = detenciones.Where(x => x.Fecha_proceso.DayOfWeek == dia  && x.EtanolId==2).ToList();
                    break;
                case "Benzodiazepina":
                    detenciones = detenciones.Where(x => x.Fecha_proceso.DayOfWeek == dia  && x.BenzodiapinaId ==2).ToList();
                    break;
                case "Anfetamina":
                    detenciones = detenciones.Where(x => x.Fecha_proceso.DayOfWeek == dia && x.AnfetaminaId == 2).ToList();
                    break;
                case "Cannabis":
                    detenciones = detenciones.Where(x => x.Fecha_proceso.DayOfWeek == dia && x.CannabisId == 2).ToList();
                    break;
                case "Cocaína":
                    detenciones = detenciones.Where(x => x.Fecha_proceso.DayOfWeek == dia  && x.CocainaId == 2).ToList();
                    break;
                case "Éxtasis":
                    detenciones = detenciones.Where(x => x.Fecha_proceso.DayOfWeek == dia && x.ExtasisId == 2).ToList();
                    break;
                default:
                    break;
            }


            



            return detenciones;
        }

        static List<Entity.Comparativomotivos> obtenerpordia(List<Entity.Comparativomotivos> detenciones, DayOfWeek dia, string motivodetencion)
        {
            detenciones = detenciones.Where(x => x.Fecha.DayOfWeek == dia && x.Motivos == motivodetencion).ToList();



            return detenciones;
        }


        /// </summary>
        /// <param name="comparativomotivos"></param>
        /// <returns></returns>
        public static object[] ReporteComparativoMoivos(List<Entity.Comparativomotivos> comparativomotivos, string[] filtros,object[] data)
        {
            int DetalleDetencionId = 0;
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Comparativo_motivos" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.Comparativodemotivos"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);


                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                //Entity.ReporteTrabajoSocial reporte = ControlReporteTrabajoSocial.GetReporteTrabajoSocialByDetalleDetencionId(DetalleDetencionId);
                TituloComparativoMotivos(documentoPdf, FuenteTitulo, FuenteTituloTabla, comparativomotivos, blau, filtros,data);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloComparativoMotivos(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, List<Entity.Comparativomotivos> comparativomotivos, BaseColor blau, string[] fitros,object[] data)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;



            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));

            }
            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            if(fitros[5] !="0" && fitros[3]=="0")
            {
                CeldaVacio(tabla, 1);
            }
            else
            {
                tabla.AddCell(cellWithRowspan);
            }
            

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Comparativo de motivos", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 1);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 9f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.BLACK);

            float[] w3 = new float[] { 15, 30, 25, 30, 25, 20, 15, 25, 25 };
            //Titulo "Datos del detenido"
            PdfPTable tablam = new PdfPTable(9);//Numero de columnas de la tabla            
            tablam.WidthPercentage = 100; //Porcentaje ancho
            tablam.SetWidths(w3);
            tablam.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            BaseColor colorrelleno = new BaseColor(128, 192, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);
            //Titulos de las columnas de las tablas
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Remisión", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Detenido", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Registrado por", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Motivo(s) detención", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Calificado por", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Situación del detenido", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Folio llamada", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Llamada recibida por", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablam, FuenteTituloTabla2, "Motivo llamada", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //renglones 
            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;
            int brincapagina = 19;
            //celdas, contenido de la tabla  
            foreach (var item in comparativomotivos)
            {
                if (i % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                i++;
                brincapagina--;
                
                var motivo = item.Motivos != null ? (item.Motivos.Length > 100 ? item.Motivos.Substring(0, 80) + "..." : item.Motivos) : "";
                CeldaParaTablas(tablam, FuenteNormal, item.Remision, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, item.Detenido.Trim(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT) ;
                CeldaParaTablas(tablam, FuenteNormal, item.Registro.ToString().Trim(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, motivo.Trim(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, item.Califico.Trim(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, item.Motivocalificacion.Trim(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, item.Foliollamada, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, item.Registrollamada, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablam, FuenteNormal, item.Motivollamada, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                if (brincapagina == 0)
                {
                    CeldaParaTablas(tablam, FuenteNormal, " ", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablam, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablam, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablam, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablam, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablam, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablam, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablam, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablam, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    //
                    CeldaParaTablas(tablam, FuenteNormal, " ", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablam, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablam, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablam, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablam, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablam, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablam, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablam, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tablam, FuenteNormal, "", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);


                    brincapagina = 24;
                }
            }
            Font FuenteNegritaq = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablam2 = new PdfPTable(1);//Numero de columnas de la tabla            
            tablam2.WidthPercentage = 100; //Porcentaje ancho
            tablam2.HorizontalAlignment = Element.ALIGN_LEFT;
            Celda(tablam2, FuenteNegritaq, "Municipio:" + data[0].ToString(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            string fechainicial = "";
            string fechafinal = "";

            if (!Convert.ToBoolean(fitros[4]))
            {
                fechainicial = Convert.ToDateTime(fitros[0]).ToShortDateString();
                fechafinal = Convert.ToDateTime(fitros[1]).ToShortDateString();

            }
            else
            {
                var date = Convert.ToDateTime(fitros[0]);
                var h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                var m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                var s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechainicial = date.ToShortDateString()+" "+h+":"+m+":" +s;
                date = Convert.ToDateTime(fitros[1]);
                 h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                 m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                 s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechafinal = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
            }

            Celda(tablam2, FuenteNegritaq, $"Del {fechainicial} al {fechafinal}", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            if (fitros[3] != "0")
            {
                var contratosub = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(fitros[3]));
                var instit = ControlInstitucion.ObtenerPorId(contratosub.InstitucionId);
                Celda(tablam2, FuenteNegritaq, "Institución:" + instit.Nombre, 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            }
            CeldaVacio(tablam2, 1);
            documentoPdf.Add(tabla);
            documentoPdf.Add(tablam2);
            documentoPdf.Add(tablam);

        }


        private static void TituloDiagnostico(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, Entity.ReporteTrabajoSocial reporteTrabajoSocial, int DetalleDetencionId, BaseColor blau)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;
            var ei = ControlDetalleDetencion.ObtenerPorDetenidoId(DetalleDetencionId);
            foreach (var val in ei)
            {
                centroId = val.CentroId;
            }

            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));

            }



            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            tabla.AddCell(cellWithRowspan);

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Diagnóstico de la persona", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);
            PdfPTable tablaDelegacionUsuario = new PdfPTable(1);
            float[] width = new float[] { 220 };
            tablaDelegacionUsuario.SetWidths(width);
            tablaDelegacionUsuario.DefaultCell.Border = Rectangle.NO_BORDER;
            PdfPCell celda;
            Chunk chunkTituloDelegacion = new Chunk("Delegación: ", FuenteNegrita);
            Chunk chunkNombreDelegacion = new Chunk(reporteTrabajoSocial.Delegacion != null ? reporteTrabajoSocial.Delegacion.ToString() : "", FuenteNormal);
            Paragraph parrafoDelegacion = new Paragraph();
            parrafoDelegacion.Add(chunkTituloDelegacion);
            parrafoDelegacion.Add(chunkNombreDelegacion);
            celda = new PdfPCell(parrafoDelegacion);
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.Border = 0;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            tablaDelegacionUsuario.AddCell(celda);



            CeldaVacio(tablaDelegacionUsuario, 1);

            //Titulo "Datos del detenido"
            PdfPTable tabla1 = new PdfPTable(1);
            tabla1.WidthPercentage = 100;
            PdfPCell tituloTabla1;
            tituloTabla1 = new PdfPCell(new Phrase("Datos de la persona ", FuenteTituloTabla));
            tituloTabla1.HorizontalAlignment = Element.ALIGN_CENTER;
            tituloTabla1.Border = 0;
            tituloTabla1.BackgroundColor = blau;
            tituloTabla1.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla1.AddCell(tituloTabla1);

            CeldaVacio(tabla1, 1);

            //Tabla datos del detenido
            PdfPTable tabla2 = new PdfPTable(4);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Nombre: ", reporteTrabajoSocial.Nombre.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Alias: ", reporteTrabajoSocial.Alias.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Escolaridad: ", reporteTrabajoSocial.Escolaridad.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Edad: ", reporteTrabajoSocial.Edad.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Nacimiento: ", (reporteTrabajoSocial.FechaNacimiento != null) ? reporteTrabajoSocial.FechaNacimiento != DateTime.MinValue ? reporteTrabajoSocial.FechaNacimiento.ToString("dd/MM/yyyy") : "Sin dato" : "Sin dato", 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Estado civil: ", reporteTrabajoSocial.EstadoCivil.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Domicilio: ", reporteTrabajoSocial.Domicilio.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Teléfono: ", reporteTrabajoSocial.Telefono.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Colonia: ", reporteTrabajoSocial.Colonia.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Ciudad: ", reporteTrabajoSocial.Municipio.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Estado: ", reporteTrabajoSocial.Estado.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Nacionalidad: ", reporteTrabajoSocial.Nacionalidad.ToString(), 0, 0, 0);

            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "", "", 0, 0, 0);

            CeldaVacio(tabla2, 3);

            //Titulo "Remision"
            PdfPTable tabla3 = new PdfPTable(1);
            tabla3.WidthPercentage = 100;
            PdfPCell tituloTabla2;
            tituloTabla2 = new PdfPCell(new Phrase("Remisión", FuenteTituloTabla));
            tituloTabla2.HorizontalAlignment = Element.ALIGN_CENTER;
            tituloTabla2.Border = 0;
            tituloTabla2.BackgroundColor = blau;
            tituloTabla2.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla3.AddCell(tituloTabla2);

            CeldaVacio(tabla3, 1);

            //Tabla datos de la remision
            PdfPTable tabla4 = new PdfPTable(2);
            tabla4.WidthPercentage = 100;
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "No. remisión: ", reporteTrabajoSocial.Expediente != null ? reporteTrabajoSocial.Expediente.ToString() : "", 0, 0, 0);
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Fecha remisión ", reporteTrabajoSocial.Fecha != DateTime.MinValue ? reporteTrabajoSocial.Fecha.ToString() : "", 0, 0, 0);
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Motivo remisión: ", reporteTrabajoSocial.Motivo != null ? reporteTrabajoSocial.Motivo.ToString() : "", 0, 0, 0);
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Lugar de detención: ", reporteTrabajoSocial.LugarDetencion != null ? reporteTrabajoSocial.LugarDetencion.ToString() : "", 0, 0, 0);
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Colonia: ", reporteTrabajoSocial.Colonia != null ? reporteTrabajoSocial.Colonia.ToString() : "", 0, 0, 0);
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "No. evento: ", reporteTrabajoSocial.Folio != null ? reporteTrabajoSocial.Folio.ToString() : "", 0, 0, 0);

            CeldaVacio(tabla4, 3);


            //Titulo "Situacion"
            PdfPTable tabla7 = new PdfPTable(1);
            tabla7.WidthPercentage = 100;
            PdfPCell tituloTabla4;
            tituloTabla4 = new PdfPCell(new Phrase("Expediente", FuenteTituloTabla));
            tituloTabla4.HorizontalAlignment = Element.ALIGN_CENTER;
            tituloTabla4.Border = 0;
            tituloTabla4.BackgroundColor = blau;
            tituloTabla4.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla7.AddCell(tituloTabla4);

            CeldaVacio(tabla7, 1);

            //Datos situacion
            PdfPTable tabla8 = new PdfPTable(4);
            tabla8.WidthPercentage = 100;
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Motivo:", reporteTrabajoSocial.MotivoRehabilitacion.ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Adicción: ", reporteTrabajoSocial.Adiccion.ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Religión: ", reporteTrabajoSocial.Religion.ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Pandilla: ", reporteTrabajoSocial.Pandilla.ToString(), 0, 0, 0);

            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Nombre madre:", reporteTrabajoSocial.NombreMadre.ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Celular madre:", reporteTrabajoSocial.CelularMadre.ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Domicilio madre:", reporteTrabajoSocial.DomicilioMadre.ToString(), 0, 0, 0);

            CeldaVacio(tabla8, 1);

            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Nombre padre:", reporteTrabajoSocial.NombrePadre.ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Celular padre:", reporteTrabajoSocial.CelularPadre.ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Domicilio padre:", reporteTrabajoSocial.DomicilioPadre.ToString(), 0, 0, 0);

            CeldaVacio(tabla8, 1);

            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Escolaridad:", reporteTrabajoSocial.EscolaridadTS.ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Notificado:", reporteTrabajoSocial.Notificado.ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Celular notificado:", reporteTrabajoSocial.CelularNotificado.ToString(), 0, 0, 0);

            CeldaVacio(tabla8, 1);

            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Tutor:", reporteTrabajoSocial.Tutor.ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Celular tutor:", reporteTrabajoSocial.Celulartutor.ToString(), 0, 0, 0);

            CeldaVacio(tabla8, 1);

            //Explicacion situacion
            PdfPTable tabla9 = new PdfPTable(1);
            tabla9.WidthPercentage = 100;
            CeldaConChunk(tabla9, FuenteNegrita, FuenteNormal, "Cuadro patológico: ", reporteTrabajoSocial.CuadroPatologico.ToString(), 0, 0, 0);
            CeldaConChunk(tabla9, FuenteNegrita, FuenteNormal, "Observación: ", reporteTrabajoSocial.Observacion.ToString(), 0, 0, 0);

            CeldaVacio(tabla9, 1);

            tablaDelegacionUsuario.WidthPercentage = 100;
            tabla2.WidthPercentage = 100;
            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaDelegacionUsuario);
            documentoPdf.Add(tabla1);
            documentoPdf.Add(tabla2);
            documentoPdf.Add(tabla3);
            documentoPdf.Add(tabla4);

            documentoPdf.Add(tabla7);
            documentoPdf.Add(tabla8);
            documentoPdf.Add(tabla9);
            tabla.DeleteBodyRows();
        }

        //private static void InformacionReciboCorteDeCaja(Document documentoPdf, Font FuenteNormal, Font FuenteTituloTabla, float espacio, List<Entity.EgresoIngreso> ingresos, List<Entity.EgresoIngreso> egresos, List<Entity.Cancelacion> cancelaciones, decimal saldoInicial, string usuario)
        //{
        //    BaseColor blau = new BaseColor(0, 61, 122);
        //    string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
        //    BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
        //    Font FuenteNormalAux = new Font(fuentecalibri, 12f, Font.NORMAL);
        //    Font FuenteNegritaAux = new Font(fuentecalibri, 12f, Font.BOLD, blau);

        //    PdfPTable tabla = new PdfPTable(9);//Numero de columnas de la tabla            
        //    tabla.WidthPercentage = 100; //Porcentaje ancho
        //    tabla.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical



        //    BaseColor colorrelleno = new BaseColor(0, 61, 122);
        //    BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);

        //    //Titulos de las columnas de las tablas
        //    CeldaParaTablas(tabla, FuenteTituloTabla, "Movimiento", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
        //    CeldaParaTablas(tabla, FuenteTituloTabla, "Concepto", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
        //    CeldaParaTablas(tabla, FuenteTituloTabla, "Folio", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
        //    CeldaParaTablas(tabla, FuenteTituloTabla, "Fecha", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
        //    CeldaParaTablas(tabla, FuenteTituloTabla, "Persona que paga", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
        //    CeldaParaTablas(tabla, FuenteTituloTabla, "Subtotal", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

        //    decimal totalIngresos = 0;
        //    decimal totalEgresos = 0;

        //    BaseColor colorgris = new BaseColor(246, 243, 242);
        //    BaseColor color = colorgris;
        //    int i = 1;
        //    //celdas, contenido de la tabla  
        //    foreach (var item in ingresos)
        //    {
        //        if (i % 2 != 0)
        //            color = colorgris;
        //        else color = colorblanco;

        //        i++;

        //        CeldaParaTablas(tabla, FuenteNormal, "Ingreso", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
        //        CeldaParaTablas(tabla, FuenteNormal, item.Concepto, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
        //        CeldaParaTablas(tabla, FuenteNormal, item.Folio.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
        //        CeldaParaTablas(tabla, FuenteNormal, item.Fecha.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
        //        CeldaParaTablas(tabla, FuenteNormal, item.PersonaQuePaga, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
        //        CeldaParaTablas(tabla, FuenteNormal, "$" + item.Total.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
        //        totalIngresos += item.Total;
        //    }

        //    foreach (var item in egresos)
        //    {
        //        if (i % 2 != 0)
        //            color = colorgris;
        //        else color = colorblanco;

        //        i++;

        //        CeldaParaTablas(tabla, FuenteNormal, "Egreso", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
        //        CeldaParaTablas(tabla, FuenteNormal, item.Concepto, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
        //        CeldaParaTablas(tabla, FuenteNormal, item.Folio.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
        //        CeldaParaTablas(tabla, FuenteNormal, item.Fecha.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
        //        CeldaParaTablas(tabla, FuenteNormal, item.PersonaQuePaga, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
        //        CeldaParaTablas(tabla, FuenteNormal, "$" + item.Total.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
        //        totalEgresos += item.Total;
        //    }

        //    CeldaVacio(tabla, 1);


        //    decimal ingresoscancelados = 0;
        //    decimal egresoscancelados = 0;
        //    foreach (var item in cancelaciones)
        //    {
        //        if (item.Tipo == "Ingreso")
        //        {
        //            ingresoscancelados += item.Total;
        //        }
        //        else if (item.Tipo == "Egreso")
        //        {
        //            egresoscancelados += item.Total;

        //        }
        //    }

        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Saldo inicial ", " $" + saldoInicial.ToString(), 0, 0, 0);

        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Ingresos ", " $" + totalIngresos.ToString(), 0, 0, 0);

        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Ingresos cancelados ", " $" + ingresoscancelados.ToString(), 0, 0, 0);

        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Egresos ", " $" + totalEgresos.ToString(), 0, 0, 0);

        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Egresos cancelados ", " $" + egresoscancelados.ToString(), 0, 0, 0);

        //    decimal saldoActual = (saldoInicial + (totalIngresos - ingresoscancelados)) - (totalEgresos - egresoscancelados);

        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Saldo actual ", " $" + saldoActual.ToString(), 0, 0, 0);

        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "En caja ", " $" + saldoInicial.ToString(), 0, 0, 0);

        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "___________________________", 0, 0, 0);

        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Total entregado ", " $" + saldoActual.ToString(), 0, 0, 0);

        //    CeldaVacio(tablaTotalesEgreso, 3);

        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "___________________________", 0, 0, 0);

        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Entrega", "", 0, 0, 0);

        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", usuario, 0, 0, 0);

        //    CeldaVacio(tablaTotalesEgreso, 3);

        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "___________________________", 0, 0, 0);

        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
        //    CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Recibe", "", 0, 0, 0);

        //    documentoPdf.Add(tabla);
        //    documentoPdf.Add(tablaEgresos);
        //    documentoPdf.Add(tablaTotalesEgreso);
        //    documentoPdf.NewPage();
        //    tabla.DeleteBodyRows();
        //    tablaTotalesEgreso.DeleteBodyRows();
        //    tablaEgresos.DeleteBodyRows();
        //}
    }
}
