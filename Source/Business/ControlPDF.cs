﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;

using System.Configuration;
using System.Linq;

namespace Business
{
    public static class ControlPDF
    {
        
        //Crear directorio 
        private static string CrearDirectorioControl(string nombre)
        {
            try
            {

                //Verificar si existe el directorio principal del repositorio de documentos
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio de la obra existe
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio para el grupo documental existe, sino crearlo
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre)));


                return string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs//" + nombre);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("No fue posible crear el repositorio para crear el archivo. ", ex.Message));
            }
        }

        //Crear celdas
        public static void Celda(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas para tablas
        public static void CeldaParaTablas(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, BaseColor colorrelleno, float espacio, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                celda.BackgroundColor = colorrelleno;
                celda.Padding = espacio;
                celda.UseAscender = true;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas vacias
        public static void CeldaVacio(PdfPTable tabla, int cantidad)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                celda.BorderWidth = 0;
                tabla.AddCell(celda);
            }

        }

        //para crear el Header y Footer
        public partial class HeaderFooter : PdfPageEventHelper
        {
            PdfContentByte cb;
            PdfTemplate headerTemplate;
            BaseFont bf = null;
            float tamanofuente = 8f;

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                }
                catch (DocumentException de) { }
                catch (System.IO.IOException ioe) { }
            }

            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                //Encabezado de Página
                Rectangle pageSize = doc.PageSize;
                PdfPTable footerTbl = new PdfPTable(1);
                BaseColor blau = new BaseColor(0, 61, 122);

                float[] TablaAncho = new float[] { 100 };
                footerTbl.SetWidths(TablaAncho);
                footerTbl.TotalWidth = 550;
                Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                PdfPCell cell;


                string pagina = "Página " + writer.PageNumber + " de ";
                //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
                {
                    float margenbottom = 15f;
                    float margenright = 280f;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, tamanofuente);
                    cb.SetTextMatrix(doc.PageSize.GetBottom(margenright), doc.PageSize.GetBottom(margenbottom));
                    cb.ShowText(pagina);
                    cb.EndText();
                    float len = bf.GetWidthPoint(pagina, tamanofuente);
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));
                }

                PdfPTable footerTbl2 = new PdfPTable(2);


                float[] TablaAncho2 = new float[] { 100, 100 };
                footerTbl2.SetWidths(TablaAncho2);
                footerTbl2.TotalWidth = doc.Right - doc.Left;
                footerTbl.HorizontalAlignment = Element.ALIGN_CENTER;
                Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                string dia = String.Format("{0:dd}", DateTime.Now);
                string anio = String.Format("{0:yyyy}", DateTime.Now);
                string hora = String.Format("{0:HH:mm}", DateTime.Now);
                string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
                string fechahora = dia + " de " + mestexto + " de " + anio + " " + hora;
                Chunk chkFecha = new Chunk(fechahora, FuenteCelda);
                Phrase phrfecha = new Phrase(chkFecha);

                //Phrase promad = new Phrase("Promad Soluciones Computarizadas " + anio, FuenteCelda2);
                Phrase promad = new Phrase("", FuenteCelda2);
                cell = new PdfPCell(promad);
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);

                cell = new PdfPCell(phrfecha);
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);
                footerTbl2.WriteSelectedRows(0, -1, 5, 25, writer.DirectContent);
            }

            private static string ObtenerMesTexto(string mes)
            {
                string mestexto = "";
                switch (mes)
                {
                    case "01":
                        mestexto = "Enero";
                        break;
                    case "02":
                        mestexto = "Febrero";
                        break;
                    case "03":
                        mestexto = "Marzo";
                        break;
                    case "04":
                        mestexto = "Abril";
                        break;
                    case "05":
                        mestexto = "Mayo";
                        break;
                    case "06":
                        mestexto = "Junio";
                        break;
                    case "07":
                        mestexto = "Julio";
                        break;
                    case "08":
                        mestexto = "Agosto";
                        break;
                    case "09":
                        mestexto = "Septiembre";
                        break;
                    case "10":
                        mestexto = "Octubre";
                        break;
                    case "11":
                        mestexto = "Noviembre";
                        break;
                    case "12":
                        mestexto = "Diciembre";
                        break;
                    default:
                        mestexto = " ";
                        break;
                }
                return mestexto;
            }

            //Necesario para incluir pagina x de y al header
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);
                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, tamanofuente);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

            }
        }
        public partial class HeaderFooter2 : PdfPageEventHelper
        {
            PdfContentByte cb;
            PdfTemplate headerTemplate;
            BaseFont bf = null;
            float tamanofuente = 8f;

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                }
                catch (DocumentException de) { }
                catch (System.IO.IOException ioe) { }
            }

            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                //Encabezado de Página
                Rectangle pageSize = doc.PageSize;
                PdfPTable footerTbl = new PdfPTable(1);
                BaseColor blau = new BaseColor(0, 61, 122);

                float[] TablaAncho = new float[] { 100 };
                footerTbl.SetWidths(TablaAncho);
                footerTbl.TotalWidth = 550;
                Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                PdfPCell cell;


                string pagina = "Página " + writer.PageNumber + " de ";
                //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
                {
                    float margenbottom = 15f;
                    float margenright = 280f;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, tamanofuente);
                    cb.SetTextMatrix(doc.PageSize.GetBottom(margenright), doc.PageSize.GetBottom(margenbottom));
                    cb.ShowText(pagina);
                    cb.EndText();
                    float len = bf.GetWidthPoint(pagina, tamanofuente);
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));
                }

                PdfPTable footerTbl2 = new PdfPTable(2);


                float[] TablaAncho2 = new float[] { 100, 100 };
                footerTbl2.SetWidths(TablaAncho2);
                footerTbl2.TotalWidth = doc.Right - doc.Left+30;
                footerTbl.HorizontalAlignment = Element.ALIGN_CENTER;
                Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                //string dia = String.Format("{0:dd}", DateTime.Now);
                //string anio = String.Format("{0:yyyy}", DateTime.Now);
                //string hora = String.Format("{0:HH:mm}", DateTime.Now);
                //string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
                //string fechahora = dia + " de " + mestexto + " de " + anio + " " + hora;
                //Chunk chkFecha = new Chunk(fechahora, FuenteCelda);
                Chunk chkFecha = new Chunk("", FuenteCelda);
                Phrase phrfecha = new Phrase(chkFecha);

                //Phrase promad = new Phrase("Promad Soluciones Computarizadas " + anio, FuenteCelda2);
                Phrase promad = new Phrase("", FuenteCelda2);
                cell = new PdfPCell(promad);
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);

                cell = new PdfPCell(phrfecha);
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);
                footerTbl2.WriteSelectedRows(0, -1, 5, 25, writer.DirectContent);
            }

            private static string ObtenerMesTexto(string mes)
            {
                string mestexto = "";
                switch (mes)
                {
                    case "01":
                        mestexto = "Enero";
                        break;
                    case "02":
                        mestexto = "Febrero";
                        break;
                    case "03":
                        mestexto = "Marzo";
                        break;
                    case "04":
                        mestexto = "Abril";
                        break;
                    case "05":
                        mestexto = "Mayo";
                        break;
                    case "06":
                        mestexto = "Junio";
                        break;
                    case "07":
                        mestexto = "Julio";
                        break;
                    case "08":
                        mestexto = "Agosto";
                        break;
                    case "09":
                        mestexto = "Septiembre";
                        break;
                    case "10":
                        mestexto = "Octubre";
                        break;
                    case "11":
                        mestexto = "Noviembre";
                        break;
                    case "12":
                        mestexto = "Diciembre";
                        break;
                    default:
                        mestexto = " ";
                        break;
                }
                return mestexto;
            }



            //Necesario para incluir pagina x de y al header
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);
                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, tamanofuente);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

            }
        }

        public partial class HeaderFooter100 : PdfPageEventHelper
        {
            PdfContentByte cb;
            PdfTemplate headerTemplate;
            BaseFont bf = null;
            float tamanofuente = 8f;

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                }
                catch (DocumentException de) { }
                catch (System.IO.IOException ioe) { }
            }

            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                //Encabezado de Página
                Rectangle pageSize = doc.PageSize;
                PdfPTable footerTbl = new PdfPTable(1);
                PdfPCell cell;

                float[] TablaAncho = new float[] { 100 };
                footerTbl.SetWidths(TablaAncho);
                footerTbl.TotalWidth = doc.Right - doc.Left;
                Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);                

                cell = new PdfPCell(new Phrase(" "));
                cell.BackgroundColor = new BaseColor(0, 61, 122);
                cell.BorderWidth = 0;
                cell.FixedHeight = 3;
                footerTbl.AddCell(cell);

                footerTbl.WriteSelectedRows(0, -1, 16, 25, writer.DirectContent);

                string pagina = "Página " + writer.PageNumber + " de ";
                //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
                {
                    float margenbottom = 15f;
                    float margenright = 280f;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, tamanofuente);
                    cb.SetTextMatrix(doc.PageSize.GetBottom(margenright), doc.PageSize.GetBottom(margenbottom));
                    cb.ShowText(pagina);
                    cb.EndText();
                    float len = bf.GetWidthPoint(pagina, tamanofuente);
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));
                }
            }

            private static string ObtenerMesTexto(string mes)
            {
                string mestexto = "";
                switch (mes)
                {
                    case "01":
                        mestexto = "Enero";
                        break;
                    case "02":
                        mestexto = "Febrero";
                        break;
                    case "03":
                        mestexto = "Marzo";
                        break;
                    case "04":
                        mestexto = "Abril";
                        break;
                    case "05":
                        mestexto = "Mayo";
                        break;
                    case "06":
                        mestexto = "Junio";
                        break;
                    case "07":
                        mestexto = "Julio";
                        break;
                    case "08":
                        mestexto = "Agosto";
                        break;
                    case "09":
                        mestexto = "Septiembre";
                        break;
                    case "10":
                        mestexto = "Octubre";
                        break;
                    case "11":
                        mestexto = "Noviembre";
                        break;
                    case "12":
                        mestexto = "Diciembre";
                        break;
                    default:
                        mestexto = " ";
                        break;
                }
                return mestexto;
            }



            //Necesario para incluir pagina x de y al header
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);
                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, tamanofuente);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

            }
        }

        /*
        #region "Socorro de ley"

        public static object GenerarPDFSocorro(string[] datos)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(20); //20 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;


            try
            {
                string nombrearchivo = "Socorro_de_ley_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.Socorro"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                //Antes de generar archivo se elimina el existente
                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    obj = new { exitoso = false, ubicacionarchivo = "", mensaje = "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                    return obj;
                }

                //Definición de fuentes
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 11f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return obj = new { exitoso = false, ubicacionarchivo = "", mensaje = ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                TituloReporte(documentoPdf, FuenteTitulo, FuenteNegrita12, datos);
                InformacionReporte(documentoPdf, FuenteNormal, FuenteNegrita, espacio, datos);

                obj = new { exitoso = true, ubicacionarchivo = ubicacionarchivo, mensaje = "" };
                return obj;
            }
            catch (Exception ex)
            {
                return obj = new { exitoso = false, ubicacionarchivo = "", mensaje = ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }*/
        /*
        
        private static void TituloReporte(Document documentoPdf, Font FuenteTitulo, Font FuenteSubtitulo, string[] datos)
        {
            PdfPTable tabla = new PdfPTable(1);
            //Numero de columnas de la tabla
            tabla.WidthPercentage = 100;                                                            //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;                                       //Alineación vertical
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(8); //1 milimetro

            //Titulo
            Celda(tabla, FuenteTitulo, "Reporte de socorro de ley", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 2);

            //Subtitulo
            string centroNombre = string.Empty;
            Entity.CentroReclusion centro = ControlCentroReclusion.ObtenerPorId(Convert.ToInt32(datos[2]));
            if (centro != null)
            {
                centroNombre = centro.Nombre;
            }
            else
            {
                centroNombre = "Todos";
            }

            string subtitulo = "Centro de reclusión: " + centroNombre;

            Celda(tabla, FuenteSubtitulo, subtitulo, 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 4);


            documentoPdf.Add(tabla);
            tabla.DeleteBodyRows();

        }*/
        /*Obsoleta
        private static void InformacionReporte(Document documentoPdf, Font FuenteNormal, Font FuenteNegrita, float espacio, string[] datos)
        {
            PdfPTable tabla = new PdfPTable(6);//Numero de columnas de la tabla

            tabla.WidthPercentage = 100; //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            BaseColor colorrelleno = new BaseColor(208, 206, 206);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);


            //Titulos de las columnas de las tablas
            CeldaParaTablas(tabla, FuenteNegrita, "Nombre", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Expediente", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Proceso", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Situación jurídica", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Centro reculusión", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Cuota", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            List<Entity.Socorro_reporte> reporte = ControlSocorroReporte.ObtenerTodos(Convert.ToInt32(datos[0]), Convert.ToInt32(datos[1]), Convert.ToInt32(datos[2]));

            string nombre = string.Empty;
            string cuota = string.Empty;
            decimal total = 0;

            //celdas, contenido de la tabla  
            foreach (var item in reporte)
            {
                nombre = item.Nombre + " " + item.Paterno + " " + item.Materno;
                cuota = "$" + item.Cuota.ToString();
                total = total + item.Cuota;
                CeldaParaTablas(tabla, FuenteNormal, nombre, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Expediente, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Proceso, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Situacion, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Centro, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, cuota, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            }

            CeldaParaTablas(tabla, FuenteNegrita, "", 1, 1, 5, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Total: $" + Math.Round(total, 2), 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);



            documentoPdf.Add(tabla);
            documentoPdf.NewPage();
            tabla.DeleteBodyRows();
        }#endregion */

        #region "Socorro de ley"

        private static void TituloReporteAuxDevoluciones1(Document documentoPdf, Font FuenteTitulo, Font FuenteSubtitulo, string nombreDetenido, string remision, string domicilio, string recibio, int internoId)
        {
            int centroId = 0;
            var ei = ControlDetalleDetencion.ObtenerPorDetenidoId(internoId);
            foreach (var val in ei)
            {
                centroId = val.CentroId;
            }

            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }

            int[] w = new int[] { 2, 8 };
            PdfPTable table = new PdfPTable(2);
            table.SetWidths(w);
            table.TotalWidth = 550;
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell;                       

            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            cellWithRowspan.PaddingRight = 10;
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_LEFT;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cellWithRowspan);
            
            //Titulo
            Celda(table, FuenteTitulo, "      ", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(table, FuenteTitulo, "Entrega de pertenencias", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            //Renglón vacio
            CeldaVacio(table, 5);

            //Tabla para datos
            BaseColor blau = new BaseColor(0, 61, 122);            
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            PdfPTable tablaDatos = new PdfPTable(1);
            table.HorizontalAlignment = Element.ALIGN_LEFT;


            CeldaConChunk(tablaDatos, FuenteNegrita, FuenteNormal, "Detenido: ", nombreDetenido, 0, Element.ALIGN_LEFT, 0);
            CeldaConChunk(tablaDatos, FuenteNegrita, FuenteNormal, "Domicilio: ", domicilio, 0, Element.ALIGN_LEFT, 0);
            CeldaConChunk(tablaDatos, FuenteNegrita, FuenteNormal, "No. remisión: ", remision, 0, Element.ALIGN_LEFT, 0);
            CeldaConChunk(tablaDatos, FuenteNegrita, FuenteNormal, "Usuario que recibió: ", recibio, 0, Element.ALIGN_LEFT, 0);
            CeldaVacio(tablaDatos, 2);


            documentoPdf.Add(table);
            documentoPdf.Add(tablaDatos);            
            table.DeleteBodyRows();
        }

        private static void TituloReporteAuxDevoluciones2(Document documentoPdf, Font FuenteTitulo, Font FuenteSubtitulo, string remision, string recibio, string condicionEntrega, string tipoEntrega, int internoId)
        {
            int centroId = 0;
            var ei = ControlDetalleDetencion.ObtenerPorDetenidoId(internoId);
            foreach (var val in ei)
            {
                centroId = val.CentroId;
            }

            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }

            int[] w = new int[] { 2, 8 };
            PdfPTable table = new PdfPTable(2);
            table.SetWidths(w);
            table.TotalWidth = 550;
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell;

            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            cellWithRowspan.PaddingRight = 10;
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_LEFT;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cellWithRowspan);

            //Titulo
            Celda(table, FuenteTitulo, "      ", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(table, FuenteTitulo, "Recibo pertenencias", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            //Renglón vacio
            CeldaVacio(table, 5);

            //Tabla para datos
            BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            PdfPTable tablaDatos = new PdfPTable(1);
            table.HorizontalAlignment = Element.ALIGN_LEFT;

            CeldaConChunk(tablaDatos, FuenteNegrita, FuenteNormal, "No. remisión: ", remision, 0, Element.ALIGN_LEFT, 0);
            CeldaConChunk(tablaDatos, FuenteNegrita, FuenteNormal, "Persona que recibe: ", recibio, 0, Element.ALIGN_LEFT, 0);
            if (!string.IsNullOrWhiteSpace(condicionEntrega))
            {
                CeldaConChunk(tablaDatos, FuenteNegrita, FuenteNormal, "Condición de entrega: ", condicionEntrega, 0, Element.ALIGN_LEFT, 0);
            }
            if (!string.IsNullOrWhiteSpace(tipoEntrega))
            {
                CeldaConChunk(tablaDatos, FuenteNegrita, FuenteNormal, "Entrega: ", tipoEntrega, 0, Element.ALIGN_LEFT, 0);
            }
            CeldaVacio(tablaDatos, 2);

            documentoPdf.Add(table);
            documentoPdf.Add(tablaDatos);
            table.DeleteBodyRows();
        }

        private static void TituloReporteAuxDonaciones(Document documentoPdf, Font FuenteTitulo, Font FuenteSubtitulo, string nombreDetenido, string remision, string domicilio, string recibio, string bolsa, int internoId)
        {
            int centroId = 0;
            var ei = ControlDetalleDetencion.ObtenerPorDetenidoId(internoId);
            foreach (var val in ei)
            {
                centroId = val.CentroId;
            }

            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }

            int[] w = new int[] { 2, 8 };
            PdfPTable table = new PdfPTable(2);
            table.SetWidths(w);
            table.TotalWidth = 550;
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell;

            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            cellWithRowspan.PaddingRight = 10;
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_LEFT;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cellWithRowspan);

            //Titulo
            Celda(table, FuenteTitulo, "      ", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(table, FuenteTitulo, "Recibo de donación/consignación de pertenencias", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            //Renglón vacio
            CeldaVacio(table, 5);

            //Tabla para datos
            BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            PdfPTable tablaDatos = new PdfPTable(1);
            float[] width = new float[] { 100 };
            tablaDatos.SetWidths(width);
            tablaDatos.WidthPercentage = 100;
            table.HorizontalAlignment = Element.ALIGN_LEFT;

            CeldaConChunk(tablaDatos, FuenteNegrita, FuenteNormal, "Detenido: ", nombreDetenido, 0, Element.ALIGN_LEFT, 0);
            
            CeldaConChunk(tablaDatos, FuenteNegrita, FuenteNormal, "Domicilio: ", domicilio, 0, Element.ALIGN_LEFT, 0);
            CeldaConChunk(tablaDatos, FuenteNegrita, FuenteNormal, "No. remisión: ", remision, 0, Element.ALIGN_LEFT, 0);
            CeldaConChunk(tablaDatos, FuenteNegrita, FuenteNormal, "Recibió: ", recibio, 0, Element.ALIGN_LEFT, 0);
            CeldaConChunk(tablaDatos, FuenteNegrita, FuenteNormal, "Bolsa: ", bolsa, 0, Element.ALIGN_LEFT, 0);
            CeldaVacio(tablaDatos, 2);

            documentoPdf.Add(table);
            documentoPdf.Add(tablaDatos);
            table.DeleteBodyRows();
        }

        public static object GenerarPDFInterno(string[] datos)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(20); //20 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Internos_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.Internos"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                //Antes de generar archivo se elimina el existente
                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    obj = new { exitoso = false, ubicacionarchivo = "", mensaje = "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                    return obj;
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD, blau);
                Font FuenteTituloTabla = new Font(fuentecalibri, 11f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return obj = new { exitoso = false, ubicacionarchivo = "", mensaje = ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                TituloReporteInterno(documentoPdf, FuenteTitulo, FuenteNegrita12, datos);
                InformacionReporteInterno(documentoPdf, FuenteNormal, FuenteNegrita, espacio, datos);

                obj = new { exitoso = true, ubicacionarchivo = ubicacionarchivo, mensaje = "" };
                return obj;
            }
            catch (Exception ex)
            {
                return obj = new { exitoso = false, ubicacionarchivo = "", mensaje = ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloReporteInterno(Document documentoPdf, Font FuenteTitulo, Font FuenteSubtitulo, string[] datos)
        {
            PdfPTable tabla = new PdfPTable(1);
            //Numero de columnas de la tabla
            tabla.WidthPercentage = 100;                                                            //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;                                       //Alineación vertical
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(8); //1 milimetro

            //Titulo
            Celda(tabla, FuenteTitulo, "Reporte de internos", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 2);

            //Subtitulo

            string subtitulo = "Fecha: " + DateTime.Now.ToShortDateString();

            Celda(tabla, FuenteSubtitulo, subtitulo, 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 4);


            documentoPdf.Add(tabla);
            tabla.DeleteBodyRows();

        }

        private static void InformacionReporteInterno(Document documentoPdf, Font FuenteNormal, Font FuenteNegrita, float espacio, string[] datos)
        {
            PdfPTable tabla = new PdfPTable(7);//Numero de columnas de la tabla

            tabla.WidthPercentage = 100; //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            BaseColor colorrelleno = new BaseColor(208, 206, 206);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);


            //Titulos de las columnas de las tablas
            CeldaParaTablas(tabla, FuenteNegrita, "Número", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Nombre", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Paterno", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Materno", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Expediente", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "NCP", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Estatus", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);


            List<Entity.Interno_Reporte> reporte = ControlInternoReporte.ObtenerTodos(datos);


            int i = 1;
            //celdas, contenido de la tabla  
            foreach (var item in reporte)
            {
                CeldaParaTablas(tabla, FuenteNormal, i.ToString(), 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Nombre, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Paterno, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Materno, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Expediente, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.NCP, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Estatus, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                i++;
            }

            documentoPdf.Add(tabla);
            documentoPdf.NewPage();
            tabla.DeleteBodyRows();
        }

        #endregion

        #region Recibo devolución de pertenencias / evidencias

        public static object[] GenerarReciboDevolucionesPDF(List<PertenenciaAux> data, string nombreDetenido, string remision, string domicilio, string recibio, string condicionEntrega, string tipoEntrega, string bolsa, string entrega, int internoId)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //20 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Recibo_devolucion_pertenencia_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.ReciboDevolucion"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;
                
                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {                    
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);                
                BaseColor blau = new BaseColor(0, 61, 122);
                Font FuenteBlau = new Font(fuentecalibri, 12f, Font.NORMAL, blau);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.NORMAL, blau);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 11f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                if (domicilio == "Calle:  # Colonia: C.P.") domicilio = "No registrado";

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                TituloReporteAuxDevoluciones1(documentoPdf, FuenteTitulo, FuenteNegrita12, nombreDetenido, remision, domicilio, recibio, internoId);
                InformacionReciboDevoluciones(documentoPdf, FuenteNormal, FuenteNegritaBlanca, espacio, data, recibio, nombreDetenido, entrega, recibio, 1, FuenteBlau);
                documentoPdf.NewPage();
                TituloReporteAuxDevoluciones2(documentoPdf, FuenteTitulo, FuenteNegrita12, remision, recibio, condicionEntrega, tipoEntrega, internoId);
                InformacionReciboDevoluciones(documentoPdf, FuenteNormal, FuenteNegritaBlanca, espacio, data, recibio, nombreDetenido, entrega, recibio, 2, FuenteBlau);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {                
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void InformacionReciboDevoluciones(Document documentoPdf, Font FuenteNormal, Font FuenteNegritaBlanca, float espacio, List<PertenenciaAux> datos, string recibio, string detenido, string entrega, string entregaA, int pagina, Font FuenteBlau)
        {
            PdfPTable tabla = new PdfPTable(7);//Numero de columnas de la tabla            
            tabla.WidthPercentage = 100; //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            PdfPTable tablaFirmas = new PdfPTable(1);
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;

            BaseColor colorrelleno = new BaseColor(0, 61, 122);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);

            //Titulos de las columnas de las tablas
            CeldaParaTablas(tabla, FuenteNegritaBlanca, "Fecha de entrada", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegritaBlanca, "Descripción", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegritaBlanca, "Observación", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegritaBlanca, "Clasificación", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegritaBlanca, "Bolsa", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegritaBlanca, "Cantidad", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegritaBlanca, "Casillero", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;

            //celdas, contenido de la tabla  
            foreach (var item in datos)
            {
                if (i % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                CeldaParaTablas(tabla, FuenteNormal, Convert.ToDateTime(item.FechaEntrada).ToString("dd/MM/yyyy HH:mm:ss"), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Nombre, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Observacion, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Clasificacion, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Bolsa, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Cantidad, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Casillero, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                i++;
            }

            CeldaVacio(tabla, 30);

            PdfPCell celdaEspacioFirmas;
            celdaEspacioFirmas = new PdfPCell(new Phrase("_________________________", FuenteBlau));
            celdaEspacioFirmas.HorizontalAlignment = Element.ALIGN_CENTER;
            celdaEspacioFirmas.Border = 0;
            celdaEspacioFirmas.VerticalAlignment = Element.ALIGN_MIDDLE;

            if (pagina == 1)
            {
                tablaFirmas.AddCell(celdaEspacioFirmas);
                Celda(tablaFirmas, FuenteBlau, "Entrega pertenencias", 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                Celda(tablaFirmas, FuenteBlau, entrega, 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            }
            else
            {
                tablaFirmas.AddCell(celdaEspacioFirmas);
                Celda(tablaFirmas, FuenteBlau, "Recibe pertenencias", 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                Celda(tablaFirmas, FuenteBlau, recibio, 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            }

            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaFirmas);
            documentoPdf.NewPage();
            tabla.DeleteBodyRows();
        }

        public static object[] GenerarReciboDonacionesPDF(List<PertenenciaAux> data, string nombreDetenido, string remision, string domicilio, string recibio, string bolsa, string entrega, int internoId)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //20 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Recibo_donacionen_pertenencia_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.ReciboDonacion"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                BaseColor blau = new BaseColor(0, 61, 122);
                Font FuenteBlau = new Font(fuentecalibri, 12f, Font.NORMAL, blau);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.NORMAL, blau);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 11f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                if (domicilio == "Calle:  # Colonia: C.P.") domicilio = "No registrado";

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                TituloReporteAuxDonaciones(documentoPdf, FuenteTitulo, FuenteNegrita12, nombreDetenido, remision, domicilio, recibio, bolsa, internoId);
                InformacionReciboDonaciones(documentoPdf, FuenteNormal, FuenteNegritaBlanca, espacio, data, recibio, nombreDetenido, entrega, recibio, FuenteBlau);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void InformacionReciboDonaciones(Document documentoPdf, Font FuenteNormal, Font FuenteNegritaBlanca, float espacio, List<PertenenciaAux> datos, string recibio, string detenido, string entrega, string entregaA, Font FuenteBlau)
        {
            PdfPTable tabla = new PdfPTable(4);//Numero de columnas de la tabla            
            tabla.WidthPercentage = 100; //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            PdfPTable tablaFirmas = new PdfPTable(4);
            tablaFirmas.WidthPercentage = 100;
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;

            BaseColor colorrelleno = new BaseColor(0, 61, 122);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);

            //Titulos de las columnas de las tablas
            CeldaParaTablas(tabla, FuenteNegritaBlanca, "Cantidad", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegritaBlanca, "Pertenencia", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegritaBlanca, "Observación", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegritaBlanca, "Clasificación", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;

            //celdas, contenido de la tabla  
            foreach (var item in datos)
            {
                if (i % 2 != 0)
                    color = colorgris;
                else color = colorblanco;
                CeldaParaTablas(tabla, FuenteNormal, item.Cantidad, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Nombre, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Observacion, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Clasificacion, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                i++;
            }

            CeldaVacio(tabla, 30);
            
            PdfPCell celdaEspacioFirmas;
            celdaEspacioFirmas = new PdfPCell(new Phrase("_____________________", FuenteBlau));
            celdaEspacioFirmas.HorizontalAlignment = Element.ALIGN_LEFT;
            celdaEspacioFirmas.Border = 0;
            celdaEspacioFirmas.VerticalAlignment = Element.ALIGN_MIDDLE;

            tablaFirmas.AddCell(celdaEspacioFirmas);
            tablaFirmas.AddCell(celdaEspacioFirmas);
            tablaFirmas.AddCell(celdaEspacioFirmas);
            tablaFirmas.AddCell(celdaEspacioFirmas);

            Celda(tablaFirmas, FuenteBlau, "Recibe pertenencias", 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteBlau, "Detenido", 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteBlau, "Entrega pertenencias", 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteBlau, "Entrega a", 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            Celda(tablaFirmas, FuenteBlau, recibio, 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteBlau, detenido, 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteBlau, entrega, 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteBlau, entregaA, 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaFirmas);
            documentoPdf.NewPage();
            tabla.DeleteBodyRows();
        }

        #endregion

        #region "Reporte de extravío"

        public static object GenerarPDFReporteExtravio(string[] datos)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(10); //10 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;


            try
            {
                string nombrearchivo = "ReporteExtravio" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("ReporteExtravio"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                string tracking = string.Empty;
                string id = string.Empty;
                string nombre = string.Empty;
                string edad = string.Empty;
                string titulo = string.Empty;
                string alias = string.Empty;
                string violento = string.Empty;
                string enfermo = string.Empty;
                string comentarios = string.Empty;
                string institucion = string.Empty;
                string direccion = string.Empty;
                string telefono = string.Empty;
                string senas = string.Empty;
                Guid trackingid;

                try
                {
                    tracking = datos[0];
                    id = datos[1];
                    nombre = datos[2];
                    edad = datos[3];
                    titulo = datos[4];
                    alias = datos[5];
                    violento = datos[6];
                    enfermo = datos[7];
                    comentarios = datos[8];
                    institucion = datos[9];
                    direccion = datos[10];
                    telefono = datos[11];
                    senas = datos[12];

                    if (!Guid.TryParse(tracking, out trackingid))
                    {
                        throw new Exception("No se logró obtener id");
                    }
                }
                catch
                {
                    obj = new { exitoso = false, ubicacionarchivo = "", mensaje = "Error al extraer información para generar el reporte. Si el problema persiste, contacte al administrador del sistema." };
                    return obj;
                }

                //Antes de generar archivo se elimina el existente
                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch
                {
                    obj = new { exitoso = false, ubicacionarchivo = "", mensaje = "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                    return obj;
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont calibrifont = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font titlefont = new Font(calibrifont, 18, Font.BOLD, blau);
                Font FuenteNegrita = new Font(calibrifont, 10f, Font.BOLD, blau);
                Font negritabold = new Font(calibrifont, 10f, Font.BOLD);
                Font FuenteNormal = new Font(calibrifont, 10f, Font.NORMAL);
                Font bigfont = new Font(calibrifont, 14, Font.BOLD, blau);
                Font smallfont = new Font(calibrifont, 8, Font.NORMAL);
                Font bigtitlefont = new Font(calibrifont, 14, Font.BOLD, blau);
                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return obj = new { exitoso = false, ubicacionarchivo = "", mensaje = ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter2();
                documentoPdf.Open();

                TituloReporteExtravio(documentoPdf, calibrifont, titulo, nombre, titlefont, bigfont, Convert.ToInt32(id));

                DetalleReporteExtravio(documentoPdf, FuenteNormal, FuenteNegrita, espacio, trackingid, edad, alias, violento, enfermo, comentarios, senas);

                ContactoReporteExtravio(documentoPdf, FuenteNormal, FuenteNegrita, espacio, institucion, direccion, telefono);

                obj = new { exitoso = true, ubicacionarchivo = ubicacionarchivo, mensaje = "" };
                return obj;
            }
            catch (Exception ex)
            {
                return obj = new { exitoso = false, ubicacionarchivo = "", mensaje = ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }


        private static void TituloReporteExtravio(Document documentoPdf, BaseFont fuentecalibri, string titulo, string nombre, Font titlefont, Font bigfont, int id)
        {
            int centroId = 0;
            var ei = ControlDetalleDetencion.ObtenerPorDetenidoId(id);
            foreach (var val in ei)
            {
                centroId = val.CentroId;
            }

            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);
            Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            string escudo = "~/Content/img/Escudo.jpg";
            string logo = "~/Content/img/logo.png";
            var logoizq = subcontrato.Banner;
            if(logoizq != "undefined" &&logo!="" && !string.IsNullOrEmpty(logoizq))
            {
                logo = logoizq;
            }
            string pathEscudo = System.Web.HttpContext.Current.Server.MapPath(escudo);
            string pathLogo = System.Web.HttpContext.Current.Server.MapPath(logo);
            iTextSharp.text.Image imageEscudo = null;
            iTextSharp.text.Image imageLogo = null;
            try
            {
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }
            catch (Exception)
            {
                pathEscudo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }

            try
            {
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }
            catch (Exception)
            {
                pathLogo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }

            var logoDER = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logoDER != "undefined" && logo != "")
            {
                logotipo = logoDER;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }

            imageLogo.ScalePercent(95f);
            
                imageLogo.ScaleAbsoluteHeight(100f);
                imageLogo.ScaleAbsoluteWidth(300f);
            
            image.ScaleAbsoluteHeight(60f);
            image.ScaleAbsoluteWidth(70f);
            Paragraph para = new Paragraph();
            Phrase ph1 = new Phrase();
            Chunk glue = new Chunk(new iTextSharp.text.pdf.draw.VerticalPositionMark());
            Paragraph main = new Paragraph();
            ph1.Add(new Chunk(imageLogo, 0, 0, true));
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(image, 0, 15, true));
            main.Add(ph1);
            para.Add(main);
            documentoPdf.Add(para);


            int[] width = new int[] { 30, 35, 35 };
            PdfPTable tabla = new PdfPTable(3);
            tabla.SetWidths(width);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell cell;

            CeldaVacio(tabla, 4);

           

            //cell = new PdfPCell(new Phrase(""));
            //// The first cell spans 5 rows  
            //cell.Rowspan = 5;
            //cell.Border = 0;
            //tabla.AddCell(cell);

            //Titulo
            Celda(tabla, titlefont, titulo, 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 2);

            Celda(tabla, bigfont, nombre, 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 4);

            documentoPdf.Add(tabla);
            tabla.DeleteBodyRows();
        }

        static string ObtenColorRojo(int color)
        {
            string colorOjos = string.Empty;
            switch (color)
            {
                case 1: colorOjos = "Azul";
                    break;
                case 2: colorOjos = "Café claro";
                    break;
                case 3: colorOjos = "Café obscuro";
                    break;
                case 4: colorOjos = "Gris";
                    break;
                case 5: colorOjos = "Verde";
                    break;
                case 6: colorOjos = "Otro";
                    break;
                case 7: colorOjos = "Sin dato";
                    break;
                default: colorOjos = string.Empty;
                    break;
            }

            return colorOjos;
        }

        static string ObtenColorPiel(int color)
        {
            string colorPiel = string.Empty;
            switch (color)
            {
                case 1: colorPiel = "Albino";
                    break;
                case 2: colorPiel = "Blanco";
                    break;
                case 3: colorPiel = "Amarillo";
                    break;
                case 4: colorPiel = "Moreno Claro";
                    break;
                case 5: colorPiel = "Moreno";
                    break;
                case 6: colorPiel = "Moreno Oscuro";
                    break;
                case 7: colorPiel = "Negro";
                    break;
                case 8: colorPiel = "Otro";
                    break;
                case 9: colorPiel = "Sin dato";
                    break;
                default: colorPiel = string.Empty;
                    break;
            }
            return colorPiel;
        }

        static string ObtenColorCabello(int color)
        {
            string colorCabello = string.Empty;
            switch (color)
            {
                case 1: colorCabello = "Albino";
                    break;
                case 2: colorCabello = "Cano total";
                    break;
                case 3: colorCabello = "Castaño claro";
                    break;
                case 4: colorCabello = "Castaño oscuro";
                    break;
                case 5: colorCabello = "Entrecano";
                    break;
                case 6: colorCabello = "Negro";
                    break;
                case 7: colorCabello = "Pelirrojo";
                    break;
                case 8: colorCabello = "Rubio";
                    break;
                case 9: colorCabello = "Sin dato";
                    break;
                default: colorCabello = string.Empty;
                    break;
            }
            return colorCabello;
        }

        static string ObtenFormaCabello(int forma)
        {
            string formaCabello = string.Empty;
            switch (forma)
            {
                case 1: formaCabello = "Crespo";
                    break;
                case 2: formaCabello = "Lacio";
                    break;
                case 3: formaCabello = "Ondulado";
                    break;
                case 4: formaCabello = "Rizado";
                    break;
                case 5: formaCabello = "Sin dato";
                    break;
                default: formaCabello = string.Empty;
                    break;
            }
            return formaCabello;
        }

        static string ObtenLabios(int tipo)
        {
            string labios = string.Empty;
            switch (tipo)
            {
                case 1: labios = "Delgados";
                    break;
                case 2: labios = "Medianos";
                    break;
                case 3: labios = "Gruesos";
                    break;
                case 4: labios = "Morrudos";
                    break;
                case 5: labios = "Sin dato";
                    break;
                default: labios = string.Empty;
                    break;
            }
            return labios;
        }

        private static void DetalleReporteExtravio(Document documentoPdf, Font FuenteNormal, Font FuenteNegrita, float espacio, Guid trackingid,
            string edad, string alias, string violento, string enfermo, string comentarios, string senas)
        {
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);

            Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(trackingid);

            Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);

            Entity.Antropometria antropometria = ControlAntropometria.ObtenerPorDetenidoId(interno.Id);

            string colorOjos = "";

            string colorPiel = "";

            string colorCabello = "";

            string formaCabello = "";

            string labios = "";
            Entity.OjoNariz ojos;
            Entity.GeneralCabello generalCabello;
            Entity.BocaLabioMenton boca;
            if (antropometria != null)
            {
                ojos = ControlOjoNariz.ObtenerPorAntropometriaId(antropometria.Id);

                generalCabello = ControlGeneralCabello.ObtenerPorAntropometriaId(antropometria.Id);

                 boca = ControlBocaLabioMenton.ObtenerPorAntropometriaId(antropometria.Id);

                 colorOjos = ojos!=null? ObtenColorRojo(ojos.ColorO):"";

                 colorPiel = generalCabello!=null?ObtenColorPiel(generalCabello.ColorPielG):"";

                 colorCabello = generalCabello!=null? ObtenColorCabello(generalCabello.ColorC):"";

                 formaCabello = generalCabello != null ? ObtenFormaCabello(generalCabello.FormaC):"";

                 labios = boca!=null? ObtenLabios(boca.EspesorL):"";
            }
            PdfPTable tabla3 = new PdfPTable(2);//Numero de columnas de la tabla
            tabla3.WidthPercentage = 100; //Porcentaje ancho
            tabla3.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical
            CeldaConChunk(tabla3, FuenteNegrita, FuenteNormal, "Estatura: ", antropometria != null ? antropometria.Estatura.ToString() : "", 0, 0, 0);
            CeldaConChunk(tabla3, FuenteNegrita, FuenteNormal, "Peso: ", antropometria != null ? antropometria.Peso.ToString() : "", 0, 0, 0);

            int[] width = new int[] { 1, 16 };
            PdfPTable tabla2 = new PdfPTable(2);
            tabla2.SetWidths(width);
            tabla2.TotalWidth = 550;
            tabla2.HorizontalAlignment = Element.ALIGN_LEFT;

            PdfPCell cellWithRowspan = new PdfPCell();
            cellWithRowspan.Rowspan = 9;
            cellWithRowspan.Border = 0;
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_MIDDLE;
            tabla2.AddCell(cellWithRowspan);

            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Alias: ", alias, 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Edad: ", edad, 0, 0, 0);
            // CeldaParaTablas(tabla2, FuenteNormal, string.Format("{0}: {1}", "Sexo",general!=null? general.SexoId.ToString():1 == 1 ? "Masculino" : "Femenino"), 1, 0, 1, colorblanco, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            //if (general!=null)
            //    {


            //}
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Sexo : ", general != null ? ControlCatalogo.Obtener(general.SexoId, 4).Nombre : " ", 0, 0, 0);

            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Piel: ", colorPiel, 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Color de cabello: ", colorCabello, 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Tipo de cabello: ", formaCabello, 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Color de ojos: ", colorOjos, 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Labios: ", labios, 0, 0, 0);            
            var celdadoble = new PdfPCell(tabla3);
            celdadoble.Border = 0;
            tabla2.AddCell(celdadoble);

            PdfPTable tabla = new PdfPTable(2);//Numero de columnas de la tabla
            tabla.WidthPercentage = 100; //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(interno.RutaImagen);
            
            if (string.IsNullOrEmpty(pathfoto))
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/avatars/male.png");
                var image = iTextSharp.text.Image.GetInstance(pathfoto);
                var celda = new PdfPCell(image, true);
                celda.Border = 0;
                tabla.AddCell(celda);
            }
            else
            {
                try
                {
                    var image = iTextSharp.text.Image.GetInstance(pathfoto);
                    var celda = new PdfPCell(image, true);
                    celda.Border = 0;
                    tabla.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/avatars/male.png");
                    var image = iTextSharp.text.Image.GetInstance(pathfoto);
                    var celda = new PdfPCell(image, true);
                    celda.Border = 0;
                    tabla.AddCell(celda);
                }
                
            }
            var celdatabla2 = new PdfPCell(tabla2);
            celdatabla2.Border = 0;
            tabla.AddCell(celdatabla2);


            documentoPdf.Add(tabla);
            tabla.DeleteBodyRows();

            tabla = new PdfPTable(1);//Numero de columnas de la tabla
            tabla.WidthPercentage = 100; //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}

            CeldaVacio(tabla, 2);

            CeldaConChunk(tabla, FuenteNegrita, FuenteNormal, "Señas particulares: ", senas, 0, 0, 0);
            CeldaConChunk(tabla, FuenteNegrita, FuenteNormal, "Violento(a): ", violento, 0, 0, 0);
            CeldaConChunk(tabla, FuenteNegrita, FuenteNormal, "Enfermo(a) mental: ", enfermo, 0, 0, 0);
            CeldaConChunk(tabla, FuenteNegrita, FuenteNormal, "Comentarios: ", comentarios, 0, 0, 0);

            documentoPdf.Add(tabla);
            tabla.DeleteBodyRows();
        }

        private static void ContactoReporteExtravio(Document documentoPdf, Font FuenteNormal, Font FuenteNegrita, float espacio, string institucion, string direccion, string telefono)
        {
            PdfPTable tabla = new PdfPTable(1);//Numero de columnas de la tabla

            tabla.WidthPercentage = 100; //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            BaseColor colorrelleno = new BaseColor(208, 206, 206);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);


            CeldaVacio(tabla, 2);
            CeldaParaTablas(tabla, FuenteNegrita, "Si tiene información por favor contacte a: ", 1, 0, 1, colorblanco, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaVacio(tabla, 2);
            CeldaConChunk(tabla, FuenteNegrita, FuenteNormal, "Institución: ", institucion, 0, 0, 0);
            CeldaConChunk(tabla, FuenteNegrita, FuenteNormal, "Dirección: ", direccion, 0, 0, 0);
            CeldaConChunk(tabla, FuenteNegrita, FuenteNormal, "Teléfono: ", telefono, 0, 0, 0);

            documentoPdf.Add(tabla);
            tabla.DeleteBodyRows();
        }

        #endregion

        #region Recibo devolución de pertenencias / evidencias


        public static object[] generarReporteDiagnostico(object[] data)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(20); //20 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Reporte_diagnostico_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.Diagnostico"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 11f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                TituloAutorizacionSalida(documentoPdf, FuenteTitulo, FuenteNegrita12, data, FuenteTituloTabla);
                //InformacionSalida(documentoPdf, FuenteNormal, FuenteNegrita, espacio, data, recibio, nombreDetenido, entrega, recibio);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }
        private static void TituloDiagnostico(Document documentoPdf, Font FuenteTitulo, Font FuenteSubtitulo, object[] data)
        {
            PdfPTable tabla = new PdfPTable(1);
            //Numero de columnas de la tabla
            tabla.WidthPercentage = 100;                                                            //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;                                       //Alineación vertical
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(8); //1 milimetro

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Diagnóstico del detenido", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD);
            PdfPTable tablaDelegacionUsuario = new PdfPTable(1);
            float[] width = new float[] { 220 };
            tablaDelegacionUsuario.SetWidths(width);
            tablaDelegacionUsuario.DefaultCell.Border = Rectangle.NO_BORDER;
            PdfPCell celda;
            Chunk chunkTituloDelegacion = new Chunk("Delegación: ", FuenteNegrita);
            Chunk chunkNombreDelegacion = new Chunk(data[0].ToString(), FuenteNormal);
            Paragraph parrafoDelegacion = new Paragraph();
            parrafoDelegacion.Add(chunkTituloDelegacion);
            parrafoDelegacion.Add(chunkNombreDelegacion);
            celda = new PdfPCell(parrafoDelegacion);
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.Border = 0;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            tablaDelegacionUsuario.AddCell(celda);

        

            CeldaVacio(tablaDelegacionUsuario, 1);

            //Titulo "Datos del detenido"
            PdfPTable tabla1 = new PdfPTable(1);
            tabla1.WidthPercentage = 100;
            PdfPCell tituloTabla1;
            tituloTabla1 = new PdfPCell(new Phrase("DATOS DEL DETENIDO", FuenteNegrita));
            tituloTabla1.HorizontalAlignment = Element.ALIGN_CENTER;
            tituloTabla1.Border = 0;
            tituloTabla1.BackgroundColor = new BaseColor(179, 220, 243);
            tituloTabla1.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla1.AddCell(tituloTabla1);

            CeldaVacio(tabla1, 1);

            //Tabla datos del detenido
            PdfPTable tabla2 = new PdfPTable(4);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Número: ", data[2].ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Alias: ", data[3].ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Escolaridad: ", data[4].ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Edad: ", data[5].ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Nacimiento: ", data[6].ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Estado civil: ", data[7].ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Domicilio: ", data[11].ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Teléfono: ", data[15].ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Colonia: ", data[12].ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Ciudad: ", data[13].ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Estado: ", data[14].ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Nacionalidad: ", data[9].ToString(), 0, 0, 0);

            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "", "", 0, 0, 0);

            CeldaVacio(tabla2, 3);

            //Titulo "Remision"
            PdfPTable tabla3 = new PdfPTable(1);
            tabla3.WidthPercentage = 100;
            PdfPCell tituloTabla2;
            tituloTabla2 = new PdfPCell(new Phrase("Remisión", FuenteNegrita));
            tituloTabla2.HorizontalAlignment = Element.ALIGN_CENTER;
            tituloTabla2.Border = 0;
            tituloTabla2.BackgroundColor = new BaseColor(179, 220, 243);
            tituloTabla2.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla3.AddCell(tituloTabla2);

            CeldaVacio(tabla3, 1);

            //Tabla datos de la remision
            PdfPTable tabla4 = new PdfPTable(2);
            tabla4.WidthPercentage = 100;
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "No. remisión: ", data[16].ToString(), 0, 0, 0);
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Fecha remisión ", data[17].ToString(), 0, 0, 0);
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Motivo remisión: ", data[18].ToString(), 0, 0, 0);
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Lugar de detención: ", data[18].ToString(), 0, 0, 0);
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Colonia: ", data[18].ToString(), 0, 0, 0);
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "No. evento: ", data[18].ToString(), 0, 0, 0);

            CeldaVacio(tabla4, 3);


            //Titulo "Situacion"
            PdfPTable tabla7 = new PdfPTable(1);
            tabla7.WidthPercentage = 100;
            PdfPCell tituloTabla4;
            tituloTabla4 = new PdfPCell(new Phrase("Expediente", FuenteNegrita));
            tituloTabla4.HorizontalAlignment = Element.ALIGN_CENTER;
            tituloTabla4.Border = 0;
            tituloTabla4.BackgroundColor = new BaseColor(179, 220, 243);
            tituloTabla4.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla7.AddCell(tituloTabla4);

            CeldaVacio(tabla7, 1);

            //Datos situacion
            PdfPTable tabla8 = new PdfPTable(4);
            tabla8.WidthPercentage = 100;
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Motivo:", data[23].ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Adicción: ", data[24].ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Religion: ", data[25].ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Pandilla: ", data[26].ToString(), 0, 0, 0);
            

            CeldaVacio(tabla8, 1);

            //Explicacion situacion
            PdfPTable tabla9 = new PdfPTable(1);
            tabla9.WidthPercentage = 100;
            CeldaConChunk(tabla9, FuenteNegrita, FuenteNormal, "Fundamento: ", data[29].ToString(), 0, 0, 0);
            CeldaConChunk(tabla9, FuenteNegrita, FuenteNormal, "Razonamiento: ", data[30].ToString(), 0, 0, 0);

            CeldaVacio(tabla9, 1);

            tablaDelegacionUsuario.WidthPercentage = 100;
            tabla2.WidthPercentage = 100;
            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaDelegacionUsuario);
            documentoPdf.Add(tabla1);
            documentoPdf.Add(tabla2);
            documentoPdf.Add(tabla3);
            documentoPdf.Add(tabla4);
          
            documentoPdf.Add(tabla7);
            documentoPdf.Add(tabla8);
            documentoPdf.Add(tabla9);
            //documentoPdf.Add(tablaDatos2);
            //documentoPdf.Add(tablaDatos3);
            tabla.DeleteBodyRows();
        }

        public static object[] generarAutorizacionSalida(object[] data)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //20 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, 15);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Recibo_autorizacion_salida_"+data[31] +"_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.AutorizacionSalida"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 11f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter100();
                documentoPdf.Open();
                TituloAutorizacionSalida(documentoPdf, FuenteTitulo, FuenteNegrita12, data, FuenteTituloTabla);
                //InformacionSalida(documentoPdf, FuenteNormal, FuenteNegrita, espacio, data, recibio, nombreDetenido, entrega, recibio);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void InformacionSalida(Document documentoPdf, Font FuenteNormal, Font FuenteNegrita, float espacio, List<PertenenciaAux> datos, string recibio, string detenido, string entrega, string entregaA)
        {
            PdfPTable tabla = new PdfPTable(4);//Numero de columnas de la tabla            
            tabla.WidthPercentage = 100; //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            PdfPTable tablaFirmas = new PdfPTable(4);
            tablaFirmas.WidthPercentage = 100;
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;

            BaseColor colorrelleno = new BaseColor(208, 206, 206);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);

            //Titulos de las columnas de las tablas
            CeldaParaTablas(tabla, FuenteNegrita, "Cantidad", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Pertenencia", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Observación", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Clasificación", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //celdas, contenido de la tabla  
            foreach (var item in datos)
            {
                CeldaParaTablas(tabla, FuenteNormal, item.Cantidad, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Nombre, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Observacion, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Clasificacion, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            }

            CeldaVacio(tabla, 30);

            PdfPCell celdaEspacioFirmas;
            celdaEspacioFirmas = new PdfPCell(new Phrase("________________________", FuenteNormal));
            celdaEspacioFirmas.HorizontalAlignment = Element.ALIGN_LEFT;
            celdaEspacioFirmas.Border = 0;
            celdaEspacioFirmas.VerticalAlignment = Element.ALIGN_MIDDLE;

            tablaFirmas.AddCell(celdaEspacioFirmas);
            tablaFirmas.AddCell(celdaEspacioFirmas);
            tablaFirmas.AddCell(celdaEspacioFirmas);
            tablaFirmas.AddCell(celdaEspacioFirmas);

            Celda(tablaFirmas, FuenteNormal, "Recibe pertenencias", 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteNormal, "Detenido", 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteNormal, "Entrega pertenencias", 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteNormal, "Entrega a", 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            Celda(tablaFirmas, FuenteNormal, recibio, 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteNormal, detenido, 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteNormal, entrega, 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteNormal, entregaA, 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaFirmas);
            documentoPdf.NewPage();
            tabla.DeleteBodyRows();
        }

        public static void CeldaConChunk(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, int bordo, int horizontal, int vertical)
        {            
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk(valor, FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            celda = new PdfPCell(elements);                
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = horizontal;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);        
        }

        private static void TituloAutorizacionSalida(Document documentoPdf, Font FuenteTitulo, Font FuenteSubtitulo, object[] data, Font titulotabla)
        {
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            // Excepciones y casos especiales
            Boolean bolHermosillo = false;

            string escudo = "~/Content/img/Escudo.jpg";
            string logo = "~/Content/img/logo.png";
            string banner_estado = "~/Content/img/Banner_Estado.png";
            var logoizq = subcontrato.Banner;
            if (logoizq != "undefined" && !string.IsNullOrEmpty(logoizq))
            {
                logo = logoizq;
            }
            string pathEscudo = System.Web.HttpContext.Current.Server.MapPath(escudo);
            // string pathLogo = System.Web.HttpContext.Current.Server.MapPath(logo);
            string pathLogo = System.Web.HttpContext.Current.Server.MapPath(banner_estado);
            iTextSharp.text.Image imageEscudo = null;
            iTextSharp.text.Image imageLogo = null;
            try
            {
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }
            catch (Exception)
            {
                // pathEscudo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                pathEscudo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/Banner_Estado.png");
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }

            try
            {
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }
            catch (Exception)
            {
                // pathLogo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                pathLogo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/Banner_Estado.png");
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }

            var logoDER = subcontrato.Logotipo;
            // string logotipo = "~/Content/img/logo.png";
            string logotipo = "~/Content/img/Escudo.jpg";

            if (logoDER != "undefined" && logo != "")
            {
                logotipo = logoDER;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                //pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/Escudo.jpg");
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }

            imageLogo.ScalePercent(95f);
                // AQUI SE ESCALA EL BANNER DEL LADO IZQUIERDO
                imageLogo.ScaleAbsoluteHeight(100f);
                imageLogo.ScaleAbsoluteWidth(300f);
                // AQUI SE ESCALA EL BANNER DEL LADO DERECHO
                // image.ScaleAbsoluteHeight(60f);
                // image.ScaleAbsoluteWidth(70f);
                image.ScaleAbsoluteHeight(100f);
                image.ScaleAbsoluteWidth(100f);

            Paragraph para = new Paragraph();
            Phrase ph1 = new Phrase();
            Chunk glue = new Chunk(new iTextSharp.text.pdf.draw.VerticalPositionMark());
            Paragraph main = new Paragraph();
            ph1.Add(new Chunk(imageLogo, 0, 0, true));
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            // ph1.Add(new Chunk(image, 0, 15, true));
            ph1.Add(new Chunk(image, 0, 0, true));
            main.Add(ph1);
            para.Add(main);
            documentoPdf.Add(para);

            int[] w = new int[] { 6, 8 };
            PdfPTable tabla = new PdfPTable(2);
            //Numero de columnas de la tabla
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;
             
            CeldaVacio(tabla, 3);
            //Titulo                                    
            Celda(tabla, FuenteTitulo, "  Autorización de salida", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
                        
            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNormal2 = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);
            PdfPTable tablaDelegacionUsuario = new PdfPTable(1);
            float[] width = new float[] { 220 };
            tablaDelegacionUsuario.SetWidths(width);
            tablaDelegacionUsuario.DefaultCell.Border = Rectangle.NO_BORDER;
            PdfPCell celda;
            Chunk chunkTituloDelegacion = new Chunk("Delegación: ", FuenteNegrita);
            Chunk chunkNombreDelegacion = new Chunk(data[0].ToString(), FuenteNormal);
            Paragraph parrafoDelegacion = new Paragraph();
            parrafoDelegacion.Add(chunkTituloDelegacion);
            parrafoDelegacion.Add(chunkNombreDelegacion);            
            celda = new PdfPCell(parrafoDelegacion);
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.Border = 0;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            tablaDelegacionUsuario.AddCell(celda);
            Chunk chunkTituloUsuario = new Chunk("Calificador: ", FuenteNegrita);
            Chunk chunkNombreUsuario = new Chunk(data[1].ToString(), FuenteNormal);
            PdfPCell celda2;
            Paragraph parrafoUsuario = new Paragraph();
            parrafoUsuario.Add(chunkTituloUsuario);
            parrafoUsuario.Add(chunkNombreUsuario);
            celda2 = new PdfPCell(parrafoUsuario);
            celda2.HorizontalAlignment = Element.ALIGN_LEFT;
            celda2.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda2.Border = 0;
            tablaDelegacionUsuario.AddCell(celda2);

            CeldaVacio(tablaDelegacionUsuario, 1);

            //Titulo "Datos del detenido"
            PdfPTable tabla1 = new PdfPTable(1);
            tabla1.WidthPercentage = 100;                                   
            PdfPCell tituloTabla1;
            tituloTabla1 = new PdfPCell(new Phrase("DATOS DEL DETENIDO", titulotabla));
            tituloTabla1.HorizontalAlignment = Element.ALIGN_CENTER;
            tituloTabla1.Border = 0;
            tituloTabla1.BackgroundColor = new BaseColor(0, 61, 122);
            tituloTabla1.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla1.AddCell(tituloTabla1);

            CeldaVacio(tabla1, 1);

            //Tabla datos del detenido
            PdfPTable tabla2 = new PdfPTable(3);
            var edad = "";
            if (data[5].ToString() != "0") edad = data[5].ToString();
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "No. remisión: ",data[2]!=null? data[2].ToString():"", 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Registro: ", data[3].ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Nombre: ", data[4].ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Edad: ", edad, 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Sexo: ", data[6].ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Estado civil: ", data[7].ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Escolaridad: ", data[8].ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Nacionalidad: ", data[9].ToString(), 0, 0, 0);
            //CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Sueldo: ", data[10].ToString(), 0, 0, 0);
            if(data[11].ToString() == "")
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Domicilio: ", "Sin dato", 0, 0, 0);
            else
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Domicilio: ", data[11].ToString(), 0, 0, 0);
            if(data[12].ToString() == "")
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Colonia: ", "Sin dato", 0, 0, 0);
            else
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Colonia: ", data[12].ToString(), 0, 0, 0);
            if(data[13].ToString() == "")
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Ciudad: ", "Sin dato", 0, 0, 0);
            else
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Ciudad: ", data[13].ToString(), 0, 0, 0);
            if(data[14].ToString() == "")
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Estado: ", "Sin dato", 0, 0, 0);
            else
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Estado: ", data[14].ToString(), 0, 0, 0);
            if(data[15].ToString() == "")
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Teléfono: ", "Sin dato", 0, 0, 0);
            else
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Teléfono: ", data[15].ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "", "", 0, 0, 0);

            CeldaVacio(tabla2, 3);

            //Titulo "Remision"
            PdfPTable tabla3 = new PdfPTable(1);
            tabla3.WidthPercentage = 100;
            PdfPCell tituloTabla2;
            tituloTabla2 = new PdfPCell(new Phrase("REMISIÓN", titulotabla));
            tituloTabla2.HorizontalAlignment = Element.ALIGN_CENTER;
            tituloTabla2.Border = 0;
            tituloTabla2.BackgroundColor = new BaseColor(0, 61, 122);
            tituloTabla2.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla3.AddCell(tituloTabla2);

            CeldaVacio(tabla3, 1);
             
            //Tabla datos de la remision
            PdfPTable tabla4 = new PdfPTable(3);
            tabla4.WidthPercentage = 100;
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Cve. unidad: ", data[16].ToString(), 0, 0, 0);
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Cve. agente: ", data[17].ToString(), 0, 0, 0);
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Nombre agente: ", data[18].ToString(), 0, 0, 0);

            CeldaVacio(tabla4, 3);

            //Titulo "Parte"
            PdfPTable tabla5 = new PdfPTable(1);
            tabla5.WidthPercentage = 100;
            PdfPCell tituloTabla3;
            tituloTabla3 = new PdfPCell(new Phrase("PARTE", titulotabla));
            tituloTabla3.HorizontalAlignment = Element.ALIGN_CENTER;
            tituloTabla3.Border = 0;
            tituloTabla3.BackgroundColor = new BaseColor(0, 61, 122);
            tituloTabla3.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla5.AddCell(tituloTabla3);

            CeldaVacio(tabla5, 1);

            //Datos parte
            PdfPTable tabla6 = new PdfPTable(1);
            tabla6.WidthPercentage = 100;
            CeldaConChunk(tabla6, FuenteNegrita, FuenteNormal, "N° evento: ", data[19].ToString(), 0, 0, 0);
            if (Convert.ToInt32(data[5]) <= 17 && Convert.ToInt32(data[5]) >= 14)
            {
                CeldaConChunk(tabla6, FuenteNegrita, FuenteNormal, "Motivo: ", data[20].ToString(), 0, 0, 0);
            }
            else
            {
                CeldaConChunk(tabla6, FuenteNegrita, FuenteNormal, "Motivo detención: ", data[20].ToString(), 0, 0, 0);
            }

            CeldaConChunk(tabla6, FuenteNegrita, FuenteNormal, "Lugar detención: ", data[21].ToString(), 0, 0, 0);
            CeldaConChunk(tabla6, FuenteNegrita, FuenteNormal, "Colonia: ", data[22].ToString(), 0, 0, 0);

            CeldaVacio(tabla6, 1);
             
            //Titulo "Situacion"
            PdfPTable tabla7 = new PdfPTable(1);
            tabla7.WidthPercentage = 100;
            PdfPCell tituloTabla4;
            tituloTabla4 = new PdfPCell(new Phrase("SITUACIÓN", titulotabla));
            tituloTabla4.HorizontalAlignment = Element.ALIGN_CENTER;
            tituloTabla4.Border = 0;
            tituloTabla4.BackgroundColor = new BaseColor(0, 61, 122);
            tituloTabla4.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla7.AddCell(tituloTabla4);

            CeldaVacio(tabla7, 1);

            //Datos situacion
            PdfPTable tabla8 = new PdfPTable(3);
            tabla8.WidthPercentage = 100;
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Fecha: ", data[23].ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Calificó: ", data[24].ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Situación: ", data[25].ToString(), 0, 0, 0);
            if (Convert.ToInt32(data[5]) <=17 && Convert.ToInt32(data[5]) >= 14 )
            {
                CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "", "", 0, 0, 0);
            }
            else
            {
                CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Solo arresto: ", data[26].ToString(), 0, 0, 0);
            }
            
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Horas: ", data[27].ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Multa: ", data[28].ToString(), 0, 0, 0);

            CeldaVacio(tabla8, 1);

            //Explicacion situacion
            PdfPTable tabla9 = new PdfPTable(1);
            tabla9.WidthPercentage = 100;
            var ss = data[29].ToString();
            var sss = ss.Length;
            //if(sss>100)
            //{
            //    CeldaVacio(tabla9, 15);
            //}
            CeldaConChunk(tabla9, FuenteNegrita, FuenteNormal2, "Fundamento: ", data[29].ToString(), 0, 0, 0);
            //if (sss > 100)
            //{
            //    CeldaVacio(tabla9, 5);
            //}

            CeldaVacio(tabla9, 1);

            CeldaConChunk(tabla9, FuenteNegrita, FuenteNormal, "Descripcion de hechos: ", data[34].ToString(), 0, 0, 0);
            CeldaConChunk(tabla9, FuenteNegrita, FuenteNormal, "Razonamiento de calificación: ", data[30].ToString(), 0, 0, 0);
            CeldaConChunk(tabla9, FuenteNegrita, FuenteNormal, "Razon de salida: ", data[32].ToString(), 0, 0, 0);
            CeldaConChunk(tabla9, FuenteNegrita, FuenteNormal, "Tipo de salida: ", data[33].ToString(), 0, 0, 0);

            CeldaVacio(tabla9, 1);

            float[] w6 = new float[] { 50,50};
            PdfPTable tablaunidad = new PdfPTable(2);
            tablaunidad.WidthPercentage = 70; //Porcentaje ancho
            tablaunidad.HorizontalAlignment = Element.ALIGN_CENTER;
            tablaunidad.SetWidths(w6);

            CeldaVacio(tablaunidad, 1);
            CeldaVacio(tablaunidad, 1);
          //  CeldaVacio(tablaunidad, 1);
            
            CeldaVacio(tablaunidad, 1);
            CeldaVacio(tablaunidad, 1);
          //  CeldaVacio(tablaunidad, 1);

            CeldaConChunk(tablaunidad, FuenteNegrita, FuenteNormal, "", "_______________________", 0, 0, 1);
            CeldaConChunk(tablaunidad, FuenteNegrita, FuenteNormal, "", "_______________________", 0, 0, 1);
          //  CeldaConChunk(tablaunidad, FuenteNegrita, FuenteNormal, "", "", 0, 0, 1);

            CeldaConChunk(tablaunidad, FuenteNegrita, FuenteNormal, data[4].ToString(), "", 0, 0, 1);
            // CeldaConChunk(tablaunidad, FuenteNegrita, FuenteNormal, "ENCARGADO DE BARANDILLA", "", 0, 0, 1);
            CeldaConChunk(tablaunidad, FuenteNegrita, FuenteNormal, "        JUEZ CALIFICADOR   ", "", 0, 0, 1);
            // CeldaConChunk(tablaunidad, FuenteNegrita, FuenteNormal, "", "", 0, 0, 1);

            tablaDelegacionUsuario.WidthPercentage = 100;
            tabla2.WidthPercentage = 100;
            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaDelegacionUsuario);
            documentoPdf.Add(tabla1);
            documentoPdf.Add(tabla2);
            if (!bolHermosillo)
            {
                documentoPdf.Add(tabla3); // Titulo de Remisión
                documentoPdf.Add(tabla4); // Datos de Remisión
                documentoPdf.Add(tabla5); // Titulo de Parte
                documentoPdf.Add(tabla6); // Datos de Parte  
            }
            documentoPdf.Add(tabla7); // Titulo de Situación
            documentoPdf.Add(tabla8); // Datos de Situación
            documentoPdf.Add(tabla9);
            documentoPdf.Add(tablaunidad);
            //documentoPdf.Add(tablaDatos2);
            //documentoPdf.Add(tablaDatos3);
            tabla.DeleteBodyRows();
        }

        #endregion

        #region Recibo de pago de multa

        public static object[] generarReciboPagoMulta(object[] data, List<Entity.PagoMulta> info)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //20 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Recibo_Caja_PagoMulta_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.PagoMulta"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD, blau);
                Font FuenteTituloTabla = new Font(fuentecalibri, 11f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                TituloReciboMulta(documentoPdf, FuenteTitulo, FuenteNegrita12, data);
                InformacionReciboMulta(documentoPdf, FuenteNormal, FuenteNegrita, espacio, info, data);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloReciboMulta(Document documentoPdf, Font FuenteTitulo, Font FuenteSubtitulo, object[] data)
        {
            int[] w = new int[] { 2, 8};
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell;
            
            var institucion = ControlInstitucion.ObtenerPorId(Convert.ToInt32(data[13]));

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }

            //Logo
            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_LEFT;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.AddCell(cellWithRowspan);

            //Titulo       
            Celda(tabla, FuenteTitulo, "     ", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            Celda(tabla, FuenteTitulo, "   Recibo de caja", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 1);

            //Nombre delegacion y usuario en barandilla
            BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);
            PdfPTable tablaDelegacionUsuario = new PdfPTable(1);
            float[] width = new float[] { 220 };
            tablaDelegacionUsuario.SetWidths(width);
            tablaDelegacionUsuario.DefaultCell.Border = Rectangle.NO_BORDER;

            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Delegación: ", data[0].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "N° de recibo: ", data[1].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "No. remisión: ", data[2].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Registro: ", data[3].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Nombre: ", data[4].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Domicilio: ", data[5].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Calificación: ", data[6].ToString(), 0, 0, 0);
            CeldaVacio(tablaDelegacionUsuario, 1);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Motivos de calificación", "", 0, 0, 0);
            CeldaVacio(tablaDelegacionUsuario, 1);

            tablaDelegacionUsuario.WidthPercentage = 100;

            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaDelegacionUsuario);
            tabla.DeleteBodyRows();
        }

        private static void InformacionReciboMulta(Document documentoPdf, Font FuenteNormal, Font FuenteNegrita, float espacio, List<Entity.PagoMulta> datos, object[] data)
        {
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseColor blau = new BaseColor(0, 61, 122);
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormalAux = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegritaAux = new Font(fuentecalibri, 12f, Font.BOLD, blau);
            Font FuenteEncabezado = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.WHITE);

            PdfPTable tabla = new PdfPTable(3);//Numero de columnas de la tabla            
            tabla.WidthPercentage = 100; //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            PdfPTable tablaTotales = new PdfPTable(3);
            tablaTotales.WidthPercentage = 100;
            tablaTotales.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPTable tablaFinales = new PdfPTable(1);
            tablaFinales.WidthPercentage = 100;
            tablaFinales.HorizontalAlignment = Element.ALIGN_CENTER;

            BaseColor colorrelleno = new BaseColor(0, 61, 122);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            BaseColor colorgris = new BaseColor(238, 238, 238);
            BaseColor color = colorgris;
            int i = 1;

            //Titulos de las columnas de las tablas
            CeldaParaTablas(tabla, FuenteEncabezado, "Artículo", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteEncabezado, "Motivo", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteEncabezado, "Multa", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //celdas, contenido de la tabla  
            foreach (var item in datos)
            {
                if (i % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                CeldaParaTablas(tabla, FuenteNormal, item.Articulo, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, item.Motivo, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, " $" + item.MultaSugerida.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                i++;
            }

            CeldaVacio(tabla, 1);

            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Total de multas ", " $" + data[7].ToString(), 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Agravante ", " $" + data[8].ToString(), 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Ajuste ", " %" + data[9].ToString(), 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Total a pagar ", " $" + data[10].ToString(), 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Horas de arresto ", data[11].ToString(), 0, 0, 0);

            CeldaVacio(tablaTotales, 3);

            CeldaConChunk(tablaFinales, FuenteNegritaAux, FuenteNormalAux, "Fundamento: ", data[12].ToString(), 0, 0, 0);
            CeldaConChunk(tablaFinales, FuenteNegritaAux, FuenteNormalAux, "Motivo de salida: ", "PAGO DE MULTA", 0, 0, 0);

            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaTotales);
            documentoPdf.Add(tablaFinales);
            documentoPdf.NewPage();
            tabla.DeleteBodyRows();
            tablaTotales.DeleteBodyRows();
            tablaFinales.DeleteBodyRows();
        }

        #endregion
        //

        
        public static object[] ReporteEpi(List<Entity.Epi> Listado,string[] parametros)
        {
            object[] obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //milimetros para los margenes
            var space = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentpdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Remisiones" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioRemisiones("Remisiones"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string pathfont = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont calibrifont = BaseFont.CreateFont(pathfont, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font titlefont = new Font(calibrifont, 13, Font.BOLD,BaseColor.WHITE);
                Font titlebig = new Font(calibrifont, 17, Font.BOLD, blau);
                Font boldfont = new Font(calibrifont, 12f, Font.BOLD, blau);
                Font normalfont = new Font(calibrifont, 10f, Font.NORMAL);
                Font title = new Font(calibrifont, 12, Font.BOLD);
                Font bigfont = new Font(calibrifont, 10, Font.BOLD);
                Font smallfont = new Font(calibrifont, 9, Font.NORMAL, BaseColor.WHITE);
                Font FuenteNegrita = new Font(calibrifont, 10f, Font.BOLD);
                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentpdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();

                documentpdf.Open();
                TituloReporteEpi(documentpdf, titlefont, boldfont, normalfont, space, titlebig, Listado, parametros,FuenteNegrita);
                return new object[] { true, ubicacionarchivo, "" };
                
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentpdf.IsOpen())
                {
                    documentpdf.Close();
                    PdfWriter.Close();
                }
            }
        }
        public static void TituloReporteEpi(Document documentoPdf, Font titlefont, Font boldfont, Font normalfont, float space, Font bigfont,List<Entity.Epi> Listado,string[] parametros,Font Negrita)
        {
            int[] w = new int[] { 2,8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var contrato = ControlContrato.ObtenerPorId(contratoUsuario.IdContrato);
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
            var institucion = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
            var logo = subcontrato.Logotipo;

            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));

            }

            var domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);
            var municipio = ControlMunicipio.Obtener(domicilio.MunicipioId??0);
            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            tabla.AddCell(cellWithRowspan);

            //Titulo                                    
            Celda(tabla, bigfont, "Ingreso de detenidos por primera vez en el año en estado de ebriedad", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 2);

            int[] w3 = new int[] {100 };
            PdfPTable tabla1 = new PdfPTable(1);
            tabla1.SetWidths(w3);
            tabla1.TotalWidth = 550;
            tabla1.HorizontalAlignment = Element.ALIGN_LEFT;
            CeldaConChunk(tabla1, boldfont, Negrita, "Institución: ", institucion.Nombre.ToString(), 0, 0, 0);
            var m = "";
            if(municipio !=null)
            {
                m = municipio.Nombre;
            }
           
            CeldaConChunk(tabla1, boldfont, Negrita, "Municipio: ", m, 0, 0, 0);
            
            var date = Convert.ToDateTime(parametros[1]);
            var h = date.Hour.ToString();
            if(date.Hour < 10) h = "0" + h;
            var min = date.Minute.ToString();
            if (date.Minute < 10) min = "0" + min;
            var s = date.Second.ToString();
            if (date.Second < 10) s = "0" + s;
            var fecha1 = date.ToShortDateString() + " " + h + ":" + min + ":" + s;

             date = Convert.ToDateTime(parametros[2]);
             h = date.Hour.ToString();
            if (date.Hour < 10) h = "0" + h;
             min = date.Minute.ToString();
            if (date.Minute < 10) min = "0" + min;
             s = date.Second.ToString();
            if (date.Second < 10) s = "0" + s;
            var fecha2= date.ToShortDateString() + " " + h + ":" + min + ":" + s;

            int[] w4 = new int[] { 3,7 };
            PdfPTable tabla4 = new PdfPTable(2);
            tabla4.SetWidths(w4);
            tabla4.TotalWidth = 550;
            tabla4.HorizontalAlignment = Element.ALIGN_LEFT;
            CeldaConChunk(tabla4, boldfont, Negrita, "Del: ", fecha1, 0, 0, 0);
            CeldaConChunk(tabla4, boldfont, Negrita, "al: ", fecha2, 0, Element.ALIGN_LEFT, 0);
            CeldaVacio(tabla4, 2);
            float[] w2 = new float[] { 10,15,20,25,10,10,30 };
            PdfPTable tabla2 = new PdfPTable(7);
            tabla2.SetWidths(w2);
            tabla2.WidthPercentage = 100;
            PdfPCell cell;
            BaseColor blau = new BaseColor(0, 61, 122);


            cell = new PdfPCell(new Phrase("#", titlefont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla2.AddCell(cell);

            cell = new PdfPCell(new Phrase("No. remisión", titlefont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla2.AddCell(cell);

            cell = new PdfPCell(new Phrase("Nombre", titlefont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla2.AddCell(cell);

            cell = new PdfPCell(new Phrase("Fecha y hora de registro", titlefont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla2.AddCell(cell);

            cell = new PdfPCell(new Phrase("Edad", titlefont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla2.AddCell(cell);

            cell = new PdfPCell(new Phrase("Sexo", titlefont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla2.AddCell(cell);

            cell = new PdfPCell(new Phrase("Conclusión médica", titlefont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla2.AddCell(cell);

            var numero = 0;
            foreach(var item in Listado)
            {
                numero = numero + 1;

                var h1 = item.Fecha.Hour.ToString();
                if (item.Fecha.Hour < 10) h1 = "0" + h1;
                var m1 = item.Fecha.Minute.ToString();
                if (item.Fecha.Minute < 10) m1 = "0" + m1;
                var s1 = item.Fecha.Second.ToString();
                if (item.Fecha.Second < 10) s1 = "0" + s1;

                var fecha = item.Fecha.ToShortDateString() + " " + h1 + ":" + m1 + ":" + s1;

                cell = new PdfPCell((new Paragraph(numero.ToString(), normalfont)));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
               
                cell.BorderWidth = 0;
                tabla2.AddCell(cell);


                cell = new PdfPCell((new Paragraph(item.Expediente, normalfont)));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidth = 0;
                tabla2.AddCell(cell);

                cell = new PdfPCell((new Paragraph(item.Nombre, normalfont)));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidth = 0;
                tabla2.AddCell(cell);

                cell = new PdfPCell((new Paragraph(fecha, normalfont)));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidth = 0;
                tabla2.AddCell(cell);

                cell = new PdfPCell((new Paragraph(item.Edad.ToString(), normalfont)));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidth = 0;
                tabla2.AddCell(cell);

                cell = new PdfPCell((new Paragraph(item.Sexo, normalfont)));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidth = 0;
                tabla2.AddCell(cell);

                cell = new PdfPCell((new Paragraph(item.Conclucion, normalfont)));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidth = 0;
                tabla2.AddCell(cell);

            }

            documentoPdf.Add(tabla);
            documentoPdf.Add(tabla1);
            documentoPdf.Add(tabla4);
            documentoPdf.Add(tabla2);

        }

        public static object ReporteRemisiones(int centro, int contrato, string tipo)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //milimetros para los margenes
            var space = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentpdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                //Nombre y ubicación archivo
                string nombrearchivo = "Remisiones" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioRemisiones("Remisiones"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string pathfont = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont calibrifont = BaseFont.CreateFont(pathfont, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font titlefont = new Font(calibrifont, 12, Font.BOLD);
                Font titlebig = new Font(calibrifont, 18, Font.BOLD, blau);
                Font boldfont = new Font(calibrifont, 12f, Font.BOLD,blau);
                Font normalfont = new Font(calibrifont, 10f, Font.NORMAL);
                Font title = new Font(calibrifont, 12, Font.BOLD);
                Font bigfont = new Font(calibrifont, 10, Font.BOLD);
                Font smallfont = new Font(calibrifont, 9, Font.NORMAL, BaseColor.WHITE);
                Font FuenteNegrita = new Font(calibrifont, 14f, Font.BOLD);
                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentpdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return obj = new { success = false, file = "", message = ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();

                documentpdf.Open();

                TitleRemisiones(documentpdf, titlefont, boldfont, normalfont, space, bigfont, centro, titlebig);
                GetDatosRemisiones(documentpdf, boldfont, normalfont, space, smallfont, centro, contrato, tipo);

                obj = new { success = true, file = ubicacionarchivo, message = "" };
                return obj;
            }
            catch (Exception ex)
            {
                return obj = new { success = false, file = "", message = ex.Message };
            }
            finally
            {
                if (documentpdf.IsOpen())
                {
                    documentpdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TitleRemisiones(Document documentpdf, Font titlefont, Font boldfont, Font normalfont, float space, Font bigfont, int centroId, Font titlebig)
        {
            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);
            string nombreCentro = institucion.Nombre;

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }

            int[] width = new int[] { 2, 8 };
            PdfPTable table = new PdfPTable(2);
            table.SetWidths(width);
            table.TotalWidth = 550;
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell;
            
            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            table.AddCell(cellWithRowspan);
            
            //string title = "Seguridad Pública Municipal";
            string lugar = nombreCentro;

            cell = new PdfPCell(new Phrase("Remisiones", titlebig));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.PaddingLeft = 10;
            table.AddCell(cell);

            //cell = new PdfPCell(new Phrase(title, titlefont));
            //cell.BorderWidth = 0;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            //cell.PaddingLeft = 10;
            //table.AddCell(cell);

            cell = new PdfPCell(new Phrase(lugar, titlefont));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.PaddingLeft = 10;
            table.AddCell(cell);

            //cell = new PdfPCell(new Phrase("En Existencia:", boldfont));
            //cell.BorderWidth = 0;
            //cell.Padding = 10;
            //table.AddCell(cell);

            CeldaVacio(table, 5);

            documentpdf.Add(table);
        }

        private static void GetDatosRemisiones(Document documentpdf, Font boldfont, Font normalfont, float space, Font smallfont, int centroId, int contrato, string tipo)
        {
            BaseColor blau = new BaseColor(0, 61, 122);
            int contador = 0;
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
            float[] width = new float[] { 100, 100 };
            PdfPTable table = new PdfPTable(2);
            table.WidthPercentage = 100;
            table.SetWidths(width);

            //var detenciones = ControlDetalleDetencion.ObteneTodos().Where(y => y.ContratoId == contrato && y.Activo && y.Tipo == tipo);
            var detenciones = ControlListadoReporteRemision.ObtenerTodos(new object[]
            {
                tipo, contrato
            });

            Font FuenteNormal2 = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita2 = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            PdfPCell cell1;
            cell1 = new PdfPCell(new Phrase("En existencia:", FuenteNegrita2));
            cell1.BorderWidth = 0;
            cell1.HorizontalAlignment = Element.ALIGN_LEFT;
            table.AddCell(cell1);

            CeldaConChunk(table, FuenteNegrita2, FuenteNormal2, "Registros encontrados: ", detenciones.Count.ToString(), 0, Element.ALIGN_RIGHT, 0);
            //cell1 = new PdfPCell(new Phrase("Registros encontrados: "+ detenciones.Count(), boldfont));
            //cell1.BorderWidth = 0;
            //cell1.HorizontalAlignment = Element.ALIGN_RIGHT;
            //table.AddCell(cell1);


            float[] w2 = new float[] { 40, 90, 30, 52, 90, 35, 90, 170 };
            PdfPTable tabla = new PdfPTable(8);
            tabla.SetWidths(w2);
            tabla.WidthPercentage = 100;
            PdfPCell cell;

            cell = new PdfPCell(new Phrase("No. remisión", FuenteNegrita));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Nombre", FuenteNegrita));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Edad", FuenteNegrita));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Registro", FuenteNegrita));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Lugar de detención", FuenteNegrita));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Unidad", FuenteNegrita));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Clave-responsable", FuenteNegrita));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Motivo detención", FuenteNegrita));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int x = 1;

            foreach (var i in detenciones)
            {         
                if (x % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                //var detenido = ControlDetenido.ObtenerPorId(i.DetenidoId);

                //Remisión
                cell = new PdfPCell(new Paragraph(i.Expediente, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = color;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);
                contador++;

                //Nombre
                cell = new PdfPCell(new Paragraph(i.Nombre.Trim() + " " + i.Paterno.Trim() + " " + i.Materno.Trim(), normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = color;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                //Edad
                string edadAux = i.Edad != 0 ? i.Edad.ToString() : string.Empty;
                cell = new PdfPCell(new Paragraph(edadAux, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = color;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);
                //Entity.General general = ControlGeneral.ObtenerPorDetenidoId(detenido.Id);
                //DateTime nacimiento;
                //if (general == null)
                //{
                //    cell = new PdfPCell(new Paragraph(""));
                //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //    cell.BackgroundColor = color;
                //    cell.BorderWidth = 0;
                //    tabla.AddCell(cell);
                //}
                //else
                //{
                //    if (general.FechaNacimineto != DateTime.MinValue)
                //    {
                //        int edad = CalcularEdad(general.FechaNacimineto);
                //        edad = general.Edaddetenido;
                //        cell = new PdfPCell(new Paragraph(edad.ToString(), normalfont));
                //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //        cell.BackgroundColor = color;
                //        cell.BorderWidth = 0;
                //        tabla.AddCell(cell);
                //    }
                //    else
                //    {
                //        var detalleDetencion2 = ControlInformacionDeDetencion.ObtenerPorInternoId(i.DetenidoId);
                //        var evento = ControlEvento.ObtenerById(detalleDetencion2.IdEvento);
                //        var detenidosEvento = ControlDetenidoEvento.ObtenerPorEventoId(evento.Id);
                //        Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
                //        string edad = "";
                //        foreach (var item in detenidosEvento)
                //        {
                //            if (item.Nombre == detenido.Nombre && item.Paterno == detenido.Paterno && item.Materno == detenido.Materno)
                //                detenidoEvento = item;
                //        }
                //        if (detenidoEvento == null) edad = "";
                //        else edad = detenidoEvento.Edad != 0 ? detenidoEvento.Edad.ToString() : "";
                //        edad = i.DetalledetencionEdad.ToString();
                //        cell = new PdfPCell(new Paragraph(edad.ToString(), normalfont));
                //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //        cell.BackgroundColor = color;
                //        cell.BorderWidth = 0;
                //        tabla.AddCell(cell);
                //    }
                //}

                //Fecha
                var date = Convert.ToDateTime(i.Fecha).ToShortDateString();
                var h = Convert.ToDateTime(i.Fecha).Hour.ToString();
                if(Convert.ToDateTime(i.Fecha).Hour<10)
                {
                    h = "0" + h;
                }
                var m = Convert.ToDateTime(i.Fecha).Minute.ToString();
                if(Convert.ToDateTime(i.Fecha).Minute<10)
                {
                    m = "0" + m;
                }
                var s = Convert.ToDateTime(i.Fecha).Second.ToString();
                if(Convert.ToDateTime(i.Fecha).Second<10)
                {
                    s = "0" + s;
                }
                var fecha = date + " " + h + ":" + m + ":" + s;
                cell = new PdfPCell((new Paragraph(fecha, normalfont)));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = color;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                //Lugar
                cell = new PdfPCell(new Paragraph(i.LugarDetencion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = color;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);
                //var infoDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(i.DetenidoId);
                List<Entity.Unidad> unidades = null;
                //if (infoDetencion == null)
                //{
                //    cell = new PdfPCell(new Paragraph(""));
                //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //    cell.BackgroundColor = color;
                //    cell.BorderWidth = 0;
                //    tabla.AddCell(cell);
                //}
                //else
                //{
                //    cell = new PdfPCell(new Paragraph(infoDetencion.LugarDetencion, normalfont));
                //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //    cell.BackgroundColor = color;
                //    cell.BorderWidth = 0;
                //    tabla.AddCell(cell);
                //    unidades = ControlUnidad.ObtenerPorEventoId(infoDetencion.IdEvento);
                //}

                //Unidad
                cell = new PdfPCell(new Paragraph(i.Unidad, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = color;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                cell = new PdfPCell(new Paragraph(i.Responsable, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = color;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);
                //if (unidades == null)
                //{
                //    cell = new PdfPCell(new Paragraph(""));
                //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //    cell.BackgroundColor = color;
                //    cell.BorderWidth = 0;
                //    tabla.AddCell(cell);

                //    cell = new PdfPCell(new Paragraph(""));
                //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //    cell.BackgroundColor = color;
                //    cell.BorderWidth = 0;
                //    tabla.AddCell(cell);
                //}
                //else
                //{
                //    var nombresUnidad = "";
                //    var nombresResponsables = "";
                //    foreach (var unidad in unidades)
                //    {
                //        nombresUnidad += unidad.Nombre + ", ";
                //        var responsable = ControlResponsable.ObtenerPorUnidadIdEventoId(new object[] { unidad.Id, infoDetencion.IdEvento });

                //        if(responsable != null)
                //        {
                //            nombresResponsables += responsable.Descripcion + "-" + responsable.Nombre + ", ";
                //        }
                //    }

                //    if (nombresUnidad.Length > 0 && nombresUnidad.Substring(nombresUnidad.Length - 2) == ", ")
                //    {
                //        nombresUnidad = nombresUnidad.Substring(0, nombresUnidad.Length - 2);
                //    }

                //    if (nombresResponsables.Length > 0 && nombresResponsables.Substring(nombresResponsables.Length - 2) == ", ")
                //    {
                //        nombresResponsables = nombresResponsables.Substring(0, nombresResponsables.Length - 2);
                //    }

                //    cell = new PdfPCell((new Paragraph(nombresUnidad, normalfont)));
                //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //    cell.BackgroundColor = color;
                //    cell.BorderWidth = 0;
                //    tabla.AddCell(cell);

                //    cell = new PdfPCell((new Paragraph(nombresResponsables, normalfont)));
                //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //    cell.BackgroundColor = color;
                //    cell.BorderWidth = 0;
                //    tabla.AddCell(cell);
                //}

                //Motivo
                cell = new PdfPCell(new Paragraph(i.Motivo, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = color;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);
                //if (infoDetencion == null)
                //{
                //    cell = new PdfPCell(new Paragraph(""));
                //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //    cell.BackgroundColor = color;
                //    cell.BorderWidth = 0;
                //    tabla.AddCell(cell);
                //}
                //else
                //{
                //    var evento = ControlEvento.ObtenerById(infoDetencion.IdEvento);

                //    var motivo = "";
                //    var detenidoevento = ControlDetenidoEvento.ObtenerPorId(infoDetencion.IdDetenido_Evento);
                //    if(detenidoevento!=null)
                //    {
                //        var mot = ControlWSAMotivo.ObtenerPorId(detenidoevento.MotivoId);
                //        if (mot != null) motivo = mot.NombreMotivoLlamada;
                //    }

                //    cell = new PdfPCell((new Paragraph(motivo, normalfont)));
                //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //    cell.BackgroundColor = color;
                //    cell.BorderWidth = 0;
                //    tabla.AddCell(cell);
                //}

            }

            documentpdf.Add(table);
            documentpdf.Add(tabla);
        }

        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }

            return edad;
        }

        private static string CrearDirectorioRemisiones(string nombre)
        {
            try
            {

                //Verificar si existe el directorio principal del repositorio de documentos
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio de la obra existe
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio para el grupo documental existe, sino crearlo
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre)));


                return string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs//" + nombre);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("No fue posible crear el repositorio para crear el archivo. ", ex.Message));
            }
        }

        public static object ReporteProximosaCumplir(int centro)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(2); //milimetros para los margenes
            var space = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentpdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                //Nombre y ubicación archivo
                string nombrearchivo = "ProximosACumplir" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioRemisiones("ProximosACumplir"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                //Definición de fuentes
                string pathfont = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont calibrifont = BaseFont.CreateFont(pathfont, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font titlefont = new Font(calibrifont, 12, Font.BOLD);
                Font boldfont = new Font(calibrifont, 10f, Font.BOLD);
                Font normalfont = new Font(calibrifont, 10f, Font.NORMAL);
                Font bigfont = new Font(calibrifont, 10, Font.BOLD);
                Font smallfont = new Font(calibrifont, 8, Font.NORMAL);
                Font bigtitlefont = new Font(calibrifont, 14, Font.BOLD);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentpdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return obj = new { success = false, file = "", message = ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();

                documentpdf.Open();

                TitleProximosaCumplir(documentpdf, titlefont, boldfont, normalfont, space, bigfont, centro, bigtitlefont);
                GetDatosProximosaCumplir(documentpdf, boldfont, normalfont, space, smallfont, centro);

                obj = new { success = true, file = ubicacionarchivo, message = "" };
                return obj;
            }
            catch (Exception ex)
            {
                return obj = new { success = false, file = "", message = ex.Message };
            }
            finally
            {
                if (documentpdf.IsOpen())
                {
                    documentpdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TitleProximosaCumplir(Document documentpdf, Font titlefont, Font boldfont, Font normalfont, float space, Font bigfont, int centroId, Font bigtitle)
        {
            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);
            Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);

            string nombreEstado = "Estado";
            string nombreMunicipio = "Municipio";
            string nombreCentro = institucion.Nombre;

            if (domicilio != null)
            {
                Entity.Estado estado = ControlEstado.ObtenerPorId(Convert.ToInt32(domicilio.EstadoId));

                if(estado != null)
                {
                    nombreEstado = estado.Nombre;
                    Entity.Municipio municipio = ControlMunicipio.Obtener(Convert.ToInt32(domicilio.MunicipioId));
                    if(municipio != null) nombreMunicipio = municipio.Nombre;
                }
            }
            
            
            float[] width = new float[] { 100 };
            PdfPTable table = new PdfPTable(1);
            table.SetWidths(width);
            table.WidthPercentage = 100;
            table.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell cell;

            string title = "Juzgado Calificador";
            string lugar = nombreCentro;

            cell = new PdfPCell(new Phrase("Ayuntamiento de "+nombreMunicipio+","+nombreEstado, titlefont));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Padding = space;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(title, titlefont));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Padding = space;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(nombreMunicipio.ToUpper()+","+nombreEstado.ToUpper(), bigtitle));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Padding = space;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Detenidos próximos a cumplir", bigtitle));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Padding = space;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(" "));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Delegación: "+lugar, boldfont));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(" "));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);

            documentpdf.Add(table);
        }

        private static void GetDatosProximosaCumplir(Document documentpdf, Font boldfont, Font normalfont, float space, Font smallfont, int centroId)
        {
            var internos = ControlDetenido.ObteneTodos();
            string fechaSalida;

            List<internoAux> listaInterno = new List<internoAux>();
            foreach (var ka in internos)
            {
                fechaSalida = "";
                var estatusI = ControlDetalleDetencion.ObtenerPorDetenidoId(ka.Id);
                foreach(var ei in estatusI)
                {
                    if(ei.CentroId == centroId && ei.Activo)
                    {
                        var calificacionI = ControlCalificacion.ObtenerPorInternoId(ka.Id);
                        if(calificacionI != null)
                        {
                            DateTime suma = Convert.ToDateTime(ei.Fecha).AddHours(calificacionI.TotalHoras);
                            fechaSalida = suma.ToString();
                        }
                        listaInterno.Add(new internoAux() { Id = ka.Id.ToString(), nombre = ka.Nombre, paterno = ka.Paterno, materno = ka.Materno, fecha = fechaSalida });

                    }
                }
            }

            listaInterno.Sort((x, y) => x.fecha.CompareTo(y.fecha));

            int contador = 0;

            float[] width = new float[] { 100 };
            PdfPTable table = new PdfPTable(1);
            table.SetWidths(width);
            table.WidthPercentage = 1;
            table.HorizontalAlignment = Element.ALIGN_CENTER;

            float[] w2 = new float[] { 35, 90, 90, 50, 25, 35, 90, 100 };
            PdfPTable tabla = new PdfPTable(8);
            tabla.SetWidths(w2);
            tabla.WidthPercentage = 100;
            PdfPCell cell;

            cell = new PdfPCell(new Phrase("Remisión", smallfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(179, 220, 243);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Nombre", smallfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(179, 220, 243);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Registro", smallfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(179, 220, 243);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Situación", smallfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(179, 220, 243);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Hrs", smallfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(179, 220, 243);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Multa", smallfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(179, 220, 243);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Cumplimiento", smallfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(179, 220, 243);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("A Disposición", smallfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(179, 220, 243);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.AddCell(cell);

            foreach (var i in listaInterno)
            {
                bool centro = false;
                var remision = "";
                List<Entity.DetalleDetencion> estatus = ControlDetalleDetencion.ObtenerPorDetenidoId(Convert.ToInt32(i.Id));

                if (estatus != null)
                {
                    foreach (var k in estatus)
                    {
                        if (k.CentroId == centroId && k.Activo == true)
                        {
                            centro = true;
                            remision = k.Expediente;
                            break;
                        }
                    }

                    if (centro == true)
                    {
                        //Remisión
                        cell = new PdfPCell(new Paragraph(remision, smallfont));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        tabla.AddCell(cell);
                        contador++;

                        //Nombre
                        tabla.AddCell(new Paragraph(i.nombre.Trim() + " " + i.paterno.Trim() + " " + i.materno.Trim(), smallfont));

                        //Fecha
                        var estatusInterno = ControlDetalleDetencion.ObtenerPorDetenidoId(Convert.ToInt32(i.Id));
                        foreach (var n in estatusInterno)
                        {
                            if (n.Estatus == 1)
                            {
                                tabla.AddCell(new Paragraph(n.Fecha.ToString(), smallfont));
                            }
                        }

                        //Situación
                        Entity.Calificacion calificacion = ControlCalificacion.ObtenerPorInternoId(Convert.ToInt32(i.Id));
                        if (calificacion != null)
                        {
                            Entity.Catalogo situacion = ControlCatalogo.Obtener(calificacion.SituacionId, 89);
                            if (situacion != null)
                            {
                                cell = new PdfPCell(new Paragraph(situacion.Nombre, smallfont));
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                tabla.AddCell(cell);
                            }
                            else tabla.AddCell(new Paragraph(""));
                        }
                        else tabla.AddCell(new Paragraph(""));

                        //Horas
                        if(calificacion != null)
                        {
                            cell = new PdfPCell(new Paragraph(calificacion.TotalHoras.ToString(), smallfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            tabla.AddCell(cell);
                        }
                        else tabla.AddCell(new Paragraph(""));

                        //Multa
                        if (calificacion != null)
                        {
                            cell = new PdfPCell(new Paragraph(calificacion.TotalAPagar.ToString(), smallfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            tabla.AddCell(cell);
                        }
                        else tabla.AddCell(new Paragraph(""));

                        //Cumplimiento
                        cell = new PdfPCell(new Paragraph(i.fecha, smallfont));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        tabla.AddCell(cell);

                        //A Disposición
                        Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);
                        cell = new PdfPCell(new Paragraph(institucion.Nombre, smallfont));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        tabla.AddCell(cell);
                        contador++;
                    }
                }

            }

            documentpdf.Add(table);
            documentpdf.Add(tabla);
            
        }

        #region Recibo de corte de caja

        public static object[] generarReciboCorteDeCaja(object[] data, List<Entity.EgresoIngreso> ingresos, List<Entity.EgresoIngreso> egresos, List<Entity.Cancelacion> cancelaciones, string logotipo)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //20 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Recibo_Caja_CorteDeCaja_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.CorteDeCaja"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD, blau);
                Font FuenteTituloTabla = new Font(fuentecalibri, 11f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                TituloReciboCorteDeCaja(documentoPdf, FuenteTitulo, FuenteNegrita12, data, logotipo);
                InformacionReciboCorteDeCaja(documentoPdf, FuenteNormal, FuenteNegrita, espacio, ingresos, egresos, cancelaciones);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloReciboCorteDeCaja(Document documentoPdf, Font FuenteTitulo, Font FuenteSubtitulo, object[] data, string logotipo)
        {

            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;                                 
            PdfPCell cell;

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo2 = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                logotipo2 = logo;
            }

            string pathfoto = "";
            Image image = null; ;
           try
            {
                 pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo2);

                image = iTextSharp.text.Image.GetInstance(pathfoto);
          

            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");

                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }

            //Logo
            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_LEFT;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.AddCell(cellWithRowspan);

            //Titulo  
            Celda(tabla, FuenteTitulo, "            ", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            Celda(tabla, FuenteTitulo, "   Recibo de caja", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);
            PdfPTable tablaDelegacionUsuario = new PdfPTable(1);
            float[] width = new float[] { 220 };
            tablaDelegacionUsuario.SetWidths(width);
            tablaDelegacionUsuario.DefaultCell.Border = Rectangle.NO_BORDER;
            var h = DateTime.Now.Hour.ToString();
            if (DateTime.Now.Hour < 10) h = "0" + h;
            var m = DateTime.Now.Minute.ToString();
            if (DateTime.Now.Minute < 10) m = "0" + m;
            var s = DateTime.Now.Second.ToString();
            if (DateTime.Now.Second < 10) s = "0" + s;
            var fecha = DateTime.Now.ToShortDateString() + " " + h + ":" + m + ":" + s;
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Delegación: ", data[0].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Fecha: ", fecha, 0, 0, 0);
            CeldaVacio(tablaDelegacionUsuario, 1);

            tablaDelegacionUsuario.WidthPercentage = 100;

            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaDelegacionUsuario);
            tabla.DeleteBodyRows();
        }

        private static void InformacionReciboCorteDeCaja(Document documentoPdf, Font FuenteNormal, Font FuenteNegrita, float espacio, List<Entity.EgresoIngreso> ingresos, List<Entity.EgresoIngreso> egresos, List<Entity.Cancelacion> cancelaciones)
        {
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormalAux = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegritaAux = new Font(fuentecalibri, 12f, Font.BOLD);

            PdfPTable tabla = new PdfPTable(6);//Numero de columnas de la tabla            
            tabla.WidthPercentage = 100; //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            PdfPTable tablaEgresos = new PdfPTable(6);//Numero de columnas de la tabla            
            tablaEgresos.WidthPercentage = 100; //Porcentaje ancho
            tablaEgresos.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            PdfPTable tablaCancelaciones = new PdfPTable(6);//Numero de columnas de la tabla            
            tablaCancelaciones.WidthPercentage = 100; //Porcentaje ancho
            tablaCancelaciones.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            PdfPTable tablaTotales = new PdfPTable(6);
            tablaTotales.WidthPercentage = 100;
            tablaTotales.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPTable tablaTotalesEgreso = new PdfPTable(6);
            tablaTotalesEgreso.WidthPercentage = 100;
            tablaTotalesEgreso.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPTable tablaTotalesCancelacion = new PdfPTable(6);
            tablaTotalesCancelacion.WidthPercentage = 100;
            tablaTotalesCancelacion.HorizontalAlignment = Element.ALIGN_CENTER;

            BaseColor colorrelleno = new BaseColor(0, 61, 122);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            BaseColor color2 = colorgris;
            BaseColor color3 = colorgris;

            //Titulos de las columnas de las tablas
            CeldaParaTablas(tabla, FuenteNegrita, "Movimiento", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Concepto", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Folio", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Fecha", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Persona que paga", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Subtotal", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            decimal totalIngresos = 0;
            decimal totalEgresos = 0;
            decimal totalCancelaciones = 0;

            int i = 1;
            int j = 1;
            int k = 1;
            //celdas, contenido de la tabla  
            foreach (var item in ingresos)
            {
                if (i % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                i++;
                var h = item.Fecha.Hour.ToString();
                if (item.Fecha.Hour < 10) h = "0" + h;
                var m = item.Fecha.Minute.ToString();
                if (item.Fecha.Minute < 10) m = "0" + m;
                var s = item.Fecha.Second.ToString();
                if (item.Fecha.Second < 10) s = "0" + s;
                var date = item.Fecha.ToShortDateString() + " " + h + ":" + m + ":" + s;
                CeldaParaTablas(tabla, FuenteNormal, "Ingreso", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, item.Concepto, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, item.Folio.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, date, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, item.PersonaQuePaga, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, "$" + item.Total.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                totalIngresos += item.Total;
            }

            CeldaVacio(tabla, 1);

            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Total ", " $" + totalIngresos.ToString(), 0, 0, 0);

            CeldaVacio(tablaTotales, 6);

            CeldaParaTablas(tablaEgresos, FuenteNegrita, "Movimiento", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEgresos, FuenteNegrita, "Concepto", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEgresos, FuenteNegrita, "Folio", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEgresos, FuenteNegrita, "Fecha", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEgresos, FuenteNegrita, "Persona que paga", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEgresos, FuenteNegrita, "Subtotal", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            foreach (var item in egresos)
            {
                if (j % 2 != 0)
                    color2 = colorgris;
                else color2 = colorblanco;

                j++;

                CeldaParaTablas(tablaEgresos, FuenteNormal, "Egreso", 1, 0, 1, color2, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablaEgresos, FuenteNormal, item.Concepto, 1, 0, 1, color2, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablaEgresos, FuenteNormal, item.Folio.ToString(), 1, 0, 1, color2, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablaEgresos, FuenteNormal, item.Fecha.ToString(), 1, 0, 1, color2, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablaEgresos, FuenteNormal, item.PersonaQuePaga, 1, 0, 1, color2, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablaEgresos, FuenteNormal, "$" + item.Total.ToString(), 1, 0, 1, color2, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                totalEgresos += item.Total;
            }

            CeldaVacio(tablaEgresos, 6);

            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Total ", " $" + totalEgresos.ToString(), 0, 0, 0);

            CeldaVacio(tablaTotalesEgreso, 6);

            CeldaParaTablas(tablaCancelaciones, FuenteNegrita, "Movimiento", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaCancelaciones, FuenteNegrita, "Tipo", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaCancelaciones, FuenteNegrita, "Folio", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaCancelaciones, FuenteNegrita, "Fecha", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaCancelaciones, FuenteNegrita, "Persona que paga", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaCancelaciones, FuenteNegrita, "Subtotal", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            foreach (var item in cancelaciones)
            {
                if (k % 2 != 0)
                    color3 = colorgris;
                else color3 = colorblanco;

                k++;

                CeldaParaTablas(tablaCancelaciones, FuenteNormal, "Cancelación", 1, 0, 1, color3, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablaCancelaciones, FuenteNormal, item.Tipo, 1, 0, 1, color3, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablaCancelaciones, FuenteNormal, item.Folio.ToString(), 1, 0, 1, color3, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablaCancelaciones, FuenteNormal, item.Fecha.ToString(), 1, 0, 1, color3, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablaCancelaciones, FuenteNormal, item.PersonaQuePaga, 1, 0, 1, color3, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tablaCancelaciones, FuenteNormal, "$" + item.Total.ToString(), 1, 0, 1, color3, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                totalCancelaciones += item.Total;
            }

            CeldaVacio(tablaCancelaciones, 6);

            CeldaConChunk(tablaTotalesCancelacion, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesCancelacion, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesCancelacion, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesCancelacion, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesCancelacion, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesCancelacion, FuenteNegritaAux, FuenteNormalAux, "Total ", " $" + totalCancelaciones.ToString(), 0, 0, 0);

            CeldaVacio(tablaTotalesCancelacion, 6);

            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaTotales);
            documentoPdf.Add(tablaEgresos);
            documentoPdf.Add(tablaTotalesEgreso);
            documentoPdf.Add(tablaCancelaciones);
            documentoPdf.Add(tablaTotalesCancelacion);
            documentoPdf.NewPage();
            tabla.DeleteBodyRows();
            tablaTotales.DeleteBodyRows();
            tablaTotalesEgreso.DeleteBodyRows();
            tablaTotalesCancelacion.DeleteBodyRows();
            tablaEgresos.DeleteBodyRows();
            tablaCancelaciones.DeleteBodyRows();
        }

        #endregion
    }


    public class PertenenciaAux
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Bolsa { get; set; }
        public string Cantidad { get; set; }
        public string Clasificacion { get; set; }
        public string Estatus { get; set; }
        public string Observacion { get; set; }
        public string Casillero { get; set; }
        public string FechaEntrada { get; set; }
    }
    
}
