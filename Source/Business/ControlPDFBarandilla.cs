﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Configuration;
using iTextSharp.text.pdf.draw;
using System.Linq;
using System.Web.Security;

namespace Business
{
    public static class ControlPDFBarandilla
    {

        // Excepciones y casos especiales
        static Boolean bolHermosillo = true;
         
        //Crear directorio 
        private static string CrearDirectorioControl(string nombre)
        {
            try
            {

                //Verificar si existe el directorio principal del repositorio de documentos
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio de la obra existe
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio para el grupo documental existe, sino crearlo
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre)));

             

                return string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs//" + nombre);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("No fue posible crear el repositorio para crear el archivo. ", ex.Message));
            }
        }
        public partial class HeaderFooter4 : PdfPageEventHelper
        {
            PdfContentByte cb;
            PdfTemplate headerTemplate;
            BaseFont bf = null;
            float tamanofuente = 8f;
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                }
                catch (DocumentException de) { }
                catch (System.IO.IOException ioe) { }
            }

            public override void OnEndPage(PdfWriter writer, Document doc)
            {


                //Encabezado de Página
                Rectangle pageSize = doc.PageSize;
                PdfPTable footerTbl = new PdfPTable(1);
                BaseColor blau = new BaseColor(0, 61, 122);

                float[] TablaAncho = new float[] { 100 };
                footerTbl.SetWidths(TablaAncho);
                footerTbl.TotalWidth = 550;
                Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                PdfPCell cell;
                CeldaParaTablas(footerTbl, FuenteCelda, " ", 1, 0, 1, blau, 0, Element.ALIGN_TOP, Element.ALIGN_TOP);


                string pagina = "Página " + writer.PageNumber + " de ";
                //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
                {
                    float margenbottom = 11f;
                    float margenright = 280f;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, tamanofuente);
                    cb.SetTextMatrix(doc.PageSize.GetBottom(margenright), doc.PageSize.GetBottom(margenbottom));
                    //cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowText(pagina);
                    cb.EndText();
                    float len = bf.GetWidthPoint(pagina, tamanofuente);
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));
                    var d = DateTime.Now.Day.ToString();
                    if (DateTime.Now.Day < 10) d = "0" + d;
                    var mess = DateTime.Now.Month.ToString();
                    if (DateTime.Now.Month < 10) mess = "0" + mess;
                    var year = DateTime.Now.Year.ToString();
                    var h = DateTime.Now.Hour.ToString();
                    if (DateTime.Now.Hour < 10) h = "0" + h;
                    var min = DateTime.Now.Minute.ToString();
                    if (DateTime.Now.Minute < 10) min = "0" + min;
                    var marca = usId.ToString() + "-" + d + mess + year + "-" + h + min;

                    string marcadeagua = "      "+ marca+"       ";
                    //float positionx = writer.PageSize.Right/2;
                    //float positiony = writer.PageSize.Top / 2;
                    float fontsize = 15f;
                    float rotation = 0f;
                    float positionx = 100;
                    float positiony = 50;

                    BaseFont bf2 = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
                    cb.BeginText();
                    BaseColor tenue = new BaseColor(232, 232, 232);
                    //cb.SetColorFill(BaseColor.LIGHT_GRAY);
                    cb.SetColorFill(tenue);
                    cb.SetFontAndSize(bf2, fontsize);
                    BaseFont bff = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.EMBEDDED);
                    float fontsiezex = 8f;
                    int xx; int y;
                    var p = 0;
                    int posicion = 0;
                    for (xx = 0; xx < 7; xx++)
                    {
                       
                        for (y = 0; y < 29; y++)
                        {
                            if (y == 00) p = 0;
                                switch (p)
                                {
                                    case 0:
                                        posicion = -20;
                                        break;
                                    case 1:
                                        posicion = -10;
                                        break;
                                    case 2:
                                        posicion = 0;
                                        break;
                                    case 3:
                                        posicion = 12;
                                        break;
                                    case 4:
                                        posicion = 20;
                                        break;

                                }
                                positionx = positionx+posicion;
                            
                            cb.SetFontAndSize(bff, fontsiezex);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, marcadeagua, positionx, positiony, rotation);
                            
                            positiony = positiony + 30;
                            p++;
                            if (p == 5) p = 0;
                        }
                        
                        positionx = positionx + 100;
                        
                        
                        positiony = 50;

                        
                    }
                    cb.EndText();
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));

                }

                PdfPTable footerTbl2 = new PdfPTable(2);


                float[] TablaAncho2 = new float[] { 100, 100 };
                footerTbl2.SetWidths(TablaAncho2);
                footerTbl2.TotalWidth = doc.Right - doc.Left;
                Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                string dia = String.Format("{0:dd}", DateTime.Now);
                string anio = String.Format("{0:yyyy}", DateTime.Now);
                string hora = String.Format("{0:HH:mm}", DateTime.Now);
                string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
                string fechahora = dia + " de " + mestexto + " de " + anio + " " + hora;
                Chunk chkFecha = new Chunk(fechahora, FuenteCelda);
                Phrase phrfecha = new Phrase(chkFecha);

                //Phrase promad = new Phrase("Promad Soluciones Computarizadas " + anio, FuenteCelda2);
                Phrase promad = new Phrase("", FuenteCelda2);
                cell = new PdfPCell(promad);
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);

                cell = new PdfPCell(phrfecha);
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);
                footerTbl2.WriteSelectedRows(0, -1, 5, 25, writer.DirectContent);
            }
            
            private static string ObtenerMesTexto(string mes)
            {
                string mestexto = "";
                switch (mes)
                {
                    case "01":
                        mestexto = "Enero";
                        break;
                    case "02":
                        mestexto = "Febrero";
                        break;
                    case "03":
                        mestexto = "Marzo";
                        break;
                    case "04":
                        mestexto = "Abril";
                        break;
                    case "05":
                        mestexto = "Mayo";
                        break;
                    case "06":
                        mestexto = "Junio";
                        break;
                    case "07":
                        mestexto = "Julio";
                        break;
                    case "08":
                        mestexto = "Agosto";
                        break;
                    case "09":
                        mestexto = "Septiembre";
                        break;
                    case "10":
                        mestexto = "Octubre";
                        break;
                    case "11":
                        mestexto = "Noviembre";
                        break;
                    case "12":
                        mestexto = "Diciembre";
                        break;
                    default:
                        mestexto = " ";
                        break;
                }
                return mestexto;
            }


            //Necesario para incluir pagina x de y al header
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                bf= BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                base.OnCloseDocument(writer, document);
                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, tamanofuente);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.SetColorFill(BaseColor.BLACK);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

            }
        }
        //para crear el Header y Footer
        public partial class HeaderFooter : PdfPageEventHelper
        {
            PdfContentByte cb;
            PdfTemplate headerTemplate;
            BaseFont bf = null;
            float tamanofuente = 8f;

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                }
                catch (DocumentException de) { }
                catch (System.IO.IOException ioe) { }
            }

            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                //Encabezado de Página
                Rectangle pageSize = doc.PageSize;
                PdfPTable footerTbl = new PdfPTable(1);
                BaseColor blau = new BaseColor(0, 61, 122);

                float[] TablaAncho = new float[] { 100 };
                footerTbl.SetWidths(TablaAncho);
                footerTbl.TotalWidth = 550;
                Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                PdfPCell cell;
                CeldaParaTablas(footerTbl, FuenteCelda, " ", 1, 0, 1, blau, 0, Element.ALIGN_TOP, Element.ALIGN_TOP);


                string pagina = "Página " + writer.PageNumber + " de ";
                //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
                {
                    float margenbottom = 11f;
                    float margenright = 280f;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, tamanofuente);
                    cb.SetTextMatrix(doc.PageSize.GetBottom(margenright), doc.PageSize.GetBottom(margenbottom));
                    cb.ShowText(pagina);
                    cb.EndText();
                    float len = bf.GetWidthPoint(pagina, tamanofuente);
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));
                }

                PdfPTable footerTbl2 = new PdfPTable(2);


                float[] TablaAncho2 = new float[] { 100, 100 };
                footerTbl2.SetWidths(TablaAncho2);
                footerTbl2.TotalWidth = doc.Right - doc.Left;
                Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                string dia = String.Format("{0:dd}", DateTime.Now);
                string anio = String.Format("{0:yyyy}", DateTime.Now);
                string hora = String.Format("{0:HH:mm}", DateTime.Now);
                string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
                string fechahora = dia + " de " + mestexto + " de " + anio + " " + hora;
                Chunk chkFecha = new Chunk(fechahora, FuenteCelda);
                Phrase phrfecha = new Phrase(chkFecha);

                //Phrase promad = new Phrase("Promad Soluciones Computarizadas " + anio, FuenteCelda2);
                Phrase promad = new Phrase("", FuenteCelda2);
                cell = new PdfPCell(promad);
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);

                cell = new PdfPCell(phrfecha);
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);
                footerTbl2.WriteSelectedRows(0, -1, 5, writer.PageSize.GetBottom(doc.BottomMargin) + 6, writer.DirectContent);
            }


            //Necesario para incluir pagina x de y al header
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);
                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, tamanofuente);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

            }
        }

        public partial class HeaderFooter3 : PdfPageEventHelper
        {
            PdfContentByte cb;
            PdfTemplate headerTemplate;
            BaseFont bf = null;
            float tamanofuente = 8f;

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                }
                catch (DocumentException de) { }
                catch (System.IO.IOException ioe) { }
            }

            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                //Encabezado de Página
                Rectangle pageSize = doc.PageSize;
                PdfPTable footerTbl = new PdfPTable(1);
                BaseColor blau = new BaseColor(0, 61, 122);

                float[] TablaAncho = new float[] { 100 };
                footerTbl.SetWidths(TablaAncho);
                footerTbl.TotalWidth = 550;
                Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                PdfPCell cell;
                CeldaParaTablas(footerTbl, FuenteCelda, " ", 1, 0, 1, blau, 0, Element.ALIGN_TOP, Element.ALIGN_TOP);


                string pagina = "Página " + writer.PageNumber + " de ";
                //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
                {
                    float margenbottom = 11f;
                    float margenright = 280f;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, tamanofuente);
                    cb.SetTextMatrix(doc.PageSize.GetBottom(margenright), doc.PageSize.GetBottom(margenbottom));
                    cb.ShowText(pagina);
                    cb.EndText();
                    float len = bf.GetWidthPoint(pagina, tamanofuente);
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));
                }

                PdfPTable footerTbl2 = new PdfPTable(2);


                float[] TablaAncho2 = new float[] { 100, 100 };
                footerTbl2.SetWidths(TablaAncho2);
                footerTbl2.TotalWidth = (doc.Right - doc.Left)+30;
                Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                string dia = String.Format("{0:dd}", DateTime.Now);
                string anio = String.Format("{0:yyyy}", DateTime.Now);
                string hora = String.Format("{0:HH:mm}", DateTime.Now);
                string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
                string fechahora = dia + " de " + mestexto + " de " + anio + " " + hora;
                Chunk chkFecha = new Chunk(fechahora, FuenteCelda);
                Phrase phrfecha = new Phrase(chkFecha);

                //Phrase promad = new Phrase("Promad Soluciones Computarizadas " + anio, FuenteCelda2);
                Phrase promad = new Phrase("", FuenteCelda2);
                cell = new PdfPCell(promad);
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);

                cell = new PdfPCell(phrfecha);
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);
                footerTbl2.WriteSelectedRows(0, -1, 5, 25, writer.DirectContent);
            }


            //Necesario para incluir pagina x de y al header
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);
                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, tamanofuente);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

            }
        }

        public static void CeldaConChunk(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, int bordo, int horizontal, int vertical)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk(valor, FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = horizontal;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }

        //Crear celdas para tablas
        public static void CeldaParaTablas(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, BaseColor colorrelleno, float espacio, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                celda.BackgroundColor = colorrelleno;
                celda.Padding = espacio;
                celda.UseAscender = true;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas para tablas
        public static void CuerpoParaCelda(PdfPTable tabla, Font Fuente, string informacion, int colspan, int bordo, BaseColor colorrelleno, float espacio, int horizontal)
        {
            PdfPCell celda;
            celda = new PdfPCell(new Paragraph(informacion, Fuente));
            celda.Border = 0;
            celda.HorizontalAlignment = horizontal;
            celda.UseAscender = true;
            tabla.AddCell(celda);
        }

        //Crear celdas vacias
        public static void CeldaVacio(PdfPTable tabla, int cantidad)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                celda.BorderWidth = 0;
                tabla.AddCell(celda);
            }

        }
        #region ficha de investigacion
        public static object generarFichaInvestigacion(Entity.DetalleDetencion Detalle, int numeroreporte)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);

            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;
            try
            {
                string nombreArchivo = "Ficha_Investigacion_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                string directorio = CrearDirectorioControl("01.Fichainvestigacion"); directorio = directorio.Replace("//", "/");
                string ubicacionArchivo = directorio + "/" + nombreArchivo;
                string pathArchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = pathArchivo + "\\" + nombreArchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    obj = new { exitoso = false, ubicacionarchivo = "", mensaje = "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                    return obj;
                }
                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);
                Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
                Font FuenteBlau = new Font(fuentecalibri, 12f, Font.NORMAL, blau);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD,blau);
                Font FuenteTituloTabla = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.WHITE);
                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return obj = new { exitoso = false, ubicacionarchivo = "", mensaje = ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter4();
                documentoPdf.Open();
                titleFichainvestigacion(documentoPdf, FuenteTitulo,FuenteTituloTabla, FuenteNegrita, FuenteNormal, espacio, Detalle,FuenteNegrita12,numeroreporte);


                obj = new { exitoso = true, ubicacionarchivo = ubicacionArchivo, mensaje = "" };
                return obj;

            }
            catch (Exception ex)
            {
                return obj = new { exitoso = false, ubicacionarchivo = "", mensaje = ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        public static void titleFichainvestigacion(Document documentoPdf, Font FuenteTitulo, Font FuentetituloTabla, Font boldfont, Font normalfont, float space, Entity.DetalleDetencion detalle, Font fuenteNegrita, int numeroreporte)
        {

            #region informacion
            PdfPTable tablaTitulo = new PdfPTable(1);
            tablaTitulo.HorizontalAlignment = Element.ALIGN_CENTER;
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            // assign image path
            string logo = "~/Content/img/Logo.png";
            string escudo = "~/Content/img/Escudo.jpg";
            string pathEscudo = System.Web.HttpContext.Current.Server.MapPath(escudo);           

            var logoDER = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logoDER != "undefined" && logo != "")
            {
                logotipo = logoDER;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }
            var logoizq = subcontrato.Banner;
            if (logoizq != "undefined" && !string.IsNullOrEmpty(logoizq))
            {
                logo = logoizq;
            }

            string pathLogo = System.Web.HttpContext.Current.Server.MapPath(logo);
            iTextSharp.text.Image imageEscudo = null;
            iTextSharp.text.Image imageLogo = null;
          if  (logoizq != "undefined" && !string.IsNullOrEmpty(logoizq))
                {
                try
                {
                    imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
                    imageLogo.ScaleAbsoluteHeight(89f);
                    imageLogo.ScaleAbsoluteWidth(300f);
                }
                catch (Exception)
                {
                    pathLogo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
                    imageLogo.ScaleAbsoluteHeight(89f);
                    imageLogo.ScaleAbsoluteWidth(200f);
                }
            }
          else
            {
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
                imageLogo.ScaleAbsoluteHeight(89f);
                imageLogo.ScaleAbsoluteWidth(200f);
            }
            try
            {
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }
            catch (Exception)
            {
                pathEscudo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }

            

            //imageLogo.ScalePercent(85f);
            //image.ScalePercent(12f);
           

            image.ScaleAbsoluteHeight(60f);
            image.ScaleAbsoluteWidth(70f);
            Paragraph para = new Paragraph();
            Phrase ph1 = new Phrase();
            Chunk glue = new Chunk(new iTextSharp.text.pdf.draw.VerticalPositionMark());
            Paragraph main = new Paragraph();
            ph1.Add(new Chunk(imageLogo, 0, 0, true));
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(image, 0, 15, true));
            main.Add(ph1);
            para.Add(main);
            documentoPdf.Add(para);
            //var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            //var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
            var contrato = ControlContrato.Obtenerporsubcontrato(subcontrato.Id);
            var instituto = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);

            //Titulo

            Celda(tablaTitulo, FuenteTitulo, "Ficha de investigación", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaVacio(tablaTitulo, 1);
            documentoPdf.Add(tablaTitulo);
            var nombrecompleto = detalle.NombreDetenido + " " + detalle.APaternoDetenido + " " + detalle.AMaternoDetenido;
            var altura = "";
            var antrometia = ControlAntropometria.ObtenerPorDetenidoId(detalle.DetenidoId);
            if (antrometia != null)
            {
                altura = antrometia.Estatura.ToString() + "mts.";
            }
            var alias = "";
            var A = ControlAlias.ObteneTodos().Where(x => x.DetenidoId == detalle.DetenidoId);
            var t = 1;
            foreach (var item in A)
            {
                if (t == 1)
                {
                    alias = item.Nombre;
                }
                else
                {
                    alias = alias + ", " + item.Nombre;
                }
                t = t + 1;
            }
            var general = ControlGeneral.ObtenerPorDetenidoId(detalle.DetenidoId);
            var interno = ControlDetenido.ObtenerPorId(detalle.DetenidoId);
            var direccion = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
            var domicilio = "";
            var cp = "";
            var motivodetencion = "";
            var unidad = "";
            var responsable = "";
            var fechaevento = "";
            var horaevento = "";
            var barrio = "";
            var direccionevento = "";
            if (direccion != null)
            {
                domicilio = direccion.Calle + " " + direccion.Numero;
                var colonia = ControlColonia.ObtenerPorId(Convert.ToInt32(direccion.ColoniaId));
                if (colonia != null)
                {
                    var municipio = ControlMunicipio.Obtener(colonia.IdMunicipio);
                    var estado = ControlEstado.Obtener(municipio.EstadoId);
                    var pais = ControlCatalogo.Obtener(estado.IdPais, Convert.ToInt32(Entity.TipoDeCatalogo.pais));
                    barrio = colonia.Asentamiento + ", " + municipio.Nombre + ", " + estado.Nombre + ", " + pais.Nombre;
                    cp = colonia.CodigoPostal;
                }
            }
            var coloniaevento = "";
            var cpevento = "";
            var info = ControlInformacionDeDetencion.ObtenerPorInternoId(detalle.DetenidoId);
            if (info != null)
            {
                var detenidoevento = ControlDetenidoEvento.ObtenerPorId(info.IdDetenido_Evento);
                if (detenidoevento != null)
                {
                    var motivo = ControlWSAMotivo.ObtenerPorId(detenidoevento.MotivoId);
                    if (motivo != null)
                    {
                        motivodetencion = motivo.NombreMotivoLlamada;
                    }
                }
                var unidadevento = ControlEventoUnidadResponsable.ObtenerTodosByEventoId(info.IdEvento);
                if (unidadevento != null && unidadevento.Count > 0)
                {
                    var u = 1;
                    var res = 1;

                    foreach (var item in unidadevento)
                    {
                        var uni = ControlUnidad.ObtenerById(item.UnidadId);
                        if (uni != null)
                        {
                            if (u == 1)
                            {
                                unidad = uni.Nombre;
                            }
                            else
                            {
                                unidad = unidad + ", " + uni.Nombre;
                            }
                            u = u + 1;
                        }

                        var r = ControlCatalogo.Obtener(item.ResponsableId, Convert.ToInt32(Entity.TipoDeCatalogo.responsable_unidad));
                        if (r != null)
                        {
                            if (res == 1)
                            {
                                responsable = r.Nombre;
                            }
                            else
                            {
                                responsable = responsable + ", " + r.Nombre;
                            }
                            res = res + 1;
                        }

                    }
                }


                var evento = ControlEvento.ObtenerById(info.IdEvento);
                if (evento != null)
                {
                    fechaevento = evento.HoraYFecha.ToShortDateString();
                    var h1 = evento.HoraYFecha.Hour.ToString();
                    if (evento.HoraYFecha.Hour < 10) h1 = "0" + h1;
                    var m1 = evento.HoraYFecha.Minute.ToString();
                    if (evento.HoraYFecha.Minute < 10) m1 = "0" + m1;
                    var s1 = evento.HoraYFecha.Second.ToString();
                    if (evento.HoraYFecha.Second < 10) s1 = "0" + s1;
                    horaevento = h1 + ":" + m1 + ":" + s1;
                    direccionevento = evento.Lugar;
                    var colonia = ControlColonia.ObtenerPorId(evento.ColoniaId);
                    if (colonia != null)
                    {
                        var municipio = ControlMunicipio.Obtener(colonia.IdMunicipio);
                        var estado = ControlEstado.ObtenerPorId(municipio.EstadoId);
                        var pais = ControlCatalogo.Obtener(estado.IdPais, Convert.ToInt32(Entity.TipoDeCatalogo.pais));
                        coloniaevento = colonia.Asentamiento + ", " + municipio.Nombre + ", " + estado.Nombre + ", " + pais.Nombre;
                        cpevento = colonia.CodigoPostal;
                    }
                }
            }
            var nacimiento = "";
            if (general != null)
            {
                if (general.FechaNacimineto > DateTime.MinValue)
                {
                    nacimiento = general.FechaNacimineto.ToShortDateString();

                }
            }
            var cpsicofisiologico = "";
            var cquimico = "";

            var cpsobervaciones = "";
            var cpsconcluciones = "";
            var clobservaciones = "";

            var medico = ControlServicoMedico.ObtenerPorDetalleDetencionId(detalle.Id);
            var cps = 1;
            var cl = 1;
            var cq = 1;
            var ss1 = 1;
            var ss3 = 0;
            var etanol = "";
            var cannabis = "";
            var Benzodiazepina = "";
            var cocaina = "";
            var anfetamina = "";
            var extasis = "";
            foreach (var item in medico)
            {
                var psicofisiologico = ControlCertificadoPsicoFisiologico.ObtenerPorId(item.CertificadoMedicoPsicofisiologicoId);
                if (psicofisiologico != null)
                {
                    if (cps == 1)
                    {
                        cpsicofisiologico = psicofisiologico.Folio + "/" + psicofisiologico.Id.ToString();
                        cpsobervaciones = psicofisiologico.Observacion_extra;


                    }
                    else
                    {

                        cpsicofisiologico = cpsicofisiologico + ", " + psicofisiologico.Folio + "/" + psicofisiologico.Id.ToString();
                        cpsobervaciones = cpsobervaciones + ", " + psicofisiologico.Observacion_extra;
                    }
                    cps = cps + 1;
                    var concluciones = "";
                    var ss2 = 1;
                    var conclucionescervicio = ControlCertificadoPsicoFisologicoConclusionDetalle.Obtener(psicofisiologico.ConclusionId);
                    foreach (var sps in conclucionescervicio)
                    {
                        var conclucion = ControlCatalogo.Obtener(sps.ConclusionId, Convert.ToInt32(Entity.TipoDeCatalogo.Conclusion));
                        if (conclucion != null)
                        {
                            if (ss2 == 1)
                            {
                                concluciones = conclucion.Nombre;
                            }
                            else
                            {
                                concluciones = concluciones + ", " + conclucion.Nombre;
                            }
                        }
                        ss2++;
                    }
                    if(concluciones!="")
                    {
                        if(ss1==1)
                        {
                            cpsconcluciones = concluciones;
                        }
                        else
                        {
                            cpsconcluciones = cpsconcluciones + ", " + concluciones;
                        }
                        ss1++;
                    }
                }

                var lesiones = ControlCertificadoLesiones.ObtenerPorId(item.CertificadoLesionId);
                if (lesiones != null)
                {
                    if (cl == 1)
                    {
                        if (!string.IsNullOrEmpty(lesiones.Observaciones_lesion))
                        {
                            clobservaciones = lesiones.Observaciones_lesion;
                            cl = cl + 1;
                        }

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(lesiones.Observaciones_lesion))
                        {
                            clobservaciones = clobservaciones + ", " + lesiones.Observaciones_lesion;
                            cl = cl + 1;
                        }
                    }
                }
                var quimico = ControlCertificadoQuimico.ObtenerPorId(item.CertificadoQuimicoId);
                if (quimico != null)
                {
                    if (cq == 1)
                    {
                        if (!string.IsNullOrEmpty(quimico.Foliocertificado))
                        {
                            cquimico = quimico.Foliocertificado;
                            cq = cq + 1;
                        }

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(quimico.Foliocertificado))
                        {
                            cquimico = cquimico + ", " + quimico.Foliocertificado;
                            cq = cq + 1;
                        }
                    }
                    if (quimico.EtanolId == 2)
                    {
                        etanol = "Positivo" + "(" + quimico.Grado.ToString() + "mg/dL)";
                    }
                    else
                    {
                        etanol = "";
                    }
                    if (quimico.CannabisId == 2)
                    {
                        cannabis = "Positivo";
                    }
                    else
                    {
                        cannabis = "";
                    }
                    if (quimico.BenzodiapinaId == 2)
                    {
                        Benzodiazepina = "Positivo";
                    }
                    else
                    {
                        Benzodiazepina = "";

                    }
                    if (quimico.CocainaId == 2)
                    {
                        cocaina = "Positivo";

                    }
                    else
                    {
                        cocaina = "";
                    }
                    if (quimico.AnfetaminaId == 2)
                    {
                        anfetamina = "Positivo";

                    }
                    else
                    {
                        anfetamina = "";
                    }
                    if (quimico.ExtasisId == 2)
                    {
                        extasis = "Positivo";

                    }
                    else
                    {
                        extasis = "";
                    }
                }
            }
            var situacion = "Pendiente";
            var calificacion = ControlCalificacion.ObtenerPorInternoId(detalle.Id);
            if (calificacion != null)
            {
                var situaciones = ControlCatalogo.Obtener(calificacion.SituacionId, Convert.ToInt32(Entity.TipoDeCatalogo.situacion_detenido));
                if (situaciones != null)
                    situacion = situaciones.Nombre;
            }
            var estatus = ControlCatalogo.Obtener(detalle.Estatus, Convert.ToInt32(Entity.TipoDeCatalogo.estatus));

            var salida = ControlSalidaEfectuadaJuez.ObtenerByDetalleDetencionId(detalle.Id);
            var tipo = "";
            var fundamento = "";
            var fechasalida = "";

            if (salida != null)
            {
                var tiposalida = ControlCatalogo.Obtener(salida.TiposalidaId, Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));
                if (tiposalida != null)
                {
                    tipo = tiposalida.Nombre;
                }
                fundamento = salida.Fundamento;
                var historial = ControlHistorial.ObtenerPorDetenido(detalle.Id).Where(x => x.Movimiento == "Salida efectuada del detenido").LastOrDefault();
                if (historial != null)
                {
                    var h = historial.Fecha.Hour.ToString();
                    if (historial.Fecha.Hour < 10) h = "0" + h;
                    var m = historial.Fecha.Minute.ToString();
                    if (historial.Fecha.Minute < 10) m = "0" + m;
                    var s = historial.Fecha.Second.ToString();
                    if (historial.Fecha.Second < 10) s = "0" + s;
                    fechasalida = historial.Fecha.ToShortDateString() + " " + h + ":" + m + ":" + s;
                }
                else
                {
                    historial = ControlHistorial.ObtenerPorDetenido(detalle.Id).Where(x => x.Movimiento == "Traslado del detenido").LastOrDefault();
                    if (historial != null)
                    {
                        var h = historial.Fecha.Hour.ToString();
                        if (historial.Fecha.Hour < 10) h = "0" + h;
                        var m = historial.Fecha.Minute.ToString();
                        if (historial.Fecha.Minute < 10) m = "0" + m;
                        var s = historial.Fecha.Second.ToString();
                        if (historial.Fecha.Second < 10) s = "0" + s;
                        fechasalida = historial.Fecha.ToShortDateString() + " " + h + ":" + m + ":" + s;
                    }
                }


            }

            var pulgarderecho = "";
            var pulgarizquierdo = "";
            var facial = "";
            var biometricos = ControlBiometricos.GetByDetenidoId(detalle.DetenidoId);
            foreach (var item in biometricos)
            {
                if (item.Descripcion.Trim() == "Pulgar derecho plana")
                {
                    pulgarderecho = item.Rutaimagen;

                }
                if (item.Descripcion.Trim() == "Pulgar izquierdo plana")
                {
                    pulgarizquierdo = item.Rutaimagen;
                }
            }
            if (!string.IsNullOrEmpty(interno.RutaImagen))
            {
                facial = interno.RutaImagen;
            }
            else
            {
                if (biometricos != null && biometricos.Count > 0)
                {
                    if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Imagen facial frontal").LastOrDefault().Descripcion))
                    {
                        facial = biometricos.Where(x => x.Descripcion == "Imagen facial frontal").LastOrDefault().Rutaimagen;
                    }
                }
            }

            var examenesMedicos = ControlExamenMedico.ObtenerTodosPorDetalleDetencionId(detalle.Id);
            List<string> folios = new List<string>();
            List<string> fechas = new List<string>();
            List<string> indicaciones = new List<string>();
            string foliosAux = string.Empty;
            string fechasAux = string.Empty;
            string indicacionesAux = string.Empty;

            folios = examenesMedicos.Select(x => x.Id.ToString()).ToList();
            fechas = examenesMedicos.Select(x => x.Fecha.ToString("yyyy-MM-dd HH:mm:ss")).ToList();
            indicaciones = examenesMedicos.Select(x => x.IndicacionesMedicas).ToList();
            foliosAux = string.Join(",", folios);
            fechasAux = string.Join(",", fechas);
            indicacionesAux = string.Join(",", indicaciones);

            int[] width = new int[] { 50, 50 };
            PdfPTable tabla2 = new PdfPTable(2);
            tabla2.SetWidths(width);
            tabla2.TotalWidth = 550;
            tabla2.HorizontalAlignment = Element.ALIGN_LEFT;
            #endregion
            #region tabla principal
            PdfPTable tablaz = new PdfPTable(1);//Numero de columnas de la tabla
            tablaz.WidthPercentage = 100; //Porcentaje ancho
            tablaz.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            CeldaConChunk(tabla2, boldfont, normalfont, "No. remisión: ", detalle.Expediente.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, boldfont, normalfont, "No. de solicitud: ", numeroreporte.ToString(), 0, 0, 0);
            var celdatablaz = new PdfPCell(tabla2);
            celdatablaz.Border = 0;

            tablaz.AddCell(celdatablaz);
            Celda(tablaz, FuenteTitulo, "DATOS GENERALES", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            //CeldaVacio(tabla2, 1);
            CeldaConChunk(tablaz, boldfont, normalfont, "Nombre: ", nombrecompleto.ToUpper(), 0, 0, 0);
            //CeldaVacio(tabla2, 1);
            tabla2 = new PdfPTable(2);
            tabla2.SetWidths(width);
            tabla2.TotalWidth = 550;
            tabla2.HorizontalAlignment = Element.ALIGN_LEFT;
            CeldaConChunk(tabla2, boldfont, normalfont, "Estatura: ", altura, 0, 0, 0);
            CeldaConChunk(tabla2, boldfont, normalfont, "ALIAS: ", alias, 0, 0, 0);
            celdatablaz = new PdfPCell(tabla2);
            celdatablaz.Border = 0;

            tablaz.AddCell(celdatablaz);
            CeldaConChunk(tablaz, boldfont, normalfont, "Fecha de nacimiento: ", nacimiento, 0, 0, 0);
            //CeldaVacio(tabla2, 1);tabla2 = new PdfPTable(2);
            tabla2 = new PdfPTable(2);
            tabla2.SetWidths(width);
            tabla2.TotalWidth = 550;
            tabla2.HorizontalAlignment = Element.ALIGN_LEFT;
            CeldaConChunk(tabla2, boldfont, normalfont, "Dirección: ", domicilio, 0, 0, 0);
            CeldaConChunk(tabla2, boldfont, normalfont, "C.P. ", cp, 0, 0, 0);
            celdatablaz = new PdfPCell(tabla2);
            celdatablaz.Border = 0;

            tablaz.AddCell(celdatablaz);
            CeldaConChunk(tablaz, boldfont, normalfont, "Colonia: ", barrio, 0, 0, 0);
            //CeldaVacio(tabla2, 3);
            Celda(tablaz, FuenteTitulo, "         LUGAR DE DETENCIÓN", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);

            int[] width2 = new int[] { 50, 50 };
            PdfPTable tabla3 = new PdfPTable(2);
            tabla3.SetWidths(width2);
            tabla3.TotalWidth = 550;
            tabla3.HorizontalAlignment = Element.ALIGN_LEFT;
            CeldaConChunk(tabla3, boldfont, normalfont, "Dirección: ", direccionevento, 0, 0, 0);
            CeldaVacio(tabla3, 1);
            CeldaConChunk(tabla3, boldfont, normalfont, "Colonia: ", coloniaevento, 0, 0, 0);
            CeldaConChunk(tabla3, boldfont, normalfont, "C.P. ", cpevento, 0, 0, 0);
            PdfPTable tablax = new PdfPTable(1);//Numero de columnas de la tabla
            tablax.WidthPercentage = 100; //Porcentaje ancho
            tablax.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            CeldaConChunk(tablax, boldfont, normalfont, "Motivo de detención: ", motivodetencion, 0, 0, 0);
            PdfPTable tablay = new PdfPTable(2);
            tablay.SetWidths(width2);
            tablay.TotalWidth = 550;
            tablay.HorizontalAlignment = Element.ALIGN_LEFT;
            CeldaConChunk(tablax, boldfont, normalfont, "Unidad que entrega: ", unidad, 0, 0, 0);
            ////CeldaVacio(tablay, 1);
            CeldaConChunk(tablax, boldfont, normalfont, "Responsable: ", responsable, 0, 0, 0);
            //CeldaVacio(tablax, 1);

            CeldaConChunk(tablay, boldfont, normalfont, "Fecha: ", fechaevento, 0, 0, 0);
            CeldaConChunk(tablay, boldfont, normalfont, "Hora: ", horaevento, 0, 0, 0);
            //CeldaVacio(tabla3, 2);
            PdfPTable tablaw = new PdfPTable(1);//Numero de columnas de la tabla
            tablaw.WidthPercentage = 100; //Porcentaje ancho
            tablaw.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            Celda(tablaw, FuenteTitulo, "OBSERVACIONES MÉDICAS", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            //CeldaVacio(tabla3, 1);
            CeldaConChunk(tablaw, boldfont, normalfont, "Folio: ", foliosAux, 0, 0, 0);
            //CeldaVacio(tablay, 1);
            CeldaConChunk(tablaw, boldfont, normalfont, "Examen médico: ", fechasAux, 0, 0, 0);
            //CeldaVacio(tablay, 1);
            CeldaConChunk(tablaw, boldfont, normalfont, "Indicaciones médicas: ", indicacionesAux, 0, 0, 0);            
            //CeldaVacio(tabla3, 1);
            var espacio = 2;
            PdfPTable tablav = new PdfPTable(2);
            tablav.SetWidths(width2);
            tablav.TotalWidth = 550;
            tablav.HorizontalAlignment = Element.ALIGN_LEFT;
            if (etanol != "" && cannabis != "")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "   Etanol: ", etanol, 0, 0, 0);
                CeldaConChunk(tablav, boldfont, normalfont, "Cannabis: ", cannabis, 0, 0, 0);

            }
            else if (etanol == "" && cannabis != "")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "Cannabis: ", cannabis, 0, 0, 0);
                CeldaVacio(tablav, 1);
            }
            else if (etanol != "" && cannabis == "")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "   Etanol: ", etanol, 0, 0, 0);
                CeldaVacio(tablav, 1);
            }
            else espacio = espacio + 2;

            if (Benzodiazepina != "" && cocaina != "")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "   Benzodiazepina: ", Benzodiazepina, 0, 0, 0);
                CeldaConChunk(tablav, boldfont, normalfont, "   Cocaína: ", cocaina, 0, 0, 0);

            }
            else if (Benzodiazepina == "" && cocaina != "")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "   Cocaína: ", cocaina, 0, 0, 0);
                CeldaVacio(tablav, 1);
            }
            else if (Benzodiazepina != "" && cocaina == "")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "   Benzodiazepina: ", Benzodiazepina, 0, 0, 0);
                CeldaVacio(tablav, 1);
            }
            else espacio = espacio + 2;

            if (anfetamina != "" && extasis != "")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "   Anfetamina: ", anfetamina, 0, 0, 0);
                CeldaConChunk(tablav, boldfont, normalfont, "   Éxtasis: ", extasis, 0, 0, 0);

            }
            else if (anfetamina == "" && extasis != "")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "   Éxtasis: ", extasis, 0, 0, 0);
                CeldaVacio(tablav, 1);
            }
            else if (anfetamina != "" && extasis == "")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "   Anfetamina: ", anfetamina, 0, 0, 0);
                CeldaVacio(tablav, 1);
            }
            else espacio = espacio + 2;
            
            PdfPTable tabla3w = new PdfPTable(1);//Numero de columnas de la tabla
            tabla3w.WidthPercentage = 100; //Porcentaje ancho
            tabla3w.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            Celda(tabla3w, FuenteTitulo, "SITUACIÓN JURÍDICA", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            PdfPTable tabla3v = new PdfPTable(2);
            tabla3v.SetWidths(width2);
            tabla3v.TotalWidth = 550;
            tabla3v.HorizontalAlignment = Element.ALIGN_LEFT;
            CeldaConChunk(tabla3v, boldfont, normalfont, "Situación: ", situacion, 0, 0, 0);
            CeldaConChunk(tabla3v, boldfont, normalfont, "Estatus: ", estatus.Nombre.ToString(), 0, 0, 0);
            CeldaConChunk(tabla3v, boldfont, normalfont, "Tipo de salida: ", tipo, 0, 0, 0);
            CeldaVacio(tabla3v, 1);
            CeldaConChunk(tabla3v, boldfont, normalfont, "Fundamento: ", fundamento, 0, 0, 0);
            CeldaVacio(tabla3v, 1);
            CeldaConChunk(tabla3v, boldfont, normalfont, "Salida: ", fechasalida, 0, 0, 0);
            CeldaVacio(tabla3v, 1);
            int[] width5 = new int[] { 35, 65 };
            PdfPTable tabla = new PdfPTable(2);//Numero de columnas de la tabla
            //tabla.WidthPercentage = 100; //Porcentaje ancho
            tabla.SetWidths(width5);
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;//Alineación vertical}

            string pathfoto2 = System.Web.HttpContext.Current.Server.MapPath(facial);

            if (string.IsNullOrEmpty(facial))
            {
                pathfoto2 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/avatars/male.png");
                iTextSharp.text.Image  image2 = iTextSharp.text.Image.GetInstance(pathfoto2);
                image2.ScaleAbsoluteHeight(150f);
                image2.ScaleAbsoluteWidth(150f);
                PdfPCell celda = new PdfPCell(image2);
               // celda.Image.ScaleAbsolute(50, 50);
                celda.Border = 0;              
                tabla.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto2);
                    image2.ScaleAbsoluteHeight(150f);
                    image2.ScaleAbsoluteWidth(150f);
                    var celda = new PdfPCell(image2);
                    //celda.Image.ScaleAbsolute(50, 50);
                    celda.Border = 0;
                    tabla.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto2 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/avatars/male.png");

                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto2);
                    image2.ScaleAbsoluteHeight(150f);
                    image2.ScaleAbsoluteWidth(150f);

                    var celda = new PdfPCell(image2);
                    //celda.Image.ScaleAbsolute(50, 50);
                    celda.Border = 0;
                    tabla.AddCell(celda);
                }

            }
            var celdatabla2 = new PdfPCell(tablaz);
            celdatabla2.Border = 0;
            tabla.AddCell(celdatabla2);


            documentoPdf.Add(tabla);
            documentoPdf.Add(tabla3);
            documentoPdf.Add(tablax);
            documentoPdf.Add(tablay);
            documentoPdf.Add(tablaw);
            documentoPdf.Add(tablav);
            documentoPdf.Add(tabla3w);
            documentoPdf.Add(tabla3v);
            PdfPTable tabla4 = new PdfPTable(1);
            tabla4.WidthPercentage = 100; //Porcentaje ancho
            tabla4.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            string pathfoto3 = System.Web.HttpContext.Current.Server.MapPath(pulgarderecho);
            if (string.IsNullOrEmpty(pulgarderecho))
            {
                pathfoto3 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto3);
                image2.ScaleAbsoluteHeight(50f);
                image2.ScaleAbsoluteWidth(50f);
                var celda = new PdfPCell(image2);
                celda.Border = 0;
                tabla4.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto3);
                    image2.ScaleAbsoluteHeight(50f);
                    image2.ScaleAbsoluteWidth(50f);
                    var celda = new PdfPCell(image2);
                    celda.Border = 0;
                    tabla4.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto3 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto3);
                    image2.ScaleAbsoluteHeight(50f);
                    image2.ScaleAbsoluteWidth(50f);
                    var celda = new PdfPCell(image2);
                    celda.Border = 0;
                    tabla4.AddCell(celda);
                }

            }

            PdfPTable tabla5 = new PdfPTable(1);
            tabla5.WidthPercentage = 100; //Porcentaje ancho
            tabla5.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            string pathfoto4 = System.Web.HttpContext.Current.Server.MapPath(pulgarizquierdo);
            if (string.IsNullOrEmpty(pulgarizquierdo))
            {
                pathfoto4 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto4);
                image2.ScaleAbsoluteHeight(50f);
                image2.ScaleAbsoluteWidth(50f);
                var celda = new PdfPCell(image2);
                celda.Border = 0;
                tabla5.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto4);
                    image2.ScaleAbsoluteHeight(50f);
                    image2.ScaleAbsoluteWidth(50f);
                    var celda = new PdfPCell(image2);
                    celda.Border = 0;
                    tabla5.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto4 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto4);
                    image2.ScaleAbsoluteHeight(50f);
                    image2.ScaleAbsoluteWidth(50f);
                    var celda = new PdfPCell(image2);
                    celda.Border = 0;
                    tabla5.AddCell(celda);
                }

            }
            PdfPTable tabla6 = new PdfPTable(3);
            tabla6.WidthPercentage = 100; //Porcentaje ancho
            tabla6.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            CeldaVacio(tabla6, 1);
            var celdatabla3 = new PdfPCell(tabla4);
            celdatabla3.Border = 0;
            tabla6.AddCell(celdatabla3);
            celdatabla3 = new PdfPCell(tabla5);
            celdatabla3.Border = 0;
            tabla6.AddCell(celdatabla3);
            documentoPdf.Add(tabla6);
            documentoPdf.NewPage();
            #endregion
            var historial2 = ControlHistorial.ObtenerPorDetenido(detalle.Id);
            PdfPTable tabla7 = new PdfPTable(1);
            tabla7.WidthPercentage = 100; //Porcentaje ancho
            tabla7.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            CeldaVacio(tabla7, 1);
            Celda(tabla7, FuenteTitulo, "USUARIOS QUE INTERVINIERON EN EL SISTEMA", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaVacio(tabla7, 1);
            Celda(tabla7, fuenteNegrita, "Registro en barandilla", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaVacio(tabla7, 1);
            documentoPdf.Add(tabla7);
            BaseColor colorrelleno = new BaseColor(0, 61, 122);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);

            float[] w1 = new float[] { 60,40 };

            PdfPTable tabla8 = new PdfPTable(2);//Numero de columnas de la tabla            
            tabla8.WidthPercentage = 100; //Porcentaje ancho
            tabla8.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical
            tabla8.SetWidths(w1);

            //Titulos de las columnas de las tablas
            CeldaParaTablas(tabla8, FuentetituloTabla, "ACCIÓN", 1, 0, 1, colorrelleno, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla8, FuentetituloTabla, "USUARIO", 1, 0, 1, colorrelleno, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            
            var us = "";
            int ii = 1;
            var historialbarandilla = ControlHistorial.ObtenerPorDetenido(detalle.DetenidoId).Where(x => x.Movimiento.Contains("barandilla"));
            var contadorhistorialbarandilla = 0;
            foreach(var item in historialbarandilla)
            {
                contadorhistorialbarandilla++;
            }
           if(contadorhistorialbarandilla>0)
            {
                foreach(var item in historialbarandilla)
                {
                    if (ii % 2 != 0)
                        color = colorgris;
                    else
                        color = colorblanco;
                    var usuario = ControlUsuario.Obtener(item.CreadoPor);
                    if(usuario!=null)
                    {
                        if(usuario.RolId==13)
                        {
                            usuario = ControlUsuario.ObteneTodos().Where(x => x.RolId == 1).FirstOrDefault();
                        }
                        us = usuario.Nombre + " " + usuario.ApellidoPaterno.ToString() + " " + usuario.ApellidoMaterno.ToString();

                    }
                    else
                    {
                        us = "";
                    }
                    CeldaParaTablas(tabla8, normalfont, item.Movimiento.ToUpper(), 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT) ;
                    CeldaParaTablas(tabla8, normalfont, us, 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    ii++;
                }
                if (ii % 2 != 0)
                    color = colorgris;
                else
                    color = colorblanco;
                CeldaParaTablas(tabla8, FuentetituloTabla, "", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla8, FuentetituloTabla, "", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                
            }
           else
            {
                color = colorgris;
                CeldaParaTablas(tabla8, FuentetituloTabla, "", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla8, FuentetituloTabla, "", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            }

            documentoPdf.Add(tabla8);
            PdfPTable tabla9 = new PdfPTable(1);
            tabla9.WidthPercentage = 100; //Porcentaje ancho
            tabla9.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            CeldaVacio(tabla9, 1);
            
            Celda(tabla9, fuenteNegrita, "Biométricos", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaVacio(tabla9, 1);
            documentoPdf.Add(tabla9);

            PdfPTable tabla10 = new PdfPTable(2);//Numero de columnas de la tabla            
            tabla10.WidthPercentage = 100; //Porcentaje ancho
            tabla10.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical
            tabla10.SetWidths(w1);
            //Titulos de las columnas de las tablas
            CeldaParaTablas(tabla10, FuentetituloTabla, "ACCIÓN", 1, 0, 1, colorrelleno, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla10, FuentetituloTabla, "USUARIO", 1, 0, 1, colorrelleno, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            ii = 1;
            var bio = 0;
            var bio1 = 1;
            foreach(var item in historial2.Where(x => x.Movimiento.Contains("Biometrico")))
            {
                bio++;
            }
            if(bio>0)
            {
                foreach (var item in historial2.Where(x => x.Movimiento.Contains("Biometrico")))
                {
                    if (ii % 2 != 0)
                        color = colorgris;
                    else
                        color = colorblanco;
                    var usuario = ControlUsuario.Obtener(item.CreadoPor);
                    if (usuario != null)
                    {
                        if (usuario.RolId == 13)
                        {
                            usuario = ControlUsuario.ObteneTodos().Where(x => x.RolId == 1).FirstOrDefault();
                        }
                        us = usuario.Nombre + " " + usuario.ApellidoPaterno.ToString() + " " + usuario.ApellidoMaterno.ToString();

                    }
                    else
                    {
                        us = "";
                    }
                    var mensaje = "Registro";
                    if (bio1 > 1) mensaje = "Actualización";

                    CeldaParaTablas(tabla10, normalfont, mensaje.ToUpper(), 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tabla10, normalfont, us, 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    bio1++;
                    ii++;
                }
                if (ii % 2 != 0)
                    color = colorgris;
                else
                    color = colorblanco;
                CeldaParaTablas(tabla10, FuentetituloTabla, "", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla10, FuentetituloTabla, "", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            }
            else
            {
                color = colorgris;
                CeldaParaTablas(tabla10, normalfont, "", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla10, FuentetituloTabla, "", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            }
            documentoPdf.Add(tabla10);

            PdfPTable tabla11 = new PdfPTable(1);
            tabla11.WidthPercentage = 100; //Porcentaje ancho
            tabla11.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            CeldaVacio(tabla11, 1);

            Celda(tabla11, fuenteNegrita, "Médicos", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaVacio(tabla11, 1);
            documentoPdf.Add(tabla11);

            PdfPTable tabla12= new PdfPTable(2);//Numero de columnas de la tabla            
            tabla12.WidthPercentage = 100; //Porcentaje ancho
            tabla12.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical
            tabla12.SetWidths(w1);
            //Titulos de las columnas de las tablas
            CeldaParaTablas(tabla12, FuentetituloTabla, "ACCIÓN", 1, 0, 1, colorrelleno, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla12, FuentetituloTabla, "USUARIO", 1, 0, 1, colorrelleno, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            ii = 1;
            var historialfisio = ControlHistorial.ObtenerPorDetenido(detalle.DetenidoId).Where(x => x.Movimiento== "Modificación de datos generales en certificado psicofisiológico");
            var contadormedico = 0;
            foreach(var item in historialfisio)
            {
                contadormedico++;
            }
            var historialquimico = ControlHistorial.ObtenerPorDetenido(detalle.DetenidoId).Where(x => x.Movimiento == "Modificación de datos generales en certificado químico");
            
            foreach(var item in historialquimico)
            {
                contadormedico++;
            }
            var historiallesiones = ControlHistorial.ObtenerPorDetenido(detalle.DetenidoId).Where(x=> x.Movimiento== "Modificación de datos generales en certificado de lesiones");
            foreach(var item in historiallesiones)
            {
                contadormedico++;
            }
            if(contadormedico>0)
            {
                foreach (var item in historialfisio)
                {
                    if (ii % 2 != 0)
                        color = colorgris;
                    else
                        color = colorblanco;
                    var usuario = ControlUsuario.Obtener(item.CreadoPor);
                    if (usuario != null)
                    {
                        if (usuario.RolId == 13)
                        {
                            usuario = ControlUsuario.ObteneTodos().Where(x => x.RolId == 1).FirstOrDefault();
                        }
                        us = usuario.Nombre + " " + usuario.ApellidoPaterno.ToString() + " " + usuario.ApellidoMaterno.ToString();

                    }
                    else
                    {
                        us = "";
                    }
                    CeldaParaTablas(tabla12, normalfont, "PSICOFISIOLÓGICO", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tabla12, normalfont, us, 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    ii++;
                }

                foreach (var item in historiallesiones)
                {
                    if (ii % 2 != 0)
                        color = colorgris;
                    else
                        color = colorblanco;
                    var usuario = ControlUsuario.Obtener(item.CreadoPor);
                    if (usuario != null)
                    {
                        if (usuario.RolId == 13)
                        {
                            usuario = ControlUsuario.ObteneTodos().Where(x => x.RolId == 1).FirstOrDefault();
                        }
                        us = usuario.Nombre + " " + usuario.ApellidoPaterno.ToString() + " " + usuario.ApellidoMaterno.ToString();

                    }
                    else
                    {
                        us = "";
                    }
                    CeldaParaTablas(tabla12, normalfont, "LESIONES", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tabla12, normalfont, us, 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    ii++;
                }
                foreach (var item in historialquimico)
                {
                    if (ii % 2 != 0)
                        color = colorgris;
                    else
                        color = colorblanco;
                    var usuario = ControlUsuario.Obtener(item.CreadoPor);
                    if (usuario != null)
                    {
                        if (usuario.RolId == 13)
                        {
                            usuario = ControlUsuario.ObteneTodos().Where(x => x.RolId == 1).FirstOrDefault();
                        }
                        us = usuario.Nombre + " " + usuario.ApellidoPaterno.ToString() + " " + usuario.ApellidoMaterno.ToString();

                    }
                    else
                    {
                        us = "";
                    }

                    CeldaParaTablas(tabla12, normalfont, "QUÍMICO", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tabla12, normalfont, us, 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    ii++;
                }
                if (ii % 2 != 0)
                    color = colorgris;
                else
                    color = colorblanco;
                CeldaParaTablas(tabla12, FuentetituloTabla, "", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla12, FuentetituloTabla, "", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            }
            else
            {
                color = colorgris;
                CeldaParaTablas(tabla12, FuentetituloTabla, "", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla12, FuentetituloTabla, "", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            }
            documentoPdf.Add(tabla12);

            PdfPTable tabla13 = new PdfPTable(1);
            tabla13.WidthPercentage = 100; //Porcentaje ancho
            tabla13.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            CeldaVacio(tabla13, 1);

            Celda(tabla13, fuenteNegrita, "Jurídico", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaVacio(tabla13, 1);
            documentoPdf.Add(tabla13);

            PdfPTable tabla14 = new PdfPTable(2);//Numero de columnas de la tabla            
            tabla14.WidthPercentage = 100; //Porcentaje ancho
            tabla14.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical
            tabla14.SetWidths(w1);
            //Titulos de las columnas de las tablas
            CeldaParaTablas(tabla14, FuentetituloTabla, "ACCIÓN", 1, 0, 1, colorrelleno, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla14, FuentetituloTabla, "USUARIO", 1, 0, 1, colorrelleno, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            ii = 1;
            if(historial2.Count>0)
            {
                var c1 = 1;
                foreach (var item in historial2.Where(x => x.Movimiento.Contains("Registro de la calificación")))
                {
                    if (ii % 2 != 0)
                        color = colorgris;
                    else
                        color = colorblanco;
                    var usuario = ControlUsuario.Obtener(item.CreadoPor);
                    if (usuario != null)
                    {
                        if (usuario.RolId == 13)
                        {
                            usuario = ControlUsuario.ObteneTodos().Where(x => x.RolId == 1).FirstOrDefault();
                        }
                        us = usuario.Nombre + " " + usuario.ApellidoPaterno.ToString() + " " + usuario.ApellidoMaterno.ToString();

                    }
                    else
                    {
                        us = "";
                    }
                    if(c1==1)
                    {
                        CeldaParaTablas(tabla14, normalfont, "CALIFICACIÓN", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                        CeldaParaTablas(tabla14, normalfont, us, 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    }
                    else
                    {
                        CeldaParaTablas(tabla14, normalfont, "RE-CALIFICACIÓN", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                        CeldaParaTablas(tabla14, normalfont, us, 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    }
                    c1++;
                    ii++;
                }
                foreach (var item in historial2.Where(x => x.Movimiento.Contains("Salida efectuada del detenido")))
                {
                    if (ii % 2 != 0)
                        color = colorgris;
                    else
                        color = colorblanco;
                    var usuario = ControlUsuario.Obtener(item.CreadoPor);
                    if (usuario != null)
                    {
                        if (usuario.RolId == 13)
                        {
                            usuario = ControlUsuario.ObteneTodos().Where(x => x.RolId == 1).FirstOrDefault();
                        }
                        us = usuario.Nombre + " " + usuario.ApellidoPaterno.ToString() + " " + usuario.ApellidoMaterno.ToString();

                    }
                    else
                    {
                        us = "";
                    }
                    CeldaParaTablas(tabla14, normalfont, "SALIDA", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tabla14, normalfont, us, 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    ii++;
                }
                foreach (var item in historial2.Where(x => x.Movimiento.Contains("Traslado del detenido")))
                {
                    if (ii % 2 != 0)
                        color = colorgris;
                    else
                        color = colorblanco;
                    var usuario = ControlUsuario.Obtener(item.CreadoPor);
                    if (usuario != null)
                    {
                        if (usuario.RolId == 13)
                        {
                            usuario = ControlUsuario.ObteneTodos().Where(x => x.RolId == 1).FirstOrDefault();
                        }
                        us = usuario.Nombre + " " + usuario.ApellidoPaterno.ToString() + " " + usuario.ApellidoMaterno.ToString();

                    }
                    else
                    {
                        us = "";
                    }
                    CeldaParaTablas(tabla14, normalfont, "TRASLADO", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    CeldaParaTablas(tabla14, normalfont, us, 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                    ii++;
                }
                if (ii % 2 != 0)
                    color = colorgris;
                else
                    color = colorblanco;
                CeldaParaTablas(tabla14, FuentetituloTabla, "", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla14, FuentetituloTabla, "", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            }
            else
            {
                if (ii % 2 != 0)
                    color = colorgris;
                else
                    color = colorblanco;
                CeldaParaTablas(tabla14, FuentetituloTabla, "", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla14, FuentetituloTabla, "", 1, 0, 1, color, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            }
            documentoPdf.Add(tabla14);
            documentoPdf.NewPage();
            PdfPTable tabla15 = new PdfPTable(1);
            tabla15.WidthPercentage = 100; //Porcentaje ancho
            tabla15.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            CeldaVacio(tabla15, 1);

            Celda(tabla15, FuenteTitulo, "BIOMÉTRICOS", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaVacio(tabla15, 1);
            if(biometricos.Count>0)
            {
                CeldaParaTablas(tabla15, FuentetituloTabla, "Biométricos (Iris)", 1, 0, 1, colorrelleno, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            }
            else
            {
                Celda(tabla15, FuenteTitulo, "El detenido no cuenta con biométricos registrados.", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_RIGHT);
            }
            
            documentoPdf.Add(tabla15);

            var irisizq = "";
            var irisderecho = "";
            var indicederechoplano = "";
            var indiceizquierdoplano = "";
            var medioderechoplano = "";
            var medioizquierdoplano="";
            var anularderechoplano = "";
            var anularizquierdoplano = "";
            var meñiquederechoplano = "";
            var meñiqueizquierdoplano = "";
            var pulgarderechorolada = "";
            var pulgarizquierdorolada = "";
            var indiicederechorolada = "";
            var indiceizquierdorolada = "";
            var medioderechorolada = "";
            var medioizquierdorolada = "";
            var anularderechorolada = "";
            var anularizquierdorolada = "";
            var meñiquederechorolada = "";
            var meñiqueizquierdorolada = "";
            if (biometricos != null && biometricos.Count > 0)
            {
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Imagen iris izquierdo").LastOrDefault().Descripcion))
                {
                    irisizq = biometricos.Where(x => x.Descripcion == "Imagen iris izquierdo").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Imagen iris derecho").LastOrDefault().Descripcion))
                {
                    irisderecho = biometricos.Where(x => x.Descripcion == "Imagen iris derecho").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Indice derecho plana").LastOrDefault().Descripcion))
                {
                    indicederechoplano = biometricos.Where(x => x.Descripcion == "Indice derecho plana").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Indice izquierdo plana").LastOrDefault().Descripcion))
                {
                    indiceizquierdoplano = biometricos.Where(x => x.Descripcion == "Indice izquierdo plana").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Medio derecho plana").LastOrDefault().Descripcion))
                {
                    medioderechoplano = biometricos.Where(x => x.Descripcion == "Medio derecho plana").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Medio izquierdo plana").LastOrDefault().Descripcion))
                {
                    medioizquierdoplano = biometricos.Where(x => x.Descripcion == "Medio izquierdo plana").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Anular derecho plana").LastOrDefault().Descripcion))
                {
                    anularderechoplano = biometricos.Where(x => x.Descripcion == "Anular derecho plana").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Anular izquierdo plana").LastOrDefault().Descripcion))
                {
                    anularizquierdoplano = biometricos.Where(x => x.Descripcion == "Anular izquierdo plana").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Meñique derecho plana").LastOrDefault().Descripcion))
                {
                    meñiquederechoplano = biometricos.Where(x => x.Descripcion == "Meñique derecho plana").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Meñique izquierdo plana").LastOrDefault().Descripcion))
                {
                    meñiqueizquierdoplano = biometricos.Where(x => x.Descripcion == "Meñique izquierdo plana").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Pulgar derecho rolada").LastOrDefault().Descripcion))
                {
                    pulgarderechorolada = biometricos.Where(x => x.Descripcion == "Pulgar derecho rolada").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Pulgar izquierdo rolada").LastOrDefault().Descripcion))
                {
                    pulgarizquierdorolada = biometricos.Where(x => x.Descripcion == "Pulgar izquierdo rolada").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Indice derecho rolada").LastOrDefault().Descripcion))
                {
                    indiicederechorolada = biometricos.Where(x => x.Descripcion == "Indice derecho rolada").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Indice izquierdo rolada").LastOrDefault().Descripcion))
                {
                    indiceizquierdorolada = biometricos.Where(x => x.Descripcion == "Indice izquierdo rolada").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Medio derecho rolada").LastOrDefault().Descripcion))
                {
                    medioderechorolada = biometricos.Where(x => x.Descripcion == "Medio derecho rolada").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Medio izquierdo rolada").LastOrDefault().Descripcion))
                {
                    medioizquierdorolada = biometricos.Where(x => x.Descripcion == "Medio izquierdo rolada").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Anular derecho rolada").LastOrDefault().Descripcion))
                {
                    anularderechorolada = biometricos.Where(x => x.Descripcion == "Anular derecho rolada").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Anular izquierdo rolada").LastOrDefault().Descripcion))
                {
                    anularizquierdorolada = biometricos.Where(x => x.Descripcion == "Anular izquierdo rolada").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Meñique derecho rolada").LastOrDefault().Descripcion))
                {
                    meñiquederechorolada = biometricos.Where(x => x.Descripcion == "Meñique derecho rolada").LastOrDefault().Rutaimagen;
                }
                if (!string.IsNullOrEmpty(biometricos.Where(x => x.Descripcion == "Meñique izquierdo rolada").LastOrDefault().Descripcion))
                {
                    meñiqueizquierdorolada = biometricos.Where(x => x.Descripcion == "Meñique izquierdo rolada").LastOrDefault().Rutaimagen;
                }
            }

            float[] w3 = new float[] { 20,30,20,30 };

            PdfPTable tabla16 = new PdfPTable(4);//Numero de columnas de la tabla            
            tabla16.WidthPercentage = 100; //Porcentaje ancho
            tabla16.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical
            tabla16.SetWidths(w3);

            CeldaParaTablas(tabla16, normalfont, "Iris derecho", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto5 = System.Web.HttpContext.Current.Server.MapPath(irisizq);
            if (string.IsNullOrEmpty(irisizq))
            {
                pathfoto5 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto5);
                image2.ScaleAbsoluteHeight(80f);
                image2.ScaleAbsoluteWidth(80f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla16.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto5);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla16.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto5 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto5);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla16.AddCell(celda);
                }

            }

            CeldaParaTablas(tabla16, normalfont, "Iris izquierdo", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto6 = System.Web.HttpContext.Current.Server.MapPath(irisderecho);
            if (string.IsNullOrEmpty(irisizq))
            {
                pathfoto6 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto6);
                image2.ScaleAbsoluteHeight(80f);
                image2.ScaleAbsoluteWidth(80f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla16.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto6);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla16.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto6 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto6);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla16.AddCell(celda);
                }

            }

            
            PdfPTable tabla17 = new PdfPTable(1);
            tabla17.WidthPercentage = 100; //Porcentaje ancho
            tabla17.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            CeldaVacio(tabla17, 1);

            CeldaParaTablas(tabla17, FuentetituloTabla, "Biométricos (Huellas planas)", 1, 0, 1, colorrelleno, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            

            float[] w2 = new float[] { 12,13,12,13,12,13,12,13 };

            PdfPTable tabla18 = new PdfPTable(8);//Numero de columnas de la tabla            
            tabla18.WidthPercentage = 100; //Porcentaje ancho
            tabla18.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical
            tabla18.SetWidths(w2);


            CeldaParaTablas(tabla18, normalfont, "Índice derecho plana", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto8 = System.Web.HttpContext.Current.Server.MapPath(indicederechoplano);
            if (string.IsNullOrEmpty(indicederechoplano))
            {
                pathfoto8 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto8);
                image2.ScaleAbsoluteHeight(80f);
                image2.ScaleAbsoluteWidth(80f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla18.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto8);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla18.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto8 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto8);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla18.AddCell(celda);
                }

            }

            CeldaParaTablas(tabla18, normalfont, "Medio derecho plana", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto9 = System.Web.HttpContext.Current.Server.MapPath(medioderechoplano);
            if (string.IsNullOrEmpty(medioderechoplano))
            {
                pathfoto9 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto9);
                image2.ScaleAbsoluteHeight(80f);
                image2.ScaleAbsoluteWidth(80f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla18.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto9);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla18.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto9 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto9);
                    var celda = new PdfPCell(image2, true);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla18.AddCell(celda);
                }

            }

            CeldaParaTablas(tabla18, normalfont, "Anular derecho plana", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto11 = System.Web.HttpContext.Current.Server.MapPath(anularderechoplano);
            if (string.IsNullOrEmpty(anularderechoplano))
            {
                pathfoto11 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto11);
                image2.ScaleAbsoluteHeight(80f);
                image2.ScaleAbsoluteWidth(80f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla18.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto11);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla18.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto11 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto11);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla18.AddCell(celda);
                }

            }


            CeldaParaTablas(tabla18, normalfont, "Meñique derecho plana", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto13 = System.Web.HttpContext.Current.Server.MapPath(meñiquederechoplano);
            if (string.IsNullOrEmpty(meñiquederechoplano))
            {
                pathfoto13 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto13);
                image2.ScaleAbsoluteHeight(80f);
                image2.ScaleAbsoluteWidth(80f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla18.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto13);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla18.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto13 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto13);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla18.AddCell(celda);
                }

            }


            CeldaParaTablas(tabla18, normalfont, "Índice izquierdo plana", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto7 = System.Web.HttpContext.Current.Server.MapPath(indiceizquierdoplano);
            if (string.IsNullOrEmpty(indiceizquierdoplano))
            {
                pathfoto7 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto7);
                image2.ScaleAbsoluteHeight(80f);
                image2.ScaleAbsoluteWidth(80f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla18.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto7);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla18.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto7 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto7);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla18.AddCell(celda);
                }

            }
            
            CeldaParaTablas(tabla18, normalfont, "Medio izquierdo plana", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto10 = System.Web.HttpContext.Current.Server.MapPath(medioizquierdoplano);
            if (string.IsNullOrEmpty(medioizquierdoplano))
            {
                pathfoto10 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto10);
                image2.ScaleAbsoluteHeight(80f);
                image2.ScaleAbsoluteWidth(80f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla18.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto10);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla18.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto10 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto10);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla18.AddCell(celda);
                }

            }
            
            CeldaParaTablas(tabla18, normalfont, "Anular izquierdo plana", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto12 = System.Web.HttpContext.Current.Server.MapPath(anularizquierdoplano);
            if (string.IsNullOrEmpty(anularizquierdoplano))
            {
                pathfoto12 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto12);
                image2.ScaleAbsoluteHeight(80f);
                image2.ScaleAbsoluteWidth(80f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla18.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto12);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla18.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto12 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto12);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla18.AddCell(celda);
                }

            }
            
            CeldaParaTablas(tabla18, normalfont, "Meñique izquierdo plana", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto14 = System.Web.HttpContext.Current.Server.MapPath(meñiqueizquierdoplano);
            if (string.IsNullOrEmpty(meñiqueizquierdoplano))
            {
                pathfoto14 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto14);
                image2.ScaleAbsoluteHeight(80f);
                image2.ScaleAbsoluteWidth(80f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla18.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto14);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);

                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla18.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto14 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto14);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla18.AddCell(celda);
                }

            }

            

            PdfPTable tabla19 = new PdfPTable(1);
            tabla19.WidthPercentage = 100; //Porcentaje ancho
            tabla19.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            CeldaVacio(tabla19, 1);

            CeldaParaTablas(tabla19, FuentetituloTabla, "Biométricos (Huellas roladas)", 1, 0, 1, colorrelleno, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            

            float[] w4 = new float[] { 10,10,10,10,10,10,10,10,10,10};

            PdfPTable tabla20 = new PdfPTable(10);//Numero de columnas de la tabla            
            tabla20.WidthPercentage = 100; //Porcentaje ancho
            tabla20.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical
            tabla20.SetWidths(w4);
            CeldaParaTablas(tabla20, normalfont, "Pulgar derecho rolada", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto15 = System.Web.HttpContext.Current.Server.MapPath(pulgarderechorolada);
            if (string.IsNullOrEmpty(pulgarderechorolada))
            {
                pathfoto15 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto15);
                image2.ScaleAbsoluteHeight(60f);
                image2.ScaleAbsoluteWidth(60f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla20.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto15);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto15 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto15);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);
                }

            }

            CeldaParaTablas(tabla20, normalfont, "Índice derecho rolada", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto17 = System.Web.HttpContext.Current.Server.MapPath(indiicederechorolada);
            if (string.IsNullOrEmpty(indiicederechorolada))
            {
                pathfoto17 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto17);
                image2.ScaleAbsoluteHeight(60f);
                image2.ScaleAbsoluteWidth(60f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla20.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto17);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto17 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto17);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);
                }

            }

            CeldaParaTablas(tabla20, normalfont, "Medio derecho rolada", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto19 = System.Web.HttpContext.Current.Server.MapPath(medioderechorolada);
            if (string.IsNullOrEmpty(medioderechorolada))
            {
                pathfoto19 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto19);
                image2.ScaleAbsoluteHeight(60f);
                image2.ScaleAbsoluteWidth(60f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla20.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto19);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto19 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto19);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);
                }

            }

            CeldaParaTablas(tabla20, normalfont, "Anular  derecho rolada", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto21 = System.Web.HttpContext.Current.Server.MapPath(anularderechorolada);
            if (string.IsNullOrEmpty(anularderechorolada))
            {
                pathfoto21 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto21);
                image2.ScaleAbsoluteHeight(60f);
                image2.ScaleAbsoluteWidth(60f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla20.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto21);
                    image2.ScaleAbsoluteHeight(80f);
                    image2.ScaleAbsoluteWidth(80f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto21 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto21);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);
                }

            }
            
            CeldaParaTablas(tabla20, normalfont, "Meñique derecho rolada", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto23 = System.Web.HttpContext.Current.Server.MapPath(meñiquederechorolada);
            if (string.IsNullOrEmpty(meñiquederechorolada))
            {
                pathfoto23 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto23);
                image2.ScaleAbsoluteHeight(60f);
                image2.ScaleAbsoluteWidth(60f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla20.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto23);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto23 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto23);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);
                }

            }

            CeldaParaTablas(tabla20, normalfont, "Pulgar izquierdo rolada", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto16 = System.Web.HttpContext.Current.Server.MapPath(pulgarizquierdorolada);
            if (string.IsNullOrEmpty(pulgarizquierdorolada))
            {
                pathfoto16 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto16);
                image2.ScaleAbsoluteHeight(60f);
                image2.ScaleAbsoluteWidth(60f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla20.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto16);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto16 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto16);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);
                }

            }

            CeldaParaTablas(tabla20, normalfont, "Índice izquierdo rolada", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto18 = System.Web.HttpContext.Current.Server.MapPath(indiceizquierdorolada);
            if (string.IsNullOrEmpty(indiceizquierdorolada))
            {
                pathfoto18 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto18);
                image2.ScaleAbsoluteHeight(60f);
                image2.ScaleAbsoluteWidth(60f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla20.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto18);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto18 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto18);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);
                }

            }

            CeldaParaTablas(tabla20, normalfont, "Medio izquierdo rolada", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto20 = System.Web.HttpContext.Current.Server.MapPath(medioizquierdorolada);
            if (string.IsNullOrEmpty(medioizquierdorolada))
            {
                pathfoto20 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto20);
                image2.ScaleAbsoluteHeight(60f);
                image2.ScaleAbsoluteWidth(60f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla20.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto20);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto20 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto20);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);
                }

            }

            CeldaParaTablas(tabla20, normalfont, "Anular izquierdo rolada", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto22 = System.Web.HttpContext.Current.Server.MapPath(anularizquierdorolada);
            if (string.IsNullOrEmpty(anularizquierdorolada))
            {
                pathfoto22 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto22);
                image2.ScaleAbsoluteHeight(60f);
                image2.ScaleAbsoluteWidth(60f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla20.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto22);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto22 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto22);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);
                }

            }

            CeldaParaTablas(tabla20, normalfont, "Meñique izquierdo rolada", 1, 0, 1, colorblanco, space, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            string pathfoto24 = System.Web.HttpContext.Current.Server.MapPath(meñiqueizquierdorolada);
            if (string.IsNullOrEmpty(meñiqueizquierdorolada))
            {
                pathfoto24 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto24);
                image2.ScaleAbsoluteHeight(60f);
                image2.ScaleAbsoluteWidth(60f);
                var celda = new PdfPCell(image2);
                celda.BackgroundColor = colorgris;
                celda.Border = 0;
                tabla20.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto24);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto24 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto24);
                    image2.ScaleAbsoluteHeight(60f);
                    image2.ScaleAbsoluteWidth(60f);
                    var celda = new PdfPCell(image2);
                    celda.BackgroundColor = colorgris;
                    celda.Border = 0;
                    tabla20.AddCell(celda);
                }

            }

            if(biometricos.Count>0)
            {
                documentoPdf.Add(tabla16);
                documentoPdf.Add(tabla17);
                documentoPdf.Add(tabla18);
                documentoPdf.Add(tabla19);
                documentoPdf.Add(tabla20);
            }
            

        }


        #endregion


        #region Ficha tecnica
        public static object GenerarFichaTecnica(Entity.DetalleDetencion Detalle, int numeroreporte)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);

            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;
            try
            {
                string nombreArchivo = "Ficha_Tecnica_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                string directorio = CrearDirectorioControl("01.FichaTecnica"); directorio = directorio.Replace("//", "/");
                string ubicacionArchivo = directorio + "/" + nombreArchivo;
                string pathArchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = pathArchivo + "\\" + nombreArchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    obj = new { exitoso = false, ubicacionarchivo = "", mensaje = "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                    return obj;
                }
                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);
                Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
                Font FuenteBlau = new Font(fuentecalibri, 12f, Font.NORMAL, blau);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return obj = new { exitoso = false, ubicacionarchivo = "", mensaje = ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter4();
                documentoPdf.Open();

                titleFichatecnica(documentoPdf, FuenteTitulo, FuenteNegrita, FuenteNormal, espacio, Detalle,numeroreporte);
                biometricosfichatecnica(documentoPdf, Detalle);
                obj = new { exitoso = true, ubicacionarchivo = ubicacionArchivo, mensaje = "" };
                return obj;

            }
            catch (Exception ex)
            {
                return obj = new { exitoso = false, ubicacionarchivo = "", mensaje = ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }
         public static void biometricosfichatecnica(Document documentoPdf,  Entity.DetalleDetencion detalle)
        {
            var pulgarderecho = "";
            var pulgarizquierdo = "";
            
            var biometricos = ControlBiometricos.GetByDetenidoId(detalle.DetenidoId);
            foreach (var item in biometricos)
            {
                if (item.Descripcion.Trim() == "Pulgar derecho plana")
                {
                    pulgarderecho = item.Rutaimagen;

                }
                if (item.Descripcion.Trim() == "Pulgar izquierdo plana")
                {
                    pulgarizquierdo = item.Rutaimagen;
                }
            }
            PdfPTable tabla4 = new PdfPTable(1);
            tabla4.WidthPercentage = 100; //Porcentaje ancho
            tabla4.HorizontalAlignment = Element.ALIGN_RIGHT;//Alineación vertical}
            string pathfoto3 = System.Web.HttpContext.Current.Server.MapPath(pulgarderecho);
            if (string.IsNullOrEmpty(pulgarderecho))
            {
                pathfoto3 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto3);
                image2.ScaleAbsoluteHeight(50f);
                image2.ScaleAbsoluteWidth(50f);
                var celda = new PdfPCell(image2);
                celda.Border = 0;
                tabla4.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto3);
                    var celda = new PdfPCell(image2);
                    image2.ScaleAbsoluteHeight(50f);
                    image2.ScaleAbsoluteWidth(50f);
                    celda.Border = 0;
                    tabla4.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto3 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto3);
                    var celda = new PdfPCell(image2);
                    image2.ScaleAbsoluteHeight(50f);
                    image2.ScaleAbsoluteWidth(50f);
                    celda.Border = 0;
                    tabla4.AddCell(celda);
                }

            }

            PdfPTable tabla5 = new PdfPTable(1);
            tabla5.WidthPercentage = 100; //Porcentaje ancho
            tabla5.HorizontalAlignment = Element.ALIGN_RIGHT;//Alineación vertical}
            string pathfoto4 = System.Web.HttpContext.Current.Server.MapPath(pulgarizquierdo);
            if (string.IsNullOrEmpty(pulgarizquierdo))
            {
                pathfoto4 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto4);
                image2.ScaleAbsoluteHeight(50f);
                image2.ScaleAbsoluteWidth(50f);
                var celda = new PdfPCell(image2);
                celda.Border = 0;
                tabla5.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto4);
                    var celda = new PdfPCell(image2);
                    image2.ScaleAbsoluteHeight(50f);
                    image2.ScaleAbsoluteWidth(50f);
                    celda.Border = 0;
                    tabla5.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto4 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto4);
                    image2.ScaleAbsoluteHeight(50f);
                    image2.ScaleAbsoluteWidth(50f);
                    var celda = new PdfPCell(image2);
                    celda.Border = 0;
                    tabla5.AddCell(celda);
                }

            }
            int[] width = new int[] { 60,20,20 };
            PdfPTable tabla6 = new PdfPTable(3);
            tabla6.SetWidths(width);
            tabla6.WidthPercentage = 100; //Porcentaje ancho
            tabla6.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            CeldaVacio(tabla6, 1);
            var celdatabla3 = new PdfPCell(tabla4);
            celdatabla3.Border = 0;
            tabla6.AddCell(celdatabla3);
            celdatabla3 = new PdfPCell(tabla5);
            celdatabla3.Border = 0;
            tabla6.AddCell(celdatabla3);
            documentoPdf.Add(tabla6);

        }
        public static void titleFichatecnica(Document documentoPdf, Font FuenteTitulo, Font boldfont, Font normalfont, float space,Entity.DetalleDetencion detalle, int numeroreporte)
        {
            PdfPTable tablaTitulo = new PdfPTable(1);
            tablaTitulo.HorizontalAlignment = Element.ALIGN_CENTER;

            string escudo = "~/Content/img/Escudo.jpg";
            string logo = "~/Content/img/logo.png";

            string pathEscudo = System.Web.HttpContext.Current.Server.MapPath(escudo);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logoDER = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logoDER != "undefined" && logo != "")
            {
                logotipo = logoDER;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }
            var logoizq = subcontrato.Banner;
            if (logoizq != "undefined" && !string.IsNullOrEmpty(logoizq))
            {
                logo = logoizq;
            }

            string pathLogo = System.Web.HttpContext.Current.Server.MapPath(logo);
            iTextSharp.text.Image imageEscudo = null;
            iTextSharp.text.Image imageLogo = null;
            try
            {
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }
            catch (Exception)
            {
                pathEscudo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }
            if (logoizq != "undefined" && !string.IsNullOrEmpty(logoizq))
            {
                try
                {
                    imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
                    imageLogo.ScaleAbsoluteHeight(89f);
                    imageLogo.ScaleAbsoluteWidth(300f);
                }
                catch (Exception)
                {
                    pathLogo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
                    imageLogo.ScaleAbsoluteHeight(90f);
                    imageLogo.ScaleAbsoluteWidth(200f);
                }
            }
            else
            {
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
                imageLogo.ScaleAbsoluteHeight(90f);
                imageLogo.ScaleAbsoluteWidth(200f);
            }

            //try
            //{
            //    imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            //}
            //catch (Exception)
            //{
            //    pathLogo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
            //    imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            //}

            //imageLogo.ScalePercent(85f);
            //image.ScalePercent(12f);
           

            image.ScaleAbsoluteHeight(60f);
            image.ScaleAbsoluteWidth(70f);
            Paragraph para = new Paragraph();
            Phrase ph1 = new Phrase();
            Chunk glue = new Chunk(new iTextSharp.text.pdf.draw.VerticalPositionMark());
            Paragraph main = new Paragraph();
            ph1.Add(new Chunk(imageLogo, 0, 0, true));
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(image, 0, 15, true));
            main.Add(ph1);
            para.Add(main);
            documentoPdf.Add(para);
            //var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            //var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
            var contrato = ControlContrato.Obtenerporsubcontrato(subcontrato.Id);
            var instituto = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);

            //Titulo
            
            Celda(tablaTitulo, FuenteTitulo, "Ficha técnica", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaVacio(tablaTitulo, 1);
            documentoPdf.Add(tablaTitulo);
            var nombrecompleto = detalle.NombreDetenido + " ";
            var appellidos= detalle.APaternoDetenido + " " + detalle.AMaternoDetenido;
            var altura = "";
            var antrometia = ControlAntropometria.ObtenerPorDetenidoId(detalle.DetenidoId);
            if(antrometia !=null)
            {
                altura = antrometia.Estatura.ToString() + "mts.";
            }
            var alias = "";
            var A = ControlAlias.ObteneTodos().Where(x => x.DetenidoId==detalle.DetenidoId);
            var t = 1;
            foreach(var item in A)
            {
                if(t==1)
                {
                    alias = item.Nombre;
                }
                else
                {
                    alias = alias + ", " + item.Nombre;
                }
                t = t + 1;
            }
            var general = ControlGeneral.ObtenerPorDetenidoId(detalle.DetenidoId);
            var interno = ControlDetenido.ObtenerPorId(detalle.DetenidoId);
            var direccion = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
            var domicilio = "";
            var cp = "";
            var motivodetencion = "";
            var unidad = "";
            var responsable = "";
            var fechaevento = "";
            var horaevento = "";
            var barrio = "";
            var direccionevento = "";
            if(direccion !=null)
            {
                domicilio = direccion.Calle + " " + direccion.Numero;
                var colonia = ControlColonia.ObtenerPorId(Convert.ToInt32(direccion.ColoniaId));
                if(colonia!=null)
                {
                    var municipio = ControlMunicipio.Obtener(colonia.IdMunicipio);
                    var estado = ControlEstado.Obtener(municipio.EstadoId);
                    var pais = ControlCatalogo.Obtener(estado.IdPais, Convert.ToInt32(Entity.TipoDeCatalogo.pais));
                    barrio = colonia.Asentamiento+", "+municipio.Nombre+", "+estado.Nombre+", "+pais.Nombre;
                    cp = colonia.CodigoPostal;
                }
            }
            var coloniaevento = "";
            var cpevento = "";
            var info = ControlInformacionDeDetencion.ObtenerPorInternoId(detalle.DetenidoId);
            if(info!=null)
            {
                var detenidoevento = ControlDetenidoEvento.ObtenerPorId(info.IdDetenido_Evento);
                if(detenidoevento!=null)
                {
                    var motivo = ControlWSAMotivo.ObtenerPorId(detenidoevento.MotivoId);
                    if(motivo!=null)
                    {
                        motivodetencion = motivo.NombreMotivoLlamada;
                    }
                }
                var unidadevento = ControlEventoUnidadResponsable.ObtenerTodosByEventoId(info.IdEvento);
                if(unidadevento!=null && unidadevento.Count>0)
                {
                    var u = 1;
                    var res = 1;

                    foreach(var item in unidadevento)
                    {
                        var uni = ControlUnidad.ObtenerById(item.UnidadId);
                        if(uni !=null)
                        {
                            if(u==1)
                            {
                                unidad = uni.Nombre;
                            }
                            else
                            {
                                unidad = unidad + ", " + uni.Nombre;
                            }
                            u = u + 1;
                        }
                        
                        var r = ControlCatalogo.Obtener(item.ResponsableId, Convert.ToInt32(Entity.TipoDeCatalogo.responsable_unidad));
                        if(r!=null)
                        {
                            if (res == 1)
                            {
                                responsable = r.Nombre;
                            }
                            else
                            {
                                responsable = responsable + ", " + r.Nombre;
                            }
                            res = res + 1;
                        }
                        
                    }
                }


                var evento = ControlEvento.ObtenerById(info.IdEvento);
                if(evento !=null)
                {
                    fechaevento = evento.HoraYFecha.ToShortDateString();
                    var h1 = evento.HoraYFecha.Hour.ToString();
                    if (evento.HoraYFecha.Hour < 10) h1 = "0" + h1;
                    var m1 = evento.HoraYFecha.Minute.ToString();
                    if (evento.HoraYFecha.Minute < 10) m1 = "0" + m1;
                    var s1 = evento.HoraYFecha.Second.ToString();
                    if (evento.HoraYFecha.Second < 10) s1 = "0" + s1;
                    horaevento = h1 + ":" + m1 + ":" + s1;
                    direccionevento = evento.Lugar;
                    var colonia = ControlColonia.ObtenerPorId(evento.ColoniaId);
                    if(colonia!=null)
                    {
                        var municipio = ControlMunicipio.Obtener(colonia.IdMunicipio);
                        var estado = ControlEstado.ObtenerPorId(municipio.EstadoId);
                        var pais = ControlCatalogo.Obtener(estado.IdPais, Convert.ToInt32(Entity.TipoDeCatalogo.pais));
                        coloniaevento = colonia.Asentamiento + ", " + municipio.Nombre + ", " + estado.Nombre + ", " + pais.Nombre;
                        cpevento = colonia.CodigoPostal;
                    }
                }
            }
            var nacimiento = "";
            if(general!=null)
            {
                if(general.FechaNacimineto >DateTime.MinValue)
                {
                    nacimiento = general.FechaNacimineto.ToShortDateString();
                   
                }
            }
            var cpsicofisiologico = "";
            var cquimico = "";
            
            var cpsobervaciones = "";
            var cpsconcluciones = "";
            var clobservaciones = "";
            
            var medico = ControlServicoMedico.ObtenerPorDetalleDetencionId(detalle.Id);
            var cps = 1;
            var cl = 1;
            var cq = 1;
            var ss1 = 1;
            var ss3 = 0;
            var etanol = "";
            var cannabis = "";
            var Benzodiazepina = "";
            var cocaina = "";
            var anfetamina = "";
            var extasis = "";
            foreach (var item in medico)
            {
                var psicofisiologico = ControlCertificadoPsicoFisiologico.ObtenerPorId(item.CertificadoMedicoPsicofisiologicoId);
                if(psicofisiologico!=null)
                {
                    if (cps == 1)
                    {
                        cpsicofisiologico = psicofisiologico.Folio + "/" + psicofisiologico.Id.ToString();
                        cpsobervaciones = psicofisiologico.Observacion_extra;
                        

                    }
                    else
                    {

                        cpsicofisiologico=cpsicofisiologico+", "+ psicofisiologico.Folio + "/" + psicofisiologico.Id.ToString();
                        cpsobervaciones = cpsobervaciones+", " + psicofisiologico.Observacion_extra;
                    }
                    cps = cps + 1;
                    var concluciones = "";
                    var ss2 = 1;
                    var conclucionescervicio = ControlCertificadoPsicoFisologicoConclusionDetalle.Obtener(psicofisiologico.ConclusionId);
                    foreach(var sps in conclucionescervicio)
                    {
                        var conclucion = ControlCatalogo.Obtener(sps.ConclusionId, Convert.ToInt32(Entity.TipoDeCatalogo.Conclusion));
                        if(conclucion!=null)
                        {
                            if(ss2==1)
                            {
                                concluciones = conclucion.Nombre;
                            }
                            else
                            {
                                concluciones = concluciones + ", " + conclucion.Nombre;
                            }
                            
                        }
                       
                        ss2++;
                    }
                    if(concluciones !="")
                    {
                        if(ss1==1)
                        {
                            cpsconcluciones = concluciones;
                        }
                        else
                        {
                            cpsconcluciones = cpsconcluciones + ", " + concluciones;
                        }
                        ss1++;
                    }
                }

                var lesiones = ControlCertificadoLesiones.ObtenerPorId(item.CertificadoLesionId);
                if(lesiones!=null)
                {
                    if(cl==1)
                    {
                        if(!string.IsNullOrEmpty(lesiones.Observaciones_lesion))
                        {
                            clobservaciones = lesiones.Observaciones_lesion;
                            cl = cl + 1;
                        }
                        
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(lesiones.Observaciones_lesion))
                        {
                            clobservaciones =clobservaciones+", "+ lesiones.Observaciones_lesion;
                            cl = cl + 1;
                        }
                    }
                }
                var quimico = ControlCertificadoQuimico.ObtenerPorId(item.CertificadoQuimicoId);
                if(quimico!=null)
                {
                    if(cq==1)
                    {
                        if(!string.IsNullOrEmpty(quimico.Foliocertificado))
                        {
                            cquimico = quimico.Foliocertificado;
                            cq = cq + 1;
                        }
                        
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(quimico.Foliocertificado))
                        {
                            cquimico = cquimico+", "+quimico.Foliocertificado;
                            cq = cq + 1;
                        }
                    }
                    if(quimico.EtanolId==2)
                    {
                        etanol = "Positivo"+"("+quimico.Grado.ToString()+"mg/dL)";
                    }
                    else
                    {
                        etanol = "";
                    }
                    if(quimico.CannabisId==2)
                    {
                        cannabis = "Positivo";
                    }
                    else
                    {
                        cannabis = "";
                    }
                    if(quimico.BenzodiapinaId==2)
                    {
                        Benzodiazepina = "Positivo";
                    }
                    else
                    {
                        Benzodiazepina = "";

                    }
                    if(quimico.CocainaId==2)
                    {
                        cocaina = "Positivo";

                    }
                    else
                    {
                        cocaina = "";
                    }
                    if(quimico.AnfetaminaId==2)
                    {
                        anfetamina = "Positivo";

                    }
                    else
                    {
                        anfetamina = "";
                    }
                    if(quimico.ExtasisId==2)
                    {
                        extasis = "Positivo";

                    }
                    else
                    {
                        extasis = "";
                    }
                }
            }
            var situacion = "Pendiente";
            var calificacion = ControlCalificacion.ObtenerPorInternoId(detalle.Id);
            if (calificacion != null)
            {
                var situaciones = ControlCatalogo.Obtener(calificacion.SituacionId, Convert.ToInt32(Entity.TipoDeCatalogo.situacion_detenido));
                if (situaciones != null)
                    situacion = situaciones.Nombre;
            }
            var estatus = ControlCatalogo.Obtener(detalle.Estatus, Convert.ToInt32(Entity.TipoDeCatalogo.estatus));

            var salida = ControlSalidaEfectuadaJuez.ObtenerByDetalleDetencionId(detalle.Id);
            var tipo = "";
            var fundamento = "";
            var fechasalida = "";

            if (salida!=null)
            {
                var tiposalida = ControlCatalogo.Obtener(salida.TiposalidaId, Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));
                if(tiposalida!=null)
                {
                    tipo = tiposalida.Nombre;
                }
                fundamento = salida.Fundamento;
                var historial = ControlHistorial.ObtenerPorDetenido(detalle.Id).Where(x=> x.Movimiento== "Salida efectuada del detenido").LastOrDefault();
                if (historial != null)
                {
                    var h = historial.Fecha.Hour.ToString();
                    if (historial.Fecha.Hour < 10) h = "0" + h;
                    var m = historial.Fecha.Minute.ToString();
                    if (historial.Fecha.Minute < 10) m = "0" + m;
                    var s = historial.Fecha.Second.ToString();
                    if (historial.Fecha.Second < 10) s = "0" + s;
                    fechasalida = historial.Fecha.ToShortDateString() + " " + h + ":" + m + ":" + s;
                }
                else
                {
                    historial = ControlHistorial.ObtenerPorDetenido(detalle.Id).Where(x => x.Movimiento == "Traslado del detenido").LastOrDefault();
                    if (historial != null)
                    {
                        var h = historial.Fecha.Hour.ToString();
                        if (historial.Fecha.Hour < 10) h = "0" + h;
                        var m = historial.Fecha.Minute.ToString();
                        if (historial.Fecha.Minute < 10) m = "0" + m;
                        var s = historial.Fecha.Second.ToString();
                        if (historial.Fecha.Second < 10) s = "0" + s;
                        fechasalida = historial.Fecha.ToShortDateString() + " " + h + ":" + m + ":" + s;
                    }
                }
               
            }

            var pulgarderecho = "";
            var pulgarizquierdo = "";
            var facial = "";
            var biometricos = ControlBiometricos.GetByDetenidoId(detalle.DetenidoId);
            foreach(var item in biometricos)
            {
                if(item.Descripcion.Trim()== "Pulgar derecho plana")
                {
                    pulgarderecho = item.Rutaimagen;

                }
                if(item.Descripcion.Trim()== "Pulgar izquierdo plana")
                {
                    pulgarizquierdo = item.Rutaimagen;
                }
            }
            if(!string.IsNullOrEmpty(interno.RutaImagen))
            {
                facial = interno.RutaImagen;
            }
            else
            {
                if(biometricos!=null && biometricos.Count>0)
                {
                   if(!string.IsNullOrEmpty(biometricos.Where(x=> x.Descripcion== "Imagen facial frontal").LastOrDefault().Descripcion))
                    {
                        facial = biometricos.Where(x => x.Descripcion == "Imagen facial frontal").LastOrDefault().Descripcion;
                    }
                }
            }

            var examenesMedicos = ControlExamenMedico.ObtenerTodosPorDetalleDetencionId(detalle.Id);
            List<string> folios = new List<string>();
            List<string> fechas = new List<string>();
            List<string> indicaciones = new List<string>();
            string foliosAux = string.Empty;
            string fechasAux = string.Empty;
            string indicacionesAux = string.Empty;

            folios = examenesMedicos.Select(x => x.Id.ToString()).ToList();
            fechas = examenesMedicos.Select(x => x.Fecha.ToString("yyyy-MM-dd HH:mm:ss")).ToList();
            indicaciones = examenesMedicos.Select(x => x.IndicacionesMedicas).ToList();
            foliosAux = string.Join(",", folios);
            fechasAux = string.Join(",", fechas);
            indicacionesAux = string.Join(",", indicaciones);

            int[] width = new int[] { 50,40 };
            PdfPTable tabla2 = new PdfPTable(2);
            tabla2.SetWidths(width);
            tabla2.TotalWidth = 550;
            tabla2.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPTable tablaz = new PdfPTable(1);//Numero de columnas de la tabla
            tablaz.WidthPercentage = 100; //Porcentaje ancho
            tablaz.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            CeldaConChunk(tabla2, boldfont, normalfont, "No. remisión: ", detalle.Expediente.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, boldfont, normalfont, "No. de solicitud: ", numeroreporte.ToString(), 0, 0, 0);
            var celdatablaz = new PdfPCell(tabla2);
            celdatablaz.Border = 0;
            
            tablaz.AddCell(celdatablaz);
            Celda(tablaz, FuenteTitulo, "DATOS GENERALES", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            //CeldaVacio(tabla2, 1);
            
            CeldaConChunk(tablaz, boldfont, normalfont, "Nombre: ", nombrecompleto.ToUpper() + appellidos.ToUpper(), 0, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            //CeldaConChunk(tabla2, boldfont, normalfont, "Nombre: ", nombrecompleto.ToUpper(), 0, 0, 0);
            //Celda(tabla2, normalfont, appellidos.ToUpper(), 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            tabla2 = new PdfPTable(2);
            tabla2.SetWidths(width);
            tabla2.TotalWidth = 550;
            tabla2.HorizontalAlignment = Element.ALIGN_LEFT;
            CeldaConChunk(tabla2, boldfont, normalfont, "Estatura: ", altura, 0, 0, 0);
            CeldaConChunk(tabla2, boldfont, normalfont, "ALIAS: ", alias, 0, 0, 0);
            celdatablaz = new PdfPCell(tabla2);
            celdatablaz.Border = 0;

            tablaz.AddCell(celdatablaz);
            CeldaConChunk(tablaz, boldfont, normalfont, "Fecha de nacimiento: ", nacimiento, 0, 0, 0);
            tabla2 = new PdfPTable(2);
            tabla2.SetWidths(width);
            tabla2.TotalWidth = 550;
            tabla2.HorizontalAlignment = Element.ALIGN_LEFT;
            CeldaConChunk(tabla2, boldfont, normalfont, "Dirección: ", domicilio, 0, 0, 0);
            CeldaConChunk(tabla2, boldfont, normalfont, "C.P. ", cp, 0, 0, 0);
            celdatablaz = new PdfPCell(tabla2);
            celdatablaz.Border = 0;

            tablaz.AddCell(celdatablaz);
            CeldaConChunk(tablaz, boldfont, normalfont, "Colonia: ", barrio, 0, 0, 0);
            
            Celda(tablaz, FuenteTitulo, "            LUGAR DE DETENCIÓN", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);

            int[] width2 = new int[] { 50,50 };
            PdfPTable tabla3 = new PdfPTable(2);
            tabla3.SetWidths(width2);
            tabla3.TotalWidth = 550;
            tabla3.HorizontalAlignment = Element.ALIGN_LEFT;
            CeldaConChunk(tabla3, boldfont, normalfont, "Dirección: ", direccionevento, 0, 0, 0);
            CeldaVacio(tabla3, 1);
            CeldaConChunk(tabla3, boldfont, normalfont, "Colonia: ", coloniaevento, 0, 0, 0);
            CeldaConChunk(tabla3, boldfont, normalfont, "C.P. ", cpevento, 0, 0, 0);
            PdfPTable tablax = new PdfPTable(1);//Numero de columnas de la tabla
            tablax.WidthPercentage = 100; //Porcentaje ancho
            tablax.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            CeldaConChunk(tablax, boldfont, normalfont, "Motivo de detención: ", motivodetencion, 0, 0, 0);
            PdfPTable tablay = new PdfPTable(2);
            tablay.SetWidths(width2);
            tablay.TotalWidth = 550;
            tablay.HorizontalAlignment = Element.ALIGN_LEFT;
            CeldaConChunk(tablax, boldfont, normalfont, "Unidad que entrega: ", unidad, 0, 0, 0);
            //CeldaVacio(tablax, 1);
            CeldaConChunk(tablax, boldfont, normalfont, "Responsable: ", responsable, 0, 0, 0);
            //CeldaVacio(tablay, 1);
            CeldaConChunk(tablay, boldfont, normalfont, "Fecha: ", fechaevento, 0, 0, 0);
            CeldaConChunk(tablay, boldfont, normalfont, "Hora: ", horaevento, 0, 0, 0);
            PdfPTable tablaw = new PdfPTable(1);//Numero de columnas de la tabla
            tablaw.WidthPercentage = 100; //Porcentaje ancho
            tablaw.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            Celda(tablaw, FuenteTitulo, "OBSERVACIONES MÉDICAS", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            //CeldaVacio(tablay, 1);
            CeldaConChunk(tablaw, boldfont, normalfont, "Folio: ", foliosAux,0,0,0);
            //CeldaVacio(tablay, 1);
            CeldaConChunk(tablaw, boldfont, normalfont, "Examen médico: ", fechasAux, 0, 0, 0);
            //CeldaVacio(tablay, 1);
            CeldaConChunk(tablaw, boldfont, normalfont, "Indicaciones médicas: ", indicacionesAux, 0, 0, 0);
            //CeldaVacio(tablay, 1);

            //CeldaConChunk(tablaw, boldfont, normalfont, "Observaciones: ", clobservaciones, 0, 0, 0);
            ////CeldaVacio(tablay, 1);

            //CeldaConChunk(tablaw, boldfont, normalfont, "Folio certificado médico químico:", cquimico, 0, 0, 0);
            ////CeldaVacio(tablay, 1);
            //CeldaConChunk(tablaw, boldfont, normalfont, "Resultados:", "", 0, 0, 0);
            CeldaVacio(tablay, 1);
            var espacio = 2;
            PdfPTable tablav = new PdfPTable(2);
            tablav.SetWidths(width2);
            tablav.TotalWidth = 550;
            tablav.HorizontalAlignment = Element.ALIGN_LEFT;
            if (etanol!=""&&cannabis!="")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "   Etanol: ", etanol, 0, 0, 0);
                CeldaConChunk(tablav, boldfont, normalfont, "Cannabis: ", cannabis, 0, 0, 0);
                
            }
            else if(etanol=="" &&cannabis!="")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "Cannabis: ", cannabis, 0, 0, 0);
                CeldaVacio(tablav, 1);
            }
            else if(etanol!=""&&cannabis=="")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "   Etanol: ", etanol, 0, 0, 0);
                CeldaVacio(tablav, 1);
            }
            else
            {
                espacio = espacio + 2;
            }

            if (Benzodiazepina != "" && cocaina != "")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "   Benzodiazepina: ", Benzodiazepina, 0, 0, 0);
                CeldaConChunk(tablav, boldfont, normalfont, "   Cocaína: ", cocaina, 0, 0, 0);

            }
            else if (Benzodiazepina == "" && cocaina != "")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "   Cocaína: ", cocaina, 0, 0, 0);
                CeldaVacio(tablav, 1);
            }
            else if (Benzodiazepina != "" && cocaina == "")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "   Benzodiazepina: ", Benzodiazepina, 0, 0, 0);
                CeldaVacio(tablav, 1);
            }
            else
            {
                espacio = espacio + 2;
            }
            
            if (anfetamina != "" && extasis != "")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "   Anfetamina: ", anfetamina, 0, 0, 0);
                CeldaConChunk(tablav, boldfont, normalfont, "   Éxtasis: ", extasis, 0, 0, 0);

            }
            else if (anfetamina == "" && extasis != "")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "   Éxtasis: ", extasis, 0, 0, 0);
                CeldaVacio(tablav, 1);
            }
            else if (anfetamina != "" && extasis == "")
            {
                CeldaConChunk(tablav, boldfont, normalfont, "   Anfetamina: ", anfetamina, 0, 0, 0);
                CeldaVacio(tablav, 1);
            }
            else { espacio = espacio + 2; }
            //CeldaVacio(tablav, espacio);
            PdfPTable tabla3w = new PdfPTable(1);//Numero de columnas de la tabla
            tabla3w.WidthPercentage = 100; //Porcentaje ancho
            tabla3w.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}

            Celda(tabla3w, FuenteTitulo, "SITUACIÓN JURÍDICA", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            PdfPTable tabla3v = new PdfPTable(2);
            tabla3v.SetWidths(width2);
            tabla3v.TotalWidth = 550;
            tabla3v.HorizontalAlignment = Element.ALIGN_LEFT;
            //CeldaVacio(tablay, 1);
            CeldaConChunk(tabla3v, boldfont, normalfont, "Situación: ", situacion, 0, 0, 0);
            CeldaConChunk(tabla3v, boldfont, normalfont, "Estatus: ", estatus.Nombre.ToString(),0,0,0);
            CeldaConChunk(tabla3v, boldfont, normalfont, "Tipo de salida: ", tipo, 0, 0, 0);
            CeldaVacio(tabla3v, 1);
            CeldaConChunk(tabla3v, boldfont, normalfont, "Fundamento: ", fundamento, 0, 0, 0);
            CeldaVacio(tabla3v, 1);
            CeldaConChunk(tabla3v, boldfont, normalfont, "Salida: ", fechasalida, 0, 0, 0);
            CeldaVacio(tabla3v, 1);
            int[] width5 = new int[] { 35,65 };
            PdfPTable tabla = new PdfPTable(2);//Numero de columnas de la tabla
            //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;//Alineación vertical}
            tabla.SetWidths(width5);
            string pathfoto2 = System.Web.HttpContext.Current.Server.MapPath(facial);

            if (string.IsNullOrEmpty(facial))
            {
                pathfoto2 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/avatars/male.png");
                iTextSharp.text.Image image2 = iTextSharp.text.Image.GetInstance(pathfoto2);
                image2.ScaleAbsoluteHeight(150f);
                image2.ScaleAbsoluteWidth(150f);
                PdfPCell celda = new PdfPCell(image2);
                celda.HorizontalAlignment = Element.ALIGN_RIGHT;
                //celda.Image.ScaleAbsolute(40, 40);
                celda.Border = 0;
                tabla.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto2);
                    image2.ScaleAbsoluteHeight(150f);
                    image2.ScaleAbsoluteWidth(150f);
                    var celda = new PdfPCell(image2);
                    celda.HorizontalAlignment = Element.ALIGN_RIGHT;
                    //celda.Image.ScaleAbsolute(50, 50);
                    celda.Border = 0;
                    tabla.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto2 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/avatars/male.png");

                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto2);
                    image2.ScaleAbsoluteHeight(150f);
                    image2.ScaleAbsoluteWidth(150f);

                    var celda = new PdfPCell(image2);
                    celda.HorizontalAlignment = Element.ALIGN_RIGHT;
                    //celda.Image.ScaleAbsolute(50, 50);
                    celda.Border = 0;
                    tabla.AddCell(celda);
                }

            }
            var celdatabla2 = new PdfPCell(tablaz);
            celdatabla2.Border = 0;
            tabla.AddCell(celdatabla2);


            documentoPdf.Add(tabla);
            documentoPdf.Add(tabla3);
            documentoPdf.Add(tablax);
            documentoPdf.Add(tablay);
            documentoPdf.Add(tablaw);
            documentoPdf.Add(tablav);
            documentoPdf.Add(tabla3w);
            documentoPdf.Add(tabla3v);
            PdfPTable tabla4 = new PdfPTable(1);
            tabla4.WidthPercentage = 100; //Porcentaje ancho
            tabla4.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            string pathfoto3 = System.Web.HttpContext.Current.Server.MapPath(pulgarderecho);
            if (string.IsNullOrEmpty(pulgarderecho))
            {
                pathfoto3 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto3);
                var celda = new PdfPCell(image2, true);
                celda.Border = 0;
                tabla4.AddCell(celda);
            }
            else
            { 
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto3);
                    var celda = new PdfPCell(image2, true);
                    celda.Border = 0;
                    tabla4.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto3 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto3);
                    var celda = new PdfPCell(image2, true);
                    celda.Border = 0;
                    tabla4.AddCell(celda);
                }

            }

            PdfPTable tabla5 = new PdfPTable(1);
            tabla5.WidthPercentage = 100; //Porcentaje ancho
            tabla5.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            string pathfoto4 = System.Web.HttpContext.Current.Server.MapPath(pulgarizquierdo);
            if (string.IsNullOrEmpty(pulgarizquierdo))
            {
                pathfoto4 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                var image2 = iTextSharp.text.Image.GetInstance(pathfoto4);
                var celda = new PdfPCell(image2, true);
                celda.Border = 0;
                tabla5.AddCell(celda);
            }
            else
            {
                try
                {
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto4);
                    var celda = new PdfPCell(image2, true);
                    celda.Border = 0;
                    tabla5.AddCell(celda);


                }
                catch (Exception)
                {
                    pathfoto4 = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    var image2 = iTextSharp.text.Image.GetInstance(pathfoto4);
                    var celda = new PdfPCell(image2, true);
                    celda.Border = 0;
                    tabla5.AddCell(celda);
                }

            }
            PdfPTable tabla6 = new PdfPTable(3);
            tabla6.WidthPercentage = 100; //Porcentaje ancho
            tabla6.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}
            CeldaVacio(tabla6, 1);
            var celdatabla3 = new PdfPCell(tabla4);
            celdatabla3.Border = 0;
            tabla6.AddCell(celdatabla3);
            celdatabla3 = new PdfPCell(tabla5);
            celdatabla3.Border = 0;
            tabla6.AddCell(celdatabla3);
            
        }
        #endregion

        #region "Informe detención"

        public static object GenerarInformeDetencion(int InternoId)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);

            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombreArchivo = "Informe_Detencion_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                string directorio = CrearDirectorioControl("01.InformeDetencion"); directorio = directorio.Replace("//", "/");
                string ubicacionArchivo = directorio + "/" + nombreArchivo;
                string pathArchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = pathArchivo + "\\" + nombreArchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    obj = new { exitoso = false, ubicacionarchivo = "", mensaje = "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                    return obj;
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);
                Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
                Font FuenteBlau = new Font(fuentecalibri, 12f, Font.NORMAL, blau);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return obj = new { exitoso = false, ubicacionarchivo = "", mensaje = ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                TitleInformeDetencion(documentoPdf, FuenteTitulo, FuenteNegrita12, FuenteNormal, espacio, InternoId);
                CuerpoInformeDetencion(documentoPdf, FuenteBlau, FuenteNormal, FuenteNegrita, espacio, InternoId, PdfWriter);

                obj = new { exitoso = true, ubicacionarchivo = ubicacionArchivo, mensaje = "" };
                return obj;
            }
            catch (Exception ex)
            {
                return obj = new { exitoso = false, ubicacionarchivo = "", mensaje = ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }

        }

        private static void TitleInformeDetencion(Document documentpdf, Font titlefont, Font boldfont, Font normalfont, float space, int internoId)
        {
            int centroId = 0;
            var ei = ControlDetalleDetencion.ObtenerPorDetenidoId(internoId);
            foreach (var val in ei)
            {
                centroId = val.CentroId;
            }

            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }

            int[] width = new int[] { 2, 8, 2};
            PdfPTable table = new PdfPTable(3);
            table.SetWidths(width);
            table.TotalWidth = 550;
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell;

            CeldaVacio(table, 3);

            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            table.AddCell(cellWithRowspan);
            string title = "Informe de Detención";

            cell = new PdfPCell(new Phrase(title, titlefont));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Padding = 10;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(" "));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(" "));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Padding = space;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(" "));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);

            documentpdf.Add(table);
        }

        private static void CuerpoInformeDetencion(Document documentoPdf,Font FuenteBlau, Font FuenteNormal, Font FuenteNegrita, float espacio, int InternoId, iTextSharp.text.pdf.PdfWriter pdfWriter)
        {
            PdfPTable tablaUno = new PdfPTable(2);//Numero de columnas de la tabla
            PdfPTable tablaDos = new PdfPTable(3);//Numero de columnas de la tabla
            PdfPTable tablaTres = new PdfPTable(2);//Numero de columnas de la tabla
            PdfPTable tableFooter = new PdfPTable(7);//Numero de columnas de la tabla

            float[] w1 = new float[] { 54, 85 };
            float[] w2 = new float[] { 70, 50, 60 };
            float[] w3 = new float[] {28,45 };
            float[] wFooter = new float[] { 30, 5, 30, 5, 30, 5, 30 };

            tablaUno.WidthPercentage = 100; //Porcentaje ancho
            tablaUno.HorizontalAlignment = Element.ALIGN_LEFT;
            tablaUno.SetWidths(w1);

            tablaDos.WidthPercentage = 100; //Porcentaje ancho
            tablaDos.HorizontalAlignment = Element.ALIGN_LEFT;
            tablaDos.SetWidths(w2);

            tablaTres.WidthPercentage = 100; //Porcentaje ancho
            tablaTres.HorizontalAlignment = Element.ALIGN_LEFT;
            tablaTres.SetWidths(w3);
            
            tableFooter.SetWidths(wFooter);
            tableFooter.WidthPercentage = 100;
            tableFooter.DefaultCell.Border = 0;
            tableFooter.HorizontalAlignment = Element.ALIGN_CENTER;

            BaseColor colorrelleno = new BaseColor(208, 206, 206);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            Entity.ReporteInformeDetencion reporte = ControlReporteInformeDetencion.GetReporteInformeDetencionByInternoId(InternoId);
            string Domicilio = "";
            string Estado = "";
            string Municipio = "";
            string Colonia;
            if (string.IsNullOrEmpty(reporte.Domicilio))
            {
                Domicilio = "Domicilio no registrado";

            }
            else
            {
                Domicilio = reporte.Domicilio;
            }
            if (string.IsNullOrEmpty(reporte.Estado))
            {
                Estado = "Estado no registrado";

            }
            else
            {
                Estado = reporte.Estado;
            }
            if (string.IsNullOrEmpty(reporte.Municipio))
            {
                Municipio = "Municipio no registrado";

            }
            else
            {
                Municipio = reporte.Municipio;
            }
            if (string.IsNullOrEmpty(reporte.Colonia))
            {
                Colonia = "Colonia no registrada";

            }
            else
            {
                Colonia = reporte.Colonia;
            }
            DateTime nacimiento;

            nacimiento = reporte.FechaNacimineto;

            int edad = 0;
            string sexo = "Sin dato";
            var interno = ControlDetenido.ObtenerPorId(InternoId);
            var general = ControlGeneral.ObtenerPorDetenidoId(InternoId);
            var info= ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
            var ev = ControlEvento.ObtenerById(info.IdEvento);
            string[] datos = { interno.Id.ToString(), true.ToString() };
            Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
            if (general != null)
            {
                if (general.FechaNacimineto != DateTime.MinValue)
                    edad = CalcularEdad(general.FechaNacimineto);
                else
                {
                    var detalleDetencion2 = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                    var evento = ControlEvento.ObtenerById(detalleDetencion2.IdEvento);
                    var detenidosEvento = ControlDetenidoEvento.ObtenerPorEventoId(evento.Id);
                    Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
                    foreach (var item in detenidosEvento)
                    {
                        if (item.Nombre == interno.Nombre && item.Paterno == interno.Paterno && item.Materno == interno.Materno)
                            detenidoEvento = item;
                    }

                    if (detenidoEvento == null) edad = 0;
                    else
                    {
                        if (detenidoEvento.Edad != 0) edad = detenidoEvento.Edad;
                        else edad = 0;
                    }
                }

                sexo = ControlCatalogo.Obtener(general.SexoId, 4).Nombre;
            }
            else
            {
                edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;
            }
            edad = detalleDetencion.DetalledetencionEdad;
            //Información para la tabla uno con dos columnas
            CeldaConChunk(tablaUno, FuenteNegrita, FuenteNormal, "No. remisión: ", reporte.Remision, 0, 0, 0);
            CeldaConChunk(tablaUno, FuenteNegrita, FuenteNormal, "Detenido: ", reporte.Detenido, 0, 0, 0);
            CeldaVacio(tablaUno, 3);

            //Información para la tabla dos con tres columnas
            CeldaConChunk(tablaDos, FuenteNegrita, FuenteNormal, "Domicilio: ", Domicilio, 0, 0, 0);
            CeldaConChunk(tablaDos, FuenteNegrita, FuenteNormal, "Estado: ", Estado, 0, 0, 0);
            CeldaConChunk(tablaDos, FuenteNegrita, FuenteNormal, "Municipio: ", Municipio, 0, 0, 0);
            CeldaVacio(tablaDos, 3);
            CeldaConChunk(tablaDos, FuenteNegrita, FuenteNormal, "Colonia: ", Colonia, 0, 0, 0);
            CeldaConChunk(tablaDos, FuenteNegrita, FuenteNormal, "Edad: ", edad > 0 ? edad.ToString() : "Edad no registrada", 0, 0, 0);
            CeldaConChunk(tablaDos, FuenteNegrita, FuenteNormal, "Sexo: ", sexo, 0, 0, 0);
            CeldaVacio(tablaDos, 3);
            var date = reporte.FechaRegistro.ToShortDateString();
            var h = reporte.FechaRegistro.Hour.ToString();
            if(reporte.FechaRegistro.Hour<10)
            {
                h = "0" + h;
            }
            var m = reporte.FechaRegistro.Minute.ToString();
            if(reporte.FechaRegistro.Minute<10)
            {
                m = "0" + m;
            }
            var s = reporte.FechaRegistro.Second.ToString();
            if(reporte.FechaRegistro.Second<10)
            {
                s = "0" + s;
            }
            var fechas = date + " " + h + ":" + m + ":" + s;
            CeldaConChunk(tablaDos, FuenteNegrita, FuenteNormal, "Fecha y hora de registro: ", fechas, 0, 0, 0);
            CeldaConChunk(tablaDos, FuenteNegrita, FuenteNormal, "Lugar de detención: ", reporte.LugarDetencion, 0, 0, 0);
            string motivo = "";
            var detenidoevento = ControlDetenidoEvento.ObtenerPorId(info.IdDetenido_Evento);
            if(detenidoevento!=null)
            {
                var mot = ControlWSAMotivo.ObtenerPorId(detenidoevento.MotivoId);
                if (mot != null)
                {
                    motivo = mot.NombreMotivoLlamada;
                }
            }
            
            CeldaConChunk(tablaDos, FuenteNegrita, FuenteNormal, "Motivo de detención: ", motivo, 0, 0, 0);
            CeldaVacio(tablaDos, 3);

            // Información para la tabla tres con una columna
            String fecha = string.Empty;

            if (reporte.FechaDetencion == DateTime.MinValue)
            {
                fecha = "Sin fecha";
            }
            else
            {
                var h1 = reporte.FechaDetencion.Hour.ToString();
                if (reporte.FechaDetencion.Hour < 10) h1 = "0" + h1;
                var m1 = reporte.FechaDetencion.Minute.ToString();
                if (reporte.FechaDetencion.Minute < 10) m1 = "0" + m1;
                var s1 = reporte.FechaDetencion.Second.ToString();
                if (reporte.FechaDetencion.Second < 10) s1 = "0" + s1;
                fecha = reporte.FechaDetencion.ToShortDateString()+" "+h1+":"+m1+":"+s1;
            }
            CeldaConChunk(tablaTres, FuenteNegrita, FuenteNormal, "Descripción del evento: ", ev.Descripcion, 0, 0, 0);
            CeldaConChunk(tablaTres, FuenteNegrita, FuenteNormal, "Fecha y hora de detención: ", fecha, 0, 0, 0);

            // Tabla que se agrega en el footer del reporte
            PdfPCell celda;
            PdfPCell celdaVacia = new PdfPCell(new Phrase(Chunk.NEWLINE));
            celdaVacia.Border = PdfPCell.NO_BORDER;

            celda = new PdfPCell(new Phrase("Capturado Por", FuenteBlau));
            celda.Colspan = 1;
            celda.BorderWidth = 0;
            celda.BorderColorTop = BaseColor.BLACK;
            celda.BorderWidthTop = 1;
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_CENTER;
            celda.BackgroundColor = colorblanco;
            celda.Padding = espacio;
            celda.UseAscender = true;
            tableFooter.AddCell(celda);

            tableFooter.AddCell(celdaVacia);

            celda = new PdfPCell(new Phrase("Juez que recibe", FuenteBlau));
            celda.Colspan = 1;
            celda.BorderWidth = 0;
            celda.BorderColorTop = BaseColor.BLACK;
            celda.BorderWidthTop = 1;
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_CENTER;
            celda.BackgroundColor = colorblanco;
            celda.Padding = espacio;
            celda.UseAscender = true;
            tableFooter.AddCell(celda);

            tableFooter.AddCell(celdaVacia);

            celda = new PdfPCell(new Phrase("Juez que autoriza salida", FuenteBlau));
            celda.Colspan = 1;
            celda.BorderWidth = 0;
            celda.BorderColorTop = BaseColor.BLACK;
            celda.BorderWidthTop = 1;
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_CENTER;
            celda.BackgroundColor = colorblanco;
            celda.Padding = espacio;
            celda.UseAscender = true;
            tableFooter.AddCell(celda);

            tableFooter.AddCell(celdaVacia);

            celda = new PdfPCell(new Phrase("Firma", FuenteBlau));
            celda.Colspan = 1;
            celda.BorderWidth = 0;
            celda.BorderColorTop = BaseColor.BLACK;
            celda.BorderWidthTop = 1;
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.VerticalAlignment = Element.ALIGN_CENTER;
            celda.BackgroundColor = colorblanco;
            celda.Padding = espacio;
            celda.UseAscender = true;
            tableFooter.AddCell(celda);                       

            documentoPdf.Add(tablaUno);
            documentoPdf.Add(tablaDos);
            documentoPdf.Add(tablaTres);

            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));

            documentoPdf.Add(tableFooter);

           
        }

        #endregion

        #region "Reporte boleta de control"

        // Reporte boleta de control
        public static object GenerarReporteBoleta(int InternoId)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);

            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombreArchivo = "Reporte_Boleta_Control_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                string directorio = CrearDirectorioControl("01.ReporteBoletaControl"); directorio = directorio.Replace("//", "/");
                string ubicacionArchivo = directorio + "/" + nombreArchivo;
                string pathArchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = pathArchivo + "\\" + nombreArchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    obj = new { exitoso = false, ubicacionarchivo = "", mensaje = "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                    return obj;
                }

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return obj = new { exitoso = false, ubicacionarchivo = "", mensaje = ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                CuerpoBoletaControl(documentoPdf, espacio, InternoId);

                obj = new { exitoso = true, ubicacionarchivo = ubicacionArchivo, mensaje = "Se ha descargado el reporte" };
                return obj;
            }
            catch (Exception ex)
            {
                return obj = new { exitoso = false, ubicacionarchivo = "", mensaje = ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }

        }

        private static void TitleReporte(Document documentpdf, Font titlefont, float space, string titulo, int InternoId)
        {
            int centroId = 0;
            var ei = ControlDetalleDetencion.ObtenerPorDetenidoId(InternoId);
            foreach (var val in ei)
            {
                centroId = val.CentroId;
            }

            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);
            Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }

            int[] width = new int[] { 2, 8 };
            PdfPTable table = new PdfPTable(2);
            table.SetWidths(width);
            table.TotalWidth = 550;
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell;
            
            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            table.AddCell(cellWithRowspan);
            
            string title = titulo;

            cell = new PdfPCell(new Phrase(title, titlefont));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Padding = 10;
            table.AddCell(cell);

            documentpdf.Add(table);
        }

        private static void TitleReporte(Document documentpdf, Font titlefont, float space, string titulo)
        {
            float[] width = new float[] { 100 };
            PdfPTable table = new PdfPTable(1);
            table.SetWidths(width);
            table.WidthPercentage = 100;
            table.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell cell;

            string title = titulo;

            cell = new PdfPCell(new Phrase(title, titlefont));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Padding = space;
            table.AddCell(cell);

            documentpdf.Add(table);
        }

        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }

            return edad;
        }

        private static string ObtenerOpcionComun(int Id)
        {
            return ControlCatalogo.Obtener(Id, Convert.ToInt32(Entity.TipoDeCatalogo.opcion_comun)).Nombre;

        }

        private static void CuerpoBoletaControl(Document documentoPdf, float espacio, int InternoId)
        {
            //Definición de fuentes
            BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita14 = new Font(fuentecalibri, 18f, Font.BOLD, blau);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            PdfPTable tablaDetenido = new PdfPTable(3);//Numero de columnas de la tabla
            PdfPTable tablaMedico = new PdfPTable(3);//Numero de columnas de la tabla
            PdfPTable tablaRemision = new PdfPTable(3);//Numero de columnas de la tabla
            PdfPTable tablaSituacion = new PdfPTable(3);//Numero de columnas de la tabla
            PdfPTable tablaSalida = new PdfPTable(3);//Numero de columnas de la tabla
            PdfPTable tablaFooter = new PdfPTable(4);//Numero de columnas de la tabla
             
            float[] w1 = new float[] { 50, 50, 50 };
            float[] w2 = new float[] { 40, 40, 40, 40 };

            tablaDetenido.WidthPercentage = 100; //Porcentaje ancho
            tablaDetenido.HorizontalAlignment = Element.ALIGN_LEFT;
            tablaDetenido.SetWidths(w1);

            tablaMedico.WidthPercentage = 100; //Porcentaje ancho
            tablaMedico.HorizontalAlignment = Element.ALIGN_LEFT;
            tablaMedico.SetWidths(w1);

            tablaRemision.WidthPercentage = 100; //Porcentaje ancho
            tablaRemision.HorizontalAlignment = Element.ALIGN_LEFT;
            tablaRemision.SetWidths(w1);

            tablaSituacion.WidthPercentage = 100; //Porcentaje ancho
            tablaSituacion.HorizontalAlignment = Element.ALIGN_LEFT;
            tablaSituacion.SetWidths(w1);

            tablaSalida.WidthPercentage = 100; //Porcentaje ancho
            tablaSalida.HorizontalAlignment = Element.ALIGN_LEFT;
            tablaSalida.SetWidths(w1);

            tablaFooter.WidthPercentage = 100; //Porcentaje ancho
            tablaFooter.HorizontalAlignment = Element.ALIGN_RIGHT;
            tablaFooter.SetWidths(w2);

            BaseColor colorrelleno = new BaseColor(208, 206, 206);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);

            //r€ dø
            Entity.ReporteBoletaControl reporte = ControlReporteBoletaControl.GetReporteBoletaControlByDetenidoId(InternoId);

            //fix3d
            //var infodetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(InternoId);
            var detail = ControlDetalleDetencion.ObtenerPorDetenidoId(InternoId).FirstOrDefault(x => x.Expediente == reporte.Remision);
            //var unidades = ControlUnidad.ObtenerPorEventoId(infodetencion.IdEvento
            int referent = Convert.ToInt32(detail.Expediente);
            var unidades = ControlUnidad.ObtenerPorEventoId(referent);
            var unidadesTuple = new List<Tuple<string, string, string>>();
            foreach (var item in unidades)
            {
                var responsable = ControlResponsable.ObtenerPorUnidadIdEventoId(new object[] { item.Id, referent });
                var corporacion = ControlCatalogo.Obtener(item != null ? item.CorporacionId : 0, Convert.ToInt32(Entity.TipoDeCatalogo.corporacion));
                unidadesTuple.Add(new Tuple<string, string, string>(corporacion.Nombre, item.Nombre, responsable.Nombre));
            }
            var evento = ControlEvento.ObtenerById(referent);
            var motivo = "";
            var detenidoevento = ControlDetenidoEvento.ObtenerPorId(referent);
           
            if(detenidoevento!=null)
            {
                var mot = ControlWSAMotivo.ObtenerPorId(detenidoevento.MotivoId);
                if (mot != null) motivo = mot.NombreMotivoLlamada;
            }
            
            DateTime nacimiento;
            string Domicilio = "";
            string Estado = "";
            string Localidad = "";
            if (string.IsNullOrEmpty(reporte.Domicilio))
            {
                Domicilio = "Domicilio no registrado";

            }
            else
            {
                Domicilio = reporte.Domicilio;
            }
            if (string.IsNullOrEmpty(reporte.Estado))
            {
                Estado = "Estado no registrado";

            }
            else
            {
                Estado = reporte.Estado;
            }
            if (string.IsNullOrEmpty(reporte.Localidad))
            {
                Localidad = "Municipio no registrado";

            }
            else
            {
                Localidad = reporte.Localidad;
            }
            nacimiento = reporte.FechaNacimiento;
            int edad = 0;
            string sexo = "Sin dato";
            var interno = ControlDetenido.ObtenerPorId(InternoId);
            var historialTiempo = ControlHistorial.ObtenerPorDetenido(InternoId).FirstOrDefault();
            var general = ControlGeneral.ObtenerPorDetenidoId(InternoId);
            if (general != null)
            {
                if (general.FechaNacimineto != DateTime.MinValue)
                    edad = CalcularEdad(general.FechaNacimineto);

                if (edad == 0)
                {
                    edad = general.Edaddetenido;
                }
                //make ag€ methàd
                //edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;
            }
            else
            {
                edad = detail.DetalledetencionEdad;
                sexo = ControlCatalogo.Obtener(detail.DetalledetencionSexoId, 4).Nombre;
            }

            // TitleReporte(documentoPdf, FuenteNegrita14, espacio, "Boleta de control", InternoId);
            TitleReporte(documentoPdf, FuenteNegrita14, espacio, "Informe de Presentación", InternoId);

            //Información para la tabla detenido
            TitleReporte(documentoPdf, FuenteNegrita, espacio, "Datos del detenido");
            var date = Convert.ToDateTime(detail.Fecha).ToShortDateString();
            var h = Convert.ToDateTime(historialTiempo.Fecha).Hour.ToString();
            if (Convert.ToDateTime(historialTiempo.Fecha).Hour<1)
            {
                h = "0" + h;
            }
            var m = Convert.ToDateTime(historialTiempo.Fecha).Minute.ToString();
            if (Convert.ToDateTime(historialTiempo.Fecha).Minute<10)
            {
                m = "0" + m;
            }
            var s = Convert.ToDateTime(historialTiempo.Fecha).Second.ToString();
            if (Convert.ToDateTime(historialTiempo.Fecha).Second<10)
            {
                s = "0" + s;
            }
            var fecha = date + " " + h + ":" + m + ":" + s;
            CeldaConChunk(tablaDetenido, FuenteNegrita, FuenteNormal, "No. remisión: ", reporte.Remision, 0, 0, 0);
            CeldaConChunk(tablaDetenido, FuenteNegrita, FuenteNormal, "Registro: ", fecha, 0, 0, 0);
            CeldaConChunk(tablaDetenido, FuenteNegrita, FuenteNormal, "Nombre: ", reporte.Nombre, 0, 0, 0);

            CeldaConChunk(tablaDetenido, FuenteNegrita, FuenteNormal, "Edad: ", edad > 0 ? edad.ToString() : "Edad no registrada", 0, 0, 0);
            CeldaConChunk(tablaDetenido, FuenteNegrita, FuenteNormal, "Sexo: ", sexo, 0, 0, 0);
            CeldaConChunk(tablaDetenido, FuenteNegrita, FuenteNormal, "Domicilio: ", Domicilio, 0, 0, 0);

            CeldaConChunk(tablaDetenido, FuenteNegrita, FuenteNormal, "Escolaridad: ", reporte.Escolaridad, 0, 0, 0);
            CeldaConChunk(tablaDetenido, FuenteNegrita, FuenteNormal, "Estado: ", Estado, 0, 0, 0);
            CeldaConChunk(tablaDetenido, FuenteNegrita, FuenteNormal, "Municipio: ", Localidad, 0, 0, 0);

            documentoPdf.Add(tablaDetenido);
            documentoPdf.Add(new Paragraph("\n"));

            // Informacion para la tabla de medico
            var listserviciomedico = ControlServicoMedico.ObtenerPorDetalleDetencionId(detail.Id);
            var examenmedico = ControlExamenMedico.ObtenerTodosPorDetalleDetencionId(detail.Id);
            
            // SI NO HAY SERVICIOS MÉDICOS, MEJOR OMITIR ESTA PARTE
            if (listserviciomedico.Count>0 || examenmedico.Count>0)
            { 
                TitleReporte(documentoPdf, FuenteNegrita, espacio, "Médico");
                var serviciomedico = ControlServicoMedico.ObtenerTodo();
            
                foreach(var item in examenmedico)
                {
                    string strusuario = "";
                
                    strusuario = ControlUsuario.Obtener(Convert.ToInt32(item.RegistradoPor)).User;
                    var strresultado = item.IndicacionesMedicas;
                    CeldaConChunk(tablaMedico, FuenteNegrita, FuenteNormal, "Fecha: ", item.FechaRegistro.ToShortDateString(), 0, 0, 0);
                    CeldaConChunk(tablaMedico, FuenteNegrita, FuenteNormal, "Realizador: ", strusuario, 0, 0, 0);
                    CeldaConChunk(tablaMedico, FuenteNegrita, FuenteNormal, "Diagnóstico: ", strresultado, 0, 0, 0);

                }
                foreach (var item in listserviciomedico)
                {
                    string strusuario="";
                    if (item.ModificadoPor != 0)
                    {
                        strusuario = ControlUsuario.Obtener(Convert.ToInt32(item.ModificadoPor)).User;
                    }
                    else
                    {
                        strusuario = ControlUsuario.Obtener(Convert.ToInt32(item.RegistradoPor)).User;

                    }
                    string strresultado = "";
                    if (item.CertificadoQuimicoId != 0)
                    {
                        var cerquimico = ControlCertificadoQuimico.ObtenerPorId(item.CertificadoQuimicoId);

                        strresultado = "Etanol: " + ObtenerOpcionComun(cerquimico.EtanolId)+" "+ (cerquimico.Grado!=0?"("+cerquimico.Grado+" mg/dL)":"");
                        strresultado += ", Benzodiazepina: " + ObtenerOpcionComun(cerquimico.BenzodiapinaId);
                        strresultado += ", Anfetaminas: " + ObtenerOpcionComun(cerquimico.AnfetaminaId);
                        strresultado += ", Cannabis: " + ObtenerOpcionComun(cerquimico.CannabisId);
                        strresultado += ", Cocaína: " + ObtenerOpcionComun(cerquimico.CocainaId);
                        strresultado += ", Éxtasis: " + ObtenerOpcionComun(cerquimico.ExtasisId);
                    }

                    CeldaConChunk(tablaMedico, FuenteNegrita, FuenteNormal, "Fecha: ", item.FechaRegistro.ToShortDateString(), 0, 0, 0);
                    CeldaConChunk(tablaMedico, FuenteNegrita, FuenteNormal, "Realizador: ",strusuario, 0, 0, 0);
                    CeldaConChunk(tablaMedico, FuenteNegrita, FuenteNormal, "Diagnóstico: ", strresultado, 0, 0, 0);
                }
                documentoPdf.Add(tablaMedico);
                documentoPdf.Add(new Paragraph("\n"));
            }
            
            // Informacion para la tabla de remision
            TitleReporte(documentoPdf, FuenteNegrita, espacio, "Remisión");

            if(unidadesTuple.Count > 1)
            {
                for (int i = 0; i < unidadesTuple.Count; i++)
                {
                    if(i == 0)
                    {
                        CeldaConChunk(tablaRemision, FuenteNegrita, FuenteNormal, "Corporación: ", unidadesTuple[0].Item1 != null ? unidadesTuple[0].Item1 : "", 0, 0, 0);
                        CeldaConChunk(tablaRemision, FuenteNegrita, FuenteNormal, "Unidad: ", unidadesTuple[0].Item2 != null ? unidadesTuple[0].Item2 : "", 0, 0, 0);
                        CeldaConChunk(tablaRemision, FuenteNegrita, FuenteNormal, "Responsable: ", unidadesTuple[0].Item3 != null ? unidadesTuple[0].Item3 : "", 0, 0, 0);
                    }
                    else
                    {
                        CeldaConChunk(tablaRemision, FuenteNegrita, FuenteNormal, "                        ", unidadesTuple[i].Item1 != null ? unidadesTuple[i].Item1 : "", 0, 0, 0);
                        CeldaConChunk(tablaRemision, FuenteNegrita, FuenteNormal, "                ", unidadesTuple[i].Item2 != null ? unidadesTuple[i].Item2 : "", 0, 0, 0);
                        CeldaConChunk(tablaRemision, FuenteNegrita, FuenteNormal, "                         ", unidadesTuple[i].Item3 != null ? unidadesTuple[i].Item3 : "", 0, 0, 0);
                    }
                }
            }
            else
            {
                CeldaConChunk(tablaRemision, FuenteNegrita, FuenteNormal, "Corporación: ", unidadesTuple.Count != 0 && unidadesTuple[0].Item1 != null ? unidadesTuple[0].Item1 : "", 0, 0, 0);
                CeldaConChunk(tablaRemision, FuenteNegrita, FuenteNormal, "Unidad: ", unidadesTuple.Count != 0 && unidadesTuple[0].Item2 != null ? unidadesTuple[0].Item2 : "", 0, 0, 0);
                CeldaConChunk(tablaRemision, FuenteNegrita, FuenteNormal, "Responsable: ", unidadesTuple.Count != 0 && unidadesTuple[0].Item3 != null ? unidadesTuple[0].Item3 : "", 0, 0, 0);
            }

            /*
            if(evento != null)
            {
                if (evento.MotivoId > 0)
                    motivo = ControlMotivoDetencion.ObtenerPorId(Convert.ToInt32(evento.MotivoId)).Descripcion;
            }*/

            CeldaConChunk(tablaRemision, FuenteNegrita, FuenteNormal, "Motivo remisión: ", motivo , 0, 0, 0);
            CeldaConChunk(tablaRemision, FuenteNegrita, FuenteNormal, "Lugar detención: ", reporte.LugarDetencion, 0, 0, 0);
            CeldaConChunk(tablaRemision, FuenteNegrita, FuenteNormal, "Colonia: ", reporte.ColoniaDetencion, 0, 0, 0);

            /*
            Celda(tablaRemision, FuenteNegrita, "Descripción de la detención:", 1, 0, 1, 0, 0);
            Celda(tablaRemision, FuenteNormal, evento != null ? evento.Descripcion : "", 3, 0, 1, 0, 0);
            documentoPdf.Add(tablaRemision);
            documentoPdf.Add(new Paragraph("\n"));
            */

            documentoPdf.Add(tablaRemision);
            PdfPTable tabla9 = new PdfPTable(1);
            tabla9.WidthPercentage = 100;
            CeldaConChunk(tabla9, FuenteNegrita, FuenteNormal, "Descripción de la detención: ", evento != null ? evento.Descripcion : "", 0, 0, 0);
            documentoPdf.Add(tabla9);
            documentoPdf.Add(new Paragraph("\n"));
            
            // EN EL CASO ESPECIFICO DE HERMOSILLO SE SOLICITO OMITIR ESTAS SECCIONES
            if (!bolHermosillo) {
                
                // Informacion para la tabla de remision
                TitleReporte(documentoPdf, FuenteNegrita, espacio, "Situación");

                CeldaConChunk(tablaSituacion, FuenteNegrita, FuenteNormal, "Situación: ", reporte.Situacion, 0, 0, 0);
                CeldaConChunk(tablaSituacion, FuenteNegrita, FuenteNormal, "Calificó: ", reporte.Califico, 0, 0, 0);
                CeldaConChunk(tablaSituacion, FuenteNegrita, FuenteNormal, " ", " ", 0, 0, 0);
                //CeldaConChunk(tablaSituacion, FuenteNegrita, FuenteNormal, "Arresto: ", reporte.Responsable, 0, 0, 0);

                CeldaConChunk(tablaSituacion, FuenteNegrita, FuenteNormal, "Horas: ", reporte.TotalHoras, 0, 0, 0);
                CeldaConChunk(tablaSituacion, FuenteNegrita, FuenteNormal, "Multa: ", reporte.Multa, 0, 0, 0);
                CeldaVacio(tablaSituacion, 1);

                documentoPdf.Add(tablaSituacion);
                documentoPdf.Add(new Paragraph("\n"));
                var responsables = "";
                var eur = ControlEventoUnidadResponsable.ObtenerTodosByEventoId(referent);
                var f = 1;
                foreach (var item in eur)
                {
                    var resp = ControlCatalogo.Obtener(item.ResponsableId, Convert.ToInt32(Entity.TipoDeCatalogo.responsable_unidad));
                    if (resp != null)
                    {
                        if (f == 1)
                        {
                            responsables = resp.Nombre;
                        }
                        else
                        {
                            responsables = responsables + ", " + resp.Nombre;
                        }
                        f++;
                    }
                }
                // Informacion para la tabla de remision
                TitleReporte(documentoPdf, FuenteNegrita, espacio, "Autorización de salida");

                CeldaConChunk(tablaSalida, FuenteNegrita, FuenteNormal, "Situación: ", reporte.Situacion, 0, 0, 0);
                CeldaConChunk(tablaSalida, FuenteNegrita, FuenteNormal, "Calificó: ", reporte.Califico != null ? reporte.Califico : "", 0, 0, 0);
                CeldaConChunk(tablaSalida, FuenteNegrita, FuenteNormal, "Arrestó: ", responsables, 0, 0, 0);

                documentoPdf.Add(tablaSalida);

            }

            // Informacion para la tabla footer
            if (bolHermosillo) {
                // EN HERMOSILLO SE SOLICITO PONER FIRMA DEL JUEZ CALIFICADOR Y OFICIALES RESPONSABLES EN LA PARTE INFERIOR
                float[] w6 = new float[] { 50, 50 };
                // PdfPTable tablaFooter = new PdfPTable(2);
                tablaFooter = new PdfPTable(2);
                tablaFooter.WidthPercentage = 70; //Porcentaje ancho
                tablaFooter.HorizontalAlignment = Element.ALIGN_CENTER;
                tablaFooter.SetWidths(w6);

                CeldaVacio(tablaFooter, 1);
                CeldaVacio(tablaFooter, 1);

                CeldaVacio(tablaFooter, 1);
                CeldaVacio(tablaFooter, 1);

                CeldaConChunk(tablaFooter, FuenteNegrita, FuenteNormal, "", "_______________________", 0, 0, 1);
                CeldaConChunk(tablaFooter, FuenteNegrita, FuenteNormal, "", "_______________________", 0, 0, 1);

                CeldaConChunk(tablaFooter, FuenteNegrita, FuenteNormal, "OFICIAL(ES) RESPONSABLE(S)", "", 0, 0, 1);
                CeldaConChunk(tablaFooter, FuenteNegrita, FuenteNormal, "        JUEZ CALIFICADOR   ", "", 0, 0, 1);
            }
            else {
                // EL CASO DEFAULT ES PONER LA FIRMA INFERIOR DEL INTERESADO.
                CeldaVacio(tablaFooter, 3);
                Font FuenteBlau = new Font(fuentecalibri, 12f, Font.NORMAL, blau);
                PdfPCell celda;
                celda = new PdfPCell(new Phrase("Firma de conformidad del infractor", FuenteBlau));
                celda.Colspan = 1;
                celda.BorderWidth = 0;
                celda.BorderColorTop = BaseColor.BLACK;
                celda.BorderWidthTop = 1;
                celda.HorizontalAlignment = Element.ALIGN_CENTER;
                celda.VerticalAlignment = Element.ALIGN_CENTER;
                celda.BackgroundColor = colorblanco;
                celda.Padding = espacio;
                celda.UseAscender = true;
                tablaFooter.AddCell(celda);

                CuerpoParaCelda(tablaFooter, FuenteNormal, "", 1, 1, colorblanco, 1, Element.ALIGN_CENTER);
            }
             
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
            documentoPdf.Add(new Paragraph("\n"));
                        
            documentoPdf.Add(tablaFooter);

            documentoPdf.NewPage();
            tablaDetenido.DeleteBodyRows();
        }

        #endregion

        public static object oficioTurnacion(int centro, int contrato, string tipo, Entity.Detenido detenido, string autoridad, int juezId)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(10); //milimetros para los margenes
            var space = iTextSharp.text.Utilities.MillimetersToPoints(3); 
            Document documentpdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                //Nombre y ubicación archivo
                string nombrearchivo = "turnacion" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("turnacion"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string pathfont = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont calibrifont = BaseFont.CreateFont(pathfont, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font titlefont = new Font(calibrifont, 12, Font.BOLD, blau);
                Font boldfont = new Font(calibrifont, 10f, Font.BOLD, blau);
                Font normalfont = new Font(calibrifont, 14f, Font.NORMAL);
                Font bigfont = new Font(calibrifont, 14, Font.BOLD, blau);
                Font smallfont = new Font(calibrifont, 8, Font.NORMAL);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentpdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return obj = new { success = false, file = "", message = ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter3();

                documentpdf.Open();

                TitleTurnacion(documentpdf, titlefont, boldfont, normalfont, space, bigfont, centro, detenido, autoridad, juezId);

                obj = new { success = true, file = ubicacionarchivo, message = "" };
                return obj;
            }
            catch (Exception ex)
            {
                return obj = new { success = false, file = "", message = ex.Message };
            }
            finally
            {
                if (documentpdf.IsOpen())
                {
                    documentpdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TitleTurnacion(Document documentpdf, Font titlefont, Font boldfont, Font normalfont, float space, Font bigfont, int centroId, Entity.Detenido detenidok, string autoridad, int juezId)
        {
            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);
            Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);
            var motivodetencion = ControlMotivoDetencion.ObtenerTodoPorInternoId(detenidok.Id);
            Entity.Usuario juez = ControlUsuario.Obtener(juezId);

            string nombreJuez = juez.Nombre.ToUpper() + " " + juez.ApellidoPaterno.ToUpper() + " " + juez.ApellidoMaterno.ToUpper();

            string motivo = "";
            string articulo = "";
            int contador = 0;

            foreach (var k in motivodetencion)
            {
                if (contador != 0)
                {
                    motivo += ", o bien " + k.Motivo;
                    articulo += ", " + k.Articulo;
                }
                else
                {
                    motivo += k.Motivo;
                    articulo += k.Articulo;
                }
                contador++;
            }

            string nombreEstado = "Estado";
            string nombreMunicipio = "Municipio";

            if (domicilio != null)
            {
                Entity.Estado estado = ControlEstado.ObtenerPorId(Convert.ToInt32(domicilio.EstadoId));

                if (estado != null)
                {
                    nombreEstado = estado.Nombre;
                    Entity.Municipio municipio = ControlMunicipio.Obtener(Convert.ToInt32(domicilio.MunicipioId));
                    if (municipio != null) nombreMunicipio = municipio.Nombre;
                }
            }

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }
            string nombreCentro = institucion.Nombre;

            float[] width = new float[] { 2, 2, 8 };
            PdfPTable table = new PdfPTable(3);
            table.SetWidths(width);
            table.WidthPercentage = 100;
            table.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell cell;

            string detenido = detenidok.Nombre + " " + detenidok.Paterno + " " + detenidok.Materno;
            string comandancia = "Comandancia";
            string title = "Coordinación de juzgados calificadores";
            string title2 = "Meza: Juzgado calificador zona " + institucion.Nombre;

            string dia = String.Format("{0:dd}", DateTime.Now);
            string anio = String.Format("{0:yyyy}", DateTime.Now);
            string hora = String.Format("{0:HH:mm}", DateTime.Now);
            string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
            string fechahora = dia + " de " + mestexto + " de " + anio;

            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            table.AddCell(cellWithRowspan);

            cell = new PdfPCell(new Phrase(""));
            // The first cell spans 5 rows  
            cell.Rowspan = 5;
            cell.Border = 0;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(title, titlefont));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = space;
            table.AddCell(cell);            

            cell = new PdfPCell(new Phrase(title2, titlefont));
            cell.BorderWidth = 0;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = space;
            table.AddCell(cell);          

            cell = new PdfPCell(new Phrase("ASUNTO: TURNACIÓN", bigfont));
            cell.BorderWidth = 0;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_RIGHT;
            cell.Padding = space;
            table.AddCell(cell);           

            cell = new PdfPCell(new Phrase(nombreMunicipio + "," + nombreEstado + "   " + fechahora, titlefont));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_RIGHT;
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(" "));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);
            
            cell = new PdfPCell(new Phrase(autoridad, bigfont));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(" "));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(" "));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("PRESENTE.-"));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase(" "));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase(" "));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);
            cell = new PdfPCell(new Phrase(" "));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);

            float[] width2 = new float[] { 100 };
            PdfPTable table2 = new PdfPTable(1);
            table2.SetWidths(width2);
            table2.WidthPercentage = 90;
            table2.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell cell2;

            cell2 = new PdfPCell(new Phrase("Por medio del presente escrito y con fundamento, además en lo dispuesto en los artículos " +
                "118 del Código de Procedimientos Penales para el Estado de " + nombreEstado + ", en relación con los diversos numerales 209 y 218 " +
                "de la Ley de Seguridad Pública para el Estado de " + nombreEstado + " y 135 del Bando de Policía y Buen Gobierno para el municipio " +
                "de " + nombreMunicipio + ", me permito poner a su disposición al C. " + detenido + ", por su probable participación en el lícito " +
                "de " + motivo + ", previsto en el artículo " + articulo + " del código penal para " + nombreEstado + ", en el entendido de que si no fuese " +
                "nos lo haga saber por escrito, para continuar con el procedimiento administrativo en los términos previstos en el Bando de Policía y " +
                "Buen Gobierno para el municipio de " + nombreMunicipio + ".", normalfont));
            cell2.BorderWidth = 0;
            cell2.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
            cell2.VerticalAlignment = Element.ALIGN_JUSTIFIED;
            cell2.Padding = space;
            table2.AddCell(cell2);

            cell2 = new PdfPCell(new Phrase(" "));
            cell2.BorderWidth = 0;
            cell2.Padding = space;
            table2.AddCell(cell2);
            cell2 = new PdfPCell(new Phrase(" "));
            cell2.BorderWidth = 0;
            cell2.Padding = space;
            table2.AddCell(cell2);
            cell2 = new PdfPCell(new Phrase(" "));
            cell2.BorderWidth = 0;
            cell2.Padding = space;
            table2.AddCell(cell2);
            cell2 = new PdfPCell(new Phrase("ATENTAMENTE", bigfont));
            cell2.BorderWidth = 0;
            cell2.HorizontalAlignment = Element.ALIGN_CENTER;
            cell2.Padding = space;
            table2.AddCell(cell2);
            cell2 = new PdfPCell(new Phrase("JUEZ CALIFICADOR "+ nombreJuez, bigfont));
            cell2.BorderWidth = 0;
            cell2.HorizontalAlignment = Element.ALIGN_CENTER;
            cell2.Padding = space;
            table2.AddCell(cell2);
            cell2 = new PdfPCell(new Phrase(institucion.Nombre.ToUpper(), bigfont));
            cell2.BorderWidth = 0;
            cell2.HorizontalAlignment = Element.ALIGN_CENTER;
            cell2.Padding = space;
            table2.AddCell(cell2);
            cell2 = new PdfPCell(new Phrase(" "));
            cell2.BorderWidth = 0;
            cell2.Padding = space;
            table2.AddCell(cell2);
            cell2 = new PdfPCell(new Phrase(" "));
            cell2.BorderWidth = 0;
            cell2.Padding = space;
            table2.AddCell(cell2);
            cell2 = new PdfPCell(new Phrase(" "));
            cell2.BorderWidth = 0;
            cell2.Padding = space;
            table2.AddCell(cell2);
            cell2 = new PdfPCell(new Phrase(nombreJuez, bigfont));
            cell2.BorderWidth = 0;
            cell2.HorizontalAlignment = Element.ALIGN_CENTER;
            cell2.Padding = space;
            table2.AddCell(cell2);

            documentpdf.Add(table);
            documentpdf.Add(table2);
        }

        public static string ObtenerMesTexto(string mes)
        {
            string mestexto = "";
            switch (mes)
            {
                case "01":
                    mestexto = "Enero";
                    break;
                case "02":
                    mestexto = "Febrero";
                    break;
                case "03":
                    mestexto = "Marzo";
                    break;
                case "04":
                    mestexto = "Abril";
                    break;
                case "05":
                    mestexto = "Mayo";
                    break;
                case "06":
                    mestexto = "Junio";
                    break;
                case "07":
                    mestexto = "Julio";
                    break;
                case "08":
                    mestexto = "Agosto";
                    break;
                case "09":
                    mestexto = "Septiembre";
                    break;
                case "10":
                    mestexto = "Octubre";
                    break;
                case "11":
                    mestexto = "Noviembre";
                    break;
                case "12":
                    mestexto = "Diciembre";
                    break;
                default:
                    mestexto = " ";
                    break;
            }
            return mestexto;
        }

        public static object generarReporteCorteDetenidos(Entity.ContratoUsuario contratoUsuario)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER.Rotate(), margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Reporte_Corte_Detenidos_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.CorteDetenidos"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return obj = new { exitoso = false, ubicacionarchivo = "", mensaje = "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);
                Font FuenteNormal = new Font(fuentecalibri, 8f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD, blau);
                Font FuenteTituloTabla = new Font(fuentecalibri, 8f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return obj = new { exitoso = false, ubicacionarchivo = "", mensaje = ex.Message }; }

                List<Entity.ListadoReporteCorteDetenidos> reporte = new List<Entity.ListadoReporteCorteDetenidos>();

                object[] dataFiltros = new object[]
                {
                    contratoUsuario.Tipo,
                    contratoUsuario.IdContrato                
                };

                reporte = ControlListadoReporteCorteDetenidos.ObtenerTodos(dataFiltros);

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                TituloReporteCorteDetenidos(documentoPdf, FuenteTitulo, FuenteNegrita12, contratoUsuario, reporte.Count);
                InformacionReciboCorteDetenidos(documentoPdf, FuenteNormal, FuenteTituloTabla, espacio, contratoUsuario, reporte);

                obj = new { exitoso = true, ubicacionarchivo = ubicacionarchivo, mensaje = "" };
                return obj;
            }
            catch (Exception ex)
            {
                return obj = new { exitoso = false, ubicacionarchivo = "", mensaje = ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloReporteCorteDetenidos(Document documentoPdf, Font FuenteTitulo, Font FuenteSubtitulo, Entity.ContratoUsuario contratoUsuario, int totalRegistros)
        {
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }

            int[] w = new int[] { 2, 8 };
            PdfPTable table = new PdfPTable(2);
            table.SetWidths(w);
            table.TotalWidth = 550;
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell;
            
            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            table.AddCell(cellWithRowspan);

            int institucionId = 0;

            if (contratoUsuario != null)
            {
                if (contratoUsuario.Tipo == "subcontrato")
                {
                    institucionId = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato).InstitucionId;
                }
            }

            var institucion = ControlInstitucion.ObtenerPorId(institucionId);
            string centroNombre = (institucion != null) ? institucion.Nombre : "";

            //Titulo
            Celda(table, FuenteTitulo, "   ", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            Celda(table, FuenteTitulo, "   Corte de detenidos", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(table, 2);

            //Nombre delegacion y usuario en barandilla
            BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);
            PdfPTable tablaDelegacionUsuario = new PdfPTable(3);
            float[] width = new float[] { 220, 90, 150 };
            tablaDelegacionUsuario.SetWidths(width);
            tablaDelegacionUsuario.DefaultCell.Border = Rectangle.NO_BORDER;

            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Delegación: ", centroNombre == string.Empty ? "Sin delegación" : centroNombre, 0, 0, 0);
            CeldaVacio(tablaDelegacionUsuario, 1);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Registros encontrados: ", totalRegistros.ToString(), 0, Element.ALIGN_RIGHT, 0);
            CeldaVacio(tablaDelegacionUsuario, 3);

            tablaDelegacionUsuario.WidthPercentage = 100;

            documentoPdf.Add(table);
            documentoPdf.Add(tablaDelegacionUsuario);
            table.DeleteBodyRows();
        }

        private static void  InformacionReciboCorteDetenidos(Document documentoPdf, Font FuenteNormal, Font FuenteTituloTabla, float espacio, Entity.ContratoUsuario contratoUsuario, List<Entity.ListadoReporteCorteDetenidos> listadoDetenidos)
        {
            var datosAlertaWeb = ControlAlertaWeb.ObtenerTodos().FirstOrDefault();
            var denoma = "Alerta web";
            var alertas = "alerta web";
            if (!string.IsNullOrEmpty(datosAlertaWeb.Denominacion))
            {
                denoma = datosAlertaWeb.Denominacion;
                alertas = denoma;
            }

            BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormalAux = new Font(fuentecalibri, 10f, Font.NORMAL, BaseColor.BLACK);
            Font FuenteNegritaAux = new Font(fuentecalibri, 12f, Font.BOLD, blau);
            Font FuenteTitulo = new Font(fuentecalibri, 10f, Font.NORMAL, BaseColor.WHITE);
            float[] w1 = new float[] { 12, 9, 17, 8, 17, 14, 13, 10, 10, 11, 10, 10, 12, 9, 14, 12 };

            PdfPTable tabla = new PdfPTable(16);//Numero de columnas de la tabla            
            tabla.WidthPercentage = 100; //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical
            tabla.SetWidths(w1);
            
            PdfPCell cell;
            cell = new PdfPCell(new Phrase("No. remisión", FuenteTitulo));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Evento", FuenteTitulo));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Nombre", FuenteTitulo));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Edad", FuenteTitulo));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Ingreso", FuenteTitulo));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Calificación", FuenteTitulo));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Motivo detención", FuenteTitulo));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Celda", FuenteTitulo));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Cumple", FuenteTitulo));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Fecha", FuenteTitulo));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Folio", FuenteTitulo));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Motivo", FuenteTitulo));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Detención", FuenteTitulo));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);            

            cell = new PdfPCell(new Phrase("Unidad", FuenteTitulo));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Responsable unidad", FuenteTitulo));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Domicilio detención", FuenteTitulo));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            foreach(var item in listadoDetenidos)
            {                
                cell = new PdfPCell(new Phrase(item.Remision, FuenteNormalAux));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;                
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Evento, FuenteNormalAux));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Nombre, FuenteNormalAux));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                string edad = item.Edad > 0 ? item.Edad.ToString() : string.Empty;

                cell = new PdfPCell(new Phrase(edad, FuenteNormalAux));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Ingreso, FuenteNormalAux));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Calificacion, FuenteNormalAux));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Motivo, FuenteNormalAux));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Celda, FuenteNormalAux));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Cumple, FuenteNormalAux));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Fecha, FuenteNormalAux));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Folio, FuenteNormalAux));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Motivos, FuenteNormalAux));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.HoraDetencion, FuenteNormalAux));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Unidad, FuenteNormalAux));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Responsable, FuenteNormalAux));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Lugar, FuenteNormalAux));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);
            }

            documentoPdf.Add(tabla);
            //documentoPdf.NewPage();
            //tabla.DeleteBodyRows();
        }



        /// <param name="DetalleDetencionId"></param>
        /// <returns></returns>
        /// 
        #region reporte de uso de sistema
        public static object[] GenerarReporteUsoSistema(List<Entity.ReporteUsosistema> Reporte,string[] parametros)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Reporte_Uso_Sistema_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.Reporteusosistema"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 30f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                TituloReporteUsosistema(documentoPdf, FuenteTitulo, FuenteTituloTabla, blau, espacio, Reporte, parametros);
                

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloReporteUsosistema(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla,  BaseColor blau,float espacio, List<Entity.ReporteUsosistema> reporte, string[] parametros)
        {
            int[] w = new int[] { 19, 70 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));

            }

            var institucion = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);

            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 3;
            cellWithRowspan.Border = 0;
            tabla.AddCell(cellWithRowspan);
            Celda(tabla, FuenteTitulo, "Reporte de uso de sistema", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_TOP);
            documentoPdf.Add(tabla);
            PdfPTable tablatitulo = new PdfPTable(1);//Numero de columnas de la tabla
            tablatitulo.WidthPercentage = 100; //Porcentaje ancho
            tablatitulo.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical}


            var direccion = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);
            var muni = "";
            if(direccion !=null)
            {
                var municipio = ControlMunicipio.Obtener(Convert.ToInt32(direccion.MunicipioId));
                if (municipio != null) muni = municipio.Nombre;
            }
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 11f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);


            CeldaConChunk(tablatitulo, FuenteNegrita, FuenteNormal, "Municipio: ", muni, 0, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            documentoPdf.Add(tablatitulo);
            tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;
            CeldaConChunk(tabla, FuenteNegrita, FuenteNormal, "Del ", parametros[0], 0, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaConChunk(tabla, FuenteNegrita, FuenteNormal, " al ", parametros[1], 0, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            documentoPdf.Add(tabla);

            tablatitulo = new PdfPTable(1);//Numero de columnas de la tabla
            tablatitulo.WidthPercentage = 100; //Porcentaje ancho
            tablatitulo.HorizontalAlignment = Element.ALIGN_LEFT;//Alineación vertical}
            CeldaConChunk(tablatitulo, FuenteNegrita, FuenteNormal, "Institución: ", institucion.Nombre.ToUpper(), 0, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaVacio(tablatitulo, 1);
            documentoPdf.Add(tablatitulo);

            int[] w1 = new int[] { 12,13,12,11,11,14,12,15 };
            PdfPTable tabla1 = new PdfPTable(8);
            tabla1.WidthPercentage = 100; //Porcentaje ancho
            tabla1.SetWidths(w1);
           
            tabla1.HorizontalAlignment = Element.ALIGN_CENTER;

            BaseColor colorrelleno = new BaseColor(0, 61, 122);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorblanco;

            CeldaParaTablas(tabla1, FuenteTituloTabla, "Fecha", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla1, FuenteTituloTabla, "USUARIO", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla1, FuenteTituloTabla, "Nombre", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla1, FuenteTituloTabla, "Apellido paterno", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla1, FuenteTituloTabla, "Apellido materno", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla1, FuenteTituloTabla, "Perfil", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla1, FuenteTituloTabla, "Subcontrato(s)", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla1, FuenteTituloTabla, "Registros", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            var numero = 1;

            foreach( var item in reporte)
            {
                if (numero % 2 != 0)
                    color = colorgris;
                else
                    color = colorblanco;

                var h = item.Fecha.Hour.ToString();
                if (item.Fecha.Hour < 10) h = "0" + h;
                var m = item.Fecha.Minute.ToString();
                if (item.Fecha.Minute < 10) m = "0" + m;
                var s = item.Fecha.Second.ToString();
                if (item.Fecha.Second < 10) s = "0" + s;
                var fecha = item.Fecha.ToShortDateString() + " " + h + ":" + m + ":" + s;
                CeldaParaTablas(tabla1, FuenteNormal, fecha, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla1, FuenteNormal, item.Usuario, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla1, FuenteNormal, item.Nombre, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla1, FuenteNormal, item.ApellidoPaterno, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla1, FuenteNormal, item.ApellidoMaterno, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla1, FuenteNormal, item.Rol, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla1, FuenteNormal, item.SubContratos, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla1, FuenteNormal, item.Movimiento, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                numero++;
            }
            documentoPdf.Add(tabla1);

        }

        #endregion

        public static object[] GeneraReporteIngresos(Guid tkg,List<Entity.ListadoReporteIngresos>reporteIngresos,object[]parametros)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            documentoPdf.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Reporte_Ingresos_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.Reporteingresos"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                
                TituloReporteIngreso(documentoPdf, FuenteTitulo, FuenteTituloTabla,  tkg, blau,reporteIngresos,parametros);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloReporteIngreso(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, Guid tkg, BaseColor blau, List<Entity.ListadoReporteIngresos> reporteIngresos, object[] parametros)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;
            var ei = ControlDetalleDetencion.ObtenerPorTrackingId(tkg);
            if (ei != null)
            {
                centroId = ei.CentroId;
            }

            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception ex)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));

            }



            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            tabla.AddCell(cellWithRowspan);

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Reporte de ingresos", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);


            var contrato = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(parametros[1]));

            var instituciond = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);
            PdfPTable tablaDelegacionUsuario = new PdfPTable(1);
            float[] width = new float[] { 220 };
            tablaDelegacionUsuario.SetWidths(width);
            tablaDelegacionUsuario.DefaultCell.Border = Rectangle.NO_BORDER;
            PdfPCell celda;
            Chunk chunkTituloDelegacion = new Chunk("Delegación: ", FuenteNegrita);
            Chunk chunkNombreDelegacion = new Chunk(instituciond.Nombre, FuenteNormal);
            Paragraph parrafoDelegacion = new Paragraph();
            parrafoDelegacion.Add(chunkTituloDelegacion);
            parrafoDelegacion.Add(chunkNombreDelegacion);
            celda = new PdfPCell(parrafoDelegacion);
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.Border = 0;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            tablaDelegacionUsuario.AddCell(celda);
            
            Font FuenteTitulo2 = new Font(fuentecalibri, 10, Font.BOLD, blau);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
            CeldaConChunk(tablaDelegacionUsuario, FuenteTitulo2, FuenteNormal, "Del: ", Convert.ToDateTime(parametros[2]).ToShortDateString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteTitulo2, FuenteNormal, "Al: ", Convert.ToDateTime(parametros[3]).ToShortDateString(), 0, 0, 0);

            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, " Registros encontrados:", reporteIngresos.Count.ToString(), 0, Element.ALIGN_RIGHT, Element.ALIGN_RIGHT);



            CeldaVacio(tablaDelegacionUsuario, 1);

            float[] w3 = new float[] { 25,18,22,10,25,20,20,25,25};
            PdfPTable tablari = new PdfPTable(9);//Numero de columnas de la tabla            
            tablari.WidthPercentage = 100; //Porcentaje ancho
            tablari.SetWidths(w3);

            var datosAlertaWeb = ControlAlertaWeb.ObtenerTodos().FirstOrDefault();
            var denoma = "Alerta web";
            var alertas = "alerta web";
            if (!string.IsNullOrEmpty(datosAlertaWeb.Denominacion))
            {
                denoma = datosAlertaWeb.Denominacion;
                alertas = denoma;
            }

            BaseColor colorrelleno = new BaseColor(0, 61, 122);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            //Titulos de las columnas de las tablas

            PdfPCell cell;
            cell = new PdfPCell(new Phrase("Nombre del detenido", FuenteTituloTabla));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tablari.AddCell(cell);

            cell = new PdfPCell(new Phrase("No. remisión", FuenteTituloTabla));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tablari.AddCell(cell);

            cell = new PdfPCell(new Phrase("Fecha registro", FuenteTituloTabla));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tablari.AddCell(cell);

            cell = new PdfPCell(new Phrase("Folio " + alertas, FuenteTituloTabla));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tablari.AddCell(cell);

            cell = new PdfPCell(new Phrase("Motivo " + alertas, FuenteTituloTabla));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tablari.AddCell(cell);

            cell = new PdfPCell(new Phrase("Horas de detención", FuenteTituloTabla));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tablari.AddCell(cell);

            cell = new PdfPCell(new Phrase("Nº Unidad", FuenteTituloTabla));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tablari.AddCell(cell);

            cell = new PdfPCell(new Phrase("Responsable de unidad", FuenteTituloTabla));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tablari.AddCell(cell);

            cell = new PdfPCell(new Phrase("Lugar de detención", FuenteTituloTabla));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tablari.AddCell(cell);

            foreach (var item in reporteIngresos)
            {
                cell = new PdfPCell(new Phrase(item.Detenido, FuenteNormal));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tablari.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Expediente, FuenteNormal));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tablari.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.FechaAux, FuenteNormal));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tablari.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Folio, FuenteNormal));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tablari.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Motivo, FuenteNormal));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tablari.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.TotalHoras.ToString(), FuenteNormal));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tablari.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Unidad, FuenteNormal));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tablari.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.Responsable, FuenteNormal));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tablari.AddCell(cell);

                cell = new PdfPCell(new Phrase(item.LugarDetencion, FuenteNormal));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.BorderWidth = 0;
                tablari.AddCell(cell);                
            }

            tablaDelegacionUsuario.WidthPercentage = 100;            
            documentoPdf.Add(tabla);
            tablaDelegacionUsuario.WidthPercentage = 100;
            documentoPdf.Add(tablaDelegacionUsuario);
            documentoPdf.Add(tablari);
           
            tabla.DeleteBodyRows();
        }


        //Crear celdas
        public static void Celda(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                tabla.AddCell(celda);
            }

        }
        //para crear el Header y Footer
        public partial class HeaderFooter2 : PdfPageEventHelper
        {
            PdfContentByte cb;
            PdfTemplate headerTemplate;
            BaseFont bf = null;
            float tamanofuente = 8f;

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                }
                catch (DocumentException de) { }
                catch (System.IO.IOException ioe) { }
            }

            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                //Encabezado de Página
                Rectangle pageSize = doc.PageSize;
                PdfPTable footerTbl = new PdfPTable(1);
                BaseColor blau = new BaseColor(0, 61, 122);

                float[] TablaAncho = new float[] { 100 };
                footerTbl.SetWidths(TablaAncho);
                footerTbl.TotalWidth = 550;
                Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                PdfPCell cell;
                CeldaParaTablas(footerTbl, FuenteCelda, " ", 1, 0, 1, blau, 0, Element.ALIGN_TOP, Element.ALIGN_TOP);


                string pagina = "Hoja " + writer.PageNumber + " de ";
                //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
                {
                    float margenbottom = 11f;
                    float margenright = 115f;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, tamanofuente);
                    cb.SetTextMatrix(doc.PageSize.GetRight(margenright), doc.PageSize.GetBottom(margenbottom));
                    cb.ShowText(pagina);
                    cb.EndText();
                    float len = bf.GetWidthPoint(pagina, tamanofuente);
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetRight(margenright) + len, doc.PageSize.GetBottom(margenbottom));
                }
                string dia = String.Format("{0:dd}", DateTime.Now);
                string anio = String.Format("{0:yyyy}", DateTime.Now);
                string hora = String.Format("{0:HH:mm}", DateTime.Now);
                string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
                string fechahora = dia + " de " + mestexto + " de " + anio + " a las " + hora + " hrs.";
                Chunk chkFecha = new Chunk(fechahora, FuenteCelda);
                Phrase phrfecha = new Phrase(chkFecha);

                cell = new PdfPCell(phrfecha);
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidth = 0;
                footerTbl.AddCell(cell);
                footerTbl.WriteSelectedRows(0, -1, 25, 25, writer.DirectContent);
            }

            private static string ObtenerMesTexto(string mes)
            {
                string mestexto = "";
                switch (mes)
                {
                    case "01":
                        mestexto = "Enero";
                        break;
                    case "02":
                        mestexto = "Febrero";
                        break;
                    case "03":
                        mestexto = "Marzo";
                        break;
                    case "04":
                        mestexto = "Abril";
                        break;
                    case "05":
                        mestexto = "Mayo";
                        break;
                    case "06":
                        mestexto = "Junio";
                        break;
                    case "07":
                        mestexto = "Julio";
                        break;
                    case "08":
                        mestexto = "Agosto";
                        break;
                    case "09":
                        mestexto = "Septiembre";
                        break;
                    case "10":
                        mestexto = "Octubre";
                        break;
                    case "11":
                        mestexto = "Noviembre";
                        break;
                    case "12":
                        mestexto = "Diciembre";
                        break;
                    default:
                        mestexto = " ";
                        break;
                }
                return mestexto;
            }

            //Necesario para incluir pagina x de y al header
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);
                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, tamanofuente);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();
            }
        }

     
    }
}
