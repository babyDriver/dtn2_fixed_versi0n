﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Linq;

using System.Configuration;

namespace Business
{
    public static class ControlPDFJuez
    {
        //para crear el Header y Footer
        public partial class HeaderFooter : PdfPageEventHelper
        {
            PdfContentByte cb;
            PdfTemplate headerTemplate;
            BaseFont bf = null;
            float tamanofuente = 8f;

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                }
                catch (DocumentException de) { }
                catch (System.IO.IOException ioe) { }
            }

            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                //Encabezado de Página
                Rectangle pageSize = doc.PageSize;
                PdfPTable footerTbl = new PdfPTable(1);
                BaseColor blau = new BaseColor(0, 61, 122);

                float[] TablaAncho = new float[] { 100 };
                footerTbl.SetWidths(TablaAncho);
                footerTbl.TotalWidth = 550;
                Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                PdfPCell cell;
                CeldaParaTablas(footerTbl, FuenteCelda, " ", 1, 0, 1, blau, 0, Element.ALIGN_TOP, Element.ALIGN_TOP);

                string pagina = "Página " + writer.PageNumber + " de ";
                //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
                {
                    float margenbottom = 11f;
                    float margenright = 280f;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, tamanofuente);
                    cb.SetTextMatrix(doc.PageSize.GetBottom(margenright), doc.PageSize.GetBottom(margenbottom));
                    cb.ShowText(pagina);
                    cb.EndText();
                    float len = bf.GetWidthPoint(pagina, tamanofuente);
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));
                }

                PdfPTable footerTbl2 = new PdfPTable(2);
                
                float[] TablaAncho2 = new float[] { 100, 100 };
                footerTbl2.SetWidths(TablaAncho2);
                footerTbl2.TotalWidth = doc.Right - doc.Left;
                Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                //string dia = String.Format("{0:dd}", DateTime.Now);
                //string anio = String.Format("{0:yyyy}", DateTime.Now);
                //string hora = String.Format("{0:HH:mm}", DateTime.Now);
                //string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
                //string fechahora = dia + " de " + mestexto + " de " + anio + " " + hora;
                //Chunk chkFecha = new Chunk(fechahora, FuenteCelda);
                Chunk chkFecha = new Chunk("", FuenteCelda);
                Phrase phrfecha = new Phrase(chkFecha);

                //Phrase promad = new Phrase("Promad Soluciones Computarizadas " + anio, FuenteCelda2);
                Phrase promad = new Phrase("", FuenteCelda2);
                cell = new PdfPCell(promad);
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);

                cell = new PdfPCell(phrfecha);
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);
                footerTbl2.WriteSelectedRows(0, -1, 5, 25, writer.DirectContent);
            }

            private static string ObtenerMesTexto(string mes)
            {
                string mestexto = "";
                switch (mes)
                {
                    case "01":
                        mestexto = "Enero";
                        break;
                    case "02":
                        mestexto = "Febrero";
                        break;
                    case "03":
                        mestexto = "Marzo";
                        break;
                    case "04":
                        mestexto = "Abril";
                        break;
                    case "05":
                        mestexto = "Mayo";
                        break;
                    case "06":
                        mestexto = "Junio";
                        break;
                    case "07":
                        mestexto = "Julio";
                        break;
                    case "08":
                        mestexto = "Agosto";
                        break;
                    case "09":
                        mestexto = "Septiembre";
                        break;
                    case "10":
                        mestexto = "Octubre";
                        break;
                    case "11":
                        mestexto = "Noviembre";
                        break;
                    case "12":
                        mestexto = "Diciembre";
                        break;
                    default:
                        mestexto = " ";
                        break;
                }
                return mestexto;
            }

            //Necesario para incluir pagina x de y al header
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);
                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, tamanofuente);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();
            }
        }

        public partial class HeaderFooter2 : PdfPageEventHelper
        {
            PdfContentByte cb;
            PdfTemplate headerTemplate;
            BaseFont bf = null;
            float tamanofuente = 8f;

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                }
                catch (DocumentException de) { }
                catch (System.IO.IOException ioe) { }
            }

            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                //Encabezado de Página
                Rectangle pageSize = doc.PageSize;
                PdfPTable footerTbl = new PdfPTable(1);
                BaseColor blau = new BaseColor(0, 61, 122);

                float[] TablaAncho = new float[] { 100 };
                footerTbl.SetWidths(TablaAncho);
                footerTbl.TotalWidth = 550;
                Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                PdfPCell cell;
                CeldaParaTablas(footerTbl, FuenteCelda, " ", 1, 0, 1, blau, 0, Element.ALIGN_TOP, Element.ALIGN_TOP);

                string pagina = "Página " + writer.PageNumber + " de ";
                //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
                {
                    float margenbottom = 11f;
                    float margenright = 280f;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, tamanofuente);
                    cb.SetTextMatrix(doc.PageSize.GetBottom(margenright), doc.PageSize.GetBottom(margenbottom));
                    cb.ShowText(pagina);
                    cb.EndText();
                    float len = bf.GetWidthPoint(pagina, tamanofuente);
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));
                }

                PdfPTable footerTbl2 = new PdfPTable(2);

                float[] TablaAncho2 = new float[] { 100, 100 };
                footerTbl2.SetWidths(TablaAncho2);
                footerTbl2.TotalWidth = doc.Right - doc.Left;
                Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                string dia = String.Format("{0:dd}", DateTime.Now);
                string anio = String.Format("{0:yyyy}", DateTime.Now);
                string hora = String.Format("{0:HH:mm}", DateTime.Now);
                string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
                string fechahora = dia + " de " + mestexto + " de " + anio + " " + hora;
                Chunk chkFecha = new Chunk(fechahora, FuenteCelda);
                Phrase phrfecha = new Phrase(chkFecha);

                //Phrase promad = new Phrase("Promad Soluciones Computarizadas " + anio, FuenteCelda2);
                Phrase promad = new Phrase("", FuenteCelda2);
                cell = new PdfPCell(promad);
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);

                cell = new PdfPCell(phrfecha);
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);
                footerTbl2.WriteSelectedRows(0, -1, 5, 25, writer.DirectContent);
            }

            private static string ObtenerMesTexto(string mes)
            {
                string mestexto = "";
                switch (mes)
                {
                    case "01":
                        mestexto = "Enero";
                        break;
                    case "02":
                        mestexto = "Febrero";
                        break;
                    case "03":
                        mestexto = "Marzo";
                        break;
                    case "04":
                        mestexto = "Abril";
                        break;
                    case "05":
                        mestexto = "Mayo";
                        break;
                    case "06":
                        mestexto = "Junio";
                        break;
                    case "07":
                        mestexto = "Julio";
                        break;
                    case "08":
                        mestexto = "Agosto";
                        break;
                    case "09":
                        mestexto = "Septiembre";
                        break;
                    case "10":
                        mestexto = "Octubre";
                        break;
                    case "11":
                        mestexto = "Noviembre";
                        break;
                    case "12":
                        mestexto = "Diciembre";
                        break;
                    default:
                        mestexto = " ";
                        break;
                }
                return mestexto;
            }

            //Necesario para incluir pagina x de y al header
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);
                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, tamanofuente);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();
            }
        }

        private static string CrearDirectorio(string nombre)
        {
            try
            {


                //Verificar si existe el directorio principal del repositorio de documentos
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio de la obra existe
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio para el grupo documental existe, sino crearlo
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre)));

            

                return string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs//" + nombre);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("No fue posible crear el repositorio para crear el archivo. ", ex.Message));
            }
        }

        public static object ReporteProximosaCumplir(int centro, int contrato)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //milimetros para los margenes
            var space = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentpdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                //Nombre y ubicación archivo
                string nombrearchivo = "ProximosACumplir" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorio("ProximosACumplir"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string pathfont = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont calibrifont = BaseFont.CreateFont(pathfont, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font titlefont = new Font(calibrifont, 12, Font.BOLD);
                Font boldfont = new Font(calibrifont, 10f, Font.BOLD, blau);
                Font normalfont = new Font(calibrifont, 10f, Font.BOLD, BaseColor.WHITE);
                Font bigfont = new Font(calibrifont, 10, Font.BOLD);
                Font smallfont = new Font(calibrifont, 8, Font.NORMAL);
                Font bigtitlefont = new Font(calibrifont, 14, Font.BOLD, blau);
                Font fuente = new Font(calibrifont, 10f, Font.BOLD);
                Font normalfont2 = new Font(calibrifont, 10f, Font.NORMAL, BaseColor.BLACK);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentpdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return obj = new { success = false, file = "", message = ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();

                documentpdf.Open();

                TitleProximosaCumplir(documentpdf, titlefont, boldfont, normalfont2, space, bigfont, centro, bigtitlefont, fuente, contrato);
                GetDatosProximosaCumplir(documentpdf, boldfont, normalfont, space, smallfont, centro, contrato);

                obj = new { success = true, file = ubicacionarchivo, message = "" };
                return obj;
            }
            catch (Exception ex)
            {
                return obj = new { success = false, file = "", message = ex.Message };
            }
            finally
            {
                if (documentpdf.IsOpen())
                {
                    documentpdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TitleProximosaCumplir(Document documentpdf, Font titlefont, Font boldfont, Font normalfont, float space, Font bigfont, int centroId, Font bigtitle, Font fuente, int contrato)
        {
            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);
            Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);
            var x = ControlSubcontrato.ObtenerByContratoId(institucion.ContratoId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);


            PdfPTable tablaTitulo = new PdfPTable(1);
            tablaTitulo.HorizontalAlignment = Element.ALIGN_CENTER;

            string escudo = "~/Content/img/Escudo.jpg";
            string logo = "~/Content/img/logo.png";
            var logoizq = subcontrato.Banner;
            if (logoizq != "undefined" && !string.IsNullOrEmpty(logoizq))
            {
                logo = logoizq;
            }
            string pathEscudo = System.Web.HttpContext.Current.Server.MapPath(escudo);
            string pathLogo = System.Web.HttpContext.Current.Server.MapPath(logo);
            iTextSharp.text.Image imageEscudo = null;
            iTextSharp.text.Image imageLogo = null;
            try
            {
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }
            catch (Exception)
            {
                pathEscudo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }

            try
            {
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }
            catch (Exception)
            {
                pathLogo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }

            var logoDER = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logoDER != "undefined" && logo != "")
            {
                logotipo = logoDER;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }

            imageLogo.ScalePercent(95f);
            
                imageLogo.ScaleAbsoluteHeight(100f);
                imageLogo.ScaleAbsoluteWidth(300f);
            
            image.ScaleAbsoluteHeight(60f);
            image.ScaleAbsoluteWidth(70f);
            Paragraph para = new Paragraph();
            Phrase ph1 = new Phrase();
            Chunk glue = new Chunk(new iTextSharp.text.pdf.draw.VerticalPositionMark());
            Paragraph main = new Paragraph();
            ph1.Add(new Chunk(imageLogo, 0, 0, true));
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(image, 0, 15, true));
            main.Add(ph1);
            para.Add(main);
            documentpdf.Add(para);


            string nombreEstado = "Estado";
            string nombreMunicipio = "Municipio";
            string nombreCentro = institucion.Nombre;

            if (domicilio != null)
            {
                Entity.Estado estado = ControlEstado.ObtenerPorId(Convert.ToInt32(domicilio.EstadoId));

                if (estado != null)
                {
                    nombreEstado = estado.Nombre;
                    Entity.Municipio municipio = ControlMunicipio.Obtener(Convert.ToInt32(domicilio.MunicipioId));
                    if (municipio != null) nombreMunicipio = municipio.Nombre;
                }
            }
            List<internoAux> listaInterno = new List<internoAux>();
            var internos = ControlDetenido.ObteneTodos();
            string fechaSalida;
            foreach (var ka in internos)
            {
                fechaSalida = "";
                var estatusI = ControlDetalleDetencion.ObtenerPorDetenidoId(ka.Id);
                foreach (var ei in estatusI)
                {
                    if (ei.ContratoId == contrato && ei.Activo)
                    {
                        var salida = ControlSalidaEfectuadaJuez.ObtenerByDetalleDetencionId(ei.Id);
                        if(salida ==null)
                        {
                            var calificacionI = ControlCalificacion.ObtenerPorInternoId(ei.Id);
                            if (calificacionI != null)
                            {
                                if (calificacionI.Activo == true)
                                {
                                    DateTime suma = Convert.ToDateTime(ei.Fecha).AddHours(calificacionI.TotalHoras);
                                    fechaSalida = suma.ToString();
                                    
                                        listaInterno.Add(new internoAux() { Id = ka.Id.ToString(), nombre = ka.Nombre, paterno = ka.Paterno, materno = ka.Materno, fecha = fechaSalida });
                                    
                                }

                            }
                            else
                            {
                                listaInterno.Add(new internoAux() { Id = ka.Id.ToString(), nombre = ka.Nombre, paterno = ka.Paterno, materno = ka.Materno, fecha = fechaSalida });

                            }
                        }
                       

                    }
                }
            }
            int contadork = 0;

            foreach (var i in listaInterno)
            {
                List<Entity.DetalleDetencion> estatus = ControlDetalleDetencion.ObtenerPorDetenidoId(Convert.ToInt32(i.Id));

                if (estatus != null)
                {
                    contadork++;
                }

            }

            int[] w = new int[] { 8, 8 };
            PdfPTable table = new PdfPTable(2);
            table.SetWidths(w);
            table.TotalWidth = 550;
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell;

            string title = "Juzgado Calificador";
            string lugar = nombreCentro;
       

            cell = new PdfPCell(new Phrase("Ayuntamiento de " + nombreMunicipio + "," + nombreEstado, bigtitle));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.PaddingLeft = 10;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(title, bigtitle));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.PaddingLeft = 10;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(nombreMunicipio.ToUpper() + "," + nombreEstado.ToUpper(), titlefont));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.PaddingLeft = 10;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Detenidos próximos a cumplir", titlefont));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.PaddingLeft = 10;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(" "));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);

            int[] w2 = new int[] { 35, 50 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w2);
            tabla.WidthPercentage = 100;
            BaseColor blau = new BaseColor(0, 61, 122);
            int contador = 0;
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal2 = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita2 = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            CeldaConChunk(tabla, FuenteNegrita2, FuenteNormal2, "Delegación: ", lugar, 0, 0, 0);
            CeldaConChunk(tabla, FuenteNegrita2, FuenteNormal2, "Registros encontrados: ", contadork.ToString(), 0, Element.ALIGN_RIGHT, 0);
            //cell = new PdfPCell(new Phrase("Registros encontrados: " + contadork, boldfont));
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            //cell.BorderWidth = 0;
            //cell.Padding = space;
            //tabla.AddCell(cell);

            documentpdf.Add(table);
            documentpdf.Add(tabla);
        }

        private static void GetDatosProximosaCumplir(Document documentpdf, Font boldfont, Font normalfont, float space, Font smallfont, int centroId, int contrato)
        {
            var internos = ControlDetenido.ObteneTodos();
            string fechaSalida;

            List<internoAux> listaInterno = new List<internoAux>();
            foreach (var ka in internos)
            {
                fechaSalida = "";
                var estatusI = ControlDetalleDetencion.ObtenerPorDetenidoId(ka.Id);
                foreach (var ei in estatusI)
                {
                    var salida = ControlSalidaEfectuadaJuez.ObtenerByDetalleDetencionId(ei.Id);
                    if(salida ==null)
                    {
                        if (ei.ContratoId == contrato && ei.Activo)
                        {
                            var calificacionI = ControlCalificacion.ObtenerPorInternoId(ei.Id);
                            if (calificacionI != null)
                            {
                                if (calificacionI.Activo == true)
                                {
                                    DateTime suma = Convert.ToDateTime(ei.Fecha).AddHours(calificacionI.TotalHoras);

                                    var date = suma.ToShortDateString();
                                    var h = suma.Hour.ToString();
                                    if (suma.Hour < 10) h = "0" + h;
                                    var m = suma.Minute.ToString();
                                    if (suma.Minute < 10) m = "0" + m;
                                    var s = suma.Second.ToString();
                                    if (suma.Second < 10) s = "0" + s;
                                    fechaSalida = date + " " + h + ":" + m + ":" + s;
                                        listaInterno.Add(new internoAux() { Id = ka.Id.ToString(), nombre = ka.Nombre, paterno = ka.Paterno, materno = ka.Materno, fecha = fechaSalida, IdDetencion = ei.Id.ToString() });
                                    
                                }

                            }
                            else
                            {
                                listaInterno.Add(new internoAux() { Id = ka.Id.ToString(), nombre = ka.Nombre, paterno = ka.Paterno, materno = ka.Materno, fecha = fechaSalida, IdDetencion = ei.Id.ToString() });
                            }
                        }
                    }
                    
                }
            }

            listaInterno.Sort((x, y) => x.fecha.CompareTo(y.fecha));            

            float[] width = new float[] { 100 };
            PdfPTable table = new PdfPTable(1);
            table.SetWidths(width);
            table.WidthPercentage = 1;
     
            table.HorizontalAlignment = Element.ALIGN_CENTER;

            float[] w2 = new float[] { 40, 90, 90,90, 50, 25, 35, 100 };
            PdfPTable tabla = new PdfPTable(8);
            tabla.SetWidths(w2);
            tabla.WidthPercentage = 100;
            PdfPCell cell;            

            cell = new PdfPCell(new Phrase("No. remisión", normalfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(0, 61, 122);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Nombre", normalfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(0, 61, 122);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Fecha de ingreso", normalfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(0, 61, 122);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Fecha de salida", normalfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(0, 61, 122);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Situación", normalfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(0, 61, 122);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Hrs", normalfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(0, 61, 122);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Multa", normalfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(0, 61, 122);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("A Disposición", normalfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(0, 61, 122);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int ii = 1;
            foreach (var i in listaInterno)
            {
                if (ii % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                var detencion = ControlDetalleDetencion.ObtenerPorId(Convert.ToInt32(i.IdDetencion));

                //Remisión
                cell = new PdfPCell(new Paragraph(detencion.Expediente, smallfont));
                cell.BackgroundColor = color;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                //Nombre
                cell = new PdfPCell(new Paragraph(i.nombre.Trim() + " " + i.paterno.Trim() + " " + i.materno.Trim(), smallfont));
                cell.BackgroundColor = color;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                //Fecha
                var date1 = Convert.ToDateTime(detencion.Fecha).ToShortDateString();
                var h1 = Convert.ToDateTime(detencion.Fecha).Hour.ToString();
                if (Convert.ToDateTime(detencion.Fecha).Hour < 10) h1 = "0" + h1;
                var m1 = Convert.ToDateTime(detencion.Fecha).Minute.ToString();
                if (Convert.ToDateTime(detencion.Fecha).Minute < 10) m1 = "0" + m1;
                var s1 = Convert.ToDateTime(detencion.Fecha).Second.ToString();
                if (Convert.ToDateTime(detencion.Fecha).Second < 10) s1 = "0" + s1;
                var F1 = date1 + " " + h1 + ":" + m1 + ":" + s1;

                cell = new PdfPCell(new Paragraph(F1, smallfont));
                cell.BackgroundColor = color;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                cell = new PdfPCell(new Paragraph(i.fecha, smallfont));
                cell.BackgroundColor = color;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                //Situación
                var calificacion = ControlCalificacion.ObtenerPorInternoId(detencion.Id);
                if (calificacion != null && calificacion.Activo != false)
                {
                    Entity.Catalogo situacion = ControlCatalogo.Obtener(calificacion.SituacionId, Convert.ToInt32(Entity.TipoDeCatalogo.situacion_detenido));
                    if (situacion != null)
                    {
                        cell = new PdfPCell(new Paragraph(situacion.Nombre, smallfont));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.BackgroundColor = color;
                        cell.BorderWidth = 0;
                        tabla.AddCell(cell);
                    }
                    else
                    {
                        cell = new PdfPCell(new Paragraph(""));
                        cell.BackgroundColor = color;
                        cell.BorderWidth = 0;
                        tabla.AddCell(cell);
                    }
                }
                else
                {
                    cell = new PdfPCell(new Paragraph(""));
                    cell.BackgroundColor = color;
                    cell.BorderWidth = 0;
                    tabla.AddCell(cell);
                }

                //Horas
                if (calificacion != null)
                {
                    cell = new PdfPCell(new Paragraph(calificacion.TotalHoras.ToString(), smallfont));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.BackgroundColor = color;
                    cell.BorderWidth = 0;
                    tabla.AddCell(cell);
                }
                else
                {
                    cell = new PdfPCell(new Paragraph(""));
                    cell.BackgroundColor = color;
                    cell.BorderWidth = 0;
                    tabla.AddCell(cell);
                }

                //Multa
                if (calificacion != null)
                {
                    cell = new PdfPCell(new Paragraph(calificacion.TotalAPagar.ToString(), smallfont));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.BackgroundColor = color;
                    cell.BorderWidth = 0;
                    tabla.AddCell(cell);
                }
                else
                {
                    cell = new PdfPCell(new Paragraph(""));
                    cell.BackgroundColor = color;
                    cell.BorderWidth = 0;
                    tabla.AddCell(cell);
                }

                //A Disposición
                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detencion.CentroId);
                cell = new PdfPCell(new Paragraph(institucion.Nombre, smallfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = color;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);
                
            }

            documentpdf.Add(table);
            documentpdf.Add(tabla);
        }

        public static object[] generarReciboPagoMulta(object[] data, List<Entity.PagoMulta> info, int internoId)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //20 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Recibo_Caja_PagoMulta_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorio("01.PagoMulta"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 11f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                TituloReciboMulta(documentoPdf, FuenteTitulo, FuenteNegrita12, data, internoId);
                InformacionReciboMulta(documentoPdf, FuenteNormal, FuenteNegrita, espacio, info, data);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloReciboMulta(Document documentoPdf, Font FuenteTitulo, Font FuenteSubtitulo, object[] data, int internoId)
        {
            int centroId = 0;
            var ei = ControlDetalleDetencion.ObtenerPorDetenidoId(internoId);
            foreach (var val in ei)
            {
                centroId = val.CentroId;
            }

            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);


            PdfPTable tablaTitulo = new PdfPTable(1);
            tablaTitulo.HorizontalAlignment = Element.ALIGN_CENTER;

            string escudo = "~/Content/img/Escudo.jpg";
            string logo = "~/Content/img/logo.png";
            var logoizq = subcontrato.Banner;
            if (logoizq != "undefined" && !string.IsNullOrEmpty(logoizq))
            {
                logo = logoizq;
            }
            string pathEscudo = System.Web.HttpContext.Current.Server.MapPath(escudo);
            string pathLogo = System.Web.HttpContext.Current.Server.MapPath(logo);
            iTextSharp.text.Image imageEscudo = null;
            iTextSharp.text.Image imageLogo = null;
            try
            {
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }
            catch (Exception)
            {
                pathEscudo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }

            try
            {
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }
            catch (Exception)
            {
                pathLogo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }

            var logoDER = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logoDER != "undefined" && logo != "")
            {
                logotipo = logoDER;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }


            imageLogo.ScalePercent(95f);
            
                imageLogo.ScaleAbsoluteHeight(100f);
                imageLogo.ScaleAbsoluteWidth(300f);
            
            image.ScaleAbsoluteHeight(60f);
            image.ScaleAbsoluteWidth(70f);
            Paragraph para = new Paragraph();
            Phrase ph1 = new Phrase();
            Chunk glue = new Chunk(new iTextSharp.text.pdf.draw.VerticalPositionMark());
            Paragraph main = new Paragraph();
            ph1.Add(new Chunk(imageLogo, 0, 0, true));
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(image, 0, 15, true));
            main.Add(ph1);
            para.Add(main);
            documentoPdf.Add(para);


            int[] w = new int[] { 6, 8 };
            PdfPTable table = new PdfPTable(2);
            table.SetWidths(w);
            table.TotalWidth = 550;
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell;

            CeldaVacio(table, 2);

            
            
            cell = new PdfPCell(new Phrase("Recibo de caja", FuenteTitulo));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Rowspan = 5;
            cell.PaddingLeft = 10;
            table.AddCell(cell);

            CeldaVacio(table, 2);

            PdfPTable tabla = new PdfPTable(1);
            //Numero de columnas de la tabla
            tabla.WidthPercentage = 100;                                                            //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;                                       //Alineación vertical
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(8); //1 milimetro                                  

            //Renglón vacio
            CeldaVacio(tabla, 2);

            //Nombre delegacion y usuario en barandilla
            BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);
            PdfPTable tablaDelegacionUsuario = new PdfPTable(1);
            float[] width = new float[] { 220 };
            tablaDelegacionUsuario.SetWidths(width);
            tablaDelegacionUsuario.DefaultCell.Border = Rectangle.NO_BORDER;

            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Delegación: ", data[0].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "N° de recibo: ", data[1].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "No. remisión: ", data[2].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Registro: ", data[3].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Nombre: ", data[4].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Domicilio: ", data[5].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Calificación: ", data[6].ToString(), 0, 0, 0);
            CeldaVacio(tablaDelegacionUsuario, 1);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Motivos de calificación", "", 0, 0, 0);
            CeldaVacio(tablaDelegacionUsuario, 1);

            tablaDelegacionUsuario.WidthPercentage = 100;

            documentoPdf.Add(table);
            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaDelegacionUsuario);
            tabla.DeleteBodyRows();
        }

        private static void InformacionReciboMulta(Document documentoPdf, Font FuenteNormal, Font FuenteNegrita, float espacio, List<Entity.PagoMulta> datos, object[] data)
        {
            BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormalAux = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegritaAux = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            PdfPTable tabla = new PdfPTable(3);//Numero de columnas de la tabla            
            tabla.WidthPercentage = 100; //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            PdfPTable tablaTotales = new PdfPTable(3);
            tablaTotales.WidthPercentage = 100;
            tablaTotales.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPTable tablaFinales = new PdfPTable(1);
            tablaFinales.WidthPercentage = 100;
            tablaFinales.HorizontalAlignment = Element.ALIGN_CENTER;

            
            Font FuenteEncabezado = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.WHITE);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);

            //Titulos de las columnas de las tablas
            CeldaParaTablas(tabla, FuenteEncabezado, "Artículo", 1, 0, 1, blau, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteEncabezado, "Motivo", 1, 0, 1, blau, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteEncabezado, "Multa", 1, 0, 1, blau, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;

            //celdas, contenido de la tabla  
            foreach (var item in datos)
            {
                if (i % 2 != 0)
                    color = colorgris;
                else color = colorblanco;
                CeldaParaTablas(tabla, FuenteNormal, item.Articulo, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, item.Motivo, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, " $" + item.MultaSugerida.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                i++;
            }

            CeldaVacio(tabla, 1);

            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Total de multas ", " $" + data[7].ToString(), 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Agravante ", " $" + data[8].ToString(), 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Ajuste ", " %" + data[9].ToString(), 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Total a pagar ", " $" + data[10].ToString(), 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Horas de arresto ", data[11].ToString(), 0, 0, 0);

            CeldaVacio(tablaTotales, 3);

            CeldaConChunk(tablaFinales, FuenteNegritaAux, FuenteNormalAux, "Fundamento: ", data[12].ToString(), 0, 0, 0);
            CeldaConChunk(tablaFinales, FuenteNegritaAux, FuenteNormalAux, "Motivo de salida: ", "PAGO DE MULTA", 0, 0, 0);

            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaTotales);
            documentoPdf.Add(tablaFinales);
            documentoPdf.NewPage();
            tabla.DeleteBodyRows();
            tablaTotales.DeleteBodyRows();
            tablaFinales.DeleteBodyRows();
        }

        #region Orden de pago

        public static object[] generarReciboOrdenDePago(object[] data, List<Entity.PagoMulta> info)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Recibo_OrdenDePago_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorio("01.PagoMulta"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes     
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 11f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                TituloOrdenDePago(documentoPdf, FuenteTitulo, FuenteNegrita12, data);
                InformacionOrdenDePago(documentoPdf, FuenteNormal, FuenteNegrita, espacio, info, data);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloOrdenDePago(Document documentoPdf, Font FuenteTitulo, Font FuenteSubtitulo, object[] data)
        {
            PdfPTable tablaTitulo = new PdfPTable(1);
            tablaTitulo.HorizontalAlignment = Element.ALIGN_CENTER;
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
            string escudo = "~/Content/img/Escudo.jpg";
            string logo = "~/Content/img/logo.png";

            var logoizq = subcontrato.Banner;
            if (logoizq != "undefined" && !string.IsNullOrEmpty(logoizq))
            {
                logo = logoizq;
            }

            string pathEscudo = System.Web.HttpContext.Current.Server.MapPath(escudo);
            string pathLogo = System.Web.HttpContext.Current.Server.MapPath(logo);
            iTextSharp.text.Image imageEscudo = null;
            iTextSharp.text.Image imageLogo = null;
            try
            {
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }
            catch (Exception)
            {
                pathEscudo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }

            try
            {
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }
            catch (Exception)
            {
                pathLogo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }
            

            var logoDER = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logoDER != "undefined" && logo != "")
            {
                logotipo = logoDER;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }

            imageLogo.ScalePercent(95f);
            
                imageLogo.ScaleAbsoluteHeight(100f);
                imageLogo.ScaleAbsoluteWidth(300f);
            
            image.ScaleAbsoluteHeight(60f);
            image.ScaleAbsoluteWidth(70f);
            Paragraph para = new Paragraph();
            Phrase ph1 = new Phrase();
            Chunk glue = new Chunk(new iTextSharp.text.pdf.draw.VerticalPositionMark());
            Paragraph main = new Paragraph();
            ph1.Add(new Chunk(imageLogo, 0, 0, true));
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(image, 0, 15, true));
            main.Add(ph1);
            para.Add(main);
            documentoPdf.Add(para);

            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;
            var ei = ControlDetalleDetencion.ObtenerPorDetenidoId(Convert.ToInt32(data[13]));
            foreach (var val in ei)
            {
                centroId = val.CentroId;
            }

            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

           

            // logo = subcontrato.Logotipo;
            //string logotipo = "~/Content/img/logo.png";

            //if (logo != "undefined" && logo != "")
            //{
            //    logotipo = logo;
            //}

            //string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            //var image = iTextSharp.text.Image.GetInstance(pathfoto);

            //PdfPCell cellWithRowspan = new PdfPCell(image, true);
            //// The first cell spans 5 rows  
            //cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            //cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            //cellWithRowspan.Rowspan = 5;
            //cellWithRowspan.Border = 0;
            //tabla.AddCell(cellWithRowspan);

            //Titulo
            Celda(tabla, FuenteTitulo, " ", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);
            Celda(tabla, FuenteTitulo, "    Orden de pago", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_LEFT);

            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);
            PdfPTable tablaDelegacionUsuario = new PdfPTable(1);
            float[] width = new float[] { 220 };
            tablaDelegacionUsuario.SetWidths(width);
            tablaDelegacionUsuario.DefaultCell.Border = Rectangle.NO_BORDER;

            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Delegación: ", data[0].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "N° de recibo: ", data[1].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "No. remisión: ", data[2].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Registro: ", data[3].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Nombre: ", data[4].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Domicilio: ", data[5].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Calificación: ", data[6].ToString(), 0, 0, 0);
            CeldaVacio(tablaDelegacionUsuario, 1);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Motivos de calificación", "", 0, 0, 0);
            CeldaVacio(tablaDelegacionUsuario, 1);

            tablaDelegacionUsuario.WidthPercentage = 100;

            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaDelegacionUsuario);
            tabla.DeleteBodyRows();
        }

        private static void InformacionOrdenDePago(Document documentoPdf, Font FuenteNormal, Font FuenteNegrita, float espacio, List<Entity.PagoMulta> datos, object[] data)
        {
            BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormalAux = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegritaAux = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            PdfPTable tabla = new PdfPTable(3);//Numero de columnas de la tabla            
            tabla.WidthPercentage = 100; //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            PdfPTable tablaTotales = new PdfPTable(3);
            tablaTotales.WidthPercentage = 100;
            tablaTotales.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPTable tablaFinales = new PdfPTable(1);
            tablaFinales.WidthPercentage = 100;
            tablaFinales.HorizontalAlignment = Element.ALIGN_CENTER;

            BaseColor colorrelleno = blau;
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);

            //Titulos de las columnas de las tablas
            CeldaParaTablas(tabla, FuenteNegrita, "Artículo", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Motivo", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Multa", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            BaseColor colorgris = new BaseColor(238, 238, 238);
            BaseColor color = colorgris;
            int ii = 1;                           

                //celdas, contenido de la tabla  
                foreach (var item in datos)
            {
                if (ii % 2 != 0)
                    color = colorgris;
                else color = colorblanco;
                CeldaParaTablas(tabla, FuenteNormal, item.Articulo, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, item.Motivo, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, " $" + item.MultaSugerida.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                ii++;
            }

            CeldaVacio(tabla, 1);

            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Total de multas ", " $" + data[7].ToString(), 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Agravante ", " $" + data[8].ToString(), 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Ajuste ", " %" + data[9].ToString(), 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Total a pagar ", " $" + data[10].ToString(), 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Horas de arresto ", data[11].ToString(), 0, 0, 0);

            CeldaVacio(tablaTotales, 3);            

            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaTotales);            
            documentoPdf.NewPage();
            tabla.DeleteBodyRows();
            tablaTotales.DeleteBodyRows();
            tablaFinales.DeleteBodyRows();
        }

        #endregion

        //Crear celdas
        public static void Celda(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                tabla.AddCell(celda);
            }

        }

        public static void CeldaConChunk(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, int bordo, int horizontal, int vertical)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk(valor, FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = horizontal;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }

        //Crear celdas para tablas
        public static void CeldaParaTablas(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, BaseColor colorrelleno, float espacio, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                celda.BackgroundColor = colorrelleno;
                celda.Padding = espacio;
                celda.UseAscender = true;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas vacias
        public static void CeldaVacio(PdfPTable tabla, int cantidad)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                celda.BorderWidth = 0;
                tabla.AddCell(celda);
            }

        }

        public static object ReporteSituaciones(int centro, int contrato, string situacion, int idSituacion, string tipoContrato)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //milimetros para los margenes
            var space = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentpdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                //Nombre y ubicación archivo
                string nombrearchivo = "SituacionDetenidos" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorio("Situacion"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string pathfont = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont calibrifont = BaseFont.CreateFont(pathfont, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font titlefont = new Font(calibrifont, 12, Font.BOLD, blau);
                Font boldfont = new Font(calibrifont, 11f, Font.BOLD, blau);
                Font normalfont = new Font(calibrifont, 10f, Font.BOLD, BaseColor.WHITE);
                Font titulotabla = new Font(calibrifont, 12f, Font.BOLD, BaseColor.WHITE);
                Font bigfont = new Font(calibrifont, 10, Font.BOLD);
                Font smallfont = new Font(calibrifont, 10, Font.NORMAL);
                Font bigtitlefont = new Font(calibrifont, 14, Font.BOLD);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentpdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return obj = new { success = false, file = "", message = ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();

                documentpdf.Open();

                TitleSituaciones(documentpdf, titlefont, boldfont, smallfont, space, bigfont, centro, bigtitlefont,contrato,situacion,idSituacion,tipoContrato);
                GetDatosSituaciones(documentpdf, boldfont, normalfont, space, smallfont, centro, contrato, situacion, idSituacion, tipoContrato, titulotabla);

                obj = new { success = true, file = ubicacionarchivo, message = "" };
                return obj;
            }
            catch (Exception ex)
            {
                return obj = new { success = false, file = "", message = ex.Message };
            }
            finally
            {
                if (documentpdf.IsOpen())
                {
                    documentpdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TitleSituaciones(Document documentpdf, Font titlefont, Font boldfont, Font smallfont, float space, Font bigfont, int centroId, Font bigtitle, int contrato, string situacion, int situacionId, string tipoContrato)
        {
            PdfPTable tablaTitulo = new PdfPTable(1);
            tablaTitulo.HorizontalAlignment = Element.ALIGN_CENTER;
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
            string escudo = "~/Content/img/Escudo.jpg";
            string logo = "~/Content/img/logo.png";
            var logoizq = subcontrato.Banner;
            if (logoizq != "undefined" && !string.IsNullOrEmpty(logoizq))
            {
                logo = logoizq;
            }
            string pathEscudo = System.Web.HttpContext.Current.Server.MapPath(escudo);
            string pathLogo = System.Web.HttpContext.Current.Server.MapPath(logo);
            iTextSharp.text.Image imageEscudo = null;
            iTextSharp.text.Image imageLogo = null;
            try
            {
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }
            catch (Exception)
            {
                pathEscudo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }

            try
            {
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }
            catch (Exception)
            {
                pathLogo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }

            

            var logoDER = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logoDER != "undefined" && logo != "")
            {
                logotipo = logoDER;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }


            imageLogo.ScalePercent(95f);
            
                imageLogo.ScaleAbsoluteHeight(100f);
                imageLogo.ScaleAbsoluteWidth(300f);
            
            image.ScaleAbsoluteHeight(60f);
            image.ScaleAbsoluteWidth(70f);
            Paragraph para = new Paragraph();
            Phrase ph1 = new Phrase();
            Chunk glue = new Chunk(new iTextSharp.text.pdf.draw.VerticalPositionMark());
            Paragraph main = new Paragraph();
            ph1.Add(new Chunk(imageLogo, 0, 0, true));
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(image, 0, 15, true));
            main.Add(ph1);
            para.Add(main);
            documentpdf.Add(para);
            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);
            Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);

          

            

            string nombreEstado = "Estado";
                string nombreMunicipio = "Municipio";
                string nombreCentro = institucion.Nombre;

                if (domicilio != null)
                {
                    Entity.Estado estado = ControlEstado.ObtenerPorId(Convert.ToInt32(domicilio.EstadoId));

                    if (estado != null)
                    {
                        nombreEstado = estado.Nombre;
                        Entity.Municipio municipio = ControlMunicipio.Obtener(Convert.ToInt32(domicilio.MunicipioId));
                        if (municipio != null) nombreMunicipio = municipio.Nombre;
                    }
                }


            int[] w = new int[] { 6, 8 };
            PdfPTable table = new PdfPTable(2);
            table.SetWidths(w);
            table.TotalWidth = 550;
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell;

            CeldaVacio(tablaTitulo, 1);
                        
       

            string title = "Juzgado Calificador";
                string lugar = nombreCentro;

                cell = new PdfPCell(new Phrase("Ayuntamiento de " + nombreMunicipio + "," + nombreEstado, titlefont));
                cell.BorderWidth = 0;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.VerticalAlignment = Element.ALIGN_LEFT;
                cell.PaddingLeft = 10;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(title, titlefont));
                cell.BorderWidth = 0;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.VerticalAlignment = Element.ALIGN_LEFT;
                cell.PaddingLeft = 10;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(nombreMunicipio.ToUpper() + "," + nombreEstado.ToUpper(), bigtitle));
                cell.BorderWidth = 0;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.VerticalAlignment = Element.ALIGN_LEFT;
                cell.PaddingLeft = 10;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Detenidos en existencia por situación", bigtitle));
                cell.BorderWidth = 0;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.VerticalAlignment = Element.ALIGN_LEFT;
                cell.PaddingLeft = 10;
                table.AddCell(cell);

            var internos = ControlDetenido.ObteneTodos();
            int contador = 0;
            int contadorK = 0;

            foreach (var i in internos)
            {

                bool centro2 = false;
                Entity.Calificacion calificacionSituacion = new Entity.Calificacion();
                List<Entity.DetalleDetencion> estatus = ControlDetalleDetencion.ObtenerPorDetenidoId(i.Id);

                if (estatus != null)
                {
                    foreach (var k in estatus)
                    {
                        calificacionSituacion = ControlCalificacion.ObtenerPorInternoId(k.Id);
                        if (calificacionSituacion != null)
                        {
                            if (k.ContratoId == contrato && k.Activo == true && calificacionSituacion.SituacionId == situacionId && k.Tipo == tipoContrato)
                            {
                                centro2 = true;
                                break;

                            }
                        }
                    }

                    if (centro2 == true)
                    {
                        contadorK++;
                    }
                }
            }
            int[] w2 = new int[] { 30, 50 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w2);
            tabla.WidthPercentage = 100;

            BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal2 = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita2 = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            CeldaConChunk(tabla, FuenteNegrita2, FuenteNormal2, "Delegación: ", lugar, 0, 0, 0);
            CeldaConChunk(tabla, FuenteNegrita2, FuenteNormal2, "Registros encontrados: ", contadorK.ToString(), 0, Element.ALIGN_RIGHT, 0);
            
            CeldaVacio(tabla, 1);
            CeldaVacio(tabla, 1);
            
            documentpdf.Add(table);
            documentpdf.Add(tabla);
        }

        private static void GetDatosSituaciones(Document documentpdf, Font boldfont, Font normalfont, float space, Font smallfont, int centroId, int contrato, string situacion, int situacionId, string tipoContrato, Font titulotabla)
        {
            var internos = ControlDetenido.ObteneTodos();
            int contador = 0;
            int contadorK = 0;

            foreach (var i in internos)
            {

                bool centro2 = false;
                Entity.Calificacion calificacionSituacion = new Entity.Calificacion();
                List<Entity.DetalleDetencion> estatus = ControlDetalleDetencion.ObtenerPorDetenidoId(i.Id);

                if (estatus != null)
                {
                    foreach (var k in estatus)
                    {
                        calificacionSituacion = ControlCalificacion.ObtenerPorInternoId(k.Id);
                        if (calificacionSituacion != null)
                        {
                            if (k.ContratoId == contrato && k.Activo == true && calificacionSituacion.SituacionId == situacionId && k.Tipo == tipoContrato)
                            {
                                centro2 = true;
                                break;

                            }
                        }
                    }

                    if (centro2 == true)
                    {
                        contadorK ++;
                    }
                }
            }

                        float[] width = new float[] { 100 };
            PdfPTable table = new PdfPTable(1);
            table.SetWidths(width);
            table.WidthPercentage = 1;
            table.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPTable tituloSituacion = new PdfPTable(1);
            tituloSituacion.WidthPercentage = 100;
            PdfPCell celda;

            

            celda = new PdfPCell(new Phrase(situacion, titulotabla));
            celda.HorizontalAlignment = Element.ALIGN_CENTER;
            celda.BackgroundColor = new BaseColor(0, 61, 122);
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda.BorderWidth = 0;
            tituloSituacion.AddCell(celda);

            documentpdf.Add(tituloSituacion);

            float[] w2 = new float[] { 35, 90, 30, 52, 50, 50, 30, 100, 35 };
            PdfPTable tabla = new PdfPTable(9);
            tabla.SetWidths(w2);
            tabla.WidthPercentage = 100;
            PdfPCell cell;

            cell = new PdfPCell(new Phrase("No. remisión", normalfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(0, 61, 122);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Nombre", normalfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(0, 61, 122);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Edad", normalfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(0, 61, 122);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);


            cell = new PdfPCell(new Phrase("Fecha detención", normalfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(0, 61, 122);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Fecha registro", normalfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(0, 61, 122);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Fecha cumplimiento", normalfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(0, 61, 122);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Horas", normalfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(0, 61, 122);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Motivo(s) calificación", normalfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(0, 61, 122);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Pertenencias", normalfont));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = new BaseColor(0, 61, 122);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int ii = 1;

            foreach (var i in internos)
            {
                if (ii % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                bool centro = false;
                var remision = "";
                Entity.Calificacion calificacionSituacion = new Entity.Calificacion();
                List<Entity.DetalleDetencion> estatus = ControlDetalleDetencion.ObtenerPorDetenidoId(i.Id);

                if (estatus != null)
                {
                    foreach (var k in estatus)
                    {
                        calificacionSituacion = ControlCalificacion.ObtenerPorInternoId(k.Id);
                        if (calificacionSituacion != null)
                        {
                            if (k.ContratoId == contrato && k.Activo == true && calificacionSituacion.SituacionId == situacionId && k.Tipo == tipoContrato)
                            {
                                centro = true;
                                remision = k.Expediente;
                                break;

                            }
                        }
                    }

                    if (centro == true)
                    {
                        ii++;
                        //Remisión
                        cell = new PdfPCell(new Paragraph(remision, smallfont));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.BackgroundColor = color;
                        cell.BorderWidth = 0;
                        tabla.AddCell(cell);
                        contador++;

                        //Nombre
                        cell = new PdfPCell(new Paragraph(i.Nombre.Trim() + " " + i.Paterno.Trim() + " " + i.Materno.Trim(), smallfont));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.BackgroundColor = color;
                        cell.BorderWidth = 0;
                        tabla.AddCell(cell);

                        //Edad
                        Entity.General general = ControlGeneral.ObtenerPorDetenidoId(i.Id);
                        DateTime nacimiento;
                        if (general == null)
                        {
                            cell = new PdfPCell(new Paragraph(""));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BackgroundColor = color;
                            cell.BorderWidth = 0;
                            tabla.AddCell(cell);
                        }
                        else
                        {
                            int edad=0;
                            if (general.FechaNacimineto != DateTime.MinValue)
                            {
                                nacimiento = general.FechaNacimineto;
                                edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;
                                edad = general.Edaddetenido;
                            }
                            else
                            {
                                edad =edad !=0?edad: general.Edaddetenido;
                            }
                            cell = new PdfPCell(new Paragraph(edad.ToString(), smallfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BackgroundColor = color;
                            cell.BorderWidth = 0;
                            tabla.AddCell(cell);
                        }

                        //Fecha detención
                        var infoDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(i.Id);
                        if (infoDetencion != null)
                        {
                            var date = infoDetencion.HoraYFecha.ToShortDateString();
                            var h = infoDetencion.HoraYFecha.Hour.ToString();
                            if (infoDetencion.HoraYFecha.Hour < 10) h = "0" + h;
                            var m = infoDetencion.HoraYFecha.Minute.ToString();
                            if (infoDetencion.HoraYFecha.Minute < 10) m = "0" + m;
                            var s = infoDetencion.HoraYFecha.Second.ToString();
                            if (infoDetencion.HoraYFecha.Second < 10) s = "0" + s;
                            var fecha1 = date + " " + h + ":" + m + ":" + s;
                            cell = new PdfPCell(new Paragraph(fecha1, smallfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BackgroundColor = color;
                            cell.BorderWidth = 0;
                            tabla.AddCell(cell);
                        }
                        else
                        {
                            cell = new PdfPCell(new Paragraph(""));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BackgroundColor = color;
                            cell.BorderWidth = 0;
                            tabla.AddCell(cell);
                        }

                        //Fecha Registro
                        var estatusInternos = ControlDetalleDetencion.ObtenerPorDetenidoId(i.Id).OrderByDescending(x => x.Fecha);
                        Entity.DetalleDetencion detencion = new Entity.DetalleDetencion();
                        var fecha = "";
                        int estatusInternoId = 0;
                        if(estatusInternos.Any(x => x.Activo))
                        {
                            detencion = estatus.First(x => x.Activo);
                            var date = Convert.ToDateTime(detencion.Fecha).ToShortDateString();
                            var h = Convert.ToDateTime(detencion.Fecha).Hour.ToString();
                            if (Convert.ToDateTime(detencion.Fecha).Hour < 10) h = "0" + h;
                            var m = Convert.ToDateTime(detencion.Fecha).Minute.ToString();
                            if (Convert.ToDateTime(detencion.Fecha).Minute < 10) m = "0" + m;
                            var s = Convert.ToDateTime(detencion.Fecha).Second.ToString();
                            if (Convert.ToDateTime(detencion.Fecha).Second < 10) s = "0" + s;
                            var Fecha2 = date + " " + h + ":" + m + ":" + s;
                            cell = new PdfPCell(new Paragraph(Fecha2, smallfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BackgroundColor = color;
                            cell.BorderWidth = 0;
                            tabla.AddCell(cell);
                            estatusInternoId = detencion.Id;
                            fecha = detencion.Fecha.ToString();
                        }
                        else
                        {
                            detencion = estatus.First();
                            cell = new PdfPCell(new Paragraph(detencion.Fecha.ToString(), smallfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BackgroundColor = color;
                            cell.BorderWidth = 0;
                            tabla.AddCell(cell);
                            estatusInternoId = detencion.Id;
                            fecha = detencion.Fecha.ToString();
                        }

                        //Fecha cumplimiento
                        var fechaSalida = "";
                        var calificacion = ControlCalificacion.ObtenerPorInternoId(estatusInternoId);
                        if (calificacion != null)
                        {
                            DateTime suma = Convert.ToDateTime(fecha).AddHours(calificacion.TotalHoras);
                            var date = suma.ToShortDateString();
                            var h = suma.Hour.ToString();
                            if (suma.Hour < 10) h = "0" + h;
                            var m = suma.Minute.ToString();
                            if (suma.Minute < 10) m = "0" + m;
                            var s = suma.Second.ToString();
                            if (suma.Second < 10) s = "0" + s;
                            fechaSalida = date + " " + h + ":" + m + ":" + s;
                            cell = new PdfPCell(new Paragraph(fechaSalida, smallfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BackgroundColor = color;
                            cell.BorderWidth = 0;
                            tabla.AddCell(cell);

                            //Horas
                            cell = new PdfPCell(new Paragraph(calificacion.TotalHoras.ToString(), smallfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BackgroundColor = color;
                            cell.BorderWidth = 0;
                            tabla.AddCell(cell);
                        }
                        else
                        {
                            DateTime suma = Convert.ToDateTime(fecha).AddHours(36);
                            var date = suma.ToShortDateString();
                            var h = suma.Hour.ToString();
                            if (suma.Hour < 10) h = "0" + h;
                            var m = suma.Minute.ToString();
                            if (suma.Minute < 10) m = "0" + m;
                            var s = suma.Second.ToString();
                            if (suma.Second < 10) s = "0" + s;
                            fechaSalida = date + " " + h + ":" + m + ":" + s;
                            cell = new PdfPCell(new Paragraph(fechaSalida, smallfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BackgroundColor = color;
                            cell.BorderWidth = 0;
                            tabla.AddCell(cell);

                            //Horas
                            cell = new PdfPCell(new Paragraph(""));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BackgroundColor = color;
                            cell.BorderWidth = 0;
                            tabla.AddCell(cell);
                        }

                        //Motivo

                        var motivosDetencionInterno = ControlMotivoDetencionInterno.GetListaMotivosDetencionByInternoId(new Entity.MotivoDetencionInterno { InternoId = detencion.Id });
                        if (motivosDetencionInterno.Count > 0)
                        {
                            string motivosConcat = "";
                            foreach (var item in motivosDetencionInterno)
                            {
                                var motivo = ControlMotivoDetencion.ObtenerPorId(item.MotivoDetencionId);
                                if (motivo != null) motivosConcat += motivo.Motivo + ",";
                            }

                            if (motivosConcat[motivosConcat.Length - 1] == ',') motivosConcat = motivosConcat.Substring(0, motivosConcat.Length - 1);

                            cell = new PdfPCell(new Paragraph(motivosConcat, smallfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BackgroundColor = color;
                            cell.BorderWidth = 0;
                            tabla.AddCell(cell);
                        }
                        else
                        {
                            cell = new PdfPCell(new Paragraph(""));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BackgroundColor = color;
                            cell.BorderWidth = 0;
                            tabla.AddCell(cell);
                        }

                        //Pertenencias
                        var pertenencias = ControlPertenencia.ObtenerPorInterno(estatusInternoId);
                        if(pertenencias.Count == 0)
                        {
                            cell = new PdfPCell(new Paragraph("No", smallfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BackgroundColor = color;
                            cell.BorderWidth = 0;
                            tabla.AddCell(cell);
                        }
                        else
                        {
                            cell = new PdfPCell(new Paragraph("Si", smallfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BackgroundColor = color;
                            cell.BorderWidth = 0;
                            tabla.AddCell(cell);
                        }
                    }
                }

            }

            documentpdf.Add(table);
            documentpdf.Add(tabla);
            
        }

        public static object ReporteEvidencias(int contrato, string centro, string inicio, string fin, string tipo)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //milimetros para los margenes
            var space = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentpdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                //Nombre y ubicación archivo
                string nombrearchivo = "Evidencias" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorio("Evidencias"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);
                Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
                Font FuenteBlau = new Font(fuentecalibri, 12f, Font.NORMAL, blau);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font Fuente12 = new Font(fuentecalibri, 12f, Font.NORMAL);
                Font smallfont = new Font(fuentecalibri, 12, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentpdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return obj = new { success = false, file = "", message = ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter2();

                documentpdf.Open();

                TitleEvidencias(documentpdf, FuenteTitulo, FuenteNegrita, FuenteNormal, space, FuenteNegrita, centro, inicio, fin, contrato);
                GetDatosEvidencias(documentpdf, FuenteBlau, FuenteNormal, space, Fuente12, contrato, inicio, fin, tipo, smallfont);

                obj = new { success = true, file = ubicacionarchivo, message = "" };
                return obj;
            }
            catch (Exception ex)
            {
                return obj = new { success = false, file = "", message = ex.Message };
            }
            finally
            {
                if (documentpdf.IsOpen())
                {
                    documentpdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TitleEvidencias(Document documentpdf, Font titlefont, Font boldfont, Font normalfont, float space, Font bigfont, string centro, string inicio, string fin, int contrato)
        {
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }

            float[] width = new float[] { 2, 8 };
            PdfPTable table = new PdfPTable(2);
            table.SetWidths(width);
            table.WidthPercentage = 100;
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell;
                       
            string title = "Datos de contrato/proyecto";
            string title2 = "Resumen de evidencias";

            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_LEFT;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cellWithRowspan);
            
            cell = new PdfPCell(new Phrase(title2, titlefont));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Padding = 10;
            table.AddCell(cell);

            CeldaVacio(table, 4);

            PdfPTable table2 = new PdfPTable(1);
            table2.WidthPercentage = 100;
            table2.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell2;

            CeldaConChunk(table2, boldfont, normalfont, "Institución: ", centro, 0, 0, 0);
            CeldaVacio(table2, 1);
            CeldaConChunk(table2, boldfont, normalfont, "Estatus: ","Recibidas", 0, 0, 0);
            CeldaVacio(table2, 1);
            CeldaConChunk(table2, boldfont, normalfont, "Del: ", inicio, 0, 0, 0);
            CeldaVacio(table2, 1);
            CeldaConChunk(table2, boldfont, normalfont, "Al: ", fin, 0, 0, 0);
            CeldaVacio(table2, 1);
            documentpdf.Add(table);
            documentpdf.Add(table2);
        }

        private static void GetDatosEvidencias(Document documentpdf, Font boldfont, Font normalfont, float space, Font smallfont, int contrato, string inicio, string fin, string tipo, Font head)
        {
            var internos = ControlDetenido.ObteneTodos();
            BaseColor blau = new BaseColor(0, 61, 122);

            float[] width = new float[] { 100 };
            PdfPTable table = new PdfPTable(1);
            table.WidthPercentage = 1;
            table.HorizontalAlignment = Element.ALIGN_CENTER;

            float[] w2 = new float[] { 40, 40, 40, 50, 30, 60, 40 };
            PdfPTable tabla = new PdfPTable(7);
            tabla.SetWidths(w2);
            tabla.WidthPercentage = 100;
            PdfPCell cell;

            cell = new PdfPCell(new Phrase("No. remisión", head));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.BorderWidth = 0;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Nombre", head));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.BorderWidth = 0;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Cantidad", head));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.BorderWidth = 0;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Clasificación", head));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.BorderWidth = 0;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Evidencia", head));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.BorderWidth = 0;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Observaciones", head));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.BorderWidth = 0;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Recibe", head));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.BorderWidth = 0;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.AddCell(cell);

            int contratoId;
            string expediente;
            string tipocontrato;

            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int x = 1;

            foreach (var k in internos)
            {
                if (x % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                contratoId = 0;
                expediente = "";
                tipocontrato = "";
                var estatus = ControlDetalleDetencion.ObtenerPorDetenidoId(k.Id);
                var detalleDetencionId = 0;
                foreach (var e in estatus)
                {
                    if (e.DetenidoId == k.Id && e.Activo == true)
                    {
                        contratoId = e.ContratoId;
                        expediente = e.Expediente;
                        tipocontrato = e.Tipo;
                        detalleDetencionId = e.Id;
                        break;
                    }
                }

                if (contratoId == contrato && tipocontrato == tipo && detalleDetencionId != 0)
                {
                    var pertenencia = ControlPertenencia.ObtenerPorInterno(detalleDetencionId);

                    foreach (var p in pertenencia)
                    {
                        var pLog = ControlPertenenciaLog.ObtenerPorId(p.Id);
                        var fechaI = Convert.ToDateTime(inicio);
                        var fechaF = Convert.ToDateTime(fin);

                        if (p.Clasificacion == 1 && pLog.Fec_Movto >= fechaI && pLog.Fec_Movto.Date <= fechaF.Date)
                        {
                            x++;

                            cell = new PdfPCell(new Paragraph(expediente, normalfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BorderWidth = 0;
                            cell.BackgroundColor = color;
                            tabla.AddCell(cell);

                            cell = new PdfPCell(new Paragraph(k.Nombre + " " + k.Paterno + " " + k.Materno, normalfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BorderWidth = 0;
                            cell.BackgroundColor = color;
                            tabla.AddCell(cell);

                            cell = new PdfPCell(new Paragraph(p.Cantidad.ToString(), normalfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BorderWidth = 0;
                            cell.BackgroundColor = color;
                            tabla.AddCell(cell);

                            cell = new PdfPCell(new Paragraph("Evidencia", normalfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BorderWidth = 0;
                            cell.BackgroundColor = color;
                            tabla.AddCell(cell);

                            cell = new PdfPCell(new Paragraph(p.PertenenciaNombre, normalfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BorderWidth = 0;
                            cell.BackgroundColor = color;
                            tabla.AddCell(cell);

                            cell = new PdfPCell(new Paragraph(p.Observacion, normalfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BorderWidth = 0;
                            cell.BackgroundColor = color;
                            tabla.AddCell(cell);

                            var recibe = ControlUsuario.Obtener(p.CreadoPor);
                            cell = new PdfPCell(new Paragraph(recibe.User, normalfont));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.BorderWidth = 0;
                            cell.BackgroundColor = color;
                            tabla.AddCell(cell);
                        }
                    }
                }
            }

            documentpdf.Add(table);
            documentpdf.Add(tabla);

            float[] width3 = new float[] { 100 };
            PdfPTable tabla2 = new PdfPTable(1);
            tabla2.SetWidths(width);
            tabla2.WidthPercentage = 100;
            tabla2.HorizontalAlignment = Element.ALIGN_RIGHT;
            PdfPCell cell2;

            cell2 = new PdfPCell(new Phrase(" "));
            cell2.BorderWidth = 0;
            cell2.Padding = space;
            tabla2.AddCell(cell2);

            Phrase p_center = new Phrase();
            p_center.Add(new Chunk("_____________________", normalfont));

            cell2 = new PdfPCell(new Phrase(p_center));
            cell2.BorderWidth = 0;
            cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell2.Padding = space;
            tabla2.AddCell(cell2);

            Phrase p_center2 = new Phrase();
            p_center2.Add(new Chunk("Juzgado Calificador", boldfont));

            cell2 = new PdfPCell(new Phrase(p_center2));
            cell2.BorderWidth = 0;
            cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell2.Padding = space;
            tabla2.AddCell(cell2);

            documentpdf.Add(tabla2);
        }

        public static object ReporteSanciones(int contrato, string centro, string inicio, string fin, string tipo)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //milimetros para los margenes
            var space = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentpdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                //Nombre y ubicación archivo
                string nombrearchivo = "Sanciones" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorio("Sanciones"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);
                Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
                Font FuenteBlau = new Font(fuentecalibri, 12f, Font.NORMAL, blau);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font Fuente12 = new Font(fuentecalibri, 12f, Font.NORMAL);
                Font smallfont = new Font(fuentecalibri, 12, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentpdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return obj = new { success = false, file = "", message = ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();

                documentpdf.Open();

                TitleSanciones(documentpdf, FuenteTitulo, FuenteNegrita, FuenteNormal, space, FuenteNegrita, centro, inicio, fin, contrato);
                GetDatosSanciones(documentpdf, FuenteBlau, FuenteNormal, space, Fuente12, contrato, inicio, fin, tipo, smallfont);

                obj = new { success = true, file = ubicacionarchivo, message = "" };
                return obj;
            }
            catch (Exception ex)
            {
                return obj = new { success = false, file = "", message = ex.Message };
            }
            finally
            {
                if (documentpdf.IsOpen())
                {
                    documentpdf.Close();
                    PdfWriter.Close();
                }
            }
        }
        private static void TitleSanciones(Document documentpdf, Font titlefont, Font boldfont, Font normalfont, float space, Font bigfont, string centro, string inicio, string fin, int contrato)
        {
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
            var calificacion = ControlCalificacion.ObtenerTodos();
            var fechaI = Convert.ToDateTime(inicio);
            var fechaF = Convert.ToDateTime(fin);
            int contador = 0;
            foreach (var c in calificacion)
            {
                var ei = ControlDetalleDetencion.ObtenerPorDetenidoId(c.InternoId);
                foreach (var k in ei)
                {
                    if (Convert.ToDateTime(k.Fecha).Date >= fechaI.Date && Convert.ToDateTime(k.Fecha).Date <= fechaF.Date && k.ContratoId == contrato && k.DetenidoId == c.InternoId)
                    {
                        contador++;
                    }
                }
            }

            PdfPTable tablaTitulo = new PdfPTable(1);
            tablaTitulo.HorizontalAlignment = Element.ALIGN_CENTER;

            string escudo = "~/Content/img/Escudo.jpg";
            string logo = "~/Content/img/logo.png";
            var logoizq = subcontrato.Banner;
            if (logoizq != "undefined" && !string.IsNullOrEmpty(logoizq))
            {
                logo = logoizq;
            }
            string pathEscudo = System.Web.HttpContext.Current.Server.MapPath(escudo);
            string pathLogo = System.Web.HttpContext.Current.Server.MapPath(logo);
            iTextSharp.text.Image imageEscudo = null;
            iTextSharp.text.Image imageLogo = null;
            try
            {
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }
            catch (Exception)
            {
                pathEscudo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }

            try
            {
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }
            catch (Exception)
            {
                pathLogo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }

            var logoDER = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logoDER != "undefined" && logo != "")
            {
                logotipo = logoDER;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }
            imageLogo.ScalePercent(95f);
            
                imageLogo.ScaleAbsoluteHeight(100f);
                imageLogo.ScaleAbsoluteWidth(300f);
            
            image.ScaleAbsoluteHeight(60f);
            image.ScaleAbsoluteWidth(70f);
            Paragraph para = new Paragraph();
            Phrase ph1 = new Phrase();
            Chunk glue = new Chunk(new iTextSharp.text.pdf.draw.VerticalPositionMark());
            Paragraph main = new Paragraph();
            ph1.Add(new Chunk(imageLogo, 0, 0, true));
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(image, 0, 15, true));
            main.Add(ph1);
            para.Add(main);
            documentpdf.Add(para);


            //var logo = subcontrato.Logotipo;
            //string logotipo = "~/Content/img/logo.png";

            //if (logo != "undefined" && logo != "")
            //{
            //    logotipo = logo;
            //}
            var lugar = institucion.Nombre;
            //string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            //var image = iTextSharp.text.Image.GetInstance(pathfoto);

            float[] width = new float[] { 2, 8 };
            PdfPTable table = new PdfPTable(2);
            table.SetWidths(width);
            table.TotalWidth = 550;
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell;
            
            string title2 = "Sanciones por juez";

            //PdfPCell cellWithRowspan = new PdfPCell(image, true);
            //// The first cell spans 5 rows  
            //cellWithRowspan.Rowspan = 5;
            //cellWithRowspan.Border = 0;
            //cellWithRowspan.HorizontalAlignment = Element.ALIGN_LEFT;
            //cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            //table.AddCell(cellWithRowspan);

            CeldaVacio(table, 1);

            cell = new PdfPCell(new Phrase(title2, titlefont));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.PaddingLeft = 10;
            table.AddCell(cell);

            float[] w = new float[] { 1, 1 };
            PdfPTable table2 = new PdfPTable(2);
            table2.SetWidths(w);
            table2.WidthPercentage = 100;
            table2.HorizontalAlignment = Element.ALIGN_LEFT;
            string[] parametros = new string[3] { contrato.ToString(), inicio, fin };

            var listasanciones = ControlReporteSancionesJuezCalificador.GetreporteSanciones(parametros);
            CeldaConChunk(table2, boldfont, normalfont, "Institución: ", lugar, 0, 0, 0);
            CeldaVacio(table2, 1);
            CeldaConChunk(table2, boldfont, normalfont, "Del: ", inicio, 0, 0, 0);
            CeldaVacio(table2, 1);
            CeldaConChunk(table2, boldfont, normalfont, "Al: ", fin, 0, 0, 0);
            CeldaConChunk(table2, boldfont, normalfont, "Registros encontrados: ", listasanciones.Count().ToString(), 0, 2, 0);
            documentpdf.Add(table);
            documentpdf.Add(table2);
        }																																														   
        private static void GetDatosSanciones(Document documentpdf, Font boldfont, Font normalfont, float space, Font smallfont, int contrato, string inicio, string fin, string tipo, Font header)
        {
            var internos = ControlDetenido.ObteneTodos();
            List<Entity.Detenido> Detenios_ = new List<Entity.Detenido>();

            var calificacion = ControlCalificacion.ObtenerTodos();

            BaseColor blau = new BaseColor(0, 61, 122);

            float[] width = new float[] { 100 };
            PdfPTable table = new PdfPTable(1);
            table.WidthPercentage = 1;
            table.HorizontalAlignment = Element.ALIGN_CENTER;

            float[] w2 = new float[] { 40, 25, 30, 50, 30, 25};
            PdfPTable tabla = new PdfPTable(6);
            tabla.SetWidths(w2);
            tabla.WidthPercentage = 100;
            PdfPCell cell;

            cell = new PdfPCell(new Phrase("Juez", header));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("No. remisión", header));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Fecha", header));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Detenido", header));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Multa económica", header));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Horas de detención", header));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = blau;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            tabla.AddCell(cell);
             
            var fechaI = Convert.ToDateTime(inicio);
            var fechaF = Convert.ToDateTime(fin);

            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);
            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int x = 1;

            string[] parametros = new string[3] {contrato.ToString(),inicio,fin};

            var listasanciones = ControlReporteSancionesJuezCalificador.GetreporteSanciones(parametros);

            foreach (var item in listasanciones)
            {
                if (x % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                //Juez
                cell = new PdfPCell(new Paragraph(item.Juez, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = color;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

               // Remision
                cell = new PdfPCell(new Paragraph(item.Expediente, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = color;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                //Fecha
                var date = item.Fecha.ToShortDateString();
                var h = item.Fecha.Hour.ToString();
                if (item.Fecha.Hour < 10) h = "0" + h;
                var m = item.Fecha.Minute.ToString();
                if (item.Fecha.Minute < 10) m = "0" + m;
                var s = item.Fecha.Second.ToString();
                if (item.Fecha.Second < 10) s = "0" + s;
                var fecha=date +" "+h+":" + m + ":" + s;
                cell = new PdfPCell(new Paragraph(fecha, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = color;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                //Detenido
                cell = new PdfPCell(new Paragraph(item.Detenido, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = color;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);
                //Multa
                cell = new PdfPCell(new Paragraph(item.Totaldemultas.ToString(), normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = color;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);
                //Horas
                cell = new PdfPCell(new Paragraph(item.Totalhoras.ToString(), normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = color;
                cell.BorderWidth = 0;
                tabla.AddCell(cell);

                x++;
            }

            //foreach (var c in calificacion)
            //{
            //    if (x % 2 != 0)
            //        color = colorgris;
            //    else color = colorblanco;

            //    var ei = ControlDetalleDetencion.ObtenerPorDetenidoId(c.InternoId);
            //    foreach (var k in ei)
            //    {
            //        if (Convert.ToDateTime(k.Fecha).Date >= fechaI.Date && Convert.ToDateTime(k.Fecha).Date <= fechaF.Date && k.ContratoId == contrato && k.DetenidoId == c.InternoId)
            //        {
            //            var usuario = ControlUsuario.Obtener(c.CreadoPor);

            //            //Juez
            //            cell = new PdfPCell(new Paragraph(usuario.Nombre + " " + usuario.ApellidoPaterno + " " + usuario.ApellidoMaterno, normalfont));
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            cell.BackgroundColor = color;
            //            cell.BorderWidth = 0;
            //            tabla.AddCell(cell);

            //            x++;

            //            var estatusInterno = ControlDetalleDetencion.ObtenerPorDetenidoId(c.InternoId);
            //            if (estatusInterno != null)
            //            {
            //                foreach (var e in estatusInterno)
            //                {
            //                    if (e.Activo == true && e.ContratoId == contrato)
            //                    {
            //                        //Remision
            //                        cell = new PdfPCell(new Paragraph(e.Expediente, normalfont));
            //                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                        cell.BackgroundColor = color;
            //                        cell.BorderWidth = 0;
            //                        tabla.AddCell(cell);

            //                        //Fecha
            //                        cell = new PdfPCell(new Paragraph(e.Fecha.ToString(), normalfont));
            //                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                        cell.BackgroundColor = color;
            //                        cell.BorderWidth = 0;
            //                        tabla.AddCell(cell);

            //                        break;
            //                    }
            //                    else
            //                    {
            //                        //Remision
            //                        cell = new PdfPCell(new Paragraph(""));
            //                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                        cell.BackgroundColor = color;
            //                        cell.BorderWidth = 0;
            //                        tabla.AddCell(cell);

            //                        //Fecha
            //                        cell = new PdfPCell(new Paragraph(""));
            //                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                        cell.BackgroundColor = color;
            //                        cell.BorderWidth = 0;
            //                        tabla.AddCell(cell);

            //                        break;
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                cell = new PdfPCell(new Paragraph("", normalfont));
            //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                cell.BackgroundColor = color;
            //                cell.BorderWidth = 0;
            //                tabla.AddCell(cell);

            //                cell = new PdfPCell(new Paragraph("", normalfont));
            //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                cell.BackgroundColor = color;
            //                cell.BorderWidth = 0;
            //                tabla.AddCell(cell);
            //            }

            //            var detenido = ControlDetenido.ObtenerPorId(c.InternoId);
            //            if (detenido != null)
            //            {
            //                //Detenido
            //                cell = new PdfPCell(new Paragraph(detenido.Nombre + " " + detenido.Paterno + " " + detenido.Materno, normalfont));
            //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                cell.BackgroundColor = color;
            //                cell.BorderWidth = 0;
            //                tabla.AddCell(cell);
            //            }
            //            else
            //            {
            //                cell = new PdfPCell(new Paragraph("", normalfont));
            //                cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //                cell.BackgroundColor = color;
            //                cell.BorderWidth = 0;
            //                tabla.AddCell(cell);
            //            }

            //            //Multa
            //            cell = new PdfPCell(new Paragraph(c.TotalDeMultas.ToString(), normalfont));
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            cell.BackgroundColor = color;
            //            cell.BorderWidth = 0;
            //            tabla.AddCell(cell);
            //            //Horas
            //            cell = new PdfPCell(new Paragraph(c.TotalHoras.ToString(), normalfont));
            //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //            cell.BackgroundColor = color;
            //            cell.BorderWidth = 0;
            //            tabla.AddCell(cell);
            //        }
            //    }
            //}



            documentpdf.Add(table);
            documentpdf.Add(tabla);

            
        }
    }										   

    public class internoAux
    {
        public string Id { get; set; }
        public string nombre { get; set; }
        public string paterno { get; set; }
        public string materno { get; set; }
        public string fecha { get; set; }
        public string IdDetencion { get; set; }
    }
}

