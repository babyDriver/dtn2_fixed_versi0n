﻿using DataAccess.CertificadoPsicoFisiologicoOrientacion;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlCertificadoPsicoFisologicoOrientacion
    {
        private static ServicioCertificadoPsicoFisiologicoOrientacion servicio = new ServicioCertificadoPsicoFisiologicoOrientacion("SQLConnectionString");

        public static int Guardar(Entity.CertificadoPsicoFisilogicoOrientacion entifadad)
        {

            return servicio.Guardar(entifadad);
        }

        public static Entity.CertificadoPsicoFisilogicoOrientacion obtenerPorId(int Id)
            {
            return servicio.ObtenerPorId(Id);

                }

    }
}
