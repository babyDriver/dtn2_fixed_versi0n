﻿using DataAccess.Usuario_Login;
using System.Collections.Generic;

namespace Business
{
    public class ControlUsuario_login
    {
        private static ServicioUsuario_Login servicio = new ServicioUsuario_Login("SQLConnectionString");

        public static int guardar(Entity.Usuario_login usr)
        {
            return servicio.Guardar(usr);
        }

        public static Entity.Usuario_login ObtenerPorusuarioId(int Id)
            {
            return servicio.ObtenerPorUsuarioId(Id);

            }
        public static List<Entity.Usuario_login> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }
        public static void Actualizar(Entity.Usuario_login usr)
        {
            servicio.Actualizar(usr);
        }

    }
}
