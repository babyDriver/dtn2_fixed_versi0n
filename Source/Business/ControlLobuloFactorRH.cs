﻿using DataAccess.LobuloFactorRH;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlLobuloFactorRH
    {
        private static ServicioLobuloFactorRH servicio = new ServicioLobuloFactorRH("SQLConnectionString");

        public static int Guardar(Entity.LobuloFactorRH lobulo)
        {
            return servicio.Guardar(lobulo);
        }

        public static void Actualizar(Entity.LobuloFactorRH lobulo)
        {
            servicio.Actualizar(lobulo);
        }

        public static Entity.LobuloFactorRH ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.LobuloFactorRH> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.LobuloFactorRH ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.LobuloFactorRH ObtenerPorAntropometriaId(int id)
        {
            return servicio.ObtenerByAntropometriaId(id);
        }
    }
}
