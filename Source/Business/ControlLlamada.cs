﻿using DataAccess.Llamada;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlLlamada
    {
        private static ServicioLlamada servicio = new ServicioLlamada("SQLConnectionString");

        public static void Actualizar(Entity.Llamada item)
        {
            var tmp = ObtenerByTrackingId(item.TrackingId);
            item.Id = tmp.Id;
            servicio.Actualizar(item);
        }

        public static void Desactivar(Guid id)
        {
            var tmp = ObtenerByTrackingId(id);
            tmp.Activo = false;
            Actualizar(tmp);
        }

        public static int Guardar(Entity.Llamada item)
        {
            return servicio.Guardar(item);
        }

        public static Entity.Llamada ObtenerById(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static Entity.Llamada ObtenerByTrackingId(Guid id)
        {
            return servicio.ObtenerByTrackingId(id);
        }

        public static List<Entity.Llamada> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }
    }
}
