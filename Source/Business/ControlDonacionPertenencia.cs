﻿using DataAccess.DonacionPertenencia;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlDonacionPertenencia
    {
        private static ServicioDonacionPertenencia servicio = new ServicioDonacionPertenencia("SQLConnectionString");

        public static int Guardar(Entity.DonacionPertenencia donacion)
        {
            return servicio.Guardar(donacion);
        }

        public static Entity.DonacionPertenencia ObtenerByTrackingId(Guid guid)
        {
            return servicio.ObtenerByTrackingId(guid);
        }
    }
}
