﻿using DataAccess.ComparativoDeLesionadoPorUnidad;
using System.Collections.Generic;
using System;


namespace Business
{
    public class ControlComparativoDeLesionadosPorUnidad
    {
        private static ServicioComparativoDeLesionadosPorUnidad servicio = new ServicioComparativoDeLesionadosPorUnidad("SQLConnectionString");
         
        public static List<Entity.Comparativodelesionadosporunidad> ObtenerComparativoDeLesionadosPorUnidad(string [] filtros)
        {
            return servicio.ObtenerComparativodelesionados(filtros);
        }
    }
}
