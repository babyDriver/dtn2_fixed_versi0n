﻿using DataAccess.ContratoCliente;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlContratoCliente
    {
        private static ServicioContratoCliente servicio = new ServicioContratoCliente("SQLConnectionString");

        public static Entity.ContratoCliente ObtenerById(int id)
        {
            return servicio.ObtenerById(id);
        }
    }
}
