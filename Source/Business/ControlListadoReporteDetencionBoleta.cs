﻿using DataAccess.ListadoReporteDetencionBoleta;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlListadoReporteDetencionBoleta
    {
        private static ServicioListadoReporteDetencionBoleta servicio = new ServicioListadoReporteDetencionBoleta("SQLConnectionString");

        public static List<Entity.ListadoReporteDetencionBoleta> ObtenerTodos(object[] data)
        {
            return servicio.ObtenerTodos(data);
        }
    }
}
