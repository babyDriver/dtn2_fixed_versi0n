﻿using DataAccess.CertificadoPsicoFisiologicoOrientacionDetalle;
using System.Collections.Generic;
using System;


namespace Business
{
    public class ControlCertificadoPsicoFisologicoOrientacionDetalle
    {
        private static ServicioCertificadoPsicoFisiologicoOrientacionDetalle servicio = new ServicioCertificadoPsicoFisiologicoOrientacionDetalle("SQLConnectionString");

        public static int Guardar(Entity.CertificadoPsicoFisiologicoOrientacionDetalle entity)
        {

            return servicio.Guardar(entity);

        }
        public static List< Entity.CertificadoPsicoFisiologicoOrientacionDetalle >Obetener(int Id)
        {
            return servicio.ObtenerPorId(Id);
        }
    }
}
