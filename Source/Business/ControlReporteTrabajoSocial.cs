﻿using DataAccess.ReporteTrabajoSocial;
using System.Collections.Generic;
using System;

namespace Business
{
   public class ControlReporteTrabajoSocial
    {
        public static Entity.ReporteTrabajoSocial GetReporteTrabajoSocialByDetalleDetencionId(int DetalleDetencionId)
        {
            ServicioReporteTrabajoSocial servicio = new ServicioReporteTrabajoSocial();
            return servicio.GetReporteTrabajoSocial(DetalleDetencionId);


        }

        public static Entity.ReporteTrabajoSocial GetReporteTrabajoSocialExpedienteId(int ExpedienteId)
        {
            ServicioReporteTrabajoSocial servicio = new ServicioReporteTrabajoSocial();
            return servicio.GetReporteTrabajoSocialExpedienteId(ExpedienteId);


        }
    }
}
