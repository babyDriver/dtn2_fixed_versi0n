﻿using DataAccess.MovimientoLog;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlMovimientoLog
    {
        private static ServicioMovimientoLog servicio = new ServicioMovimientoLog("SQLConnectionString");

        public static void Actualizar(Entity.MovimientoLog item)
        {
            servicio.Actualizar(item);
        }
        public static Entity.MovimientoLog ObtenerMovimientoLog(Entity.MovimientoLog filtro)
        {
            return servicio.Obtener(filtro);
        }
    }
}
