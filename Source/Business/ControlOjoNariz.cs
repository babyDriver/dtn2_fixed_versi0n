﻿using DataAccess.OjoNariz;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlOjoNariz
    {
        private static ServicioOjoNariz servicio = new ServicioOjoNariz("SQLConnectionString");

        public static int Guardar(Entity.OjoNariz ojo)
        {
            return servicio.Guardar(ojo);
        }

        public static void Actualizar(Entity.OjoNariz ojo)
        {
            servicio.Actualizar(ojo);
        }

        public static Entity.OjoNariz ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.OjoNariz> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.OjoNariz ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.OjoNariz ObtenerPorAntropometriaId(int id)
        {
            return servicio.ObtenerByAntropometriaId(id);
        }
    }
}
