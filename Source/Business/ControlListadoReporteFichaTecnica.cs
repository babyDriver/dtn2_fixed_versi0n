﻿using DataAccess.ListadoReporteFichaTecnica;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlListadoReporteFichaTecnica
    {
        private static ServicioListadoReporteFichaTecnica servicio = new ServicioListadoReporteFichaTecnica("SQLConnectionString");

        public static List<Entity.ListadoReporteFichaTecnica> ObtenerTodos(object[] data)
        {
            return servicio.ObtenerTodos(data);
        }
    }
}
