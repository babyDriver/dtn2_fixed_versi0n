﻿using DataAccess.Pertenencia;
using System.Collections.Generic;
using System;


namespace Business
{
    public class ControlPertenencia
    {
        private static ServicioPertenencia servicio = new ServicioPertenencia("SQLConnectionString");

        public static int Guardar(Entity.Pertenencia pertenencia)
        {
            return servicio.Guardar(pertenencia);
        }

        public static void Actualizar(Entity.Pertenencia pertenencia)
        {
            servicio.Actualizar(pertenencia);
        }

        public static Entity.Pertenencia ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static Entity.Pertenencia ObtenerPorTrackingId(Guid tracking)
        {
            return servicio.ObtenerById(tracking);
        }

        public static List<Entity.Pertenencia> ObtenerPorInterno(int id)
        {
            return servicio.ObtenerByInterno(id);
        }

        public static List<Entity.Pertenencia> ObtenerPorMultiplesIds(object[] parametros)
        {
            return servicio.ObtenerByMultipleIds(parametros);
        }

        public static string ReglaNegocio(int detalledetencionId)
        {

            var calificacion = ControlCalificacion.ObtenerPorInternoId(detalledetencionId);
            var detalledetencio = ControlDetalleDetencion.ObtenerPorId(detalledetencionId);
            Entity.CalificacionIngreso calificacionIngreso = null;
            if (calificacion != null)
            {
                calificacionIngreso = ControlCalificacionIngreso.ObtenerPorCalificacionId(calificacion.Id);
            }

            if (calificacionIngreso == null && calificacion == null)
            {


                return " Ha seleccionado pertenencia de detenidos que no tienen calificación, por favor seleccione pertenencias con detenidos calificados.";

            }

            if(calificacion.TrabajoSocial)
            {
                if (calificacion.TotalAPagar <= 0 && calificacion.TotalHoras <= 0)
                {
                    var salidaefectuada = ControlSalidaEfectuada.ObtenerByDetalledetencionId(detalledetencionId);

                    if (salidaefectuada == null)
                    {

                        return " Ha seleccionado pertenencia de detenidos que no se le ha dado salida , por favor seleccione pertenencias de detenidos que tengan salida efectuada.";
                    }


                }

            }
            else
         
            if (calificacion.SoloArresto)
            {

                DateTime fechaSalidaAdd = new DateTime();
                DateTime fechaSalida = detalledetencio.Fecha.Value;
                TimeSpan horasRestantes;
                fechaSalidaAdd = fechaSalida.AddHours(calificacion.TotalHoras);
                horasRestantes = fechaSalidaAdd.Subtract(DateTime.Now);

                if (horasRestantes.Days > 0 || horasRestantes.Hours > 0 || horasRestantes.Minutes > 0)
                {
                    return " Ha seleccionado pertenencia de detenidos que no han cumplido con el tiempo de arresto, por favor seleccione pertenencias con detenidos que hayan cumplido con el tiempo de arresto.";
                }
                    //DateTime.Now.Subtract(fechaSalidaAdd).;

                
            }
            else
            {
                if (calificacion.TotalAPagar > 0)
                {
                    if ((calificacionIngreso != null? calificacionIngreso.IngresoId:0) == 0)
                    {
                        return " Ha seleccionado pertenencia de detenidos que no han pagado la multa, por favor seleccione pertenencias de detenidos con multas pagadas.";

                    }
                    DateTime fechaSalidaAdd = new DateTime();
                    DateTime fechaSalida = detalledetencio.Fecha.Value;
                    TimeSpan horasRestantes;
                    fechaSalidaAdd = fechaSalida.AddHours(calificacion.TotalHoras);
                    horasRestantes = fechaSalidaAdd.Subtract(DateTime.Now);

                    if (horasRestantes.Days > 0 || horasRestantes.Hours > 0 || horasRestantes.Minutes > 0)
                    {
                        return " Ha seleccionado pertenencia de detenidos que no han cumplido con el tiempo de arresto, por favor seleccione pertenencias con detenidos que hayan cumplido con el tiempo de arresto.";
                    }

                    var salidaefectuada = ControlSalidaEfectuada.ObtenerByDetalledetencionId(detalledetencionId);

                    if (salidaefectuada==null)
                    {

                        return " Ha seleccionado pertenencia de detenidos que no se le ha dado salida , por favor seleccione pertenencias de detenidos que tengan salida efectuada.";
                    }

                }

                

                

            }





            return "";
        }

        public static List<object> GetPertenencias(object[] parametros)
        {
            List<object> lista = new List<object> { };

            try
            {                
                var listaTemp = ObtenerPorMultiplesIds(parametros);

                foreach (var pertenencia in listaTemp)
                {
                    object pertenenciaTmp = new
                    {
                        PertenenciaId = pertenencia.Id,
                        Nombre = pertenencia.PertenenciaNombre,
                        Cantidad = pertenencia.Cantidad,
                        Estatus = ControlEstatusPertenencia.ObtenerPorId(pertenencia.Estatus).Nombre,
                        Observacion = pertenencia.Observacion,
                        Clasificacion = ControlClasificacionEvidencia.ObtenerPorId(pertenencia.Clasificacion).Nombre,
                        Bolsa = pertenencia.Bolsa,
                        CreadoPor = pertenencia.CreadoPor
                    };

                    lista.Add(pertenenciaTmp);
                }

                return lista;
            }
            catch (System.Exception ex)
            {
                return null;
            }
        }
    }
}
