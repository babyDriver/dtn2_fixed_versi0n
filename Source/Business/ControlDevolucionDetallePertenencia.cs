﻿using DataAccess.DevolucionDetallePertenencia;
using System.Collections.Generic;
using System;


namespace Business
{
    public class ControlDevolucionDetallePertenencia
    {
        private static ServicioDevolucionDetallePertenencia servicio = new ServicioDevolucionDetallePertenencia("SQLConnectionString");

        public static int Guardar(Entity.DevolucionDetallePertenencia devolucion)
        {
            return servicio.Guardar(devolucion);
        }

        public static List<Entity.DevolucionDetallePertenencia> ObtenerByDevolucionId(int id)
        {
            return servicio.ObtenerByDevolucionId(id);
        }
    }
}
