﻿using DataAccess.Casillero;
using System.Collections.Generic;

namespace Business
{
    public static class ControlCasillero
    {
        private static ServicioCasillero servicio = new ServicioCasillero("SQLConnectionString");

        public static int Guardar(Entity.Casillero casillero)
        {
            return servicio.Guardar(casillero);
        }
        public static void Actualizar(Entity.Casillero casillero)
        {
            servicio.Actualizar(casillero);
        }

        public static List<Entity.Casillero> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Casillero ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static Entity.Casillero ObtenerPorNombre(string nombre)
        {
            return servicio.ObtenerByNombre(nombre);
        }
    }
}
