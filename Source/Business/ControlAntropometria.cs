﻿using DataAccess.Antropometria;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlAntropometria
    {
        private static ServicioAntropometria servicio = new ServicioAntropometria("SQLConnectionString");

        public static int Guardar(Entity.Antropometria antropometria)
        {
            return servicio.Guardar(antropometria);
        }

        public static void Actualizar(Entity.Antropometria antropometria)
        {
            servicio.Actualizar(antropometria);
        }

        public static Entity.Antropometria ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static Entity.Antropometria ObtenerPorDetenidoId(int id)
        {
            return servicio.ObtenerByDetenidoId(id);
        }

        public static List<Entity.Antropometria> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Antropometria ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }
    }
}
