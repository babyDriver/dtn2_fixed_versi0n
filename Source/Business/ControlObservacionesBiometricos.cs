﻿using DataAccess.ObservacionesBiometricos;
using System.Collections.Generic;

namespace Business
{
   public class ControlObservacionesBiometricos
    {
        private static ServicioObservacionesBiometricos servicio = new ServicioObservacionesBiometricos("SQLConnectionString");

        public static int Guardar(Entity.ObservacionesBiometricos item)
        {
            return servicio.Guardar(item);
        }
        public static void Actualizar(Entity.ObservacionesBiometricos item)
        {
              servicio.Actualizar(item);
        }
        public static Entity.ObservacionesBiometricos GetByDetenidId(int DetenidoId)
        {
            return servicio.GetByDetenidoId(DetenidoId);
        }
    }
}
