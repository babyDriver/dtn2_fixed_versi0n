﻿using DataAccess.AlertaWeb;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlAlertaWeb
    {
        private static ServicioAlertaWeb servicio = new ServicioAlertaWeb("SQLConnectionString");

        public static List<Entity.AlertaWeb> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.AlertaWeb ObtenerPorId(int id)
        {
            return servicio.ObtenerPorId(id);
        }

        public static Entity.AlertaWeb ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerPorTrackingId(trackingId);
        }

        public static int Guardar(Entity.AlertaWeb alertaWeb)
        {
            return servicio.Guardar(alertaWeb);
        }

        public static void Actualizar(Entity.AlertaWeb alertaWeb)
        {
            servicio.Actualizar(alertaWeb);
        }
    }
}
