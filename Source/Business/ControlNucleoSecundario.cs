﻿using DataAccess.NucleoSecundario;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlNucleoSecundario
    {
        private static ServicioNucleoSecundario servicio = new ServicioNucleoSecundario("SQLConnectionString");

        public static int Guardar(Entity.NucleoSecundario item)
        {
            return servicio.Guardar(item);
        }

        public static void Actualizar(Entity.NucleoSecundario item)
        {
            servicio.Actualizar(item);
        }

        public static Entity.NucleoSecundario ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }


        public static Entity.NucleoSecundario ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }



      
    }
}
