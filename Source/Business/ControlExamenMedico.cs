﻿using DataAccess.ExamenMedico;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlExamenMedico
    {
        private static ServicioExamenMedico servicio = new ServicioExamenMedico("SQLConnectionString");

        public static int Guardar(Entity.ExamenMedico item)
        {
            return servicio.Guardar(item);
        }

        public static void Actualizar(Entity.ExamenMedico item)
        {
            servicio.Actualizar(item);
        }

        public static Entity.ExamenMedico ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

   
        public static Entity.ExamenMedico ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.ExamenMedico ObtenerPorDetalleDetencionId(int id)
        {
            return servicio.ObtenerByDetalleDetencionId(id);
        }

        public static List<Entity.ExamenMedico> ObtenerTodosPorDetalleDetencionId(int id)
        {
            return servicio.ObtenerTodosByDetalleDetencionId(id);
        }

    }
}
