﻿using DataAccess.CertificadoQuimico;
using System.Collections.Generic;
using System;

namespace Business
{
    public  class ControlCertificadoQuimico
    {
        private static ServicioCertificadoQuimico servicio = new ServicioCertificadoQuimico("SQLConnectionString");

        public static void Actualizar(Entity.CertificadoQuimico certificadoQuimico)
        {
            servicio.Actualizar(certificadoQuimico);

        }
        public static int Guardar(Entity.CertificadoQuimico certificadoQuimico)
        {

            return servicio.Guardar(certificadoQuimico);
        }

        public static List<Entity.CertificadoQuimico> ObTenerTodo()
        {
            return servicio.ObtenerTodos();

        }

        public static Entity.CertificadoQuimico ObtenerPorId(int Id)
        {
            return servicio.ObtenerById(Id);
        }

        public static Entity.CertificadoQuimico ObtenerPorFolio(string[] Id)
        {
            return servicio.ObtenerByFolioCertificadoId(Id);
        }

        public static Entity.CertificadoQuimico ObtenerPorTrackingId(string TrackingId)
        {
            return servicio.ObtenerByTrackingId(TrackingId);
        }
    }
}
