﻿using DataAccess.DetenidosPorTipoLesion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class ControlDetenidosPorTipoLesion
    {
        private static ServicioDetenidosPorTipoLesion servicio = new ServicioDetenidosPorTipoLesion("SQLConnectionString");

        public static List<Entity.DetenidosPorTipoLesion> ObtenerDetenidosPorTipoLesion(string[] filtros)
        {
            return servicio.ObtenerDetenidosPorTipoLesion(filtros);
        }
    }
}
