﻿using DataAccess.EventoUnidadResponsable;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlEventoUnidadResponsable
    {
        private static ServicioEventoUnidadResponsable servicio = new ServicioEventoUnidadResponsable("SQLConnectionString");

        public static void Guardar(Entity.EventoUnidadResponsable eventoUnidadResponsable)
        {
            servicio.Guardar(eventoUnidadResponsable);
        }
        public static Entity.EventoUnidadResponsable ObtenerByEventoId(int EventoId)
        {
            return servicio.ObtenerByEvendoId(EventoId);
        }

        public static List<Entity.EventoUnidadResponsable> ObtenerTodosByEventoId(int EventoId)
        {
            return servicio.ObtenerTodosByEvendoId(EventoId);
        }
    }
}
