﻿using DataAccess.ClasificacionEvidencia;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlClasificacionEvidencia
    {
        private static ServicioClasificacionEvidencia servicio = new ServicioClasificacionEvidencia("SQLConnectionString");

        public static List<Entity.ClasificacionEvidencia> ObtenerTodos()
        {
            return servicio.ObtenerTodo();
        }

        public static Entity.ClasificacionEvidencia ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }
    }
}
