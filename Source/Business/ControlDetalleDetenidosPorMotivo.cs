﻿using DataAccess.DetalleDetenidoPorMotivo;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlDetalleDetenidosPorMotivo
    {
        private static ServicioDetalleDetenidoPorMotivo servicio = new ServicioDetalleDetenidoPorMotivo("SQLConnectionString");

        public static List<Entity.Detalledetenidopormotivo> GetDetalledetenidopormotivos(string[] filtros)
        {
            return servicio.GetIDetalleDetenidoPorMotivo(filtros);
        }
    }
}
