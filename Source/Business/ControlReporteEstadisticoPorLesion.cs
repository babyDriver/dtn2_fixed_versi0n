﻿using DataAccess.ReporteEstadisticoPorLesion;
using System.Collections.Generic;
using System;

namespace Business
{
   public class ControlReporteEstadisticoPorLesion
    {
        private static ServicioReporteEstadisticoPorLesion servicio = new ServicioReporteEstadisticoPorLesion("SQLConnectionString");
        public static List<Entity.ReporteEstadisticoPorLesion> GetPorLesions(string [] filtro)
        {
            return servicio.GetByFilter(filtro);
        }
    }
}
