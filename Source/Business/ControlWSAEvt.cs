﻿using DataAccess.WSAEvt;

namespace Business
{
    public class ControlWSAEvt
    {
        private static ServicioWSAEvt servicio = new ServicioWSAEvt("SQLConnectionString");

        public static int Guardar(Entity.WSAEvt item)
        {
            return servicio.Guardar(item);
        }

        public static Entity.WSAEvt ObtenerPorId(int id)
        {
            return servicio.ObtenerPorId(id);
        }
    }
}
