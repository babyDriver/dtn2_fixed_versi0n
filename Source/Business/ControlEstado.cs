﻿using DataAccess.Estado;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public static class ControlEstado
    {
        private static ServicioEstado servicio = new ServicioEstado("SQLConnectionString");

        public static int Guardar(Entity.Estado estado)
        {
            return servicio.Guardar(estado);
        }
        public static void Actualizar(Entity.Estado estado)
        {
            servicio.Actualizar(estado);
        }

        public static Entity.Estado Obtener(int id)
        {
            return servicio.ObtenerPorId(id);
        }

        public static List<Entity.Estado> ObtenerPorPais(int id)
        {
            return servicio.ObtenerPorPaisId(id).Where(x => x.Habilitado).ToList();
        }

        public static Entity.Estado Obtener(System.Guid trackingId)
        {
            return servicio.ObtenerPorTrackingId(trackingId);
        }

        public static List<Entity.Estado> ObtenerTodo()
        {
            return servicio.ObtenerTodos().Where(x=>x.Habilitado).ToList();
        }

        public static Entity.Estado ObtenerPorId(int id)
        {
            return servicio.ObtenerPorId(id);
        }

    }
}
