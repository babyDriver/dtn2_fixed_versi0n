﻿using DataAccess.ReporteCorteDetenidos;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlReporteCorteDetenidos
    {
        public static List<Entity.ReporteCorteDetenido> ObtenerReporteByContrato(object[] parametros)
        {
            ServicioReporteCorteDetenidos servicio = new ServicioReporteCorteDetenidos();
            return servicio.ObtenerReporteByContrato(parametros);
        }
    }
}
