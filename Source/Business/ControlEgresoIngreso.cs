﻿using DataAccess.EgresoIngreso;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlEgresoIngreso
    {
        private static ServicioEgresoIngreso servicio = new ServicioEgresoIngreso("SQLConnectionString");

        public static string Desactivar(Guid guid)
        {
            var tmp = ObtenerPorTrackingId(guid);
            tmp.Habilitado = tmp.Habilitado ? false : true;
            servicio.Actualizar(tmp);
            string mensaje = tmp.Habilitado ? "habilitado" : "deshabilitado";
            return mensaje;
        }

        public static List<Entity.EgresoIngreso> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static int Guardar(Entity.EgresoIngreso item)
        {
            return servicio.Guardar(item);
        }

        public static Entity.EgresoIngreso ObtenerPorTrackingId(Guid tracking)
        {
            return servicio.ObtenerByTrackingId(tracking);
        }

        public static Entity.EgresoIngreso ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static void Actualizar(Entity.EgresoIngreso item)
        {
            servicio.Actualizar(item);
        }

        public static Entity.EgresoIngreso ObtenerFolio(object[] parametros)
        {
            return servicio.ObtenerFolio(parametros);
        }

        public static Entity.EgresoIngreso ObtenerByFolioAndTipo(object[] parametros)
        {
            return servicio.ObtenerByFolioAndTipo(parametros);
        }

        public static List<Entity.EgresoIngreso> ObtenerTodoByFecha(object[] parametros)
        {
            return servicio.ObtenerAllByDate(parametros);
        }
    }
}
