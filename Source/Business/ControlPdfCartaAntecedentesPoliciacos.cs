﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Linq;

namespace Business
{
    public class ControlPdfCartaAntecedentesPoliciacos
    {


        //Crear directorio 
        private static string CrearDirectorioControl(string nombre)
        {
            try
            {

                //Verificar si existe el directorio principal del repositorio de documentos
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio de la obra existe
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio para el grupo documental existe, sino crearlo
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre)));


                return string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs//" + nombre);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("No fue posible crear el repositorio para crear el archivo. ", ex.Message));
            }
        }


        //Crear celdas
        public static void Celda(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas para tablas
        public static void CeldaParaTablas(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, BaseColor colorrelleno, float espacio, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                celda.BackgroundColor = colorrelleno;
                celda.Padding = espacio;
                celda.UseAscender = true;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas vacias
        public static void CeldaVacio(PdfPTable tabla, int cantidad)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                celda.BorderWidth = 0;
                tabla.AddCell(celda);
            }

        }



        //para crear el Header y Footer
        public partial class HeaderFooter : PdfPageEventHelper
        {
            PdfContentByte cb;
            PdfTemplate headerTemplate;
            BaseFont bf = null;
            float tamanofuente = 8f;

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                }
                catch (DocumentException de) { }
                catch (System.IO.IOException ioe) { }
            }

            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                //Encabezado de Página
                Rectangle pageSize = doc.PageSize;
                PdfPTable footerTbl = new PdfPTable(1);
                BaseColor blau = new BaseColor(0, 61, 122);

                float[] TablaAncho = new float[] { 100 };
                footerTbl.SetWidths(TablaAncho);
                footerTbl.TotalWidth = 550;
                Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                PdfPCell cell;
                CeldaParaTablas(footerTbl, FuenteCelda, " ", 1, 0, 1, blau, 0, Element.ALIGN_TOP, Element.ALIGN_TOP);


                string pagina = "Página " + writer.PageNumber + " de ";
                //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
                {
                    float margenbottom = 11f;
                    float margenright = 280f;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, tamanofuente);
                    cb.SetTextMatrix(doc.PageSize.GetBottom(margenright), doc.PageSize.GetBottom(margenbottom));
                    cb.ShowText(pagina);
                    cb.EndText();
                    float len = bf.GetWidthPoint(pagina, tamanofuente);
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));
                }

                PdfPTable footerTbl2 = new PdfPTable(2);


                float[] TablaAncho2 = new float[] { 100, 100 };
                footerTbl2.SetWidths(TablaAncho2);
                footerTbl2.TotalWidth = doc.Right - doc.Left;
                Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                string dia = String.Format("{0:dd}", DateTime.Now);
                string anio = String.Format("{0:yyyy}", DateTime.Now);
                string hora = String.Format("{0:HH:mm}", DateTime.Now);
                string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
                string fechahora = dia + " de " + mestexto + " de " + anio + " " + hora;
                Chunk chkFecha = new Chunk(fechahora, FuenteCelda);
                Phrase phrfecha = new Phrase(chkFecha);

                //Phrase promad = new Phrase("Promad Soluciones Computarizadas " + anio, FuenteCelda2);
                Phrase promad = new Phrase("", FuenteCelda2);
                cell = new PdfPCell(promad);
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);

                cell = new PdfPCell(phrfecha);
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);
                footerTbl2.WriteSelectedRows(0, -1, 5, 25, writer.DirectContent);
            }

            private static string ObtenerMesTexto(string mes)
            {
                string mestexto = "";
                switch (mes)
                {
                    case "01":
                        mestexto = "Enero";
                        break;
                    case "02":
                        mestexto = "Febrero";
                        break;
                    case "03":
                        mestexto = "Marzo";
                        break;
                    case "04":
                        mestexto = "Abril";
                        break;
                    case "05":
                        mestexto = "Mayo";
                        break;
                    case "06":
                        mestexto = "Junio";
                        break;
                    case "07":
                        mestexto = "Julio";
                        break;
                    case "08":
                        mestexto = "Agosto";
                        break;
                    case "09":
                        mestexto = "Septiembre";
                        break;
                    case "10":
                        mestexto = "Octubre";
                        break;
                    case "11":
                        mestexto = "Noviembre";
                        break;
                    case "12":
                        mestexto = "Diciembre";
                        break;
                    default:
                        mestexto = " ";
                        break;
                }
                return mestexto;
            }


            //Necesario para incluir pagina x de y al header
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);
                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, tamanofuente);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

            }
        }

        public static void CeldaConChunk(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, int bordo, int horizontal, int vertical)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk(valor, FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = horizontal;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }

        public static object[] generarReporteDiagnostico(Entity.CartaAntecedentePoliciacoConsecutivo carta)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "CartaNoAntecentesPoliciacos" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.CartaNoAntecentesPoliciacos"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteTitulo2 = new Font(fuentecalibri, 14f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
               
                TituloCartaAntecedente(documentoPdf, FuenteTitulo, FuenteTitulo2, FuenteTituloTabla,  carta, blau);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloCartaAntecedente(Document documentoPdf, Font FuenteTitulo, Font FuenteTitulo2, Font FuenteTituloTabla, Entity.CartaAntecedentePoliciacoConsecutivo carta, BaseColor blau)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;




            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }
            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            tabla.AddCell(cellWithRowspan);

            //Titulo
            CeldaVacio(tabla, 3);
            Celda(tabla, FuenteTitulo, "DIRECCIÓN GENERAL DE SEGURIDAD PÚBLICA", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tabla, FuenteTitulo2, "FORMATO REFERENCIA", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            PdfPTable tablaFolio = new PdfPTable(1);
            tablaFolio.WidthPercentage = 100;

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            CeldaVacio(tablaFolio, 1);
            var cellFolio = new PdfPCell(new Phrase($"Folio: {carta.Consecutivo}", FuenteNegrita));
            cellFolio.BorderWidth = 0;
            cellFolio.HorizontalAlignment = Element.ALIGN_RIGHT;
            tablaFolio.AddCell(cellFolio);
            CeldaVacio(tablaFolio, 1);
            

            //Tabla datos del detenido
            PdfPTable tabla2 = new PdfPTable(1);
            
            carta.cuerpo = carta.cuerpo.Replace("[Ciudadano]", carta.Ciudadano).Replace("[Ciudad]", carta.Ciudad).Replace("[Domicilio]", carta.Domicilio).Replace("[Años]", carta.Anio.ToString()).Replace("[Antecedentes]", carta.Antecedentes == 0 ? "NO" : "SI").Replace("[Fecha Antecedente]", carta.FechaAntecedente.ToString().Substring(0,10)).Replace("[día]", carta.Fecha.Day.ToString()).Replace("[mes]", carta.Fecha.ToString("MMMM")).Replace("[año]", carta.Fecha.Year.ToString());
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "", carta.cuerpo, 0, 0, 0);

            //var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            int idContrato = contratoUsuario.IdContrato;


            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "", "", 0, 0, 0);

            CeldaVacio(tabla2, 3);
            var cell1 = new PdfPCell(new Phrase("Atentamente ", FuenteNegrita));
            cell1.BorderWidth = 0;
            cell1.HorizontalAlignment = Element.ALIGN_CENTER;
            tabla2.AddCell(cell1);
            var parametros = ControlParametroContrato.TraerTodos();
            //CeldaVacio(tabla2, 3);
            var parametros2 = parametros.Where(X => X.Parametroid == 3 && X.ContratoId== idContrato);

            if(parametros2.Count() > 0)
            {
                cell1 = new PdfPCell(new Phrase(parametros2.FirstOrDefault().Valor, FuenteNegrita));
                cell1.BorderWidth = 0;
                cell1.HorizontalAlignment = Element.ALIGN_CENTER;
                tabla2.AddCell(cell1);
            }            

            CeldaVacio(tabla2, 3);

            var parametros3 = parametros.Where(X => X.Parametroid == 2 && X.ContratoId == idContrato);

            if(parametros3.Count() > 0)
            {
                cell1 = new PdfPCell(new Phrase(parametros3.FirstOrDefault().Valor, FuenteNegrita));
                cell1.BorderWidth = 0;
                cell1.HorizontalAlignment = Element.ALIGN_CENTER;
                tabla2.AddCell(cell1);
            }
            
            CeldaVacio(tabla2, 3);

            cell1 = new PdfPCell(new Phrase("Elaboró: " + carta.Elaboro, FuenteNormal));
            cell1.BorderWidth = 0;
            cell1.HorizontalAlignment = Element.ALIGN_LEFT;
            tabla2.AddCell(cell1);
            CeldaVacio(tabla2, 1);
            cell1 = new PdfPCell(new Phrase("Reviso: " + carta.Reviso, FuenteNormal));
            cell1.BorderWidth = 0;
            cell1.HorizontalAlignment = Element.ALIGN_LEFT;
            tabla2.AddCell(cell1);
            CeldaVacio(tabla2, 1);
            cell1 = new PdfPCell(new Phrase("Recibo caja no.:" + carta.ReciboCaja, FuenteNormal));
            cell1.BorderWidth = 0;
            cell1.HorizontalAlignment = Element.ALIGN_LEFT;
            tabla2.AddCell(cell1);
            CeldaVacio(tabla2, 1);


            tabla2.WidthPercentage = 100;
            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaFolio);

            string rutaimg = "";
            rutaimg = System.Web.HttpContext.Current.Server.MapPath(carta.Rutafoto);


            iTextSharp.text.Image imagen = null;
            try
            {

                imagen = iTextSharp.text.Image.GetInstance(rutaimg);
            imagen.BorderWidth = 0;
            imagen.Alignment = Element.ALIGN_CENTER;
            float percentage = 0.0f;
            percentage = 150 / imagen.Width;
            imagen.ScalePercent(percentage * 100);


            documentoPdf.Add(imagen);
           

            }
            catch (Exception)
            {
                PdfPTable tablaerror = new PdfPTable(1);
                CeldaVacio(tablaerror, 7);
                documentoPdf.Add(tablaerror);
            }

            documentoPdf.Add(tabla2);
      
           
            tabla.DeleteBodyRows();
        }

        private static void TituloDiagnostico(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, Entity.ReporteTrabajoSocial reporteTrabajoSocial, int DetalleDetencionId, BaseColor blau)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            int centroId = 0;
            var ei = ControlDetalleDetencion.ObtenerPorDetenidoId(DetalleDetencionId);
            foreach (var val in ei)
            {
                centroId = val.CentroId;
            }

            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            var image = iTextSharp.text.Image.GetInstance(pathfoto);

            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            tabla.AddCell(cellWithRowspan);

            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Diagnóstico del detenido", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);
            PdfPTable tablaDelegacionUsuario = new PdfPTable(1);
            float[] width = new float[] { 220 };
            tablaDelegacionUsuario.SetWidths(width);
            tablaDelegacionUsuario.DefaultCell.Border = Rectangle.NO_BORDER;
            PdfPCell celda;
            Chunk chunkTituloDelegacion = new Chunk("Delegación: ", FuenteNegrita);
            Chunk chunkNombreDelegacion = new Chunk(reporteTrabajoSocial.Delegacion != null ? reporteTrabajoSocial.Delegacion.ToString() : "", FuenteNormal);
            Paragraph parrafoDelegacion = new Paragraph();
            parrafoDelegacion.Add(chunkTituloDelegacion);
            parrafoDelegacion.Add(chunkNombreDelegacion);
            celda = new PdfPCell(parrafoDelegacion);
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.Border = 0;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            tablaDelegacionUsuario.AddCell(celda);



            CeldaVacio(tablaDelegacionUsuario, 1);

            //Titulo "Datos del detenido"
            PdfPTable tabla1 = new PdfPTable(1);
            tabla1.WidthPercentage = 100;
            PdfPCell tituloTabla1;
            tituloTabla1 = new PdfPCell(new Phrase("Datos del detenido", FuenteTituloTabla));
            tituloTabla1.HorizontalAlignment = Element.ALIGN_CENTER;
            tituloTabla1.Border = 0;
            tituloTabla1.BackgroundColor = blau;
            tituloTabla1.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla1.AddCell(tituloTabla1);

            CeldaVacio(tabla1, 1);

            //Tabla datos del detenido
            PdfPTable tabla2 = new PdfPTable(4);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Nombre: ", reporteTrabajoSocial.Nombre.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Alias: ", reporteTrabajoSocial.Alias.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Escolaridad: ", reporteTrabajoSocial.Escolaridad.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Edad: ", reporteTrabajoSocial.Edad.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Nacimiento: ", (reporteTrabajoSocial.FechaNacimiento != null) ? reporteTrabajoSocial.FechaNacimiento.ToString() : "", 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Estado civil: ", reporteTrabajoSocial.EstadoCivil.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Domicilio: ", reporteTrabajoSocial.Domicilio.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Teléfono: ", reporteTrabajoSocial.Telefono.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Colonia: ", reporteTrabajoSocial.Colonia.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Ciudad: ", reporteTrabajoSocial.Municipio.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Estado: ", reporteTrabajoSocial.Estado.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Nacionalidad: ", reporteTrabajoSocial.Nacionalidad.ToString(), 0, 0, 0);

            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "", "", 0, 0, 0);

            CeldaVacio(tabla2, 3);

            //Titulo "Remision"
            PdfPTable tabla3 = new PdfPTable(1);
            tabla3.WidthPercentage = 100;
            PdfPCell tituloTabla2;
            tituloTabla2 = new PdfPCell(new Phrase("Remisión", FuenteTituloTabla));
            tituloTabla2.HorizontalAlignment = Element.ALIGN_CENTER;
            tituloTabla2.Border = 0;
            tituloTabla2.BackgroundColor = blau;
            tituloTabla2.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla3.AddCell(tituloTabla2);

            CeldaVacio(tabla3, 1);

            //Tabla datos de la remision
            PdfPTable tabla4 = new PdfPTable(2);
            tabla4.WidthPercentage = 100;
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "No. remisión: ", reporteTrabajoSocial.Expediente.ToString(), 0, 0, 0);
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Fecha remisión ", reporteTrabajoSocial.Fecha.ToString(), 0, 0, 0);
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Motivo remisión: ", reporteTrabajoSocial.Motivo.ToString(), 0, 0, 0);
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Lugar de detención: ", reporteTrabajoSocial.LugarDetencion.ToString(), 0, 0, 0);
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Colonia: ", reporteTrabajoSocial.Colonia.ToString(), 0, 0, 0);
            CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "No. evento: ", reporteTrabajoSocial.Folio.ToString(), 0, 0, 0);

            CeldaVacio(tabla4, 3);


            //Titulo "Situacion"
            PdfPTable tabla7 = new PdfPTable(1);
            tabla7.WidthPercentage = 100;
            PdfPCell tituloTabla4;
            tituloTabla4 = new PdfPCell(new Phrase("Expediente", FuenteTituloTabla));
            tituloTabla4.HorizontalAlignment = Element.ALIGN_CENTER;
            tituloTabla4.Border = 0;
            tituloTabla4.BackgroundColor = blau;
            tituloTabla4.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla7.AddCell(tituloTabla4);

            CeldaVacio(tabla7, 1);

            //Datos situacion
            PdfPTable tabla8 = new PdfPTable(4);
            tabla8.WidthPercentage = 100;
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Motivo:", reporteTrabajoSocial.MotivoRehabilitacion.ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Adicción: ", reporteTrabajoSocial.Adiccion.ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Religión: ", reporteTrabajoSocial.Religion.ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Pandilla: ", reporteTrabajoSocial.Pandilla.ToString(), 0, 0, 0);


            CeldaVacio(tabla8, 1);

            //Explicacion situacion
            PdfPTable tabla9 = new PdfPTable(1);
            tabla9.WidthPercentage = 100;
            CeldaConChunk(tabla9, FuenteNegrita, FuenteNormal, "Cuadro patológico: ", reporteTrabajoSocial.CuadroPatologico.ToString(), 0, 0, 0);
            CeldaConChunk(tabla9, FuenteNegrita, FuenteNormal, "observación: ", reporteTrabajoSocial.Observacion.ToString(), 0, 0, 0);

            CeldaVacio(tabla9, 1);

            tablaDelegacionUsuario.WidthPercentage = 100;
            tabla2.WidthPercentage = 100;
            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaDelegacionUsuario);
            documentoPdf.Add(tabla1);
            documentoPdf.Add(tabla2);
            documentoPdf.Add(tabla3);
            documentoPdf.Add(tabla4);

            documentoPdf.Add(tabla7);
            documentoPdf.Add(tabla8);
            documentoPdf.Add(tabla9);
            tabla.DeleteBodyRows();
        }
    }
}
