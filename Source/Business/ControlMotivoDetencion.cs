﻿using DataAccess.MotivoDetencion;
using System.Collections.Generic;
using System;

namespace Business
{
     public class ControlMotivoDetencion
    {
        private static ServicioMotivoDetencion servicio = new ServicioMotivoDetencion("SQLConnectionString");

        public static int Guardar(Entity.MotivoDetencion motivoDetencion)
        {
            return servicio.Guardar(motivoDetencion);
        }

        public static void Actualizar(Entity.MotivoDetencion motivoDetencion)
        {
            servicio.Actualizar(motivoDetencion);
        }

        public static Entity.MotivoDetencion ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.MotivoDetencion> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.MotivoDetencion ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }
        public static List<Entity.MotivoDetencion> ObtenerHabilitados(int tipoCatalogo)
        {
            return servicio.ObtenerHabilitados(tipoCatalogo);
        }

        public static List<Entity.MotivoDetencion> ObtenerTodoPorInternoId(int id)
        {
            return servicio.ObtenerTodoByInternoId(id);           
        }
        public static List<Entity.MotivoDetencion> ObtenerTodosContratousuarioId(int UsuarioId)
        {
            return servicio.ObtenerTodosContratoUsuario(UsuarioId);
        }
    }
}
