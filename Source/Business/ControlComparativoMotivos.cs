﻿using DataAccess.ComparativoMotivos;
using System.Collections.Generic;
using System;


namespace Business
{
    public class ControlComparativoMotivos
    {
        private static ServicioComparativoMotivos servicio = new ServicioComparativoMotivos("SQLConnectionString");

        public static List<Entity.Comparativomotivos> GetComparativomotivos(string[] filtros)
        {

            return servicio.GetComparativomotivos(filtros);

        }
   
    }
}
