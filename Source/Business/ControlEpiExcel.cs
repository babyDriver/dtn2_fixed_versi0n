﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using OfficeOpenXml;
using System.Configuration;
using System.Linq;
using OfficeOpenXml.Style;
using System.Drawing;

namespace Business
{
    public static class ControlEpiExcel
    {
        private static string CrearDirectorioRemisiones(string nombre)
        {
            try
            {

                //Verificar si existe el directorio principal del repositorio de documentos
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio de la obra existe
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio para el grupo documental existe, sino crearlo
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre)));


                return string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs//" + nombre);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("No fue posible crear el repositorio para crear el archivo. ", ex.Message));
            }
        }
        public static object[] EpiExcel(List<Entity.Epi> Listado,string[] parametros)
        {
            bool generado = false;
            object[] obj = null;
            ExcelPackage xlPackage = null;
            try
            {

                string nombrearchivo = "Epi" + DateTime.Now.ToString("yyMMddHHmmss") + ".xlsx";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioRemisiones("Remisiones"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                FileInfo newFile = new FileInfo(urlArchivo);
                xlPackage = new ExcelPackage(newFile);
                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Presupuesto");

                SetTitlesAndFormat(worksheet,parametros,Listado);
                //SetLogo(worksheet);

                xlPackage.Save();
                obj = new object[] { true, ubicacionarchivo,  "" };
                generado = true;
                return obj;

            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };

            }


        }
        private static void SetTitlesAndFormat(ExcelWorksheet worksheet,string[] Parametros,List<Entity.Epi> Listado)
        {
            string celda = string.Empty;

            //          Cells[Renglon,Columna]
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var contrato = ControlContrato.ObtenerPorId(contratoUsuario.IdContrato);
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
            var institucion = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
            var domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);
            var municipio = ControlMunicipio.Obtener(domicilio.MunicipioId ?? 0);

            var date = Convert.ToDateTime(Parametros[1]);
            var h = date.Hour.ToString();
            if (date.Hour < 10) h = "0" + h;
            var min = date.Minute.ToString();
            if (date.Minute < 10) min = "0" + min;
            var s = date.Second.ToString();
            if (date.Second < 10) s = "0" + s;
            var fecha1 = date.ToShortDateString() + " " + h + ":" + min + ":" + s;

            date = Convert.ToDateTime(Parametros[2]);
            h = date.Hour.ToString();
            if (date.Hour < 10) h = "0" + h;
            min = date.Minute.ToString();
            if (date.Minute < 10) min = "0" + min;
            s = date.Second.ToString();
            if (date.Second < 10) s = "0" + s;
            var fecha2 = date.ToShortDateString() + " " + h + ":" + min + ":" + s;
            worksheet.Cells[2, 3].Value = "Ingreso de detenidos por primera vez en el año en estado de ebriedad";
            celda = "C2";
            worksheet.Cells[celda].Style.Font.Size = 18;
            worksheet.Cells[celda].Style.Font.Bold = true;
            //worksheet.Cells["B2:D5"].Merge = true;
            //worksheet.Cells["B2:D5"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            //worksheet.Cells["B2:D5"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            //celda = "B7:B9";
            worksheet.Cells[4, 2].Value = "Institución:";
            worksheet.Cells[4, 3].Value =institucion.Nombre;
            worksheet.Cells[5, 2].Value = "Municipio:";
            var m = "";
            if (municipio != null) m = municipio.Nombre;
            worksheet.Cells[5, 3].Value = m;
            worksheet.Cells[6, 2].Value = "Del: ";
            worksheet.Cells[6, 3].Value = fecha1;
            worksheet.Cells[6, 4].Value = "al: ";
            worksheet.Cells[6, 5].Value = fecha2;
            celda = "B6";
            worksheet.Cells[celda].Style.Font.Bold = true;
            celda = "D6";
            worksheet.Cells[celda].Style.Font.Bold = true;



            worksheet.Cells[8, 1].Value = "#";
            worksheet.Cells[8, 2].Value = "No. Remisión";
            worksheet.Cells[8, 3].Value = "Nombre";
            worksheet.Cells[8, 4].Value = "Fecha y hora de registro";
            worksheet.Cells["D8:F8"].Merge = true;
            worksheet.Cells[8, 7].Value = "Edad";
            worksheet.Cells[8, 8].Value = "Sexo";
            worksheet.Cells[8, 9].Value = "Conclusión médica";
            
            
            celda = "A8:I8";
            worksheet.Cells[celda].Style.Font.Bold = true;
            worksheet.Cells[celda].Style.Font.Size = 9;
            worksheet.Cells[celda].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            worksheet.Cells[celda].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[celda].Style.WrapText = true;
            

            //Relleno P.U. AUTOPISTA
            
            

           
            worksheet.Cells["C8:E8"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

            //Bordes a titulos del presupuesto
            worksheet.Cells["A8"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            worksheet.Cells["B8"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            worksheet.Cells["C8"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            worksheet.Cells["D8:F8"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            worksheet.Cells["G8"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            worksheet.Cells["H8"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            worksheet.Cells["I8"].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            worksheet.Cells["A8:I8"].Style.Font.Bold = true;

            var numero = 1;
            foreach(var item in Listado)
            {
                var n = 8 + numero;
                worksheet.Cells[n, 1].Value = numero.ToString();
                worksheet.Cells[n, 2].Value = item.Expediente;
                worksheet.Cells[n, 3].Value = item.Nombre;
                var h1 = item.Fecha.Hour.ToString();
                if (item.Fecha.Hour < 10) h1 = "0" + h1;
                var m1 = item.Fecha.Minute.ToString();
                if (item.Fecha.Minute < 10) m1 = "0" + m1;
                var s1 = item.Fecha.Second.ToString();
                if (item.Fecha.Second < 10) s1 = "0" + s1;
                var f = item.Fecha.ToShortDateString() + " " + h1 + ":" + m1 + ":" + s1;
                worksheet.Cells[n, 4].Value = f;
                var c = "D" + n.ToString() + ":F" + n.ToString();
                worksheet.Cells[c].Merge = true;
                worksheet.Cells[n, 7].Value = item.Edad.ToString(); ;
                worksheet.Cells[n, 8].Value = item.Sexo;
                worksheet.Cells[n, 9].Value = item.Conclucion;
                numero = numero + 1;
            }
            var t = (8 + Listado.Count).ToString();
            celda = "A9:A" + t;
            worksheet.Cells[celda].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            celda = "B9:B" + t;
            worksheet.Cells[celda].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            celda = "C9:C" + t;
            worksheet.Cells[celda].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            celda = "D9:D" + t;
            worksheet.Cells[celda].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            celda = "E9:E" + t;
            worksheet.Cells[celda].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            celda = "F9:F" + t;
            worksheet.Cells[celda].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            celda = "G9:G" + t;
            worksheet.Cells[celda].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            celda = "H9:H" + t;
            worksheet.Cells[celda].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            celda = "H9:I" + t;
            worksheet.Cells[celda].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            //Ancho en columnas
            worksheet.Column(1).Width = 3;
            worksheet.Column(2).Width = 12;
            worksheet.Column(3).Width = 30;
            worksheet.Column(4).Width = 13;
            worksheet.Column(5).Width = 13;
            worksheet.Column(6).Width = 14.14;
            worksheet.Column(7).Width = 10.71;
            worksheet.Column(8).Width = 10.86;
            worksheet.Column(9).Width =60;
            
           

            //Alto en filas
            worksheet.Row(1).Height = 11.25;
            worksheet.Row(2).Height = 21.75;
            worksheet.Row(3).Height = 21.75;
            worksheet.Row(4).Height = 16.5;
            worksheet.Row(5).Height = 16.5;
            worksheet.Row(6).Height = 16.5;
            worksheet.Row(7).Height = 16.5;
            worksheet.Row(8).Height = 16.5;
            worksheet.Row(9).Height = 15.75;
            worksheet.Row(10).Height = 24;

        }


        private static void SetLogo(ExcelWorksheet worksheet)
        {
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var contrato = ControlContrato.ObtenerPorId(contratoUsuario.IdContrato);
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
            var institucion = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
            var logo = subcontrato.Logotipo;

            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                //if (System.IO.File.Exists(logo))
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            FileInfo img_logo = new FileInfo(pathfoto);
            var picture = worksheet.Drawings.AddPicture(img_logo.Name, img_logo);
            picture.SetPosition(20, 805);
            picture.SetSize(285, 130);

        }
    }
}
