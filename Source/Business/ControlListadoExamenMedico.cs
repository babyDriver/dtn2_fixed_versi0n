﻿using DataAccess.ListadoExamenMedico;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlListadoExamenMedico
    {
        private static ServicioListadoExamenMedico servicio = new ServicioListadoExamenMedico("SQLConnectionString");

        public static List<Entity.ListadoExamenMedico> ObtenerTodos(object[] data)
        {
            return servicio.ObtenerTodos(data);
        }
    }
}
