﻿using DataAccess.Subcontrato;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlSubcontrato
    {
        private static ServicioSubcontrato servicio = new ServicioSubcontrato("SQLConnectionString");

        public static int Guardar(Entity.Subcontrato item)
        {
            return servicio.Guardar(item);
        }

        public static Entity.Subcontrato ObtenerPorTrackingId(Guid tracking)
        {
            return servicio.ObtenerByTrackingId(tracking);
        }

        public static Entity.Subcontrato ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static void Actualizar(Entity.Subcontrato item)
        {
            servicio.Actualizar(item);
        }

        public static List<Entity.Subcontrato> ObtenerByContratoId(int id)
        {
            return servicio.ObtenerByContratoId(id);
        }

        public static List<Entity.Subcontrato> ObtenerByUsuarioId(int id)
        {
            return servicio.ObtenerByUsuarioId(id);
        }

        public static List<Entity.Subcontrato> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }
    }
}
