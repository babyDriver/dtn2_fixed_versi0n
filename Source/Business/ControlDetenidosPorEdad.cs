﻿using DataAccess.DetenidosPorEdad;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlDetenidosPorEdad
    {
        private static ServicioDetenidosPoredad servicio = new ServicioDetenidosPoredad("SQLConnectionString");
        public static List<Entity.Detenidosporedad> ObtenerDetenidosporedad(string [] filtros)
        {
            return servicio.GetDetenidosporEdad(filtros);

        }

        public static List<Entity.Detenidosporedad> ObtenerDetenidosporedadLesion(string[] filtros)
        {
            return servicio.GetDetenidosporEdadLesion(filtros);
        }
    }
}
