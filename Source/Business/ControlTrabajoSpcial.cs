﻿using DataAccess.TrabajoSocial;
using System.Collections.Generic;
using System;


namespace Business
{
    
    public class ControlTrabajoSpcial
    {
        public static ServicioTrabajoSocial servicio = new ServicioTrabajoSocial("SQLConnectionString");
        public static int Guardar(Entity.TrabajoSocial trabajoSocial)
        {
            return servicio.guardarTs(trabajoSocial);
        }
        public static void SalidaEfectuada(Entity.TrabajoSocial trabajoSocial)
        {
            servicio.SalidaTs(trabajoSocial);
        }

        public static Entity.TrabajoSocial ObtenerByDetenidoId(int id)
        {
            return servicio.ObtenerByDetenidoId(id);
        }
    }
}
