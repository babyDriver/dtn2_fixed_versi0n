﻿using DataAccess.TrabajoSocial;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlExpedienteTrabajosocial
    {
        private static ServicioExpedientetrabajoSocialcs servicio = new ServicioExpedientetrabajoSocialcs();

        public static int Guardar(Entity.ExpedienteTrabajoSocial trabajoSocial)
        {
            return servicio.Guardar(trabajoSocial);
        }
        public static void Actualizar(Entity.ExpedienteTrabajoSocial trabajoSocial)
        {
            servicio.Actualizar(trabajoSocial);
        }
    }
}
