﻿using DataAccess.Matrizpermiso;
using System.Collections.Generic;


namespace Business
{
    public class ControlMatrizpermiso
    {
        private static ServicioMatrizpermiso servicio = new ServicioMatrizpermiso("SQLConnectionString");

        public static int Guardar(Entity.Matrizpermiso matriz)
        {
            return servicio.Guardar(matriz);
        }

        public static void Actualizar(Entity.Matrizpermiso matriz)
        {
            servicio.Actualizar(matriz);
        }

        public static Entity.Matrizpermiso ObtenerPorUsuarioId(int id)
        {
            return servicio.ObtenerByUsuarioId(id);
        }
        
        public static List<Entity.Matrizpermiso> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }
    }
}
