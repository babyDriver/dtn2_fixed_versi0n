﻿using DataAccess.Catalogo;
using System;
using System.Collections.Generic;

namespace Business
{
    public static class ControlCatalogo
    {
        private static ServicioCatalogo servicio = new ServicioCatalogo("SQLConnectionString");

        public static Entity.Catalogo Obtener(int id, int tipoCatalogo)
        {            
            var parametros = new object[2];
            parametros[0] = id;
            parametros[1] = tipoCatalogo;
            
            return servicio.ObtenerPorId(parametros);
        }

        public static Entity.Catalogo Obtener(string nombre, int tipoCatalogo)
        {
            var parametros = new object[2];
            parametros[0] = nombre;
            parametros[1] = tipoCatalogo;

            return servicio.ObtenerPorNombre(parametros);
        }

        public static Entity.Catalogo Obtener(System.Guid trackingId, int tipoCatalogo)
        {
            var parametros = new object[2];
            parametros[0] = trackingId;
            parametros[1] = tipoCatalogo;
            return servicio.ObtenerPorTrackingId(parametros);
        }

    
        public static List<Entity.Catalogo> ObtenerTodo(int tipoCatalogo)
        {
            return servicio.ObtenerTodos(tipoCatalogo);
        }

        public static List<Entity.Catalogo> ObtenerHabilitados(int tipoCatalogo)
        {
            return servicio.ObtenerHabilitados(tipoCatalogo);
        }
       

        public static int Guardar(Entity.Catalogo catalogo)
        {
            return servicio.Guardar(catalogo);
        }

        public static void Actualizar(Entity.Catalogo catalogo)
        {
            servicio.Actualizar(catalogo);
        }

    }
}
