﻿using DataAccess.ReporteEstadisticoTrazabilidad;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlReporteEstadisticoTrazabilidad
    {
        private static ServicioReporteEstadisticoTrazabilidad servicio = new ServicioReporteEstadisticoTrazabilidad("SQLConnectionString");

        public static List<Entity.ReporteEstadisticoTrazabilidad> GetTrazabilidad(string[]filtro)
        {
            return servicio.GetList(filtro);
        }
    }
}
