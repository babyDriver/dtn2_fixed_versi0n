﻿using DataAccess.ReporteBoletaControl;
using System.Collections.Generic;
using System;


namespace Business
{
    public class ControlReporteBoletaControl
    {
        public static Entity.ReporteBoletaControl GetReporteBoletaControlByDetenidoId(int InternoId)
        {
            try
            {
                ServicioReporteBoletaControl servicio = new ServicioReporteBoletaControl();
                return servicio.GetReporteBoletaControl(InternoId);
            }

            catch (Exception ex) {
                throw new Exception(string.Concat("Err00000000000r. ", ex.Message));
            }
        }
    }
}
