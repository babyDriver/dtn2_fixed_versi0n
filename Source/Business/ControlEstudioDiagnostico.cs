﻿using DataAccess.EstudioDiganostico;
using System.Collections.Generic;
using System;

namespace Business
{
   public class ControlEstudioDiagnostico
    {
        private static ServicioEsutudioDiagnostico servicio = new ServicioEsutudioDiagnostico();

        public static int Guardar(Entity.EstudioDiagnostico estudioDiagnostico)
        {
            return servicio.Guardar(estudioDiagnostico);
        }
        public static Entity.EstudioDiagnostico ObtenerByTrackingId(string TRACKING)
        {
            return servicio.ObtenerByTrackingId( new Guid (TRACKING));
        }
    }
}
