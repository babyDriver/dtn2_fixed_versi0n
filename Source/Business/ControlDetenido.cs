﻿using DataAccess.Detenido;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlDetenido
    {
        private static ServicioDetenido servicio = new ServicioDetenido("SQLConnectionString");

        public static int Guardar(Entity.Detenido interno)
        {
            return servicio.Guardar(interno);
        }

        public static void Actualizar(Entity.Detenido interno)
        {
            servicio.Actualizar(interno);
        }

        public static Entity.Detenido ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.Detenido> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Detenido ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static List<Entity.Detenido> ObtenePorNombre(string nombre)
        {
            return servicio.ObtenerByNombre(nombre);
        }
    }
}
