﻿using DataAccess.Colonia;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Business
{
    public class ControlColonia
    {
        private static ServicioColonia servicio = new ServicioColonia("SQLConnectionString");

        public static int Guardar(Entity.Colonia colonia)
        {
            return servicio.Guardar(colonia);
        }

        //public static Entity.Colonia ObtenerPorMunicipioCodigoColonia(object[] parametros)
        //{
        //    return servicio.ObtenerByMunicipioAndCodigoAndColonia(parametros);
        //}        
        
        public static Entity.Colonia ObtenerPorMunicipioCodigoColonia(string parametro)
        {
            return servicio.ObtenerByMunicipioAndCodigoAndColonia(parametro);
        }

        public static void Actualizar(Entity.Colonia colonia)
        {
            servicio.Actualizar(colonia);
        }

        public static Entity.Colonia ObtenerPorTrackingId(Guid guid)
        {
            return servicio.ObtenerByTrackingId(guid);
        }

        public static List<Entity.Colonia> ObtenerPorMunicipioId(int id)
        {
            return servicio.ObtenerByMunicipioId(id).Where(x => x.Habilitado).ToList();
        }

        public static Entity.Colonia ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.Colonia> ObtenerTodas()
        {
            return servicio.ObtenerTodas().Where(x => x.Habilitado).ToList();
        }
    }
}
