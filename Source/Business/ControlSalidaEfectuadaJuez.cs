﻿using DataAccess.SalidaEfectuadaJuez;
using System.Collections.Generic;
using System;

namespace Business
{
   public class ControlSalidaEfectuadaJuez
    {
        private static ServicioSalidaEfectuadaJuez servicio = new ServicioSalidaEfectuadaJuez("SQLConnectionString");

        public static int Guardar(Entity.SalidaEfectuadaJuez salidaEfectuadajuez)
        {
            return servicio.Guardar(salidaEfectuadajuez);
        }

        public static Entity.SalidaEfectuadaJuez ObtenerByDetalleDetencionId(int id)
        {
            return servicio.ObtenerByDetalleDetencionId(id);
        }
        public static void Actualizar(Entity.SalidaEfectuadaJuez salidaEfectuadaJuez)
        {
            servicio.Actualizar(salidaEfectuadaJuez);
        }
    }
}
