﻿using DataAccess.CertificadoLesionAntecedentesDetalle;
using System.Collections.Generic;
using System;
namespace Business
{
    public class ControlCertificadoLesionAntecedentesDetalle
    {
        private static ServicioCertificadoLesionAntecedentesDetalle servicio = new ServicioCertificadoLesionAntecedentesDetalle("SQLConnectionString");


        public static int Guardar(Entity.CertificadoAntecedentesDetalle certificadoAntecedentesDetalle)
        {

            return servicio.Guardar(certificadoAntecedentesDetalle);
        }
        public static List<Entity.CertificadoAntecedentesDetalle> ObteneroPortipoAntecedenteIdn(int Id)
        {
            return servicio.ObtenerPorId(Id);
        }

    }
}
