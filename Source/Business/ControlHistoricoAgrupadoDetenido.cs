﻿using DataAccess.HistoricoAgrupadoDetenido;
using System.Collections.Generic;
using System;

namespace Business
{
   public class ControlHistoricoAgrupadoDetenido
    {
        private static ServicioHistoricoAgrupadoDetenido servicio = new ServicioHistoricoAgrupadoDetenido("SQLConnectionString");
        public static int Guardar(Entity.HistoricoAgrupadoDetenido historicoAgrupadoDetenido)
        {
            return servicio.save(historicoAgrupadoDetenido);
        }
        public static List<Entity.HistoricoAgrupadoDetenido> HistoricosAgrupoadoPorDetenidooriginal(int DetenidoOriginalId)
        {
            return servicio.ObtenerPorDetenidoOriginal(DetenidoOriginalId);
        }
        public static List<Entity.HistoricoAgrupadoDetenido> HistoricosAgrupoadoPorDetenido(int DetenidoOriginalId)
        {
            return servicio.ObtenerPorDetenidol(DetenidoOriginalId);
        }
    }
}
