﻿using DataAccess.SubcontratoWSAInstitucion;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlSubcontratoWSAInstitucion
    {
        private static ServicioSubcontratoWSAInstitucion servicio = new ServicioSubcontratoWSAInstitucion("SQLConnectionString");

        public static List<Entity.SubcontratoWSAInstitucion> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static List<Entity.SubcontratoWSAInstitucion> ObtenerPorWSAInstitucionId(int wsainstitucionId)
        {
            return servicio.ObtenerPorWSAInstitucionId(wsainstitucionId);
        }

        public static List<Entity.SubcontratoWSAInstitucion> ObtenerPorSubcontratoId(int subcontratoId)
        {
            return servicio.ObtenerPorSubcontratoId(subcontratoId);
        }

        public static Entity.SubcontratoWSAInstitucion ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerPorTrackingId(trackingId);
        }

        public static Entity.SubcontratoWSAInstitucion ObtenerPorId(int id)
        {
            return servicio.ObtenerPorId(id);
        }

        public static int Guardar(Entity.SubcontratoWSAInstitucion subcontratoWSAInstitucion)
        {
            return servicio.Guardar(subcontratoWSAInstitucion);
        }

        public static void Actualizar(Entity.SubcontratoWSAInstitucion subcontratoWSAInstitucion)
        {
            servicio.Actualizar(subcontratoWSAInstitucion);
        }

        public static void Eliminar(int id)
        {
            servicio.Eliminar(id);
        }
    }
}
