﻿using DataAccess.ReporteUsosistema;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlReporteUsosistema
    {
        public static ServicioReporteUsosistema servicio = new ServicioReporteUsosistema("SQLConnectionString");
        public static List<Entity.ReporteUsosistema> Obtenerporfechas(string[] fechas)
        {
            return servicio.Obtenerporfechas(fechas);
        }
    }
}
