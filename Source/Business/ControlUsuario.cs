﻿using DataAccess.Usuario;
using System;
using System.Collections.Generic;

namespace Business
{
    public static class ControlUsuario
    {
        private static ServicioUsuario servicio = new ServicioUsuario("SQLConnectionString");

        public static int Guardar(Entity.Usuario usuario)
        {
            return servicio.Guardar(usuario);
        }

        public static void Actualizar(Entity.Usuario usuario)
        {
            servicio.Actualizar(usuario);
        }

        public static Entity.Usuario Obtener(int id)
        {
            return servicio.ObtenerUsuario(id);
        }

        public static Entity.Usuario ObtenerPorEmail(string email)
        {
            return servicio.ObtenerByEmail(email);
        }

        public static Entity.Usuario ObtenerPorMovil(string movil)
        {
            return servicio.ObtenerByMovil(movil);
        }

        public static Entity.Usuario ObtenerPorUsuario(string usuario)
        {
            return servicio.ObtenerByUsuario(usuario);
        }

        public static Entity.Usuario Obtener(Guid provideruserkey)
        {
            return servicio.ObtenerUsuario(provideruserkey);
        }

        public static List<Entity.Usuario> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }


    }
}