﻿using DataAccess.Domicilio;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlDomicilio
    {
        private static ServicioDomicilio servicio = new ServicioDomicilio("SQLConnectionString");

        public static int Guardar(Entity.Domicilio nacimineto)
        {
            return servicio.Guardar(nacimineto);
        }

        public static void Actualizar(Entity.Domicilio nacimineto)
        {
            servicio.Actualizar(nacimineto);
        }

        public static Entity.Domicilio ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static Entity.Domicilio ObtenerPorInternoId(int id)
        {
            return servicio.ObtenerByInternoId(id);
        }

        public static List<Entity.Domicilio> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Domicilio ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.Domicilio ObtenerPorInternoIdTipo(string[] datos)
        {
            return servicio.ObtenerByInternoIdTipo(datos);
        }
    }
}
