﻿using DataAccess.ReporteSancionesJuezCalificador;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlReporteSancionesJuezCalificador
    {
        private static ServicioReporteSancionesJuezcalificador servicio = new ServicioReporteSancionesJuezcalificador("SQLConnectionString");

        public static List<Entity.ReporteSancionesjuezcalificador> GetreporteSanciones(string[] parametros)
        {
            return servicio.GetList(parametros);
        }


    }
}
