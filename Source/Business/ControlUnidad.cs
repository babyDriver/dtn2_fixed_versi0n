﻿using DataAccess.Unidad;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlUnidad
    {
        private static ServicioUnidad servicio = new ServicioUnidad("SQLConnectionString");

        public static List<Entity.Unidad> ObtenerPorEventoId(int id)
        {
            return servicio.ObtenerPorEventoId(id);
        }
        public static int Guardar(Entity.Unidad unidad)
        {

            return servicio.Guardar(unidad);

        }
        public static void Actualizar(Entity.Unidad unidad)
        {
            servicio.Actualizar(unidad);

        }
        public static Entity.Unidad ObtenerById(int Id)

        {
            return servicio.ObtenerById(Id);
        }

    }
}
