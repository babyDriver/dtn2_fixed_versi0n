﻿using DataAccess.General;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlGeneral
    {
        private static ServicioGeneral servicio = new ServicioGeneral("SQLConnectionString");

        public static int Guardar(Entity.General alias)
        {
            return servicio.Guardar(alias);
        }

        public static void Actualizar(Entity.General general)
        {
            servicio.Actualizar(general);
        }

        public static Entity.General ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.General> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.General ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.General ObtenerPorDetenidoId(int id)
        {
            return servicio.ObtenerByDetenidoId(id);
        }
    }
}
