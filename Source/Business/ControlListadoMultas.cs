﻿using DataAccess.ListadoMultas;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlListadoMultas
    {
        private static ServicioListadoMultas servicio = new ServicioListadoMultas("SQLConnectionString");

        public static List<Entity.ListadoMultas> ObtenerTodos(object[] data)
        {
            return servicio.ObtenerTodos(data);
        }
    }
}
