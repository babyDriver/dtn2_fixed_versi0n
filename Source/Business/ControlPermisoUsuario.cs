﻿using DataAccess.PermisoUsuario;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlPermisoUsuario
    {
        private static ServicioPermisoUsuario servicio = new ServicioPermisoUsuario("SQLConnectionString");

        public static int Guardar(Entity.PermisoUsuario permiso)
        {
            return servicio.Guardar(permiso);
        }

        public static void Actualizar(Entity.PermisoUsuario permiso)
        {
            servicio.Actualizar(permiso);
        }

        public static Entity.PermisoUsuario ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.PermisoUsuario> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static List<Entity.PermisoUsuario> ObtenePorHabilitadoAcitvo(Entity.PermisoUsuario permiso)
        {
            return servicio.ObtenerByHabilitadoAcitvo(permiso);
        }

    }
}
