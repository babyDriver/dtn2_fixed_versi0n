﻿using DataAccess.DetenidosActivosSelect;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlDetenidosActivosSelect
    {
        private static ServicioDetenidosActivosSelect servicio = new ServicioDetenidosActivosSelect("SQLConnectionString");

        public static List<Entity.DetenidosActivosSelect> ObtenerTodos(object[] data)
        {
            return servicio.ObtenerTodos(data);
        }
    }
}
