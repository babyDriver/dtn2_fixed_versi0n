﻿using DataAccess.Antecedente;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlAntecedente
    {
        private static ServicioAntecedente servicio = new ServicioAntecedente("SQLConnectionString");

        public static int Guardar(Entity.Antecedente antecedente)
        {
            return servicio.Guardar(antecedente);
        }

        public static void Actualizar(Entity.Antecedente antecedente)
        {
            servicio.Actualizar(antecedente);
        }

        public static Entity.Antecedente ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.Antecedente> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Antecedente ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.Antecedente ObtenerPorDetenidoId(int id)
        {
            return servicio.ObtenerByDetenidoId(id);
        }

        public static Entity.Antecedente ObtenerPorProcesoId(int id)
        {
            return servicio.ObtenerByProcesoId(id);
        }

    }
}
