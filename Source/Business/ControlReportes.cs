﻿using DataAccess.Reportes;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlReportes
    {
        private static ServicioReportes servicio = new ServicioReportes("SQLConnectionString");

        public static List<Entity.Reportes> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static List<Entity.Reportes> ObtenerPorPantallaId(int pantallaId)
        {
            return servicio.ObtenerPorPantallaId(pantallaId);
        }

        public static Entity.Reportes ObtenerPorId(int id)
        {
            return servicio.ObtenerPorId(id);
        }

        public static Entity.Reportes ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerPorTrackingId(trackingId);
        }
    }
}
