﻿using DataAccess.WSAMotivo;
using System.Collections.Generic;

namespace Business
{
    public class ControlWSAMotivo
    {
        private static ServicioWSAMotivo servicio = new ServicioWSAMotivo("SQLConnectionString");

        public static List<Entity.WSAMotivo> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.WSAMotivo ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static int Guardar(Entity.WSAMotivo motivo)
        {
            return servicio.Guardar(motivo);
        }

        public static void Actualizar(Entity.WSAMotivo motivo)
        {
            servicio.Actualizar(motivo);
        }
    }
}
