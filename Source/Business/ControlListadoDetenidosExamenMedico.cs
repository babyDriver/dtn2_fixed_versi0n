﻿using DataAccess.ListadoDetenidosExamenMedico;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlListadoDetenidosExamenMedico
    {
        private static ServicioListadoDetenidosExamenMedico servicio = new ServicioListadoDetenidosExamenMedico("SQLConnectionString");

        public static List<Entity.ListadoDetenidosExamenMedico> ObtenerTodos(object[] data)
        {
            return servicio.ObtenerTodos(data);
        }
    }
}
