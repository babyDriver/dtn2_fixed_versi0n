﻿using DataAccess.ListadoReporteExamenMedico;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlListadoReporteExamenMedico
    {
        private static ServicioListadoReporteExamenMedico servicio = new ServicioListadoReporteExamenMedico("SQLConnectionString");

        public static List<Entity.ListadoReporteExamenMedico> ObtenerTodos(object[] data)
        {
            return servicio.ObtenerTodos(data);
        }
    }
}
