﻿using DataAccess.DashboardAnioTrabajo;
using System.Collections.Generic;

namespace Business
{
    public class ControlDashBoardAnioTrabajo
    {
        private static ServicioDashBoardAnioTrabajo servicio = new ServicioDashBoardAnioTrabajo("SQLConnectionString");

        public static List<Entity.DasboardAnioTrabajo> GetAniosTrabajo()
        {
            return servicio.GetYears();
        }
    }
}
