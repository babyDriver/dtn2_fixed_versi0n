﻿using DataAccess.JuezCalificadorList;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlJuezCalificadorList
    {
        private static ServicioJuezCalificadorList servicio = new ServicioJuezCalificadorList("SQLConnectionString");

        public static List<Entity.JuezCalificadorList> ObtenerTodos(object[] data)
        {
            return servicio.ObtenerTodos(data);
        }
    }
}
