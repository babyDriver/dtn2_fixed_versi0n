﻿using DataAccess.WSAEstado;
using System.Collections.Generic;

namespace Business
{
    public static class ControlWSAEstado
    {
        private static ServicioWSAEstado servicio = new ServicioWSAEstado("SQLConnectionString");

        public static Entity.WSAEstado ObtenerPorId(int id)
        {
            return servicio.ObtenerPorId(id);
        }

        public static List<Entity.WSAEstado> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static int Guardar(Entity.WSAEstado estado)
        {
            return servicio.Guardar(estado);
        }

        public static void Actualizar(Entity.WSAEstado estado)
        {
            servicio.Actualizar(estado);
        }
    }
}
