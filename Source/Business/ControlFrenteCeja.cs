﻿using DataAccess.FrenteCeja;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlFrenteCeja
    {
        private static ServicioFrenteCeja servicio = new ServicioFrenteCeja("SQLConnectionString");

        public static int Guardar(Entity.FrenteCeja frente)
        {
            return servicio.Guardar(frente);
        }

        public static void Actualizar(Entity.FrenteCeja frente)
        {
            servicio.Actualizar(frente);
        }

        public static Entity.FrenteCeja ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.FrenteCeja> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.FrenteCeja ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.FrenteCeja ObtenerPorAntropometriaId(int id)
        {
            return servicio.ObtenerByAntropometriaId(id);
        }
    }
}
