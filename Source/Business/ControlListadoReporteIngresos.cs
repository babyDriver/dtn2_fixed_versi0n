﻿using DataAccess.ListadoReporteIngresos;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlListadoReporteIngresos
    {
        private static ServicioListadoReporteIngresos servicio = new ServicioListadoReporteIngresos("SQLConnectionString");

        public static List<Entity.ListadoReporteIngresos> ObtenerTodos(object[] data)
        {
            return servicio.ObtenerTodos(data);
        }

        public static List<Entity.ListadoReporteIngresos> ObtenerPorFechas(object[] data)
        {
            return servicio.ObtenerPorFechas(data);
        }
    }
}
