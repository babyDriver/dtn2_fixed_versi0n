﻿using DataAccess.ListadoReporteOficio;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlListadoReporteOficio
    {
        private static ServicioListadoReporteOficio servicio = new ServicioListadoReporteOficio("SQLConnectionString");

        public static List<Entity.ListadoReporteOficio> ObtenerTodos(object[] data)
        {
            return servicio.ObtenerTodos(data);
        }
    }
}
