﻿using DataAccess.Institucion;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlInstitucion
    {
        private static ServicioInstitucion servicio = new ServicioInstitucion("SQLConnectionString");

        public static List<Entity.Institucion> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }
    }
}
