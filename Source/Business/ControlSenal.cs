﻿using DataAccess.Senal;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlSenal
    {
        private static ServicioSenal servicio = new ServicioSenal("SQLConnectionString");

        public static int Guardar(Entity.Senal senal)
        {
            return servicio.Guardar(senal);
        }

        public static void Actualizar(Entity.Senal senal)
        {
            servicio.Actualizar(senal);
        }

        public static Entity.Senal ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static Entity.Senal ObtenerPorDetenidoId(int id)
        {
            return servicio.ObtenerByDetenidoId(id);
        }

        public static List<Entity.Senal> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Senal ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }
    }
}
