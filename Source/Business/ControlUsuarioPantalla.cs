﻿using DataAccess.UsuarioPantalla;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlUsuarioPantalla
    {
        private static ServicioUsuarioPantalla servicio = new ServicioUsuarioPantalla("SQLConnectionString");

        public static Entity.UsuarioPantalla ObtenerPorUsuarioIdPantallaId(string[] id)
        {
            return servicio.ObtenerByUsuarioIdPantalla(id);
        }


    }
}
