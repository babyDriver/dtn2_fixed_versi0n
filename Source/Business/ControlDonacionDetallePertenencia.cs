﻿using DataAccess.DonacionDetallePertenencia;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlDonacionDetallePertenencia
    {
        private static ServicioDonacionDetallePertenencia servicio = new ServicioDonacionDetallePertenencia("SQLConnectionString");

        public static int Guardar(Entity.DonacionDetallePertenencia donacion)
        {
            return servicio.Guardar(donacion);
        }

        public static List<Entity.DonacionDetallePertenencia> ObtenerByDonacionId(int id)
        {
            return servicio.ObtenerByDonacionId(id);
        }
    }
}
