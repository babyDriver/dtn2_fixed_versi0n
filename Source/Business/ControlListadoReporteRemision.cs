﻿using DataAccess.ListadoReporteRemision;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlListadoReporteRemision
    {
        private static ServicioListadoReporteRemision servicio = new ServicioListadoReporteRemision("SQLConnectionString");

        public static List<Entity.ListadoReporteRemision> ObtenerTodos(object[] data)
        {
            return servicio.ObtenerTodos(data);
        }
    }
}
