﻿using DataAccess.ReportesLog;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlReportesLog
    {
        private static ServicioReportesLog servicio = new ServicioReportesLog("SQLConnectionString");

        public static int Guardar(Entity.ReportesLog reportes)
        {
            return servicio.Guardar(reportes);
        }
    }
}
