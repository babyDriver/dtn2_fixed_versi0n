﻿using DataAccess.CartaAntecedentePoliciacoConsecutivo;
using System.Collections.Generic;
using System;

namespace Business
{
   public static class ControlCartaAntecedentesPoliciacosConsecutivo
    {
        private static ServicioCartaAntecedentePoliciacoConsecutivo servicio = new ServicioCartaAntecedentePoliciacoConsecutivo("SQLConnectionString");

        public static int Guardar(Entity.CartaAntecedentePoliciacoConsecutivo carta)
        {
            return servicio.Guardar(carta);
        }
        public static void Actualizar(Entity.CartaAntecedentePoliciacoConsecutivo carta)
        {
            servicio.Actualizar(carta);
        }

        public static Entity.CartaAntecedentePoliciacoConsecutivo obteberByContratoId(int ContratoId)
        {
            return servicio.GetByContratoId(ContratoId);
        }
        public static Entity.CartaAntecedentePoliciacoConsecutivo obteberById(int Id)
        {
            return servicio.GetById(Id);
        }
    }
}
