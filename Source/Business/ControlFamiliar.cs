﻿using DataAccess.Familiar;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlFamiliar
    {
        private static ServicioFamiliar servicio = new ServicioFamiliar("SQLConnectionString");

        public static int Guardar(Entity.Familiar item)
        {
            return servicio.Guardar(item);
        }

        public static void Actualizar(Entity.Familiar item)
        {
            servicio.Actualizar(item);
        }

        public static Entity.Familiar ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.Familiar> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Familiar ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.Familiar ObtenerPorDetenidoId(int id)
        {
            return servicio.ObtenerByDetenidoId(id);
        }

        public static Entity.Familiar ObtenerPorFamiliar(Entity.Familiar item)
        {
            return servicio.ObtenerByFamiliar(item);
        }
    }
}
