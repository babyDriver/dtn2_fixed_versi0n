﻿using DataAccess.InternoReporte;
using System.Collections.Generic;

namespace Business
{
    public static class ControlInternoReporte
    {
        private static ServicioInternoReporte servicio = new ServicioInternoReporte("SQLConnectionString");

        public static List<Entity.Interno_Reporte> ObtenerTodos(object[] datos)
        {

            return servicio.ObtenerTodosPorParametros(datos);
        }


        public static Entity.Interno_Reporte Obtener(object[] datos)
        {

            return servicio.ObtenerPorParametros(datos);
        }
    }
}
