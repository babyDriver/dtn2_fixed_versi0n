﻿using DataAccess.SubProceso;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlSubProceso
    {
        private static ServicioSubProceso servicio = new ServicioSubProceso("SQLConnectionString");

        public static List<Entity.SubProceso> GetAll()
        {
            return servicio.ObtenerTodo();
        }
    }
}
