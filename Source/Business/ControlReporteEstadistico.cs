﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataAccess.ReporteEstadistico;

namespace Business
{
    public class ControlReporteEstadistico
    {
        public static ServicioReporteEstadistico servicio = new ServicioReporteEstadistico("SQLConnectionString");

        public static List<Entity.Reporteestadistico>ObtenerTodos()
        {
            return servicio.GetAll();

        }
    }
}
