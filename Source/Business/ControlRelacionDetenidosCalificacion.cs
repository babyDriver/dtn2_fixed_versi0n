﻿using DataAccess.RelacionDetenidoCalificacion;
using System.Collections.Generic;

namespace Business
{
    public class ControlRelacionDetenidosCalificacion
    {
        private static ServicioRelacionDetenidoCalificacion servicio = new ServicioRelacionDetenidoCalificacion("SQLConnectionString");

        public static List<Entity.RelacionDetenidoCalificacion> GetDet(string[] filtros)
        {
            return servicio.GetList(filtros);
        }
    }
}