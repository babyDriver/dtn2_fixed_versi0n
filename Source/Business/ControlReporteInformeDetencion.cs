﻿using DataAccess.ReporteInformeDetencion;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlReporteInformeDetencion
    {
        public static Entity.ReporteInformeDetencion GetReporteInformeDetencionByInternoId(int InternoId)
        {
            ServicionReporteInformeDetencion servicio = new ServicionReporteInformeDetencion();
            return servicio.GetReporteInformeDetencion(InternoId);
        }
    }
}
