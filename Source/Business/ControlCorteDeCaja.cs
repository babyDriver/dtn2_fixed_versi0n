﻿using DataAccess.CorteDeCaja;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlCorteDeCaja
    {
        private static ServicioCorteDeCaja servicio = new ServicioCorteDeCaja("SQLConnectionString");

        public static int Guardar(Entity.CorteDeCaja item)
        {
            return servicio.Guardar(item);
        }

        public static Entity.CorteDeCaja ObtenerUltimoCorte(object[] parametros)
        {
            return servicio.ObtenerLastCorte(parametros);
        }

        public static void Actualizar(Entity.CorteDeCaja item)
        {
            servicio.Actualizar(item);
        }

        public static Entity.CorteDeCaja ObtenerByTrackingId(Guid guid)
        {
            return servicio.ObtenerByTrackingId(guid);
        }
    }
}
