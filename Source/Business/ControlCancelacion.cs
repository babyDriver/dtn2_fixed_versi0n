﻿using DataAccess.Cancelacion;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlCancelacion
    {
        private static ServicioCancelacion servicio = new ServicioCancelacion("SQLConnectionString");

        public static string Desactivar(Guid guid)
        {
            var tmp = ObtenerPorTrackingId(guid);
            tmp.Habilitado = tmp.Habilitado ? false : true;
            servicio.Actualizar(tmp);
            string mensaje = tmp.Habilitado ? "habilitado" : "deshabilitado";
            return mensaje;
        }

        public static List<Entity.Cancelacion> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static int Guardar(Entity.Cancelacion item)
        {
            return servicio.Guardar(item);
        }

        public static Entity.Cancelacion ObtenerPorTrackingId(Guid tracking)
        {
            return servicio.ObtenerByTrackingId(tracking);
        }

        public static Entity.Cancelacion ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static void Actualizar(Entity.Cancelacion item)
        {
            servicio.Actualizar(item);
        }

        public static Entity.Cancelacion ObtenerFolio(object[] parametros)
        {
            return servicio.ObtenerFolio(parametros);
        }

        public static List<Entity.Cancelacion> ObtenerTodoByFecha(object[] parametros)
        {
            return servicio.ObtenerAllByDate(parametros);
        }

        public static Entity.Cancelacion ObtenerByTipoAndFolio(object[] parametros)
        {
            return servicio.ObtenerByTipoAndFolio(parametros);
        }
    }
}
