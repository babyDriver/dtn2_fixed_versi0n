﻿using DataAccess.ReporteEstadisticoDetenidoPorTipoLesionCL;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlReporteEstadisticoDetenidoPorTipoLesionCL
    {
        private static ServicioReporteEstadisticoDetenidoPorTipoLesionCL servicio=new ServicioReporteEstadisticoDetenidoPorTipoLesionCL("SQLConnectionString");

        public static List<Entity.ReporteEstadisticoDetenidoPorTipoLesionCL> GetByFilter(string[] filter)
        {
            return servicio.GetByFilter(filter);
        }
    }
}
