﻿using DataAccess.CertificadoLesion;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlCertificadoLesiones
    {
        private static ServicioCertificadolesion servicio = new ServicioCertificadolesion("SQLConnectionString");

        public static void Actualizar(Entity.CertificadoLesion certificadoLesion)
        {
            servicio.Actualizar(certificadoLesion);

        }
        public static int Guardar(Entity.CertificadoLesion certificadoLesion)
        {

            return servicio.Guardar(certificadoLesion);
        }

        public static List<Entity.CertificadoLesion>ObTenerTodo()
        {
            return servicio.ObtenerTodos();

        }

        public static Entity.CertificadoLesion ObtenerPorId(int Id)
        {
            return servicio.ObtenerById(Id);
        }

        public static Entity.CertificadoLesion ObtenerPorTrackingId(string TrackingId)
        {
            return servicio.ObtenerByTrackingId(TrackingId);
        }

    }
}
