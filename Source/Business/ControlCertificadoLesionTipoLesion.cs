﻿using DataAccess.CertificadoLesionTipoLesion;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlCertificadoLesionTipoLesion
    {
        private static ServicioCertificadoLesionTipoLesion servicio = new ServicioCertificadoLesionTipoLesion("SQLConnectionString");
        public static int Guardar(Entity.CertificadoLesionTipoLesion certificadoLesionTipo)
        {
            return servicio.Guardar(certificadoLesionTipo);
        }
        public static Entity.CertificadoLesionTipoLesion ObtenerPorId(int Id)
        {
            return servicio.ObtenerPorId(Id);

        }
    }
}
