﻿using DataAccess.DescriptivoDetenidos;
using System.Collections.Generic;
namespace Business
{
    public class ControlDescriptivoDetenidos
    {
        private static ServicioDescriptivoDetenidos servicio = new ServicioDescriptivoDetenidos("SQLConnectionString");

        public static List<Entity.DescriptivoDetenidos> ObtenerDescriptivo(string[] filtros)
        {
            return servicio.GetDetail(filtros);
        }
    }
}