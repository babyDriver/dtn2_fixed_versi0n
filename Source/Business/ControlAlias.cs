﻿using DataAccess.Alias;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlAlias
    {
        private static ServicioAlias servicio = new ServicioAlias("SQLConnectionString");

        public static int Guardar(Entity.Alias alias)
        {
            return servicio.Guardar(alias);
        }

        public static void Actualizar(Entity.Alias alias)
        {
            servicio.Actualizar(alias);
        }

        public static Entity.Alias ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.Alias> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Alias ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.Alias ObtenerPorDetenidoId(int id)
        {
            return servicio.ObtenerByDetenidoId(id);
        }

        public static Entity.Alias ObtenerPorAlias(Entity.Alias alias)
        {
            return servicio.ObtenerByAlias(alias);
        }
    }
}
