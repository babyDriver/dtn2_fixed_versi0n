﻿using DataAccess.InformacionDeDetencion;
using System.Collections.Generic;
using System;


namespace Business
{
    public class ControlInformacionDeDetencion
    {
        private static ServicioInformacionDeDetencion servicio = new ServicioInformacionDeDetencion("SQLConnectionString");


        public static int Guardar(Entity.InformacionDeDetencion item)
        {
            return servicio.Guardar(item);
        }

        public static void Actualizar(Entity.InformacionDeDetencion item)
        {
            servicio.Actualizar(item);
        }

        public static Entity.InformacionDeDetencion ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static Entity.InformacionDeDetencion ObtenerPorInternoId(int id)
        {
            return servicio.ObtenerByInternoId(id);
        }

        public static Entity.InformacionDeDetencion ObtenerPorTrackingId(Guid guid)
        {
            return servicio.ObtenerByTrackingId(guid);
        }

        public static List<Entity.InformacionDeDetencion> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }
    }
}
