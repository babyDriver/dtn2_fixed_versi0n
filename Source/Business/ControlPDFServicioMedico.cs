﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Linq;

namespace Business
{
    public class ControlPDFServicioMedico
    {
        //Crear directorio 
        private static string CrearDirectorioControl(string nombre)
        {
            try
            {

                //Verificar si existe el directorio principal del repositorio de documentos
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio de la obra existe
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio para el grupo documental existe, sino crearlo
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre)));


                return string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs//" + nombre);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("No fue posible crear el repositorio para crear el archivo. ", ex.Message));
            }
        }


        //Crear celdas
        public static void Celda(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas para tablas
        public static void CeldaParaTablas(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, BaseColor colorrelleno, float espacio, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                celda.BackgroundColor = colorrelleno;
                celda.Padding = espacio;
                celda.UseAscender = true;
                tabla.AddCell(celda);
            }
        }

        public static void CeldaParaTablasServicioMedico(PdfPTable tabla, Font fuente, string valor, int bordo, int horizontal, int vertical)
        {
            PdfPCell celda = new PdfPCell(new Phrase(valor, fuente));
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = horizontal;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }

        //Crear celdas vacias
        public static void CeldaVacio(PdfPTable tabla, int cantidad)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                celda.BorderWidth = 0;
                tabla.AddCell(celda);
            }

        }

        private static string ObtenerMesTexto(string mes)
        {
            string mestexto = "";
            switch (mes)
            {
                case "01":
                    mestexto = "Enero";
                    break;
                case "02":
                    mestexto = "Febrero";
                    break;
                case "03":
                    mestexto = "Marzo";
                    break;
                case "04":
                    mestexto = "Abril";
                    break;
                case "05":
                    mestexto = "Mayo";
                    break;
                case "06":
                    mestexto = "Junio";
                    break;
                case "07":
                    mestexto = "Julio";
                    break;
                case "08":
                    mestexto = "Agosto";
                    break;
                case "09":
                    mestexto = "Septiembre";
                    break;
                case "10":
                    mestexto = "Octubre";
                    break;
                case "11":
                    mestexto = "Noviembre";
                    break;
                case "12":
                    mestexto = "Diciembre";
                    break;
                default:
                    mestexto = " ";
                    break;
            }
            return mestexto;
        }

        //para crear el Header y Footer
        public partial class HeaderFooter : PdfPageEventHelper
        {
            PdfContentByte cb;
            PdfTemplate headerTemplate;
            BaseFont bf = null;
            float tamanofuente = 8f;

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                }
                catch (DocumentException de) { }
                catch (System.IO.IOException ioe) { }
            }

            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                //Encabezado de Página
                Rectangle pageSize = doc.PageSize;
                PdfPTable footerTbl = new PdfPTable(1);
                BaseColor blau = new BaseColor(0, 61, 122);

                float[] TablaAncho = new float[] { 100 };
                footerTbl.SetWidths(TablaAncho);
                footerTbl.TotalWidth = 550;
                Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                PdfPCell cell;
                CeldaParaTablas(footerTbl, FuenteCelda, " ", 1, 0, 1, blau, 0, Element.ALIGN_TOP, Element.ALIGN_TOP);


                string pagina = "Página " + writer.PageNumber + " de ";
                //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
                {
                    float margenbottom = 11f;
                    float margenright = 280f;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, tamanofuente);
                    cb.SetTextMatrix(doc.PageSize.GetBottom(margenright), doc.PageSize.GetBottom(margenbottom));
                    cb.ShowText(pagina);
                    cb.EndText();
                    float len = bf.GetWidthPoint(pagina, tamanofuente);
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));
                }

                PdfPTable footerTbl2 = new PdfPTable(3);


                float[] TablaAncho2 = new float[] {25 ,100, 100 };
                footerTbl2.SetWidths(TablaAncho2);
                footerTbl2.TotalWidth = doc.Right-5;
                Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                string dia = String.Format("{0:dd}", DateTime.Now);
                string anio = String.Format("{0:yyyy}", DateTime.Now);
                string hora = String.Format("{0:HH:mm}", DateTime.Now);
                string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
                string fechahora = dia + " de " + mestexto + " de " + anio + " " + hora;
                Chunk chkFecha = new Chunk(fechahora, FuenteCelda);
                //Phrase phrfecha = new Phrase(chkFecha);
                Phrase phrfecha = new Phrase("");

                //Phrase promad = new Phrase("Promad Soluciones Computarizadas " + anio, FuenteCelda2);
                Phrase promad = new Phrase("", FuenteCelda2);
                cell = new PdfPCell(promad);
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 0;
                cell.BorderColorTop = blau;

                footerTbl2.AddCell(cell);

                cell = new PdfPCell(promad);
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                
                footerTbl2.AddCell(cell);


                cell = new PdfPCell(phrfecha);
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);
                footerTbl2.WriteSelectedRows(0, -1, 5, 25, writer.DirectContent);
            }

          


            //Necesario para incluir pagina x de y al header
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);
                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, tamanofuente);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

            }
        }
        

        // procedimiento asociado al dispositivo
        public static void CeldaConChunkespecial0(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, int bordo, int horizontal, int vertical)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk("Se realiza las pruebas semicuantitativa por el método de Alcohol Deshidrogenasa  ", FuenteValor);
            Chunk chunk3 = new Chunk("(ADH) ", FuenteTitulo);
            Chunk chunk4 = new Chunk("para el etanol. Se realizan las pruebas semicuantitativas por el método de Inmunoensayo Enzimático Homogéneo ", FuenteValor);
            Chunk chunk5 = new Chunk("(EIA) ", FuenteTitulo);
            Chunk chunk6 = new Chunk("de la muestra a analizar:  ", FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            elements.Add(chunk3);
            elements.Add(chunk4);
            elements.Add(chunk5);
            elements.Add(chunk6);
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }
        public static void CeldaConChunkespecial1(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, int bordo, int horizontal, int vertical)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk("Se realiza la prueba semicuantitativa por el método de inmunoensayo enzimático ", FuenteValor);
            Chunk chunk3 = new Chunk("(EIA) ", FuenteTitulo);
            Chunk chunk4 = new Chunk("de la muestra a analizar:", FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            elements.Add(chunk3);
            elements.Add(chunk4);
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }
        public static void CeldaConChunkespecial2(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, int bordo, int horizontal, int vertical)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk("Se realiza las pruebas semicuantitativa por el método de Alcohol Deshidrogenasa  ", FuenteValor);
            Chunk chunk3 = new Chunk("(ADH) ", FuenteTitulo);
            Chunk chunk4 = new Chunk("para el etanol. Se realizan las pruebas semicuantitativas por el método de Inmunoensayo Enzimático Multiplicado ", FuenteValor);
            Chunk chunk5 = new Chunk("(EMIT) ", FuenteTitulo);
            Chunk chunk6 = new Chunk("de la muestra a analizar:  ", FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            elements.Add(chunk3);
            elements.Add(chunk4);
            elements.Add(chunk5);
            elements.Add(chunk6);
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }

        public static void CeldaConChunk(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, int bordo, int horizontal, int vertical)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk(valor, FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = horizontal;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }

        public static void CeldaConChunkqui(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor,string nombre, int bordo, int horizontal, int vertical)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk(valor, FuenteValor);
            Chunk chunk3 = new Chunk(nombre, FuenteTitulo);
            Chunk chunkPunto = new Chunk(".", FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            elements.Add(chunk3);
            elements.Add(chunkPunto);
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }

        public static void CeldaConChunkpsico(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, string nombre,string edad, int bordo, int horizontal, int vertical)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk(valor, FuenteValor);
            Chunk chunk3 = new Chunk(nombre, FuenteTitulo);
            Chunk chunk4 = new Chunk(edad, FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            elements.Add(chunk3);
            elements.Add(chunk4);
            elements.Alignment = Element.ALIGN_JUSTIFIED;
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }


        public static object[] GeneraCertificadoLesion(Entity.ServicioMedico serviciomedico)
        {
            object obj = null;
            var Topmargen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var Bottommargen = iTextSharp.text.Utilities.MillimetersToPoints(7.5f); //5 milimetros para los margenes
            var Leftmargen = iTextSharp.text.Utilities.MillimetersToPoints(25); //5 milimetros para los margenes
            var Rightmargen = iTextSharp.text.Utilities.MillimetersToPoints(25); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, Leftmargen, Rightmargen, Topmargen, Bottommargen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "CertificadoLesiones" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.CertificadoLesiones"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteTitulo2 = new Font(fuentecalibri, 14f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();

                TituloCertificadoLesion(documentoPdf, FuenteTitulo, FuenteTitulo2, FuenteTituloTabla, serviciomedico, blau);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloCertificadoLesion(Document documentoPdf, Font FuenteTitulo, Font FuenteTitulo2, Font FuenteTituloTabla, Entity.ServicioMedico servicioMedico, BaseColor blau)
        {
            PdfPTable tablaTitulo = new PdfPTable(1);
            tablaTitulo.HorizontalAlignment = Element.ALIGN_CENTER;

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
            var contrato = ControlContrato.Obtenerporsubcontrato(subcontrato.Id);
            var instituto = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
            
            string escudo = "~/Content/img/Escudo.jpg";
            string logo = "~/Content/img/logo.png";
            var logoizq = subcontrato.Banner;
            if(logoizq != "undefined" && !string.IsNullOrEmpty(logoizq))
            {
                logo = logoizq;
            }
            string pathEscudo = System.Web.HttpContext.Current.Server.MapPath(escudo);
            string pathLogo = System.Web.HttpContext.Current.Server.MapPath(logo);
            iTextSharp.text.Image imageEscudo = null;
            iTextSharp.text.Image imageLogo = null;
            try
            {
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }
            catch (Exception)
            {
                pathEscudo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }

            try
            {
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }
            catch (Exception)
            {
                pathLogo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }

            //var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            //var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logoDER = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logoDER != "undefined" && logo != "")
            {
                logotipo = logoDER;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }




            imageLogo.ScalePercent(95f);
            //image.ScalePercent(12f);
            
                imageLogo.ScaleAbsoluteHeight(100f);
                imageLogo.ScaleAbsoluteWidth(300f);
            
            image.ScaleAbsoluteHeight(60f);
            image.ScaleAbsoluteWidth(70f);
            Paragraph para = new Paragraph();
            Phrase ph1 = new Phrase();
            Chunk glue = new Chunk(new iTextSharp.text.pdf.draw.VerticalPositionMark());
            Paragraph main = new Paragraph();
            ph1.Add(new Chunk(imageLogo, 0, 0, true));
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(image, 0, 15, true));
            main.Add(ph1);
            para.Add(main);
            documentoPdf.Add(para);

            //Titulo
            //Celda(tablaTitulo, FuenteTitulo, "SECRETARÍA DE SEGURIDAD PÚBLICA", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaTitulo, FuenteTitulo, instituto.Nombre.ToUpper(), 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaTitulo, FuenteTitulo2, "CERTIFICADO MÉDICO DE LESIONES", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaVacio(tablaTitulo, 1);

            PdfPTable tablaraya = new PdfPTable(1);
            tablaraya.WidthPercentage = 100;
            float tamanofuente = 8f;
            Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);
            Phrase promad = new Phrase("", FuenteCelda2);
            var cell = new PdfPCell(promad);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 2;
            cell.BorderColorTop = blau;

            tablaraya.AddCell(cell);
            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);
            var detalleDetencion = ControlDetalleDetencion.ObtenerPorId(servicioMedico.DetalledetencionId);

            //Titulo "Datos del detenido"

            //Tabla datos del detenido
            PdfPTable tabla2 = new PdfPTable(1);
            tabla2.WidthPercentage = 100;

            CeldaVacio(tabla2, 2);

            var cellFolio = new PdfPCell(new Phrase($"No. remisión: {detalleDetencion.Expediente}", FuenteNegrita));
            cellFolio.BorderWidth = 0;
            cellFolio.HorizontalAlignment = Element.ALIGN_RIGHT;
            
            tabla2.AddCell(cellFolio);
            

            CeldaVacio(tabla2, 1);

            int usuarioid = 0;
            var certificadoLesiones = ControlCertificadoLesiones.ObtenerPorId(servicioMedico.CertificadoLesionId);
            if (certificadoLesiones.Modificadopor != 0)
            {
                usuarioid = certificadoLesiones.Modificadopor;
            }
            else
            {
                usuarioid = certificadoLesiones.Registradopor;
            }
            var user = ControlUsuario.Obtener(usuarioid);
            if (user.RolId == 13)
            {
                user = ControlUsuario.Obtener(8);
            }


            var detenido = ControlDetenido.ObtenerPorId(detalleDetencion.DetenidoId);
            var general = ControlGeneral.ObtenerPorDetenidoId(detalleDetencion.DetenidoId);


            int edad = 0;

            if (general.FechaNacimineto != DateTime.MinValue)
            {
                edad = CalcularEdad(general.FechaNacimineto);
            }
            else
            {
                edad = general.Edaddetenido;
            }
            var sexo = ControlCatalogo.Obtener(general.SexoId, Convert.ToInt32(Entity.TipoDeCatalogo.sexo));
            string cedula = (!string.IsNullOrEmpty(user.CUIP)) ? user.CUIP : "";
            var fecha = certificadoLesiones.Modificadopor != 0 ? certificadoLesiones.Fechaultimamodificacion : certificadoLesiones.FechaRegistro;

            string Contenido = "El suscrito Médico Cirujano en turno de la "+instituto.Nombre+" Dr(a). " + user.Nombre + " " + user.ApellidoPaterno + " " + user.ApellidoMaterno + " con cédula profesional " + cedula + " certifica que siendo las " + certificadoLesiones.Fechavaloracion.ToString("HH:mm") + " horas del día " + GetFechaString(certificadoLesiones.Fechavaloracion) + " Examina Clínicamente a una persona del sexo " + sexo.Descripcion + " el cual dijo llamarse ";

            //carta.cuerpo = carta.cuerpo.Replace("[Ciudadano]", carta.Ciudadano).Replace("[Ciudad]", carta.Ciudad).Replace("[Domicilio]", carta.Domicilio).Replace("[Años]", carta.Anio.ToString()).Replace("[Antecedentes]", carta.Antecedentes == 0 ? "NO" : "SI").Replace("[Fecha Antecedente]", carta.FechaAntecedente.ToString().Substring(0, 10)).Replace("[día]", carta.Fecha.Day.ToString()).Replace("[mes]", carta.Fecha.ToString("MMMM")).Replace("[año]", carta.Fecha.Year.ToString());
        
            CeldaConChunkpsico(tabla2, FuenteNegrita, FuenteNormal, "", Contenido, detenido.Nombre + " " + detenido.Paterno + " " + detenido.Materno, " y tener una edad " + edad.ToString() + " años.", 0, 0, 0);
         

            CeldaVacio(tabla2,1);
            //string contenido2 = ""; //"Recolección de la muestra a las " + certificadoQuimico.Fecha_proceso.ToShortTimeString() + " horas del día " + certificadoQuimico.Fecha_proceso.ToShortDateString();
           
            //CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "", contenido2, 0, 0, 0);
             

            CeldaVacio(tabla2, 1);
            var cell1 = new PdfPCell(new Phrase("El Examinado Anteriormente Descrito a la Exploración física: ", FuenteNegrita));
            cell1.BorderWidth = 0;
            cell1.HorizontalAlignment = Element.ALIGN_CENTER;
            
            tabla2.AddCell(cell1);
           
            //var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            int idContrato = contratoUsuario.IdContrato;

            CeldaVacio(tabla2,3);
            var lesiones = ControlCertificadoLesionTipoLesionDetalle.ObteneroPortipoLesion(certificadoLesiones.Tipo_lesionId);

            string CadenaLesiones = "";
            foreach (var item in lesiones)
            {
                var tipoLesion = ControlCatalogo.Obtener(item.TipoLesionId, Convert.ToInt32(Entity.TipoDeCatalogo.tipo_lesion));
                CadenaLesiones += tipoLesion.Nombre + ",";
            }
            if (CadenaLesiones.Length > 0)
            {
                CadenaLesiones = CadenaLesiones.Substring(0, CadenaLesiones.Length - 1);
            }
             
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Lesiones: ", "", 0, 0, 0);
       

            int espaciosVacio = 0;
            if (certificadoLesiones.Sinlesion == 0)
            {
                var observaciones_lesion = certificadoLesiones.Observaciones_lesion;
                if (!string.IsNullOrEmpty(observaciones_lesion) && observaciones_lesion.Trim().Last() != '.')
                    observaciones_lesion = observaciones_lesion.Trim() + ".";

                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "", observaciones_lesion, 0, 0, 0);


                CeldaVacio(tabla2, 1);
                if (certificadoLesiones.Envio_hospital == 1)
                {

                    CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Se envía a hospital para valoración ", "", 0, 0, 0);

                }
                else
                {
                    espaciosVacio += 1;
                }
                if (certificadoLesiones.Lesion_simple_vista == 1)
                {

                    CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Lesiones a simple vista: ", certificadoLesiones.Lesion_simple_vista == 1 ? "Si" : "No", 0, 0, 0);

                }
                else
                {
                    espaciosVacio += 1;
                }

                if (certificadoLesiones.Fallecimiento == 1)
                {

                    CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Fallecimiento: ", certificadoLesiones.Fallecimiento == 1 ? "Si" : "No", 0, 0, 0);

                }
                else
                {
                    espaciosVacio += 1;
                }
                CeldaVacio(tabla2, 1);
            }
            else
            {

                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "No hay lesiones visibles a simple vista", "", 0, 0, 0);
                CeldaVacio(tabla2, 1);
            }
            
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "", "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ ", 0, 0, 0);
         

            var alergias = ControlCertificadoLesionAlergiaDetalle.ObteneroPortipoLesion(certificadoLesiones.Tipo_alergiaId);

            string cadenaalergias = "";

            foreach (var item in alergias)
            {
                var alergiaentity = ControlCatalogo.Obtener(item.AlergiaId, Convert.ToInt32(Entity.TipoDeCatalogo.tipo_alergia));
                cadenaalergias += alergiaentity.Nombre + ",";
            }

            if (cadenaalergias.Length > 0)
            {
                cadenaalergias = cadenaalergias.Substring(0, cadenaalergias.Length - 1);
            }
        
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Alergias: ", "", 0, 0, 0);


            var observacion_alergia = certificadoLesiones.Observacion_alergia;
            if (!string.IsNullOrEmpty(observacion_alergia) && observacion_alergia.Trim().Last() != '.')
                observacion_alergia = observacion_alergia.Trim() + ".";

            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "", observacion_alergia, 0, 0, 0);

            PdfPTable tablaEncabezadoObservacionAntecedentes = new PdfPTable(1);
            tablaEncabezadoObservacionAntecedentes.WidthPercentage = 100;

            CeldaConChunk(tablaEncabezadoObservacionAntecedentes, FuenteNegrita, FuenteNormal, "", "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ ", 0, 0, 0);

            var antecedentes = ControlCertificadoLesionAntecedentesDetalle.ObteneroPortipoAntecedenteIdn(certificadoLesiones.AntecedentesId);
            string cadenaantecedentes = "";

            foreach (var item in antecedentes)
            {
                var antecedenteentity = ControlCatalogo.Obtener(item.AntecedenteId, Convert.ToInt32(Entity.TipoDeCatalogo.Antecedentes));
                cadenaantecedentes += antecedenteentity.Nombre + ",";
            }
            if (cadenaantecedentes.Length>0) cadenaantecedentes = cadenaantecedentes.Substring(0, cadenaantecedentes.Length - 1);
             
            CeldaConChunk(tablaEncabezadoObservacionAntecedentes, FuenteNegrita, FuenteNormal, "Antecedentes: ", "", 0, 0, 0);

            var observaciones_antecedentes = certificadoLesiones.Observaciones_antecedentes;
            if (!string.IsNullOrEmpty(observaciones_antecedentes) && observaciones_antecedentes.Trim().Last() != '.')
                observaciones_antecedentes = observaciones_antecedentes.Trim() + ".";

            PdfPTable tablaObservacionAntecedentes = new PdfPTable(1);
            tablaObservacionAntecedentes.WidthPercentage = 100;
            CeldaParaTablasServicioMedico(tablaObservacionAntecedentes, FuenteNormal, observaciones_antecedentes, 0, 0, 0);
            CeldaVacio(tablaObservacionAntecedentes, 1);

            PdfPTable tablaEncabezadoObservacionGeneral = new PdfPTable(1);
            tablaEncabezadoObservacionGeneral.WidthPercentage = 100;

            CeldaConChunk(tablaEncabezadoObservacionGeneral, FuenteNegrita, FuenteNormal, "", "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ ", 0, 0, 0);
            CeldaConChunk(tablaEncabezadoObservacionGeneral, FuenteNegrita, FuenteNormal, "Observaciones generales: ", "", 0, 0, 0);

            var observacion_general = certificadoLesiones.Observacion_general;
            if (!string.IsNullOrEmpty(observacion_general) && observacion_general.Trim().Last() != '.')
                observacion_general = observacion_general.Trim() + ".";

            PdfPTable tablaObservacionGeneral = new PdfPTable(1);
            //we4pon
            tablaObservacionGeneral.WidthPercentage = 100;
            CeldaParaTablasServicioMedico(tablaObservacionGeneral, FuenteNormal, observacion_general, 0, 0, 0);

            PdfPTable tabla52 = new PdfPTable(1);

            //CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "Etanol         :", certificadoQuimico.EtanolId == 1 ? "Negativo" : "Positivo (" + certificadoQuimico.Grado + " mg/dl)", 0, Element.ALIGN_CENTER, 0);
            //CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "Benzodiazepinas:", certificadoQuimico.BenzodiapinaId == 1 ? "Negativo" : "Positivo", 0, Element.ALIGN_CENTER, 0);
            //CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "Anfetaminas    :", certificadoQuimico.AnfetaminaId == 1 ? "Negativo" : "Positivo", 0, Element.ALIGN_CENTER, 0);
            //CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "Cannabis       :", certificadoQuimico.CannabisId == 1 ? "Negativo" : "Positivo", 0, Element.ALIGN_CENTER, 0);
            //CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "Cocaina        :", certificadoQuimico.CocainaId == 1 ? "Negativo" : "Positivo", 0, Element.ALIGN_CENTER, 0);
            //CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "Extasis        :", certificadoQuimico.ExtasisId == 1 ? "Negativo" : "Positivo", 0, Element.ALIGN_CENTER, 0);
            cell1 = new PdfPCell();
            cell1.BorderWidth = 0;
            cell1.HorizontalAlignment = Element.ALIGN_CENTER;
            cell1.AddElement(tabla52);
            tabla2.AddCell(cell1);
            //CeldaVacio(tabla2, 3);
            //cell1 = new PdfPCell(new Phrase("Atentamente ", FuenteNegrita));
            //cell1.BorderWidth = 0;
            //cell1.HorizontalAlignment = Element.ALIGN_CENTER;
            //tabla2.AddCell(cell1);
            //  var parametros = ControlParametroContrato.TraerTodos();

            //if (espaciosVacio == 0)
            //{
            //    espaciosVacio += 1;
            //}
            //else if (espaciosVacio == 1)
            //{
            //    espaciosVacio += 2;
            //}
            //else if (espaciosVacio == 2)
            //{
            //    espaciosVacio += 2;
            //}
            //else if (espaciosVacio == 3)
            //{
            //    espaciosVacio += 3;
            //}
            //CeldaVacio(tabla2, (3+espaciosVacio));

            //cell1 = new PdfPCell(new Phrase("Elaboró: " + carta.Elaboro, FuenteNormal));
            //cell1.BorderWidth = 0;
            //cell1.HorizontalAlignment = Element.ALIGN_LEFT;
            //tabla2.AddCell(cell1);
            //CeldaVacio(tabla2, 1);
            //cell1 = new PdfPCell(new Phrase("Reviso: " + carta.Reviso, FuenteNormal));
            //cell1.BorderWidth = 0;
            //cell1.HorizontalAlignment = Element.ALIGN_LEFT;
            //tabla2.AddCell(cell1);
            //CeldaVacio(tabla2, 1);
            //cell1 = new PdfPCell(new Phrase("Recibo caja no.:" + carta.ReciboCaja, FuenteNormal));
            //cell1.BorderWidth = 0;
            //cell1.HorizontalAlignment = Element.ALIGN_LEFT;
            //tabla2.AddCell(cell1);
            //CeldaVacio(tabla2, 1);


            documentoPdf.Add(tablaTitulo);
            documentoPdf.Add(tablaraya);
            documentoPdf.Add(tabla2);
            documentoPdf.Add(tablaEncabezadoObservacionAntecedentes);
            documentoPdf.Add(tablaObservacionAntecedentes);
            documentoPdf.Add(tablaEncabezadoObservacionGeneral);
            documentoPdf.Add(tablaObservacionGeneral);

            var heighTablaFirmas = 42;
            var numEspaciosFirma = 0;
            float alturalogo = imageLogo.Height;
            
                alturalogo = 89;
            
            var alturaTotal = 
                alturalogo + 
                tablaTitulo.TotalHeight + 
                tablaraya.TotalHeight + 
                tabla2.TotalHeight + 
                tablaEncabezadoObservacionAntecedentes.TotalHeight + 
                tablaObservacionAntecedentes.TotalHeight + 
                tablaEncabezadoObservacionGeneral.TotalHeight + 
                (tablaObservacionGeneral.TotalHeight + 70) + 
                heighTablaFirmas;

            //alturaTotal = alturaTotal > 735 && alturaTotal <= 777 ? 777 : alturaTotal;
            alturaTotal = alturaTotal > 735 && alturaTotal <= 808 ? 808 : alturaTotal;
            var docTop = (int)documentoPdf.Top;
            var numPaginas = (int)Math.Floor(alturaTotal / docTop) + 1;

            // 
            while (alturaTotal + (numEspaciosFirma * 10) < (documentoPdf.Top - 7.5) * numPaginas)
            {
                numEspaciosFirma++;
            }

            //tablaFirma tiene height de 42 con la raya y el usuario a firmar
            PdfPTable tablaFirma = new PdfPTable(1);
            tablaFirma.WidthPercentage = 100f;
            CeldaVacio(tablaFirma, numEspaciosFirma > 5 ? numEspaciosFirma - 5 - numPaginas : numEspaciosFirma);

            var lesionCode = new PdfPCell()
            {
                Border = PdfPCell.NO_BORDER,
                Phrase = new Phrase($"F-PR-RCM-03/CM", FuenteNegrita)
            };
            //cellFolio.BorderWidth = 80;
            lesionCode.HorizontalAlignment = Element.ALIGN_LEFT;
            tablaFirma.AddCell(lesionCode);

            var celdaFirma = new PdfPCell(new Phrase("______________________________________", FuenteNegrita));
            celdaFirma.BorderWidth = 0;
            celdaFirma.HorizontalAlignment = Element.ALIGN_CENTER;
            tablaFirma.AddCell(celdaFirma);
            CeldaVacio(tablaFirma, 1);

            var celdaPersonaFirma = new PdfPCell(new Phrase($"Dr(a). {user.Nombre} {user.ApellidoPaterno} {user.ApellidoMaterno}", FuenteNegrita));
            celdaPersonaFirma.BorderWidth = 0;
            celdaPersonaFirma.HorizontalAlignment = Element.ALIGN_CENTER;
            tablaFirma.AddCell(celdaPersonaFirma);

            documentoPdf.Add(tablaFirma);


            tablaTitulo.DeleteBodyRows();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviciomedico"></param>
        /// <returns></returns>
        public static object[] GeneraCertificadoQuimico(Entity.ServicioMedico serviciomedico,Entity.ImprimeResultadoCertificadoQuimico resultadoCertificadoQuimico)
        {
            object obj = null;
            var Topmargen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var Bottommargen = iTextSharp.text.Utilities.MillimetersToPoints(7.5f); //5 milimetros para los margenes
            var Leftmargen = iTextSharp.text.Utilities.MillimetersToPoints(25); //5 milimetros para los margenes
            var Rightmargen = iTextSharp.text.Utilities.MillimetersToPoints(25); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, Leftmargen, Rightmargen, Topmargen, Bottommargen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "CertificadoQuimico" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.CertificadoQuimico"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteTitulo2 = new Font(fuentecalibri, 14f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();

                TituloCertificadoQuimico(documentoPdf, FuenteTitulo, FuenteTitulo2, FuenteTituloTabla, serviciomedico, blau, resultadoCertificadoQuimico);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloCertificadoQuimico(Document documentoPdf, Font FuenteTitulo, Font FuenteTitulo2, Font FuenteTituloTabla, Entity.ServicioMedico servicioMedico, BaseColor blau, Entity.ImprimeResultadoCertificadoQuimico resultadoCertificadoQuimico)
        {
            PdfPTable tablaTitulo = new PdfPTable(1);
            tablaTitulo.HorizontalAlignment = Element.ALIGN_CENTER;

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
            
            var contrato = ControlContrato.Obtenerporsubcontrato(subcontrato.Id);
            var instituto = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
            string escudo = "~/Content/img/Escudo.jpg";
            string logo = "~/Content/img/logo.png";
            var logoizq = subcontrato.Banner;
            if(logoizq != "undefined" && !string.IsNullOrEmpty(logoizq))
            {
                logo = logoizq;
            }
            string pathEscudo = System.Web.HttpContext.Current.Server.MapPath(escudo);
            string pathLogo = System.Web.HttpContext.Current.Server.MapPath(logo);
            iTextSharp.text.Image imageEscudo = null;
            iTextSharp.text.Image imageLogo = null;
            try
            {
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }
            catch (Exception)
            {
                pathEscudo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }

            try
            {
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }
            catch (Exception)
            {
                pathLogo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }

            var logoDER = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logoDER != "undefined" && logo != "")
            {
                logotipo = logoDER;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }

            imageLogo.ScalePercent(95f);
            //image.ScalePercent(12f);
            
                
                imageLogo.ScaleAbsoluteHeight(100f);
                imageLogo.ScaleAbsoluteWidth(300f);
                
            
            image.ScaleAbsoluteHeight(60f);
            image.ScaleAbsoluteWidth(70f);

            Paragraph para = new Paragraph();
            Phrase ph1 = new Phrase();
            Chunk glue = new Chunk(new iTextSharp.text.pdf.draw.VerticalPositionMark());
            Paragraph main = new Paragraph();
            ph1.Add(new Chunk(imageLogo, 0, 0, true));
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(image, 0, 15, true));
            main.Add(ph1);
            para.Add(main);

            documentoPdf.Add(para);

            //Titulo
            Celda(tablaTitulo, FuenteTitulo, instituto.Nombre.ToUpper(), 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaTitulo, FuenteTitulo2, "CERTIFICADO QUÍMICO", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaVacio(tablaTitulo, 1);
            PdfPTable tablaraya = new PdfPTable(1);
            tablaraya.WidthPercentage = 100;
            float tamanofuente = 8f;
            Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);
            Phrase promad = new Phrase("", FuenteCelda2);
            var cell = new PdfPCell(promad);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 2;
            cell.BorderColorTop = blau;

            tablaraya.AddCell(cell);

            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            //Titulo "Datos del detenido"

            //Tabla datos del detenido
            PdfPTable tabla2 = new PdfPTable(1);
            //float[] w = new float[]{ 2, 8, 1 };
            //tabla2.SetWidths( w);

            CeldaVacio(tabla2, 1);
            var detalleDetencion = ControlDetalleDetencion.ObtenerPorId(servicioMedico.DetalledetencionId);
            var cellFolio = new PdfPCell(new Phrase($"No. remisión: {detalleDetencion.Expediente}", FuenteNegrita));
            cellFolio.BorderWidth = 0;
            cellFolio.HorizontalAlignment = Element.ALIGN_RIGHT;
            //CeldaVacio(tabla2, 1);
            tabla2.AddCell(cellFolio);
            //CeldaVacio(tabla2, 1);

            CeldaVacio(tabla2, 1);
            var certificadoQuimico = ControlCertificadoQuimico.ObtenerPorId(servicioMedico.CertificadoQuimicoId);

            int idquimico = 0;
            if (certificadoQuimico.Modificadopor != 0)
            {
                idquimico = certificadoQuimico.Modificadopor;
            }
            else
            {
                idquimico = certificadoQuimico.Registradopor;
            }
            var user = ControlUsuario.Obtener(idquimico);
            if (user.RolId == 13) {
                user = ControlUsuario.Obtener(8);
            }

            //var detalleDetencion = ControlDetalleDetencion.ObtenerPorId(servicioMedico.DetalledetencionId);
            var detenido = ControlDetenido.ObtenerPorId(detalleDetencion.DetenidoId);

            string cedula = (!string.IsNullOrEmpty(user.CUIP)) ? user.CUIP : "";
            string Contenido = "El suscrito Q.F.B. " + user.Nombre + " " + user.ApellidoPaterno + " " + user.ApellidoMaterno + " Químico en turno de "+instituto.Nombre+", con cédula profesional " + cedula.ToString() + " certifica que siendo las " + certificadoQuimico.Fecha_proceso.ToString("HH:mm") + " horas del día " + GetFechaString(certificadoQuimico.Fecha_proceso) + " se efectuó examen toxicológico en la orina de quien dijo llamarse ";

            //carta.cuerpo = carta.cuerpo.Replace("[Ciudadano]", carta.Ciudadano).Replace("[Ciudad]", carta.Ciudad).Replace("[Domicilio]", carta.Domicilio).Replace("[Años]", carta.Anio.ToString()).Replace("[Antecedentes]", carta.Antecedentes == 0 ? "NO" : "SI").Replace("[Fecha Antecedente]", carta.FechaAntecedente.ToString().Substring(0, 10)).Replace("[día]", carta.Fecha.Day.ToString()).Replace("[mes]", carta.Fecha.ToString("MMMM")).Replace("[año]", carta.Fecha.Year.ToString());
            //CeldaVacio(tabla2, 1);
            CeldaConChunkqui(tabla2, FuenteNegrita, FuenteNormal, "", Contenido, detenido.Nombre + " " + detenido.Paterno + " " + detenido.Materno, 0, 0, 0);
            //CeldaVacio(tabla2, 1);

            CeldaVacio(tabla2, 3);

            //string contenido2 = "Recolección de la muestra a las " + certificadoQuimico.Fecha_proceso.ToShortTimeString() + " horas del día " + certificadoQuimico.Fecha_proceso.ToShortDateString();
            string contenido2 = "Recolección de la muestra a las " + certificadoQuimico.Fecha_toma.ToString("HH:mm") + " horas del día " + GetFechaString(certificadoQuimico.Fecha_toma) + ".";
            //CeldaVacio(tabla2, 1);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "", contenido2, 0, 0, 0);
            //CeldaVacio(tabla2, 1);

            CeldaVacio(tabla2, 3);
            var cell1 = new PdfPCell(new Phrase("Procedimiento ", FuenteNegrita));
            cell1.BorderWidth = 0;
            cell1.HorizontalAlignment = Element.ALIGN_CENTER;
            tabla2.AddCell(cell1);
            CeldaVacio(tabla2, 1);
            //var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            int idContrato = contratoUsuario.IdContrato;


            //Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

            //string dia = String.Format("{0:dd}", DateTime.Now); 
            // string anio = String.Format("{0:yyyy}", DateTime.Now);
            // string hora = String.Format("{0:HH:mm}", DateTime.Now);
            //// string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
            // string fechahora = dia + " de " + mestexto + " de " + anio + " a las " + hora + " hrs.";
            //   Chunk chkFecha = new Chunk("Impreso el " + fechahora, FuenteCelda);



            var opcionComun = ControlCatalogo.ObtenerTodo(Convert.ToInt32(Entity.TipoDeCatalogo.opcion_comun));


            //if (certificadoQuimico.EtanolId == 2|| certificadoQuimico.EtanolId == 1)
            if (certificadoQuimico.EquipoautilizarId == 1)
            {

                //CeldaVacio(tabla2, 1);
                CeldaConChunkespecial0(tabla2, FuenteNegrita, FuenteNormal, "", "", 0, 0, 0);
                 
            }
            else if (certificadoQuimico.EquipoautilizarId == 2)
            {

                CeldaConChunkespecial1(tabla2, FuenteNegrita, FuenteNormal, "", "", 0, 0, 0);

            }
            else
            {

                CeldaConChunkespecial2(tabla2, FuenteNegrita, FuenteNormal, "", "", 0, 0, 0);

            }
            CeldaVacio(tabla2, 3);
            cell1 = new PdfPCell(new Phrase("Resultado ", FuenteNegrita));
            cell1.BorderWidth = 0;
            cell1.HorizontalAlignment = Element.ALIGN_CENTER;
            //CeldaVacio(tabla2, 1);
            tabla2.AddCell(cell1);
            //CeldaVacio(tabla2, 1);
            CeldaVacio(tabla2, 1);

            int[] w2 = new int[] { 3, 3, 4, 2 };
            PdfPTable tabla52 = new PdfPTable(4);
            tabla52.SetWidths(w2);
            tabla52.TotalWidth = 550;
            tabla52.HorizontalAlignment = Element.ALIGN_CENTER;

            var etanolcomun = ControlCatalogo.Obtener(certificadoQuimico.EtanolId, Convert.ToInt32(Entity.TipoDeCatalogo.opcion_comun));

            if (resultadoCertificadoQuimico.PrintEtanol)
            {
                CeldaVacio(tabla52, 1);
                CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "Etanol:", "", 0, Element.ALIGN_LEFT, 0);
                if (certificadoQuimico.EtanolId <= 2)
                {
                    CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "", etanolcomun.Nombre.ToUpper() == "NEGATIVO" ? "Negativo" : "Positivo (" + certificadoQuimico.Grado + " mg/dL)", 0, Element.ALIGN_LEFT, 0);
                }
                else
                {
                    string optetanol = opcionComun.FirstOrDefault(x => x.Id == certificadoQuimico.EtanolId).Nombre;
                    CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "", optetanol, 0, Element.ALIGN_LEFT, 0);
                }
                CeldaVacio(tabla52, 1);
                //CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "Etanol         :", certificadoQuimico.EtanolId == 1 ? "Negativo" : "Positivo (" + certificadoQuimico.Grado + " mg/dl)", 0, Element.ALIGN_CENTER, 0);
            }

            if (resultadoCertificadoQuimico.PrintBenzodiazepina)
            {
                CeldaVacio(tabla52, 1);
                CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "Benzodiazepinas:", "", 0, Element.ALIGN_LEFT, 0);
                if (certificadoQuimico.BenzodiapinaId <= 2)
                {
                    CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "", certificadoQuimico.BenzodiapinaId == 1 ? "Negativo" : "Positivo", 0, Element.ALIGN_LEFT, 0);
                }
                else
                {
                    string optbenzo = opcionComun.FirstOrDefault(x => x.Id == certificadoQuimico.BenzodiapinaId).Nombre;
                    CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "", optbenzo, 0, Element.ALIGN_LEFT, 0);
                }
                CeldaVacio(tabla52, 1);
            }

            if (resultadoCertificadoQuimico.PrintAnfetamina)
            {

                CeldaVacio(tabla52, 1);
                CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "Anfetaminas:", "", 0, Element.ALIGN_LEFT, 0);
                if (certificadoQuimico.AnfetaminaId <= 2)
                {
                    CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "", certificadoQuimico.AnfetaminaId == 1 ? "Negativo" : "Positivo", 0, Element.ALIGN_LEFT, 0);
                }
                else
                {
                    string optanfeta = opcionComun.FirstOrDefault(x => x.Id == certificadoQuimico.AnfetaminaId).Nombre;
                    CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "", optanfeta, 0, Element.ALIGN_LEFT, 0);
                }
                //CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "Anfetaminas    :", certificadoQuimico.AnfetaminaId == 1 ? "Negativo" : "Positivo", 0, Element.ALIGN_LEFT, 0);
                CeldaVacio(tabla52, 1);
            }

            if (resultadoCertificadoQuimico.PrintCannabis)
            {
                CeldaVacio(tabla52, 1);
                CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "Cannabis:", "", 0, Element.ALIGN_LEFT, 0);
                if (certificadoQuimico.CannabisId <= 2)
                {
                    CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "", certificadoQuimico.CannabisId == 1 ? "Negativo" : "Positivo", 0, Element.ALIGN_LEFT, 0);
                }
                else
                {
                    string optcanabis = opcionComun.FirstOrDefault(x => x.Id == certificadoQuimico.CannabisId).Nombre;
                    CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "", optcanabis, 0, Element.ALIGN_LEFT, 0);
                }
                //CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "Cannabis       :", certificadoQuimico.CannabisId == 1 ? "Negativo" : "Positivo", 0, Element.ALIGN_LEFT, 0);
                CeldaVacio(tabla52, 1);
            }

            if (resultadoCertificadoQuimico.PrintCocaina)
            {
                CeldaVacio(tabla52, 1);
                CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "Cocaína:", "", 0, Element.ALIGN_LEFT, 0);

                if (certificadoQuimico.CocainaId <= 2)
                {
                    CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "", certificadoQuimico.CocainaId == 1 ? "Negativo" : "Positivo", 0, Element.ALIGN_LEFT, 0);
                }
                else
                {
                    string cocaina = opcionComun.FirstOrDefault(x => x.Id == certificadoQuimico.CocainaId).Nombre;
                    CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "", cocaina, 0, Element.ALIGN_LEFT, 0);
                }
                //CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "Cocaína        :", certificadoQuimico.CocainaId == 1 ? "Negativo" : "Positivo", 0, Element.ALIGN_LEFT, 0);
                CeldaVacio(tabla52, 1);
            }

            if (resultadoCertificadoQuimico.PrintExtasis)
            {
                CeldaVacio(tabla52, 1);
                CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "Éxtasis:", "", 0, Element.ALIGN_LEFT, 0);

                if (certificadoQuimico.ExtasisId <= 2)
                {
                    CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "", certificadoQuimico.ExtasisId == 1 ? "Negativo" : "Positivo", 0, Element.ALIGN_LEFT, 0);
                }
                else
                {
                    string extasis = opcionComun.FirstOrDefault(x => x.Id == certificadoQuimico.ExtasisId).Nombre;
                    CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "", extasis, 0, Element.ALIGN_LEFT, 0);
                }

                //CeldaConChunk(tabla52, FuenteNegrita, FuenteNormal, "Éxtasis        :", certificadoQuimico.ExtasisId == 1 ? "Negativo" : "Positivo", 0, Element.ALIGN_CENTER, 0);
                CeldaVacio(tabla52, 1);
            }

            cell1.AddElement(tabla52);
            //CeldaVacio(tabla2, 1);
            tabla2.AddCell(cell1);
            //CeldaVacio(tabla2, 1);
            
            //cell1 = new PdfPCell(new Phrase("Atentamente ", FuenteNegrita));
            //cell1.BorderWidth = 0;
            //cell1.HorizontalAlignment = Element.ALIGN_CENTER;
            //tabla2.AddCell(cell1);
            //var parametros = ControlParametroContrato.TraerTodos();
            //CeldaVacio(tabla2, 3);

            //cell1 = new PdfPCell(new Phrase("Elaboró: " + carta.Elaboro, FuenteNormal));
            //cell1.BorderWidth = 0;
            //cell1.HorizontalAlignment = Element.ALIGN_LEFT;
            //tabla2.AddCell(cell1);
            //CeldaVacio(tabla2, 1);
            //cell1 = new PdfPCell(new Phrase("Reviso: " + carta.Reviso, FuenteNormal));
            //cell1.BorderWidth = 0;
            //cell1.HorizontalAlignment = Element.ALIGN_LEFT;
            //tabla2.AddCell(cell1);
            //CeldaVacio(tabla2, 1);
            //cell1 = new PdfPCell(new Phrase("Recibo caja no.:" + carta.ReciboCaja, FuenteNormal));
            //cell1.BorderWidth = 0;
            //cell1.HorizontalAlignment = Element.ALIGN_LEFT;
            //tabla2.AddCell(cell1);
            //CeldaVacio(tabla2, 1);

            tabla2.WidthPercentage = 100;
            documentoPdf.Add(tablaTitulo);
            documentoPdf.Add(tablaraya);
            documentoPdf.Add(tabla2);

            var heighTablaFirmas = 42;
            var numEspaciosFirma = 0;
            float alturalogo = imageLogo.Height; ;
            
           
                alturalogo = 89;
            
            var alturaTotal =
                alturalogo +
                tablaTitulo.TotalHeight +
                tablaraya.TotalHeight +
                tabla2.TotalHeight +
                heighTablaFirmas;

            //alturaTotal = alturaTotal > 735 && alturaTotal <= 777 ? 777 : alturaTotal;
            alturaTotal = alturaTotal > 735 && alturaTotal <= 808 ? 808 : alturaTotal;
            var docTop = (int)documentoPdf.Top;
            var numPaginas = (int)Math.Floor(alturaTotal / docTop) + 1;

            while (alturaTotal + (numEspaciosFirma * 10) < (documentoPdf.Top - 7.5) * numPaginas)
            {
                numEspaciosFirma++;
            }

            //tablaFirma tiene height de 42 con la raya y el usuario a firmar
            PdfPTable tablaFirma = new PdfPTable(1);
            tablaFirma.WidthPercentage = 100f;
            var s = numEspaciosFirma > 5 ? numEspaciosFirma - 5 - numPaginas : numEspaciosFirma;
            CeldaVacio(tablaFirma, s);

            // we4pon

            var chemistryCode = new PdfPCell()
            {
                Border = PdfPCell.NO_BORDER,
                Phrase = new Phrase($"F-PR-RCM-04/CM", FuenteNegrita)
            };
            //cellFolio.BorderWidth = 80;
            chemistryCode.HorizontalAlignment = Element.ALIGN_LEFT;
            tablaFirma.AddCell(chemistryCode);
                        
            var celdaFirma = new PdfPCell(new Phrase("______________________________________", FuenteNegrita));
            celdaFirma.BorderWidth = 0;
            celdaFirma.HorizontalAlignment = Element.ALIGN_CENTER;
            tablaFirma.AddCell(celdaFirma);
            CeldaVacio(tablaFirma, 1);

            var celdaPersonaFirma = new PdfPCell(new Phrase($"Q.F.B. {user.Nombre} {user.ApellidoPaterno} {user.ApellidoMaterno}", FuenteNegrita));
            var celdaAditionalFirma = new PdfPCell()
            {
                Border = PdfPCell.NO_BORDER,
                Phrase = new Phrase($"", FuenteNegrita)
            };
            celdaPersonaFirma.BorderWidth = 0;
            celdaPersonaFirma.HorizontalAlignment = Element.ALIGN_CENTER;
            tablaFirma.AddCell(celdaPersonaFirma);
            tablaFirma.AddCell(celdaAditionalFirma);

            documentoPdf.Add(tablaFirma);

            tablaTitulo.DeleteBodyRows();
        }


        public static object[] GeneraCertificadoPsicofisiologico(Entity.ServicioMedico serviciomedico)
        {
            object obj = null;
            var Topmargen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var Bottommargen = iTextSharp.text.Utilities.MillimetersToPoints(7.5f); //5 milimetros para los margenes
            var Leftmargen = iTextSharp.text.Utilities.MillimetersToPoints(25); //5 milimetros para los margenes
            var Rightmargen = iTextSharp.text.Utilities.MillimetersToPoints(25); //5 milimetros para los margenes
            Document documentoPdf = new Document(PageSize.LETTER, Leftmargen, Rightmargen, Topmargen, Bottommargen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "CertificadoPsicofisiológico" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.CertificadoPsicofisiológico"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 16f, Font.BOLD, blau);
                Font FuenteTitulo2 = new Font(fuentecalibri, 12f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();

                TituloCertificadoPsicofisiologico(documentoPdf, FuenteTitulo, FuenteTitulo2, FuenteTituloTabla, serviciomedico, blau);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }
        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }

            return edad;
        }
        private static void TituloCertificadoPsicofisiologico(Document documentoPdf, Font FuenteTitulo, Font FuenteTitulo2, Font FuenteTituloTabla, Entity.ServicioMedico servicioMedico, BaseColor blau)
        {
            PdfPTable tablaTitulo = new PdfPTable(1);
            tablaTitulo.HorizontalAlignment = Element.ALIGN_CENTER;

            string escudo = "~/Content/img/Escudo.jpg";
            string logo = "~/Content/img/logo.png";

            string pathEscudo = System.Web.HttpContext.Current.Server.MapPath(escudo);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logoDER = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logoDER != "undefined" && logo != "")
            {
                logotipo = logoDER;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }
            var logoizq = subcontrato.Banner;
            if (logoizq != "undefined" && !string.IsNullOrEmpty(logoizq))
            {
                logo = logoizq;
            }

            string pathLogo = System.Web.HttpContext.Current.Server.MapPath(logo);
            iTextSharp.text.Image imageEscudo = null;
            iTextSharp.text.Image imageLogo = null;
            try
            {
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }
            catch (Exception)
            {
                pathEscudo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }

            try
            {
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }
            catch (Exception)
            {
                pathLogo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }

            imageLogo.ScalePercent(85f);
            //image.ScalePercent(12f);

            imageLogo.ScaleAbsoluteHeight(100f);
            imageLogo.ScaleAbsoluteWidth(300f);


            image.ScaleAbsoluteHeight(60f);
            image.ScaleAbsoluteWidth(70f);
            Paragraph para = new Paragraph();
            Phrase ph1 = new Phrase();
            Chunk glue = new Chunk(new iTextSharp.text.pdf.draw.VerticalPositionMark());
            Paragraph main = new Paragraph();
            ph1.Add(new Chunk(imageLogo, 0, 0, true));
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(image, 0, 15, true));
            main.Add(ph1);
            para.Add(main);
            documentoPdf.Add(para);
            //var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            //var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
            var contrato = ControlContrato.Obtenerporsubcontrato(subcontrato.Id);
            var instituto = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);

            //Titulo
            Celda(tablaTitulo, FuenteTitulo, instituto.Nombre.ToUpper(), 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaTitulo, FuenteTitulo2, "CERTIFICADO MÉDICO PSICOFISIOLÓGICO", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaVacio(tablaTitulo, 1);

            PdfPTable tablaraya = new PdfPTable(1);
            tablaraya.WidthPercentage = 100;
            float tamanofuente = 8f;
            Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);
            Phrase promad = new Phrase("", FuenteCelda2);
            var cell = new PdfPCell(promad);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 2;
            cell.BorderColorTop = blau;

            tablaraya.AddCell(cell);
            //Nombre delegacion y usuario en barandilla
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 11f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);

            //Titulo "Datos del detenido"
            //Tabla datos del detenido
            PdfPTable tablaContenido = new PdfPTable(1);
            tablaContenido.WidthPercentage = 100;

            //CeldaVacio(tablaContenido, 2);
            var certificado = ControlCertificadoPsicoFisiologico.ObtenerPorId(servicioMedico.CertificadoMedicoPsicofisiologicoId);

            int usuarioid = 0;

            if (certificado.Modificadopor != 0)
            {
                usuarioid = certificado.Modificadopor;
            }
            else
            {
                usuarioid = certificado.Registradopor;
            }
            var user = ControlUsuario.Obtener(usuarioid);
            if (user.RolId == 13)
            {
                user = ControlUsuario.Obtener(8);
            }

            var detalleDetencion = ControlDetalleDetencion.ObtenerPorId(servicioMedico.DetalledetencionId);
            var detenido = ControlDetenido.ObtenerPorId(detalleDetencion.DetenidoId);
            var general = ControlGeneral.ObtenerPorDetenidoId(detalleDetencion.DetenidoId);
            var sexo = ControlCatalogo.Obtener(general.SexoId, Convert.ToInt32(Entity.TipoDeCatalogo.sexo));

            var cellCode = new PdfPCell(new Phrase($"No. remisión: {detalleDetencion.Expediente}", FuenteNegrita));
            cellCode.BorderWidth = 0;
            cellCode.HorizontalAlignment = Element.ALIGN_RIGHT;

            tablaContenido.AddCell(cellCode);

            int edad = 0;

            if (general.FechaNacimineto != DateTime.MinValue)
            {
                edad = CalcularEdad(general.FechaNacimineto);

            }
            else
            {
                edad = general.Edaddetenido;

            }

            string cedula = (!string.IsNullOrEmpty(user.CUIP)) ? user.CUIP : "";
            string Contenido = $"El   suscrito   Médico   Cirujano   en   turno   de   {instituto.Nombre} Dr(a). { user.Nombre} {user.ApellidoPaterno} {user.ApellidoMaterno} con  cédula  profesional {cedula} certifica  que siendo las {certificado.FechaValoracion.ToString("HH:mm")} horas del día {GetFechaString(certificado.FechaValoracion)} se le realiza el examen  psicofisiológico a una persona del sexo {sexo.Descripcion} el cual dijo llamarse ";
            CeldaConChunkpsico(tablaContenido, FuenteNegrita, FuenteNormal, "", Contenido, detenido.Nombre + " " + detenido.Paterno + " " + detenido.Materno, " y tener una edad " + edad.ToString() + " años.", 0, 0, 0);

            CeldaVacio(tablaContenido, 1);
            var cellSection1 = new PdfPCell(new Phrase("EL EXAMINADO ANTERIORMENTE DESCRITO PRESENTA:", FuenteNegrita));
            cellSection1.BorderWidth = 0;
            cellSection1.HorizontalAlignment = Element.ALIGN_LEFT;

            tablaContenido.AddCell(cellSection1);


            //Obtener orientaciones
            var orientacionesId = ControlCertificadoPsicoFisologicoOrientacionDetalle.Obetener(certificado.OrientacionId).Select(x => x.OrientacionId);
            var orientaciones = "";
            foreach (var id in orientacionesId)
            {
                orientaciones += ControlCatalogo.Obtener(id, 113).Nombre + ", ";
            }
            if (orientacionesId.Count() > 0) orientaciones = orientaciones.Substring(0, orientaciones.Length - 2);
            //

            //Obtener conclusiones
            var conclusionesId = ControlCertificadoPsicoFisologicoConclusionDetalle.Obtener(certificado.ConclusionId).Select(x => x.ConclusionId);
            var conclusiones = "";
            foreach (var id in conclusionesId)
            {
                conclusiones += ControlCatalogo.Obtener(id, 135).Nombre + ", ";
            }

            if (conclusionesId.Count() > 0) conclusiones = conclusiones.Substring(0, conclusiones.Length - 2);
            //
            PdfPTable tabla1 = new PdfPTable(2);
            tabla1.WidthPercentage = 100;
            CeldaConChunk(tabla1, FuenteNegrita, FuenteNormal, "                ALIENTO: ", ControlCatalogo.Obtener(certificado.AlientoId, 103).Nombre, 0, Element.ALIGN_LEFT, 0);



            CeldaConChunk(tabla1, FuenteNegrita, FuenteNormal, "                ORIENTADO EN: ", orientaciones, 0, Element.ALIGN_LEFT, 0);



            CeldaConChunk(tabla1, FuenteNegrita, FuenteNormal, "                MARCHA: ", ControlCatalogo.Obtener(certificado.MarchaId, 106).Nombre, 0, Element.ALIGN_LEFT, 0);



            CeldaConChunk(tabla1, FuenteNegrita, FuenteNormal, "                ACTITUD: ", ControlCatalogo.Obtener(certificado.ActitudId, 126).Nombre, 0, Element.ALIGN_LEFT, 0);



            CeldaConChunk(tabla1, FuenteNegrita, FuenteNormal, "                ATENCIÓN: ", ControlCatalogo.Obtener(certificado.AtencionId, 112).Nombre, 0, Element.ALIGN_LEFT, 0);



            CeldaConChunk(tabla1, FuenteNegrita, FuenteNormal, "                CAVIDAD ORAL: ", ControlCatalogo.Obtener(certificado.Cavidad_oralId, 127).Nombre, 0, Element.ALIGN_LEFT, 0);



            CeldaConChunk(tabla1, FuenteNegrita, FuenteNormal, "                PUPILAS: ", ControlCatalogo.Obtener(certificado.PupilaId, 117).Nombre, 0, Element.ALIGN_LEFT, 0);




            CeldaConChunk(tabla1, FuenteNegrita, FuenteNormal, "                DISCURSO: ", ControlCatalogo.Obtener(certificado.Reflejo_pupilarId, 107).Nombre, 0, Element.ALIGN_LEFT, 0);



            CeldaConChunk(tabla1, FuenteNegrita, FuenteNormal, "                CONJUNTIVAS: ", ControlCatalogo.Obtener(certificado.ConjuntivaId, 105).Nombre, 0, Element.ALIGN_LEFT, 0);



            CeldaConChunk(tabla1, FuenteNegrita, FuenteNormal, "                LENGUAJE: ", ControlCatalogo.Obtener(certificado.LenguajeId, 111).Nombre, 0, Element.ALIGN_LEFT, 0);


            CeldaVacio(tabla1, 2);
            PdfPTable tablaContenido2 = new PdfPTable(2);
            tablaContenido2.WidthPercentage = 100;
            var cellSection2 = new PdfPCell(new Phrase("COORDINACIÓN MOTRIZ PRUEBA DEDO-DEDO:", FuenteNegrita));
            cellSection2.BorderWidth = 0;
            cellSection2.HorizontalAlignment = Element.ALIGN_LEFT;

            tablaContenido2.AddCell(cellSection2);
            var cellSection3 = new PdfPCell(new Phrase("COORDINACIÓN MOTRIZ PRUEBA DEDO-NARIZ:", FuenteNegrita));
            cellSection3.BorderWidth = 0;
            cellSection3.HorizontalAlignment = Element.ALIGN_LEFT;

            tablaContenido2.AddCell(cellSection3);
            PdfPTable tabla2 = new PdfPTable(2);
            tabla2.WidthPercentage = 100;
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "                OJOS ABIERTOS: ", ControlCatalogo.Obtener(certificado.Ojos_abiertos_dedo_dedoId, 128).Nombre, 0, Element.ALIGN_LEFT, 0);



            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "                OJOS CERRADOS: ", ControlCatalogo.Obtener(certificado.Ojos_cerrados_dedo_dedoId, 130).Nombre, 0, Element.ALIGN_LEFT, 0);


            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "                OJOS ABIERTOS: ", ControlCatalogo.Obtener(certificado.Ojos_abiertos_dedo_narizId, 129).Nombre, 0, Element.ALIGN_LEFT, 0);

            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "                OJOS CERRADOS: ", ControlCatalogo.Obtener(certificado.Ojos_cerrados_dedo_narizId, 131).Nombre, 0, Element.ALIGN_LEFT, 0);

            CeldaVacio(tabla2, 1);
            CeldaVacio(tabla2, 1);

            PdfPTable tablaContenido3 = new PdfPTable(1);
            tablaContenido3.WidthPercentage = 100;

            var cellSection4 = new PdfPCell(new Phrase($"PRUEBA DE ROMBERG: {ControlCatalogo.Obtener(certificado.RombergId, 110).Nombre}", FuenteNegrita));
            cellSection4.BorderWidth = 0;
            cellSection4.HorizontalAlignment = Element.ALIGN_LEFT;

            tablaContenido3.AddCell(cellSection4);



            CeldaConChunk(tablaContenido3, FuenteNegrita, FuenteNormal, "                SUS SIGNOS VITALES SON: ", certificado.No_valorabe == 1 ? "NO VALORABLE" : $"T.A: {certificado.TA}    PULSO: {certificado.Pulso}   F.R: {certificado.FR}     F.C: {certificado.FC}", 0, Element.ALIGN_LEFT, 0);

            PdfPTable tablaContenido4 = new PdfPTable(1);
            tablaContenido4.WidthPercentage = 100;

            CeldaConChunk(tablaContenido4, FuenteNegrita, FuenteNormal, "                CONCLUSIÓN: ", conclusiones, 0, Element.ALIGN_LEFT, 0);


            var observacion = certificado.Observacion_extra;
            if (!string.IsNullOrEmpty(observacion) && observacion.Trim().Last() != '.')
                observacion = observacion.Trim() + ".";
                observacion = observacion.Replace("\r\n", " ");

            PdfPTable tablaObservaciones = new PdfPTable(1);
            //we4pon
            //tablaObservaciones.WidthPercentage = 100;
            tablaObservaciones.WidthPercentage = 100;

            int catracteres = 60;
            //int catracteres = 80;
            if (certificado.Sustento_toxicologico == 1)
            {
                catracteres += 132;
            }

            CeldaParaTablasServicioMedico(tablaObservaciones, FuenteNegrita, "OBSERVACIONES: ", 0, Element.ALIGN_LEFT, 0);
            CeldaParaTablasServicioMedico(tablaObservaciones, FuenteNormal, observacion, 0, Element.ALIGN_LEFT, 0);
            int caracsustento = 0;
            if (certificado.Sustento_toxicologico == 1)
            {
                CeldaParaTablasServicioMedico(tablaObservaciones, FuenteNormal, "LA CONCLUSIÓN QUE SE EXHIBE EN ESTE DOCUMENTO SE REALIZÓ CON SUSTENTO EN EL EXAMEN TOXICOLÓGICO EL CUAL SE ANEXA A ESTE CERTIFICADO.", 0, Element.ALIGN_LEFT, 0);
                caracsustento = 132;
            }


            documentoPdf.Add(tablaTitulo);
            documentoPdf.Add(tablaraya);
            documentoPdf.Add(tablaContenido);
            documentoPdf.Add(tabla1);
            documentoPdf.Add(tablaContenido2);
            documentoPdf.Add(tabla2);
            documentoPdf.Add(tablaContenido3);
            documentoPdf.Add(tablaContenido4);
            bool isnewpage = false;
            if (observacion.Length + caracsustento > 1200)
            {
                documentoPdf.NewPage();
                isnewpage = true;
            }
            if (certificado.Sustento_toxicologico == 0 && observacion.Length + caracsustento > 1200 && observacion.Length + caracsustento <= 1300)
            {
                CeldaVacio(tablaObservaciones, 5);
            }
            documentoPdf.Add(tablaObservaciones);
            float alturalogo = imageLogo.Height; ;


            alturalogo = 89;

            var heighTablaFirmas = 42;
            var numEspaciosFirma = 0;
            var alturaTotal =
               alturalogo +
                tablaTitulo.TotalHeight +
                tablaraya.TotalHeight +
                tablaContenido.TotalHeight +
                tablaObservaciones.TotalHeight +
                heighTablaFirmas;

            //alturaTotal = alturaTotal > 735 && alturaTotal <= 777 ? 777 : alturaTotal;
            alturaTotal = alturaTotal > 735 && alturaTotal <= 808 ? 808 : alturaTotal;
            var docTop = (int)documentoPdf.Top;
            var numPaginas = (int)Math.Floor(alturaTotal / docTop) + 1;


            while (alturaTotal + (numEspaciosFirma * 10) < (documentoPdf.Top - 7.5) * numPaginas)
            {
                numEspaciosFirma++;
            }


            //tablaFirma tiene height de 42 con la raya y el usuario a firmar
            PdfPTable tablaFirma = new PdfPTable(1);
            tablaFirma.WidthPercentage = 100f;
            if (string.IsNullOrWhiteSpace(observacion) && certificado.Sustento_toxicologico == 1)
            {

                var f = numEspaciosFirma > 4 ? numEspaciosFirma - ((numPaginas == 1) ? 22 : 1) : numEspaciosFirma;
                var tablaaltura3 = tabla1.TotalHeight;
                if (tablaaltura3 >= 100) f += -5;
                else if (tablaaltura3 >= 90) f += -2;
                CeldaVacio(tablaFirma, f);
            }
            else
            {
                int restar = 0;
                var tablaAltura = tablaObservaciones.TotalHeight;
                if (tablaAltura >= 180) restar = 11;

                else if (tablaAltura >= 160) restar = 10;
                else if (tablaAltura >= 150) restar = 12;
                else if (tablaAltura >= 140) restar = 15;
                else if (tablaAltura >= 120) restar = 15;
                else if (tablaAltura >= 115) restar = 17;
                else if (tablaAltura >= 110) restar = 17;
                else if (tablaAltura >= 95) restar = 17;
                else if (tablaAltura >= 88) restar = 20;
                else if (tablaAltura >= 73) restar = 20;
                else if (tablaAltura >= 64) restar = 22;
                else if (tablaAltura >= 52) restar = 22;
                else if (tablaAltura >= 40) restar = 24;
                else if (tablaAltura >= 28) restar = 25;
                else if (tablaAltura >= 16) restar = 28;
                else restar = 26;
                var tablaaltura2 = tablaContenido4.TotalHeight;
                if (tablaaltura2 >= 30) restar += -1;
                var tablaaltura3 = tabla1.TotalHeight;
                if (tablaaltura3 >= 100) restar += -2;
                if (tablaAltura >= 120 && tablaaltura2 >= 30 && tablaaltura3 >= 100) restar += -2;
                if (isnewpage)
                {

                    restar += 20;

                }
                var primero = 0;
                primero = numEspaciosFirma > 4 ? numEspaciosFirma - ((numPaginas == 1) ? (numEspaciosFirma - 5) : restar) : numEspaciosFirma;
                CeldaVacio(tablaFirma, numEspaciosFirma > 4 ? numEspaciosFirma - ((numPaginas == 1) ? (numEspaciosFirma - restar) : restar) : numEspaciosFirma);
            }


            var psychoCode = new PdfPCell()
            {
                Border = PdfPCell.NO_BORDER,
                Phrase = new Phrase($"F-PR-RCM-02/CM", FuenteNegrita)
            };
            //cellFolio.BorderWidth = 80;
            
            psychoCode.HorizontalAlignment = Element.ALIGN_LEFT;
            tablaFirma.AddCell(psychoCode);

            var celdaFirma = new PdfPCell(new Phrase("______________________________________", FuenteNegrita));
            celdaFirma.BorderWidth = 0;
            celdaFirma.HorizontalAlignment = Element.ALIGN_CENTER;
            tablaFirma.AddCell(celdaFirma);
            CeldaVacio(tablaFirma, 1);

            var celdaPersonaFirma = new PdfPCell(new Phrase($"Dr(a). {user.Nombre} {user.ApellidoPaterno} {user.ApellidoMaterno}", FuenteNegrita));
            celdaPersonaFirma.BorderWidth = 0;
            celdaPersonaFirma.HorizontalAlignment = Element.ALIGN_CENTER;
            tablaFirma.AddCell(celdaPersonaFirma);

            documentoPdf.Add(tablaFirma);

            tablaTitulo.DeleteBodyRows();
        }

        private static string GetFechaString(DateTime fecha)
        {
            string dia = fecha.Day.ToString();
            string anio = fecha.Year.ToString();
            string mestexto = ObtenerMesTexto(fecha.ToString("MM"));

            return dia + " de " + mestexto + " de " + anio;
        }
    }
}
