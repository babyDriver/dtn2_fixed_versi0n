﻿using DataAccess.HistorialServicioMedico;
using System.Collections.Generic;

namespace Business
{
    public class ControlHistorialServicioMedico
    {
        private static ServicioHistorialServicioMedico servicio = new ServicioHistorialServicioMedico("SQLConnectionString");

        public static List<Entity.HistorialServicioMedico> ObtenerPorIdServicioMedico(int id)
        {
            return servicio.ObtenerPorIdServicioMedico(id);
        }

        public static Entity.HistorialServicioMedico ObtenerPorIdHistorial(int id)
        {
            return servicio.ObtenerPorIdHistorial(id);
        }

        public static Entity.HistorialServicioMedico ObtenerPorId(int id)
        {
            return servicio.ObtenerPorId(id);
        }

        public static int Guardar(Entity.HistorialServicioMedico historialServicioMedico)
        {
            return servicio.Guardar(historialServicioMedico);
        }
    }
}
