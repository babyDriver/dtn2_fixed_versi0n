﻿using DataAccess.ReporteEstadisticoInformeDetencion;
using System.Collections.Generic;

namespace Business
{
    public class ControlReporteEstadisticoInformeDetencion
    {
        private static ServicioReporteEstadisticoInformeDetencion servicio = new ServicioReporteEstadisticoInformeDetencion("SQLConnectionString");

        public static List<Entity.ReporteEstadisticoInformeDetencion> ObtenerInformes(string[] filtros)
        {
            return servicio.GetDetail(filtros);
        }
    }
}