﻿using DataAccess.Epi;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlEpi
    {
        public static ServicioEpi servicio = new ServicioEpi("SQLConnectionString");
        public static List<Entity.Epi> ObtenerListadoPorFechas(string[] fechas)
        {
            return servicio.ObtenerListadoPorFechas(fechas);
        }
    }
}
