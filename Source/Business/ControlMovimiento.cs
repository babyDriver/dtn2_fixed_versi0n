﻿using DataAccess.Movimiento;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlMovimiento
    {
        private static ServicioMovimiento servicio = new ServicioMovimiento("SQLConnectionString");

        public static int Guardar(Entity.Movimiento movimiento)
        {
            return servicio.Guardar(movimiento);
        }

        public static void Actualizar(Entity.Movimiento movimiento)
        {
            servicio.Actualizar(movimiento);
        }

        public static Entity.Movimiento ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }
        public static List<Entity.Movimiento> ObtenerPorDetalleDetencionId(int DetalleDetencionId)
        {
            return servicio.ObtenerPorDetalleDetencionId(DetalleDetencionId);
        }
        public static List<Entity.Movimiento> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Movimiento ObtenerPorInternoId(int id)
        {
            return servicio.ObtenerByInternoId(id);
        }
    }
}
