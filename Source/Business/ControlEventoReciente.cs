﻿using DataAccess.EventoReciente;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlEventoReciente
    {
        private static ServicioEventoReciente servicio = new ServicioEventoReciente("SQLConnectionString");

        public static int Guardar(Entity.EventoReciente eventoReciente)
        {
            return servicio.Guardar(eventoReciente);
        }

        public static void Actualizar(Entity.EventoReciente eventoReciente)
        {
            servicio.Actualizar(eventoReciente);
        }

        public static Entity.EventoReciente ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }
        public static Entity.EventoReciente ObtenerPorHora(int hora)
        {
            return servicio.ObtenerByHora(hora);
        }
        public static List<Entity.EventoReciente> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }
    }
}
