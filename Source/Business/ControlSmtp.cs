﻿using System;
using System.Collections.Specialized;
using System.Web.UI.WebControls;
using System.Net.Mail;
using Entity;
using System.Text.RegularExpressions;
using System.Net;
using System.ComponentModel;
using DataAccess.Smtp;
using System.Configuration;

namespace Business
{
    public static class ControlSmtp
    {
        private static ServicioSmtp servicio = new ServicioSmtp("SQLConnectionString");

        public static void EnviarMail(String asunto, ListDictionary parametros, string correo, TipoDeMensaje tipoMensaje, string cc, string bcc)
        {
            if (string.Equals(ConfigurationManager.AppSettings["CorreoHabilitado"], "0")) return;

            var mail = new MailDefinition();

            switch (tipoMensaje)
            {
                case TipoDeMensaje.ModificacionDeUsuario:
                    mail.BodyFileName = ConfigurationManager.AppSettings["mensajeModificacionUsuario"].ToString();
                    mail.Subject = String.Concat(ConfigurationManager.AppSettings["Titulo"], @" | Ha cambiado la información de tu cuenta"); 
                    break;
                case TipoDeMensaje.RecuperacionDeContraseña:
                    mail.BodyFileName = ConfigurationManager.AppSettings["mensajeRecuperarContrasena"].ToString();
                    mail.Subject = String.Concat(ConfigurationManager.AppSettings["Titulo"], @" | Recuperación de contraseña"); 
                    break;
                case TipoDeMensaje.NuevoUsuario:
                    mail.BodyFileName = ConfigurationManager.AppSettings["mensajeUsuarioNuevo"].ToString();
                    mail.Subject = ConfigurationManager.AppSettings["Titulo"].ToString();
                    break;
                case TipoDeMensaje.ReestablecerContrasena:
                    mail.BodyFileName = ConfigurationManager.AppSettings["mensajeReestablecerContrasena"].ToString();
                    mail.Subject = String.Concat(ConfigurationManager.AppSettings["Titulo"], @" | Restablecimiento de contraseña");
                    break;
                case TipoDeMensaje.ModificacionDePerfil:
                    mail.BodyFileName = ConfigurationManager.AppSettings["mensajeModificarPerfil"].ToString();
                    mail.Subject = String.Concat(ConfigurationManager.AppSettings["Titulo"], @" | Ha cambiado la información de tu cuenta");
                    break;
            }
            var parametrosCorreo = ObtenerSmtp();
            mail.IsBodyHtml = true;
            mail.From = parametrosCorreo.From;

            MailMessage message = new MailMessage();
            Button btn = new Button();

            message = mail.CreateMailMessage(correo, parametros, btn);
            
            //Reemplazo de texto en cuerpo de correo
            var strBody = message.Body;
            var nombreapp = ConfigurationManager.AppSettings["NombreApp"].ToString();
            var sitioweb = ConfigurationManager.AppSettings["Url"].ToString();
            var inicialessistema = ConfigurationManager.AppSettings["InicialesSistema"].ToString();
            strBody = strBody.Replace("http_sitio", sitioweb);
            strBody = strBody.Replace("NombreApp", nombreapp);
            strBody = strBody.Replace("Iniciales_Sistema", inicialessistema);
            strBody = strBody.Replace("anio_", DateTime.Now.Year.ToString());
            message.Body = strBody;
            message.From = new MailAddress(parametrosCorreo.From, nombreapp);

            if (!string.IsNullOrEmpty(cc))
                message.CC.Add(new MailAddress(cc));

            if (!string.IsNullOrEmpty(parametrosCorreo.CC))
                message.CC.Add(new MailAddress(parametrosCorreo.CC));

            if (!string.IsNullOrEmpty(bcc))
                message.Bcc.Add(new MailAddress(bcc));

            message.Bcc.Add(new MailAddress(parametrosCorreo.From, nombreapp));

            SmtpClient servidorCorreo = new SmtpClient(parametrosCorreo.Host);
            servidorCorreo.Port = parametrosCorreo.Port;
            servidorCorreo.EnableSsl = true;
            servidorCorreo.DeliveryMethod = SmtpDeliveryMethod.Network;
            servidorCorreo.UseDefaultCredentials = false;
            servidorCorreo.Credentials = new NetworkCredential(parametrosCorreo.Username, parametrosCorreo.Password);
            
            try
            {
                servidorCorreo.SendCompleted += (s, e) => {
                    servidorCorreo.Dispose();
                    message.Dispose();
                };
                servidorCorreo.SendAsync(message, null);

                //servidorCorreo.SendAsync(message,null);
                //servidorCorreo.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat(ex.Message, "<br />", ex.InnerException, "<br />", ex.StackTrace));
            }
        }

        static void SMTP_SendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if ((e.Error == null) && !e.Cancelled)
            {
                //enviado
            }
            else
            {
                //error
            }
        }

        public static void NotificarPorCorreo(string nombre, string username, string password, string rol, string email, string zona, string empresa, TipoDeMensaje tipo, string cc, string bcc)
        {
            var parametrosCorreo = new ListDictionary();
            parametrosCorreo.Add("<%nombre%>", nombre);
            parametrosCorreo.Add("<%usuario%>", username);
            parametrosCorreo.Add("<%password%>", password);
            parametrosCorreo.Add("<%rol%>", rol);
            parametrosCorreo.Add("<%zona%>", zona);
            parametrosCorreo.Add("<%empresa%>", empresa);
            parametrosCorreo.Add("<%email%>", email);
            
            var destinatario = email;
            EnviarMail(string.Empty, parametrosCorreo, destinatario, tipo, cc, bcc);
        }

        public static bool ValidateEmail(string email)
        {
            string expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

            if (Regex.IsMatch(email, expresion))
                return Regex.Replace(email, expresion, String.Empty).Length == 0;
            else
                return false;
        }

        public static Smtp ObtenerSmtp()
        {
            return servicio.ObtenerConfiguracion();
        }

        public static int Guardar(Entity.Smtp smtp)
        {
            return servicio.Guardar(smtp);
        }

        public static void Actualizar(Entity.Smtp smtp)
        {
            servicio.Actualizar(smtp);
        }

        public static Smtp ObtenerByTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }
    }
}