﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Business
{
    public class ControlPdfReporteSalidaTrasladocs
    {
        private static string CrearDirectorioControl(string nombre)
        {
            try
            {

                //Verificar si existe el directorio principal del repositorio de documentos
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio de la obra existe
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio para el grupo documental existe, sino crearlo
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre)));


                return string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs//" + nombre);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("No fue posible crear el repositorio para crear el archivo. ", ex.Message));
            }
        }


        //Crear celdas
        public static void Celda(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas para tablas
        public static void CeldaParaTablas(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, BaseColor colorrelleno, float espacio, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                celda.BackgroundColor = colorrelleno;
                celda.Padding = espacio;
                celda.UseAscender = true;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas vacias
        public static void CeldaVacio(PdfPTable tabla, int cantidad)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                celda.BorderWidth = 0;
                tabla.AddCell(celda);
            }

        }

        public static object[] generarAutorizacionSalida(object[] data)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //20 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Recibo_autorizacion_salida_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.AutorizacionSalida"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 11f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                TituloAutorizacionSalida(documentoPdf, FuenteTitulo, FuenteNegrita12, data, FuenteTituloTabla);
                //InformacionSalida(documentoPdf, FuenteNormal, FuenteNegrita, espacio, data, recibio, nombreDetenido, entrega, recibio);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void InformacionSalida(Document documentoPdf, Font FuenteNormal, Font FuenteNegrita, float espacio, List<PertenenciaAux> datos, string recibio, string detenido, string entrega, string entregaA)
        {
            PdfPTable tabla = new PdfPTable(4);//Numero de columnas de la tabla            
            tabla.WidthPercentage = 100; //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            PdfPTable tablaFirmas = new PdfPTable(4);
            tablaFirmas.WidthPercentage = 100;
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;

            BaseColor colorrelleno = new BaseColor(208, 206, 206);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);

            //Titulos de las columnas de las tablas
            CeldaParaTablas(tabla, FuenteNegrita, "Cantidad", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Pertenencia", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Observación", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteNegrita, "Clasificación", 1, 1, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //celdas, contenido de la tabla  
            foreach (var item in datos)
            {
                CeldaParaTablas(tabla, FuenteNormal, item.Cantidad, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Nombre, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Observacion, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                CeldaParaTablas(tabla, FuenteNormal, item.Clasificacion, 1, 1, 1, colorblanco, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            }

            CeldaVacio(tabla, 30);

            PdfPCell celdaEspacioFirmas;
            celdaEspacioFirmas = new PdfPCell(new Phrase("________________________", FuenteNormal));
            celdaEspacioFirmas.HorizontalAlignment = Element.ALIGN_LEFT;
            celdaEspacioFirmas.Border = 0;
            celdaEspacioFirmas.VerticalAlignment = Element.ALIGN_MIDDLE;

            tablaFirmas.AddCell(celdaEspacioFirmas);
            tablaFirmas.AddCell(celdaEspacioFirmas);
            tablaFirmas.AddCell(celdaEspacioFirmas);
            tablaFirmas.AddCell(celdaEspacioFirmas);

            Celda(tablaFirmas, FuenteNormal, "Recibe pertenencias", 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteNormal, "Detenido", 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteNormal, "Entrega pertenencias", 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteNormal, "Entrega a", 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            Celda(tablaFirmas, FuenteNormal, recibio, 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteNormal, detenido, 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteNormal, entrega, 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            Celda(tablaFirmas, FuenteNormal, entregaA, 1, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaFirmas);
            documentoPdf.NewPage();
            tabla.DeleteBodyRows();
        }

        public static void CeldaConChunk(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, int bordo, int horizontal, int vertical)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk(valor, FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = horizontal;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }

        private static void TituloAutorizacionSalida(Document documentoPdf, Font FuenteTitulo, Font FuenteSubtitulo, object[] data, Font titulotabla)
        {
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var juezcalificador = ControlUsuario.Obtener(usId);
            if (juezcalificador.RolId == 13)
            {
                juezcalificador = ControlUsuario.Obtener(8);
            }
            var instituciondispocion = ControlCatalogo.Obtener(Convert.ToInt32(data[1]),Convert.ToInt32(Entity.TipoDeCatalogo.delegacion));
            string escudo = "~/Content/img/Escudo.jpg";
            string logo = "~/Content/img/logo.png";
            var logoizq = subcontrato.Banner;
            if (logoizq != "undefined" && !string.IsNullOrEmpty(logoizq))
            {
                logo = logoizq;
            }
            string pathEscudo = System.Web.HttpContext.Current.Server.MapPath(escudo);
            string pathLogo = System.Web.HttpContext.Current.Server.MapPath(logo);
            iTextSharp.text.Image imageEscudo = null;
            iTextSharp.text.Image imageLogo = null;
            try
            {
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }
            catch (Exception)
            {
                pathEscudo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }

            try
            {
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }
            catch (Exception)
            {
                pathLogo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }

            var logoDER = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logoDER != "undefined" && logo != "")
            {
                logotipo = logoDER;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }

            imageLogo.ScalePercent(95f);
            
                imageLogo.ScaleAbsoluteHeight(100f);
                imageLogo.ScaleAbsoluteWidth(300f);
            
            image.ScaleAbsoluteHeight(60f);
            image.ScaleAbsoluteWidth(70f);
            Paragraph para = new Paragraph();
            Phrase ph1 = new Phrase();
            Chunk glue = new Chunk(new iTextSharp.text.pdf.draw.VerticalPositionMark());
            Paragraph main = new Paragraph();
            ph1.Add(new Chunk(imageLogo, 0, 0, true));
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(image, 0, 15, true));
            main.Add(ph1);
            para.Add(main);
            documentoPdf.Add(para);
            var ctrl = new ControlDatosDetenido();

            var datosdetenido = ctrl.GetByDetalleDetencionId(Convert.ToInt32(data[0]));
            
            Entity.Traslado traslado_ = new Entity.Traslado();
            traslado_.DetalleDetencionId = Convert.ToInt32(data[0]);
            var dd = ControlDetalleDetencion.ObtenerPorId(traslado_.DetalleDetencionId);
            var info = ControlInformacionDeDetencion.ObtenerPorInternoId(dd.DetenidoId);
            var evento = ControlEvento.ObtenerById(info.IdEvento);
            var motivo = "";
            var detenidoevento = ControlDetenidoEvento.ObtenerPorId(info.IdDetenido_Evento);
            if(detenidoevento!=null)
            {
                var mot = ControlWSAMotivo.ObtenerPorId(detenidoevento.MotivoId);

                if (mot != null) motivo = mot.NombreMotivoLlamada;
            }
            
            traslado_.DelegacionId= Convert.ToInt32(data[1]);
            var traslado = ControlTraslado.GetTrasladoNyKeys(traslado_);
            var tiposalida = ControlCatalogo.Obtener(traslado.TiposalidaId, Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));
            int[] w = new int[] { 1, 8 };
            PdfPTable tabla = new PdfPTable(2);
            //Numero de columnas de la tabla
            tabla.SetWidths(w);
            tabla.TotalWidth = 550;
            tabla.HorizontalAlignment = Element.ALIGN_LEFT;

            
            CeldaVacio(tabla, 3);
            //Titulo                                    
            Celda(tabla, FuenteTitulo, "Autorización de salida a disposición de otra autoridad", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(tabla, 3);
            CeldaVacio(tabla, 3);

            //Nombre delegacion y usuario en barandilla
            BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);
            PdfPTable tablaDelegacionUsuario = new PdfPTable(1);
            float[] width = new float[] { 220 };
            tablaDelegacionUsuario.SetWidths(width);
            tablaDelegacionUsuario.DefaultCell.Border = Rectangle.NO_BORDER;
            PdfPCell celda;
            Chunk chunkTituloDelegacion = new Chunk("Delegación: ", FuenteNegrita);
            Chunk chunkNombreDelegacion = new Chunk(datosdetenido.CentroReclusion, FuenteNormal);
            Chunk chunkTituloinstituciondisp = new Chunk("Institución a disposición : ", FuenteNegrita);
            Chunk chunkNombrestituciondisp = new Chunk(instituciondispocion.Nombre, FuenteNormal);
            Paragraph parrafoDelegacion = new Paragraph();
            parrafoDelegacion.Add(chunkTituloDelegacion);
            parrafoDelegacion.Add(chunkNombreDelegacion);
            celda = new PdfPCell(parrafoDelegacion);
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.Border = 0;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;
            tablaDelegacionUsuario.AddCell(celda);

            Paragraph parrafoinst = new Paragraph();
            parrafoinst.Add(chunkTituloinstituciondisp);
            parrafoinst.Add(chunkNombrestituciondisp);
            celda = new PdfPCell(parrafoinst);
            celda.HorizontalAlignment = Element.ALIGN_LEFT;
            celda.Border = 0;
            celda.VerticalAlignment = Element.ALIGN_MIDDLE;


            tablaDelegacionUsuario.AddCell(celda);
            var usuario = ControlUsuario.Obtener(contratoUsuario.IdUsuario);
            if(usuario.RolId == 13)
            {
                usuario = ControlUsuario.Obtener(8);
            }
            var historial = ControlHistorial.ObtenerPorDetenido(Convert.ToInt32(data[0])).Where(x => x.Movimiento == "Traslado del detenido");
            var ff = DateTime.Now;
            foreach (var item in historial)
            {
                ff = item.Fecha;
                usuario = ControlUsuario.Obtener(item.CreadoPor);
            }
            var historialcalif = ControlHistorial.ObtenerPorDetenido(Convert.ToInt32(data[0])).Where(x => x.Movimiento.Contains("calif")).FirstOrDefault();
            var usid = usuario.Id;
            if(historialcalif!=null)
            {
                usid = historialcalif.CreadoPor;
            }
            var usuario2 = ControlUsuario.Obtener(usid);
            Chunk chunkTituloUsuario = new Chunk("Usuario barandilla: ", FuenteNegrita);
            Chunk chunkNombreUsuario = new Chunk(usuario.User, FuenteNormal);
            PdfPCell celda2;
            Paragraph parrafoUsuario = new Paragraph();
            parrafoUsuario.Add(chunkTituloUsuario);
            parrafoUsuario.Add(chunkNombreUsuario);
            celda2 = new PdfPCell(parrafoUsuario);
            celda2.HorizontalAlignment = Element.ALIGN_LEFT;
            celda2.VerticalAlignment = Element.ALIGN_MIDDLE;
            celda2.Border = 0;
            tablaDelegacionUsuario.AddCell(celda2);

            CeldaVacio(tablaDelegacionUsuario, 1);

            //Titulo "Datos del detenido"
            PdfPTable tabla1 = new PdfPTable(1);
            tabla1.WidthPercentage = 100;
            PdfPCell tituloTabla1;
            tituloTabla1 = new PdfPCell(new Phrase("DATOS DEL DETENIDO", titulotabla));
            tituloTabla1.HorizontalAlignment = Element.ALIGN_CENTER;
            tituloTabla1.Border = 0;
            tituloTabla1.BackgroundColor = new BaseColor(0, 61, 122);
            tituloTabla1.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla1.AddCell(tituloTabla1);

            CeldaVacio(tabla1, 1);

            //Tabla datos del detenido
            PdfPTable tabla2 = new PdfPTable(3);
            var h1 = datosdetenido.Fecha.Hour.ToString();
            if (datosdetenido.Fecha.Hour < 10) h1 = "0" + h1;
            var m1 = datosdetenido.Fecha.Minute.ToString();
            if (datosdetenido.Fecha.Minute < 10) m1 = "0" + m1;
            var s1 = datosdetenido.Fecha.Second.ToString();
            if (datosdetenido.Fecha.Second < 10) s1 = "0" + s1;
            var f1 = datosdetenido.Fecha.ToShortDateString() + " " + h1 + ":" + m1 + ":" + s1;
            var edad = "";
            if (dd.DetalledetencionEdad != 0) edad = dd.DetalledetencionEdad.ToString();
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "No. remisión: ", datosdetenido.Expediente, 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Registro: ", f1, 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Nombre: ", datosdetenido.Nombredetenido, 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Edad: ", edad, 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Sexo: ", datosdetenido.Sexo, 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Estado civil: ", datosdetenido.EstadoCivil.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Escolaridad: ", datosdetenido.Escolaridad.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Nacionalidad: ", datosdetenido.Nacionalidad.ToString(), 0, 0, 0);
            //CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Sueldo: ", data[10].ToString(), 0, 0, 0);
            if (datosdetenido.Domicilio.ToString() == "")
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Domicilio: ", "Sin dato", 0, 0, 0);
            else
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Domicilio: ", datosdetenido.Domicilio.ToString(), 0, 0, 0);
            if (datosdetenido.Colonia.ToString() == "")
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Colonia: ", "Sin dato", 0, 0, 0);
            else
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Colonia: ", datosdetenido.Colonia.ToString(), 0, 0, 0);
            if (datosdetenido.Municipio.ToString() == "")
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Ciudad: ", "Sin dato", 0, 0, 0);
            else
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Ciudad: ", datosdetenido.Municipio.ToString(), 0, 0, 0);
            if (datosdetenido.Estado.ToString() == "")
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Estado: ", "Sin dato", 0, 0, 0);
            else
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Estado: ", datosdetenido.Estado.ToString(), 0, 0, 0);
            if (datosdetenido.Telefono.ToString() == "")
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Teléfono: ", "Sin dato", 0, 0, 0);
            else
                CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "Teléfono: ", datosdetenido.Telefono.ToString(), 0, 0, 0);
            CeldaConChunk(tabla2, FuenteNegrita, FuenteNormal, "", "", 0, 0, 0);

            CeldaVacio(tabla2, 3);

            //Titulo "Remision"
            PdfPTable tabla3 = new PdfPTable(1);
            tabla3.WidthPercentage = 100;
            PdfPCell tituloTabla2;
            tituloTabla2 = new PdfPCell(new Phrase("REMISIÓN", titulotabla));
            tituloTabla2.HorizontalAlignment = Element.ALIGN_CENTER;
            tituloTabla2.Border = 0;
            tituloTabla2.BackgroundColor = new BaseColor(0, 61, 122);
            tituloTabla2.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla3.AddCell(tituloTabla2);

            CeldaVacio(tabla3, 1);

            //Tabla datos de la remision
            PdfPTable tabla4 = new PdfPTable(3);
            tabla4.WidthPercentage = 100;
            var eventounidadresponsable = ControlEventoUnidadResponsable.ObtenerTodosByEventoId(evento.Id);
            if (eventounidadresponsable.Count() == 0)
            {
                CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Cve unidad: ", "", 0, 0, 0);
                CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Cve agente: ", "", 0, 0, 0);
                CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Nombre agente: ", "", 0, 0, 0);
            }
            foreach (var item in eventounidadresponsable)
            {
                var unidad = ControlUnidad.ObtenerById(item.UnidadId);
                var responsable = ControlCatalogo.Obtener(item.ResponsableId, Convert.ToInt32(Entity.TipoDeCatalogo.responsable_unidad));
                CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Cve unidad: ", unidad.Nombre, 0, 0, 0);
                CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Cve agente: ", item.UnidadId.ToString(), 0, 0, 0);
                CeldaConChunk(tabla4, FuenteNegrita, FuenteNormal, "Nombre agente: ", responsable.Nombre, 0, 0, 0);
            }
            
            

            CeldaVacio(tabla4, 3);

            //Titulo "Parte"
            PdfPTable tabla5 = new PdfPTable(1);
            tabla5.WidthPercentage = 100;
            PdfPCell tituloTabla3;
            tituloTabla3 = new PdfPCell(new Phrase("PARTE", titulotabla));
            tituloTabla3.HorizontalAlignment = Element.ALIGN_CENTER;
            tituloTabla3.Border = 0;
            tituloTabla3.BackgroundColor = new BaseColor(0, 61, 122);
            tituloTabla3.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla5.AddCell(tituloTabla3);

            CeldaVacio(tabla5, 1);

            //Datos parte
            PdfPTable tabla6 = new PdfPTable(1);
            tabla6.WidthPercentage = 100;
            CeldaConChunk(tabla6, FuenteNegrita, FuenteNormal, "N° Evento: ", datosdetenido.FolioEvento, 0, 0, 0);
            CeldaConChunk(tabla6, FuenteNegrita, FuenteNormal, "Motivo detención: ", motivo, 0, 0, 0);
            CeldaConChunk(tabla6, FuenteNegrita, FuenteNormal, "Lugar detención: ", datosdetenido.Lugardetencion, 0, 0, 0);
            CeldaConChunk(tabla6, FuenteNegrita, FuenteNormal, "Colonia: ", datosdetenido.Coloniadetencion.ToString(), 0, 0, 0);

            CeldaVacio(tabla6, 1);

            //Titulo "Situacion"
            PdfPTable tabla7 = new PdfPTable(1);
            tabla7.WidthPercentage = 100;
            PdfPCell tituloTabla4;
            tituloTabla4 = new PdfPCell(new Phrase("SITUACIÓN", titulotabla));
            tituloTabla4.HorizontalAlignment = Element.ALIGN_CENTER;
            tituloTabla4.Border = 0;
            tituloTabla4.BackgroundColor = new BaseColor(0, 61, 122);
            tituloTabla4.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla7.AddCell(tituloTabla4);

            CeldaVacio(tabla7, 1);

            //Datos situacion
            PdfPTable tabla8 = new PdfPTable(3);
            tabla8.WidthPercentage = 100;
            
            var h2 = ff.Hour.ToString();
            if (ff.Hour < 10) h2 = "0" + h2;
            var m2 = ff.Minute.ToString();
            if (ff.Minute < 10) m2 = "0" + m2;
            var s2 = ff.Second.ToString();
            if (ff.Second < 10) s2 = "0" + s2;
            var f2 = ff.ToShortDateString() + " " + h2 + ":" + m2 + ":" + s2;


            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Fecha: ", f2, 0, 0, 0) ;
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Autorizó salida: ", juezcalificador.User, 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Situación: ", "A disposición de otra autoridad ", 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Solo arresto: ",datosdetenido.Soloarresto==1?"Si":"No" , 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Horas: ", datosdetenido.Totalhoras.ToString(), 0, 0, 0);
            CeldaConChunk(tabla8, FuenteNegrita, FuenteNormal, "Multa: ", datosdetenido.Totalapagar.ToString(), 0, 0, 0);

            CeldaVacio(tabla8, 1);

            //Explicacion situacion
            PdfPTable tabla9 = new PdfPTable(1);
            tabla9.WidthPercentage = 100;
            CeldaConChunk(tabla9, FuenteNegrita, FuenteNormal, "Fundamento: ", traslado.Fundamento, 0, 0, 0);
            CeldaConChunk(tabla9, FuenteNegrita, FuenteNormal, "Razonamiento: ", datosdetenido.Razon, 0, 0, 0);

            CeldaVacio(tabla9, 1);

            PdfPTable table10 = new PdfPTable(1);
            CeldaVacio(table10, 3);
            var cel21 = new PdfPCell(new Phrase(juezcalificador.User));
            cel21.BorderWidth = 0;
            cel21.HorizontalAlignment = Element.ALIGN_CENTER;
            table10.AddCell(cel21);
            var cell1 = new PdfPCell(new Phrase("____________________", FuenteNegrita));
            cell1.BorderWidth = 0;
            cell1.HorizontalAlignment = Element.ALIGN_CENTER;
            table10.AddCell(cell1);

            cell1 = new PdfPCell(new Phrase("Firma juez", FuenteNegrita));
            cell1.BorderWidth = 0;
            cell1.HorizontalAlignment = Element.ALIGN_CENTER;
            table10.AddCell(cell1);

            tablaDelegacionUsuario.WidthPercentage = 100;
            tabla2.WidthPercentage = 100;
            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaDelegacionUsuario);
            documentoPdf.Add(tabla1);
            documentoPdf.Add(tabla2);
            documentoPdf.Add(tabla3);
            documentoPdf.Add(tabla4);
            documentoPdf.Add(tabla5);
            documentoPdf.Add(tabla6);
            documentoPdf.Add(tabla7);
            documentoPdf.Add(tabla8);
            documentoPdf.Add(tabla9);
            documentoPdf.Add(table10);
            //documentoPdf.Add(tablaDatos2);
            //documentoPdf.Add(tablaDatos3);
            tabla.DeleteBodyRows();
        }
    }
    public partial class HeaderFooter : PdfPageEventHelper
    {
        PdfContentByte cb;
        PdfTemplate headerTemplate;
        BaseFont bf = null;
        float tamanofuente = 8f;

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            try
            {
                bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb = writer.DirectContent;
                headerTemplate = cb.CreateTemplate(100, 100);
            }
            catch (DocumentException de) { }
            catch (System.IO.IOException ioe) { }
        }

        public override void OnEndPage(PdfWriter writer, Document doc)
        {
            //Encabezado de Página
            Rectangle pageSize = doc.PageSize;
            PdfPTable footerTbl = new PdfPTable(1);
            BaseColor blau = new BaseColor(0, 61, 122);

            float[] TablaAncho = new float[] { 100 };
            footerTbl.SetWidths(TablaAncho);
            footerTbl.TotalWidth = 550;
            Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

            PdfPCell cell;


            string pagina = "Página " + writer.PageNumber + " de ";
            //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
            {
                float margenbottom = 15f;
                float margenright = 280f;
                cb.BeginText();
                cb.SetFontAndSize(bf, tamanofuente);
                cb.SetTextMatrix(doc.PageSize.GetBottom(margenright), doc.PageSize.GetBottom(margenbottom));
                cb.ShowText(pagina);
                cb.EndText();
                float len = bf.GetWidthPoint(pagina, tamanofuente);
                cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));
            }

            PdfPTable footerTbl2 = new PdfPTable(2);


            float[] TablaAncho2 = new float[] { 100, 100 };
            footerTbl2.SetWidths(TablaAncho2);
            footerTbl2.TotalWidth = doc.Right - doc.Left;
            footerTbl.HorizontalAlignment = Element.ALIGN_CENTER;
            Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

            //string dia = String.Format("{0:dd}", DateTime.Now);
            //string anio = String.Format("{0:yyyy}", DateTime.Now);
            //string hora = String.Format("{0:HH:mm}", DateTime.Now);
            //string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
            //string fechahora = dia + " de " + mestexto + " de " + anio + " " + hora;
            //Chunk chkFecha = new Chunk(fechahora, FuenteCelda);
            Chunk chkFecha = new Chunk("", FuenteCelda);
            Phrase phrfecha = new Phrase(chkFecha);

            //Phrase promad = new Phrase("Promad Soluciones Computarizadas " + anio, FuenteCelda2);
            Phrase promad = new Phrase("", FuenteCelda2);
            cell = new PdfPCell(promad);
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 2;
            cell.BorderColorTop = blau;
            footerTbl2.AddCell(cell);

            cell = new PdfPCell(phrfecha);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthBottom = 0;
            cell.BorderWidthTop = 2;
            cell.BorderColorTop = blau;
            footerTbl2.AddCell(cell);
            footerTbl2.WriteSelectedRows(0, -1, 5, 25, writer.DirectContent);
        }

        private static string ObtenerMesTexto(string mes)
        {
            string mestexto = "";
            switch (mes)
            {
                case "01":
                    mestexto = "Enero";
                    break;
                case "02":
                    mestexto = "Febrero";
                    break;
                case "03":
                    mestexto = "Marzo";
                    break;
                case "04":
                    mestexto = "Abril";
                    break;
                case "05":
                    mestexto = "Mayo";
                    break;
                case "06":
                    mestexto = "Junio";
                    break;
                case "07":
                    mestexto = "Julio";
                    break;
                case "08":
                    mestexto = "Agosto";
                    break;
                case "09":
                    mestexto = "Septiembre";
                    break;
                case "10":
                    mestexto = "Octubre";
                    break;
                case "11":
                    mestexto = "Noviembre";
                    break;
                case "12":
                    mestexto = "Diciembre";
                    break;
                default:
                    mestexto = " ";
                    break;
            }
            return mestexto;
        }

        //Necesario para incluir pagina x de y al header
        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);
            headerTemplate.BeginText();
            headerTemplate.SetFontAndSize(bf, tamanofuente);
            headerTemplate.SetTextMatrix(0, 0);
            headerTemplate.ShowText((writer.PageNumber - 1).ToString());
            headerTemplate.EndText();

        }
    }
}
