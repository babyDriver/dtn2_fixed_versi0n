﻿using DataAccess.CertificadoLesionAntecedentes;
using System.Collections.Generic;
using System;

namespace Business
{
   public class ControlCertificadoLesionAntecedentes
    {
        private static ServicioCertificadoLesionAntecedentes servicio = new ServicioCertificadoLesionAntecedentes("SQLConnectionString");
        public static int Guardar(Entity.CertificadoLesionAntecedentes certificadoLesionAntecedentes)
        {
            return servicio.Guardar(certificadoLesionAntecedentes);
        }
        public static Entity.CertificadoLesionAntecedentes ObtenerPorId(int Id)
        {
            return servicio.ObtenerPorId(Id);

        }
    }
}
