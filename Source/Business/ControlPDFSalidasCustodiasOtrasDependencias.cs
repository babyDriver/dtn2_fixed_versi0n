﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Business
{
    public static class ControlPDFSalidasCustodiasOtrasDependencias
    {
        //Crear directorio 
        private static string CrearDirectorioControl(string nombre)
        {
            try
            {
                //Verificar si existe el directorio principal del repositorio de documentos
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio de la obra existe
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio para el grupo documental existe, sino crearlo
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre)));

                return string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs//" + nombre);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("No fue posible crear el repositorio para crear el archivo. ", ex.Message));
            }
        }

        //Crear celdas
        public static void Celda(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas para tablas
        public static void CeldaParaTablas(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, BaseColor colorrelleno, float espacio, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                celda.BackgroundColor = colorrelleno;
                celda.Padding = espacio;
                celda.UseAscender = true;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas vacias
        public static void CeldaVacio(PdfPTable tabla, int cantidad)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                celda.BorderWidth = 0;
                tabla.AddCell(celda);
            }

        }

        //para crear el Header y Footer
        public partial class HeaderFooter : PdfPageEventHelper
        {
            PdfContentByte cb;
            PdfTemplate headerTemplate;
            BaseFont bf = null;
            float tamanofuente = 8f;

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                }
                catch (DocumentException de) { }
                catch (System.IO.IOException ioe) { }
            }

            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                //Encabezado de Página
                Rectangle pageSize = doc.PageSize;
                PdfPTable footerTbl = new PdfPTable(1);
                BaseColor blau = new BaseColor(0, 61, 122);

                float[] TablaAncho = new float[] { 100 };
                footerTbl.SetWidths(TablaAncho);
                footerTbl.TotalWidth = 550;
                Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                PdfPCell cell;
                CeldaParaTablas(footerTbl, FuenteCelda, " ", 1, 0, 1, blau, 0, Element.ALIGN_TOP, Element.ALIGN_TOP);


                string pagina = "Página " + writer.PageNumber + " de ";
                //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
                {
                    float margenbottom = 11f;
                    float margenright = 280f;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, tamanofuente);
                    cb.SetTextMatrix(doc.PageSize.GetBottom(margenright), doc.PageSize.GetBottom(margenbottom));
                    cb.ShowText(pagina);
                    cb.EndText();
                    float len = bf.GetWidthPoint(pagina, tamanofuente);
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));
                }

                PdfPTable footerTbl2 = new PdfPTable(2);


                float[] TablaAncho2 = new float[] { 100, 100 };
                footerTbl2.SetWidths(TablaAncho2);
                footerTbl2.TotalWidth = doc.Right - doc.Left;
                Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                string dia = string.Format("{0:dd}", DateTime.Now);
                string anio = string.Format("{0:yyyy}", DateTime.Now);
                string hora = string.Format("{0:HH:mm}", DateTime.Now);
                string mestexto = ObtenerMesTexto(string.Format("{0:MM}", DateTime.Now));
                string fechahora = dia + " de " + mestexto + " de " + anio + " " + hora;
                Chunk chkFecha = new Chunk(fechahora, FuenteCelda);
                Phrase phrfecha = new Phrase(chkFecha);

                //Phrase promad = new Phrase("Promad Soluciones Computarizadas " + anio, FuenteCelda2);
                Phrase promad = new Phrase("", FuenteCelda2);
                cell = new PdfPCell(promad);
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);

                cell = new PdfPCell(phrfecha);
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);
                footerTbl2.WriteSelectedRows(0, -1, 5, 25, writer.DirectContent);
            }

            private static string ObtenerMesTexto(string mes)
            {
                string mestexto = "";
                switch (mes)
                {
                    case "01":
                        mestexto = "Enero";
                        break;
                    case "02":
                        mestexto = "Febrero";
                        break;
                    case "03":
                        mestexto = "Marzo";
                        break;
                    case "04":
                        mestexto = "Abril";
                        break;
                    case "05":
                        mestexto = "Mayo";
                        break;
                    case "06":
                        mestexto = "Junio";
                        break;
                    case "07":
                        mestexto = "Julio";
                        break;
                    case "08":
                        mestexto = "Agosto";
                        break;
                    case "09":
                        mestexto = "Septiembre";
                        break;
                    case "10":
                        mestexto = "Octubre";
                        break;
                    case "11":
                        mestexto = "Noviembre";
                        break;
                    case "12":
                        mestexto = "Diciembre";
                        break;
                    default:
                        mestexto = " ";
                        break;
                }
                return mestexto;
            }

            //Necesario para incluir pagina x de y al header
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);
                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, tamanofuente);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();
            }
        }

        #region Reporte salidas y custodias a otras dependencias
        public static object[] generarPDF(object[] data, DateTime fechaInicio, DateTime fechaFin, bool utilizarHora, List<Entity.DetalleDetencion> detenciones)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //20 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Salidas_Hacia_Otras_Instituciones" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.Intoxicacion"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);
                Font FuenteNormal = new Font(fuentecalibri, 9f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD, blau);
                Font FuenteTituloTabla = new Font(fuentecalibri, 9f, Font.BOLD, BaseColor.BLACK);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                TituloDocumento(documentoPdf, FuenteTitulo, FuenteNegrita12, data, fechaInicio, fechaFin, utilizarHora);

                var detencionesFiltradas = new List<Entity.DetalleDetencion>();
                if (utilizarHora) detencionesFiltradas = detenciones.Where(x => x.Fecha.Value >= fechaInicio && x.Fecha.Value <= fechaFin.AddMinutes(1).AddSeconds(-1)).ToList();
                else detencionesFiltradas = detenciones.Where(x => x.Fecha.Value.Date >= fechaInicio.Date && x.Fecha.Value.Date <= fechaFin.Date).ToList();

                CuerpoDocumento(documentoPdf, FuenteNormal, FuenteTituloTabla, espacio, detencionesFiltradas);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloDocumento(Document documentoPdf, Font FuenteTitulo, Font FuenteSubtitulo, object[] data, DateTime fechaInicio, DateTime fechaFin, bool utilizaHora)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable table = new PdfPTable(2);
            table.SetWidths(w);
            table.TotalWidth = 550;
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell;

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }
            //Logo
            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_LEFT;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cellWithRowspan);

            //Titulo     
            Celda(table, FuenteTitulo, "     ", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            Celda(table, FuenteTitulo, "   Salidas y Custodias a Otras Dependencias", 0, 0, 1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(table, 3);

            //Nombre delegacion y usuario en barandilla
            //BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablaDelegacionUsuario = new PdfPTable(1);
            float[] width = new float[] { 220 };
            tablaDelegacionUsuario.SetWidths(width);
            tablaDelegacionUsuario.DefaultCell.Border = Rectangle.NO_BORDER;


            string fechainicial = "";
            string fechafinal = "";

            if (!utilizaHora)
            {
                fechainicial = Convert.ToDateTime(fechaInicio).ToShortDateString();
                fechafinal = Convert.ToDateTime(fechaFin).ToShortDateString();

            }
            else
            {
                var date = Convert.ToDateTime(fechaInicio);
                var h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                var m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                var s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechainicial = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
                date = Convert.ToDateTime(fechaFin);
                h = date.Hour.ToString();
                if (date.Hour < 10) h = "0" + h;
                m = date.Minute.ToString();
                if (date.Minute < 10) m = "0" + m;
                s = date.Second.ToString();
                if (date.Second < 10) s = "0" + s;
                fechafinal = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
            }
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Municipio: ", data[0].ToString(), 0, 0, 0);



            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, $"Del {fechainicial} al {fechafinal}", "", 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Delegación: ", data[1].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Calificaciones: ", "Todas", 0, 0, 0);
            CeldaVacio(tablaDelegacionUsuario, 1);

            tablaDelegacionUsuario.WidthPercentage = 100;

            documentoPdf.Add(table);
            documentoPdf.Add(tablaDelegacionUsuario);
            table.DeleteBodyRows();
        }

        private static void CuerpoDocumento(Document documentoPdf, Font FuenteNormal, Font FuenteTituloTabla, float espacio, List<Entity.DetalleDetencion> detenciones)
        {
            BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormalAux = new Font(fuentecalibri, 9f, Font.NORMAL);
            Font FuenteNegritaAux = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.BLACK);
            Font FuenteTituloTabla2 = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablaEncabezado = new PdfPTable(4);
            tablaEncabezado.WidthPercentage = 100;
            tablaEncabezado.HorizontalAlignment = Element.ALIGN_RIGHT;
            tablaEncabezado.SetWidths(new float[] { 65, 15, 10, 10 });


            PdfPTable tabla = new PdfPTable(3);//Numero de columnas de la tabla            
            tabla.WidthPercentage = 100; //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_RIGHT;//Alineación vertical
            tabla.SetWidths(new float[] { 80, 10, 10 });

            PdfPTable tablaTotales = new PdfPTable(3);
            tablaTotales.WidthPercentage = 100;
            tablaTotales.HorizontalAlignment = Element.ALIGN_RIGHT;

            //BaseColor colorrelleno = new BaseColor(0, 61, 122);
            BaseColor colorrelleno = new BaseColor(192, 224, 255);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);

            //Titulos de las columnas de las tablas
            CeldaParaTablas(tabla, FuenteTituloTabla2, "Motivo(s) Calificaciòn", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteTituloTabla2, "Rem. por Motivo", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteTituloTabla2, "%", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;

            var traslados = new List<Entity.Traslado>();
            var motivosDeshabilitados = new List<Entity.MotivoDetencion>();
            foreach (var detencion in detenciones)
            {
                var buscarTraslado = new Entity.Traslado();
                buscarTraslado.DetalleDetencionId = detencion.Id;
                var traslado = ControlTraslado.GetTrasladoByDetalleDetencion(buscarTraslado);
                if (traslado != null)
                {
                    var motivoDeshabilitado = 0;

                    var motivosDetencionLista = new List<Entity.MotivoDetencionInterno>();
                    var motivosDetencionChecar = ControlMotivoDetencionInterno.GetListaMotivosDetencionByInternoId(new Entity.MotivoDetencionInterno() { InternoId = traslado.DetalleDetencionId });
                    foreach (var item in motivosDetencionChecar)
                    {
                        motivosDetencionLista.Add(item);
                    }
                    var motivosLista = new List<Entity.MotivoDetencion>();
                    foreach (var item in motivosDetencionLista.Select(x => x.MotivoDetencionId).Distinct())
                    {
                        var motivoDet = ControlMotivoDetencion.ObtenerPorId(item);
                        if(motivoDet != null)
                        {
                            if (!motivosLista.Contains(motivoDet))
                            {
                                if (motivoDet.Habilitado == 1)
                                {
                                    motivosLista.Add(motivoDet);
                                }
                                else
                                {
                                    motivosDeshabilitados.Add(motivoDet);
                                    motivoDeshabilitado++;
                                }
                            }
                        }
                    }

                    if(motivosLista.Count > 0 || motivoDeshabilitado == 0)
                    {
                        traslados.Add(traslado);
                    }
                }
            }

            var totalNoCalificados = 0;
            var motivosDetencionInterno = new List<Entity.MotivoDetencionInterno>();
            foreach (var traslado in traslados)
            {
                var motivos = ControlMotivoDetencionInterno.GetListaMotivosDetencionByInternoId(new Entity.MotivoDetencionInterno() { InternoId = traslado.DetalleDetencionId });

                if (motivos.Count > 0)
                {
                    foreach (var motivoDetencionInterno in motivos)
                    {
                        if (!motivosDeshabilitados.Any(x => x.Id == motivoDetencionInterno.MotivoDetencionId))
                        {
                            motivosDetencionInterno.Add(motivoDetencionInterno);
                        }
                    }
                }
                else totalNoCalificados++;//Si no tienen motivos, no fueron calificados, se les agregara al motivo "Traslado a otra autoridad sin calificación".
            }

            var nombreMotivos = new List<Entity.MotivoDetencion>();
            foreach (var item in motivosDetencionInterno.Select(x => x.MotivoDetencionId).Distinct())
            {
                var motivo = ControlMotivoDetencion.ObtenerPorId(item);
                if (motivo != null)
                {
                    if (!nombreMotivos.Contains(motivo))
                    {
                        if(motivo.Habilitado == 1)
                        {
                            nombreMotivos.Add(motivo);
                        }
                    }
                }
            }
            
            //Tabla Encabezado (donde aparecen los totales de los dias)
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, "DEPARTAMENTO JURIDICO", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, "Remisiones", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, traslados.Count.ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteTituloTabla2, "100.00 %", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaVacio(tablaEncabezado, 4);
            CeldaVacio(tablaEncabezado, 2);
            CeldaParaTablas(tablaEncabezado, FuenteNormal, (motivosDetencionInterno.Count + totalNoCalificados).ToString(), 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaEncabezado, FuenteNormal, "100.00 %", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaVacio(tablaEncabezado, 4);


            foreach (var item in nombreMotivos.OrderBy(x => x.Motivo))//Faltan escribir al ultimo los que no se calificaron pero aun asi se trasladaron
            {
                if (i % 2 != 0) color = colorgris;
                else color = colorblanco;

                i++;

                var totalRemisionesMismoMotivo = motivosDetencionInterno.Where(x => x.MotivoDetencionId == item.Id).Count();
                var porcentaje = totalRemisionesMismoMotivo / (double)(motivosDetencionInterno.Count + totalNoCalificados);

                var motivo = item.Motivo[0].ToString().ToUpper() + item.Motivo.Substring(1).ToLower();

                CeldaParaTablas(tabla, FuenteNormal, motivo, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, totalRemisionesMismoMotivo.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, porcentaje.ToString("p2"), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            }
            //Para los que no fueron calificados
            CeldaParaTablas(tabla, FuenteNormal, "Traslado a otra autoridad sin calificación", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla, FuenteNormal, totalNoCalificados.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
            CeldaParaTablas(tabla, FuenteNormal, (totalNoCalificados / (double)(motivosDetencionInterno.Count + totalNoCalificados)).ToString("p2"), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);


            CeldaVacio(tabla, 10);

            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Detenidos por motivo: ", (motivosDetencionInterno.Count + totalNoCalificados).ToString(), 0, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);

            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotales, FuenteNegritaAux, FuenteNormalAux, "Detenidos reales: ", traslados.Count.ToString(), 0, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);

            documentoPdf.Add(tablaEncabezado);
            documentoPdf.Add(tabla);
            documentoPdf.Add(tablaTotales);
            documentoPdf.NewPage();
            tablaEncabezado.DeleteBodyRows();
            tabla.DeleteBodyRows();
            tablaTotales.DeleteBodyRows();
        }

        public static void CeldaConChunk(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, int bordo, int horizontal, int vertical)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk(valor, FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = horizontal;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }
        #endregion
    }
}
