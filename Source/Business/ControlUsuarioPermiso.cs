﻿using DataAccess.UsuarioPermiso;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlUsuarioPermiso
    {
        private static ServicioUsuarioPermiso servicio = new ServicioUsuarioPermiso("SQLConnectionString");

        public static int Guardar(Entity.UsuarioPermiso permiso)
        {
            return servicio.Guardar(permiso);
        }

        public static void EliminarByUsuarioId(int usuarioId)
        {
            servicio.EliminarByUsuarioId(usuarioId);
        }

        public static void Actualizar(Entity.UsuarioPermiso permiso)
        {
            servicio.Actualizar(permiso);
        }

        public static void ActualizarHabilitadoPorUsuarioId(Entity.UsuarioPermiso permiso)
        {
            servicio.ActualizarHabilitadoByUserioId(permiso);
        }

        public static Entity.UsuarioPermiso ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.UsuarioPermiso> ObtenerPorUsuarioId(int usuarioId)
        {
            return servicio.ObtenerByUsuarioId(usuarioId);
        }

        public static Entity.UsuarioPermiso ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.UsuarioPermiso ObtenerPorParametros(Entity.UsuarioPermiso parametros)
        {
            return servicio.ObtenerByParametros(parametros);
        }

        public static List<Entity.UsuarioPermiso> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }
    }
}
