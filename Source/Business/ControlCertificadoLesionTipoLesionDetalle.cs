﻿using DataAccess.CertificadoLesionTipoLesionDetalle;
using System.Collections.Generic;
using System;

namespace Business
{
   public  class ControlCertificadoLesionTipoLesionDetalle
    {
        private static ServicioCertificadoLesionTipoLesionDetalle servicio = new ServicioCertificadoLesionTipoLesionDetalle("SQLConnectionString");

      
        public static int Guardar(Entity.CertificadoLesionTipoLesionDetalle lesionDetalle)
        {

            return servicio.Guardar(lesionDetalle);
        }
        public static List<Entity.CertificadoLesionTipoLesionDetalle> ObteneroPortipoLesion(int Id)
        {
            return servicio.ObtenerPorId(Id);
        }
    }
}
