﻿using DataAccess.Calificacion;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlCalificacion
    {
        private static ServicioCalificacion servicio = new ServicioCalificacion("SQLConnectionString");

        public static List<Entity.Calificacion> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }       

        public static int Guardar(Entity.Calificacion item)
        {
            return servicio.Guardar(item);
        }

        public static Entity.Calificacion ObtenerPorTrackingId(object[] parametros)
        {
            return servicio.ObtenerByTrackingId(parametros);
        }

        public static Entity.Calificacion ObtenerPorId(object[] parametros)
        {
            return servicio.ObtenerById(parametros);
        }

        public static Entity.Calificacion ObtenerPorInternoId(int id)
        {
            return servicio.ObtenerByInternoId(id);
        }

        public static void Actualizar(Entity.Calificacion item)
        {
            servicio.Actualizar(item);
        }
    }
}
