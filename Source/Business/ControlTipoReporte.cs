﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataAccess.TipoReporte;

namespace Business
{
    public  class ControlTipoReporte
    {
        private static ServicioTipoReporte servicio = new ServicioTipoReporte("SQLConnectionString");

        public static List<Entity.Tiporeporte> ObtenerTodos()
        {
            return servicio.GetTiporeportes();
        }

    }
}
