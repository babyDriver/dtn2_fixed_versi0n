﻿using DataAccess.ListadoReporteCorteDetenidos;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlListadoReporteCorteDetenidos
    {
        private static ServicioListadoReporteCorteDetenidos servicio = new ServicioListadoReporteCorteDetenidos("SQLConnectionString");

        public static List<Entity.ListadoReporteCorteDetenidos> ObtenerTodos(object[] data)
        {
            return servicio.ObtenerTodos(data);
        }
    }
}
