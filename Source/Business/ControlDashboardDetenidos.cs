﻿using DataAccess.DashboardDetenidos;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlDashboardDetenidos
    {
        private static ServicioDashboardDetenidos servicio = new ServicioDashboardDetenidos("SQLConnectionString");

        public static List<Entity.DashboarDetenidos>GetAll()
        {

            return servicio.GetAll();
        }
        public static List<Entity.DashboarDetenidos> GetbyFechas(string[] fechas)
        {

            return servicio.GetbyDates(fechas);
        }
        public static List<Entity.DashboarDetenidos> GetByAnio(int Anio)
        {
            return servicio.ObtenerPorAnio(Anio);
        }
        public static List<Entity.DashboarDetenidos> ObtenerEnTrabajoSocial(int Anio)
        {
            return servicio.ObtenerEnTrabajoSocail(Anio);
        }

    }

    }

