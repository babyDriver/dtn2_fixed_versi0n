﻿using DataAccess.RelacionDetenidosPorSalidaAutorizada;
using System.Collections.Generic;

namespace Business
{
    public class ControlRelacionDetenidosPorSalidaAutorizada
    {
        private static ServicioRelacionDetenidosPorSalidaAutorizada servicio = new ServicioRelacionDetenidosPorSalidaAutorizada("SQLConnectionString");

        public static List<Entity.RelacionDetenidosPorSalidaAutorizada> GetDet(string[] filtros)
        {
            return servicio.GetDetails(filtros);
        }

    }
}