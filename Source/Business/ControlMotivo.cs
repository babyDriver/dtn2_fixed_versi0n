﻿using DataAccess.Motivo;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlMotivo
    {
        private static ServicioMotivo servicio = new ServicioMotivo("SQLConnectionString");

        public static int Guardar(Entity.Motivo Motivo)
        {
            return servicio.Guardar(Motivo);
        }

        public static void Actualizar(Entity.Motivo Motivo)
        {
            servicio.Actualizar(Motivo);
        }

        public static Entity.Motivo ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.Motivo> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Motivo ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.Motivo ObtenerPorNombre(string nombre)
        {
            return servicio.ObtenerByNombre(nombre);
        }

    }

}