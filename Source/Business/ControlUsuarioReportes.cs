﻿using DataAccess.UsuarioReportes;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlUsuarioReportes
    {
        private static ServicioUsuarioReportes servicio = new ServicioUsuarioReportes("SQLConnectionString");

        public static int Guardar(Entity.UsuarioReportes usuarioReportes)
        {
            return servicio.Guardar(usuarioReportes);
        }

        public static void Actualizar(Entity.UsuarioReportes usuarioReportes)
        {
            servicio.Actualizar(usuarioReportes);
        }

        public static void EliminarPorUsuarioId(int usuarioId)
        {
            servicio.EliminarPorUsuarioId(usuarioId);
        }

        public static List<Entity.UsuarioReportes> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static List<Entity.UsuarioReportes> ObtenerPorUsuarioId(int usuarioId)
        {
            return servicio.ObtenerPorUsuarioId(usuarioId);
        }

        public static List<Entity.UsuarioReportes> ObtenerPorReporteId(int reporteId)
        {
            return servicio.ObtenerPorReporteId(reporteId);
        }

        public static Entity.UsuarioReportes ObtenerPorId(int id)
        {
            return servicio.ObtenerPorId(id);
        }

        public static Entity.UsuarioReportes ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerPorTrackingId(trackingId);
        }

        public static Entity.UsuarioReportes ObtenerPorUsuarioIdReporteId(int[] usuarioId_reporteId)
        {
            return servicio.ObtenerPorUsuarioIdReporteId(usuarioId_reporteId);
        }

        public static Entity.UsuarioReportes ObtenerPorUsuarioIdPantallaId(int[] usuarioId_pantallaId)
        {
            return servicio.ObtenerPorUsuarioIdPantallaId(usuarioId_pantallaId);
        }
    }
}
