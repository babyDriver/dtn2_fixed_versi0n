﻿using DataAccess.ListadoInformacionDetencionModal;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlListadoInformacionDetencionModal
    {
        private static ServicioListadoInformacionDetencionModal servicio = new ServicioListadoInformacionDetencionModal("SQLConnectionString");

        public static List<Entity.ListadoInformacionDetencionModal> ObtenerTodos(object[] data)
        {
            return servicio.ObtenerTodos(data);
        }
    }
}
