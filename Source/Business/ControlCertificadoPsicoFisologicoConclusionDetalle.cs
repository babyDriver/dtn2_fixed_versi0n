﻿using DataAccess.CertificadoPsicoFisiologicoConclusionDetalle;
using System.Collections.Generic;
using System;


namespace Business
{
    public class ControlCertificadoPsicoFisologicoConclusionDetalle
    {
        private static ServicioCertificadoPsicoFisiologicoConclusionDetalle servicio = new ServicioCertificadoPsicoFisiologicoConclusionDetalle("SQLConnectionString");

        public static int Guardar(Entity.CertificadoPsicoFisiologicoConclusionDetalle entity)
        {

            return servicio.Guardar(entity);

        }
        public static List< Entity.CertificadoPsicoFisiologicoConclusionDetalle> Obtener(int Id)
        {
            return servicio.ObtenerPorId(Id);
        }
    }
}
