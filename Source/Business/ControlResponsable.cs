﻿using DataAccess.ResponsableUnidad;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlResponsable
    {
        private static ServicioResponsable servicio = new ServicioResponsable("SQLConnectionString");

        public static Entity.Catalogo ObtenerPorUnidadIdEventoId(object[] parametros)
        {
            return servicio.ObtenerPorUnidadIdEventoId(parametros);
        }
    }
}
