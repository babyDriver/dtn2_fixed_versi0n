﻿using DataAccess.DescuentoAgravanteJuezCalificador;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlDescuentoAgravanteJuezCalificador
    {
        public static ServicioDescuentoAgravanteJuezCalificador servicio = new ServicioDescuentoAgravanteJuezCalificador("SQLConnectionString");
        public static List<Entity.Descuentosagravantesjuezcalificador> GetDescuentosagravantesjuezcalificador(string[] filtros)
        {
            return servicio.GetDescuentosagravantesjuezcalificador(filtros);
        }
    }
}
