﻿using DataAccess.Pantalla;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlPantalla
    {
        private static ServicioPantalla servicio = new ServicioPantalla("SQLConnectionString");

        public static int Guardar(Entity.Pantalla pantalla)
        {
            return servicio.Guardar(pantalla);
        }

        public static void Actualizar(Entity.Pantalla pantalla)
        {
            servicio.Actualizar(pantalla);
        }

        public static Entity.Pantalla ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.Pantalla> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static List<Entity.Pantalla> ObtenePorHabilitadoAcitvo(Entity.Pantalla pantalla)
        {
            return servicio.ObtenerByHabilitadoAcitvo(pantalla);
        }

        public static Entity.Pantalla ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }
    }
}
