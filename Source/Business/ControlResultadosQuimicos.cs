﻿using DataAccess.ResultadosQuimicos;
using System.Collections.Generic;
using System;

namespace Business
{
   public  class ControlResultadosQuimicos
    {
        private static ServicioResultadoQuimico servicio = new ServicioResultadoQuimico("SQLConnectionString");

        public static List<Entity.ResultadosQuimicos>ObtenerResultadosQuimicos()
        {
            return servicio.GetResultados();
        }
    }
}
