﻿using DataAccess.BocaLabioMenton;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlBocaLabioMenton
    {
        private static ServicioBocaLabioMenton servicio = new ServicioBocaLabioMenton("SQLConnectionString");

        public static int Guardar(Entity.BocaLabioMenton boca)
        {
            return servicio.Guardar(boca);
        }

        public static void Actualizar(Entity.BocaLabioMenton boca)
        {
            servicio.Actualizar(boca);
        }

        public static Entity.BocaLabioMenton ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.BocaLabioMenton> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.BocaLabioMenton ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.BocaLabioMenton ObtenerPorAntropometriaId(int id)
        {
            return servicio.ObtenerByAntropometriaId(id);
        }
    }
}
