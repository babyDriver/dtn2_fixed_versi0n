﻿using DataAccess.Evento;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlEvento
    {
        private static ServicioEvento servicio = new ServicioEvento("SQLConnectionString");

        private static ServicioFolio servicioFolio = new ServicioFolio("SQLConnectionString");

        public static void Actualizar(Entity.Evento item)
        {
            var tmp = ObtenerByTrackingId(item.TrackingId);
            item.Id = tmp.Id;
            servicio.Actualizar(item);
        }

        public static void Desactivar(Guid id)
        {
            var tmp = ObtenerByTrackingId(id);
            tmp.Activo = false;
            Actualizar(tmp);
        }

        public static int Guardar(Entity.Evento item)
        {
            return servicio.Guardar(item);
        }

        public static Entity.Evento ObtenerById(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static Entity.Evento ObtenerByTrackingId(Guid id)
        {
            return servicio.ObtenerByTrackingId(id);
        }

        public static List<Entity.Evento> ObtenerEventos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Contador ObtenerFolio(int contract)
        {
            return servicioFolio.ObtenerFolio(contract);
        }

        public static List<Entity.Evento> ObtenerPorLlamadaId(int id)
        {
            return servicio.ObtenerByLlamadaId(id);
        }

        public static List<Entity.Evento> ObtenerPorFechas(object[] parametros)
        {
            return servicio.ObtenerByFechas(parametros);
        }
    }
}
