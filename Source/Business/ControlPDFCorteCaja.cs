﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Configuration;

namespace Business
{
    public static class ControlPDFCorteCaja
    {
        //Crear directorio 
        private static string CrearDirectorioControl(string nombre)
        {
            try
            {

                //Verificar si existe el directorio principal del repositorio de documentos
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio de la obra existe
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio para el grupo documental existe, sino crearlo
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre)));


                return string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs//" + nombre);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("No fue posible crear el repositorio para crear el archivo. ", ex.Message));
            }
        }

        //Crear celdas
        public static void Celda(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas para tablas
        public static void CeldaParaTablas(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, BaseColor colorrelleno, float espacio, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                celda.BackgroundColor = colorrelleno;
                celda.Padding = espacio;
                celda.UseAscender = true;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas vacias
        public static void CeldaVacio(PdfPTable tabla, int cantidad)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                celda.BorderWidth = 0;
                tabla.AddCell(celda);
            }

        }



        //para crear el Header y Footer
        public partial class HeaderFooter : PdfPageEventHelper
        {
            PdfContentByte cb;
            PdfTemplate headerTemplate;
            BaseFont bf = null;
            float tamanofuente = 8f;

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                }
                catch (DocumentException de) { }
                catch (System.IO.IOException ioe) { }
            }

            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                //Encabezado de Página
                Rectangle pageSize = doc.PageSize;
                PdfPTable footerTbl = new PdfPTable(1);
                BaseColor blau = new BaseColor(0, 61, 122);

                float[] TablaAncho = new float[] { 100 };
                footerTbl.SetWidths(TablaAncho);
                footerTbl.TotalWidth = 550;
                Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                PdfPCell cell;
                CeldaParaTablas(footerTbl, FuenteCelda, " ", 1, 0, 1, blau, 0, Element.ALIGN_TOP, Element.ALIGN_TOP);


                string pagina = "Página " + writer.PageNumber + " de ";
                //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
                {
                    float margenbottom = 11f;
                    float margenright = 280f;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, tamanofuente);
                    cb.SetTextMatrix(doc.PageSize.GetBottom(margenright), doc.PageSize.GetBottom(margenbottom));
                    cb.ShowText(pagina);
                    cb.EndText();
                    float len = bf.GetWidthPoint(pagina, tamanofuente);
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));
                }

                PdfPTable footerTbl2 = new PdfPTable(2);


                float[] TablaAncho2 = new float[] { 100, 100 };
                footerTbl2.SetWidths(TablaAncho2);
                footerTbl2.TotalWidth = doc.Right - doc.Left;
                Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                string dia = String.Format("{0:dd}", DateTime.Now);
                string anio = String.Format("{0:yyyy}", DateTime.Now);
                string hora = String.Format("{0:HH:mm}", DateTime.Now);
                string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
                string fechahora = dia + " de " + mestexto + " de " + anio + " " + hora;
                Chunk chkFecha = new Chunk(fechahora, FuenteCelda);
                Phrase phrfecha = new Phrase(chkFecha);

                //Phrase promad = new Phrase("Promad Soluciones Computarizadas " + anio, FuenteCelda2);
                Phrase promad = new Phrase("", FuenteCelda2);
                cell = new PdfPCell(promad);
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);

                cell = new PdfPCell(phrfecha);
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);
                footerTbl2.WriteSelectedRows(0, -1, 5, 25, writer.DirectContent);
            }

            private static string ObtenerMesTexto(string mes)
            {
                string mestexto = "";
                switch (mes)
                {
                    case "01":
                        mestexto = "Enero";
                        break;
                    case "02":
                        mestexto = "Febrero";
                        break;
                    case "03":
                        mestexto = "Marzo";
                        break;
                    case "04":
                        mestexto = "Abril";
                        break;
                    case "05":
                        mestexto = "Mayo";
                        break;
                    case "06":
                        mestexto = "Junio";
                        break;
                    case "07":
                        mestexto = "Julio";
                        break;
                    case "08":
                        mestexto = "Agosto";
                        break;
                    case "09":
                        mestexto = "Septiembre";
                        break;
                    case "10":
                        mestexto = "Octubre";
                        break;
                    case "11":
                        mestexto = "Noviembre";
                        break;
                    case "12":
                        mestexto = "Diciembre";
                        break;
                    default:
                        mestexto = " ";
                        break;
                }
                return mestexto;
            }

            //Necesario para incluir pagina x de y al header
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);
                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, tamanofuente);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();
            }
        }                                             
        
        #region Recibo de corte de caja

        public static object[] generarReciboCorteDeCaja(object[] data, List<Entity.EgresoIngreso> ingresos, List<Entity.EgresoIngreso> egresos, List<Entity.Cancelacion> cancelaciones, decimal saldoInicial, string usuario, string logotipo)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //20 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Recibo_Caja_CorteDeCaja_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.CorteDeCaja"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD, blau);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD, blau);
                Font FuenteTituloTabla = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
                TituloReciboCorteDeCaja(documentoPdf, FuenteTitulo, FuenteNegrita12, data, logotipo);
                InformacionReciboCorteDeCaja(documentoPdf, FuenteNormal, FuenteTituloTabla, espacio, ingresos, egresos, cancelaciones, saldoInicial, usuario);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloReciboCorteDeCaja(Document documentoPdf, Font FuenteTitulo, Font FuenteSubtitulo, object[] data, string logotipo)
        {
            int[] w = new int[] { 2, 8 };
            PdfPTable table = new PdfPTable(2);
            table.SetWidths(w);
            table.TotalWidth = 550;
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell;

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo2 = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                logotipo2 = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo2);
           
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            catch (Exception)
            {
                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));

            }

            //Logo
            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_LEFT;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cellWithRowspan);

            //Titulo     
            Celda(table, FuenteTitulo, "     ", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            Celda(table, FuenteTitulo, "   Recibo de caja", 0, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);

            //Renglón vacio
            CeldaVacio(table, 3);

            //Nombre delegacion y usuario en barandilla
            BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita = new Font(fuentecalibri, 12f, Font.BOLD, blau);
            PdfPTable tablaDelegacionUsuario = new PdfPTable(1);
            float[] width = new float[] { 220 };
            tablaDelegacionUsuario.SetWidths(width);
            tablaDelegacionUsuario.DefaultCell.Border = Rectangle.NO_BORDER;
            var date = DateTime.Now;
            var h = date.Hour.ToString();
            if (date.Hour < 10) h = "0" + h;
            var m = date.Minute.ToString();
            if (date.Minute < 10) m = "0" + m;
            var s = date.Second.ToString();
            if (date.Second < 10) s = "0" + s;
            var fecha = date.ToShortDateString() + " " + h + ":" + m + ":" + s;
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Delegación: ", data[0].ToString(), 0, 0, 0);
            CeldaConChunk(tablaDelegacionUsuario, FuenteNegrita, FuenteNormal, "Fecha: ", fecha, 0, 0, 0);
            CeldaVacio(tablaDelegacionUsuario, 1);

            tablaDelegacionUsuario.WidthPercentage = 100;

            documentoPdf.Add(table);
            documentoPdf.Add(tablaDelegacionUsuario);
            table.DeleteBodyRows();
        }

        private static void InformacionReciboCorteDeCaja(Document documentoPdf, Font FuenteNormal, Font FuenteTituloTabla, float espacio, List<Entity.EgresoIngreso> ingresos, List<Entity.EgresoIngreso> egresos, List<Entity.Cancelacion> cancelaciones, decimal saldoInicial, string usuario)
        {
            BaseColor blau = new BaseColor(0, 61, 122);
            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormalAux = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegritaAux = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            PdfPTable tabla = new PdfPTable(6);//Numero de columnas de la tabla            
            tabla.WidthPercentage = 100; //Porcentaje ancho
            tabla.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            PdfPTable tablaEgresos = new PdfPTable(6);//Numero de columnas de la tabla            
            tablaEgresos.WidthPercentage = 100; //Porcentaje ancho
            tablaEgresos.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            PdfPTable tablaCancelaciones = new PdfPTable(6);//Numero de columnas de la tabla            
            tablaCancelaciones.WidthPercentage = 100; //Porcentaje ancho
            tablaCancelaciones.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            PdfPTable tablaTotales = new PdfPTable(6);
            tablaTotales.WidthPercentage = 100;
            tablaTotales.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPTable tablaTotalesEgreso = new PdfPTable(3);
            tablaTotalesEgreso.WidthPercentage = 100;
            tablaTotalesEgreso.HorizontalAlignment = Element.ALIGN_CENTER;                    

            BaseColor colorrelleno = new BaseColor(0, 61, 122);
            BaseColor colorblanco = new BaseColor(System.Drawing.Color.White);

            //Titulos de las columnas de las tablas
            CeldaParaTablas(tabla, FuenteTituloTabla, "Movimiento", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteTituloTabla, "Concepto", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteTituloTabla, "Folio", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteTituloTabla, "Fecha", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteTituloTabla, "Persona que paga", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabla, FuenteTituloTabla, "Subtotal", 1, 0, 1, colorrelleno, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);

            decimal totalIngresos = 0;
            decimal totalEgresos = 0;

            BaseColor colorgris = new BaseColor(246, 243, 242);
            BaseColor color = colorgris;
            int i = 1;
            //celdas, contenido de la tabla  
            foreach (var item in ingresos)
            {
                if (i % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                i++;
                var h = item.Fecha.Hour.ToString();
                if (item.Fecha.Hour < 10) h = "0" + h;
                var m = item.Fecha.Minute.ToString();
                if (item.Fecha.Minute < 10) m = "0" + m;
                var s = item.Fecha.Second.ToString();
                if (item.Fecha.Second < 10) s = "0" + s;
                var fecha = item.Fecha.ToShortDateString() + " " + h + ":" + m + ":" + s;
                CeldaParaTablas(tabla, FuenteNormal, "Ingreso", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, item.Concepto, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, item.Folio.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, fecha, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, item.PersonaQuePaga, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, "$" + item.Total.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                totalIngresos += item.Total;
            }

            foreach (var item in egresos)
            {
                if (i % 2 != 0)
                    color = colorgris;
                else color = colorblanco;

                i++;
                var h = item.Fecha.Hour.ToString();
                if (item.Fecha.Hour < 10) h = "0" + h;
                var m = item.Fecha.Minute.ToString();
                if (item.Fecha.Minute < 10) m = "0" + m;
                var s = item.Fecha.Second.ToString();
                if (item.Fecha.Second < 10) s = "0" + s;
                var fecha = item.Fecha.ToShortDateString() + " " + h + ":" + m + ":" + s;
                CeldaParaTablas(tabla, FuenteNormal, "Egreso", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, item.Concepto, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, item.Folio.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, fecha, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, item.PersonaQuePaga, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                CeldaParaTablas(tabla, FuenteNormal, "$" + item.Total.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_RIGHT);
                totalEgresos += item.Total;
            }

            CeldaVacio(tabla, 1);


            decimal ingresoscancelados = 0;
            decimal egresoscancelados = 0;
            foreach (var item in cancelaciones)
            {
                if (item.Tipo == "Ingreso")
                {
                    ingresoscancelados += item.Total;
                }
                else if (item.Tipo == "Egreso")
                {
                    egresoscancelados += item.Total;

                }
            }

            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);            
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Saldo inicial ", " $" + saldoInicial.ToString(), 0, 0, 0);

            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);           
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Ingresos ", " $" + totalIngresos.ToString(), 0, 0, 0);

            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Ingresos cancelados ", " $" +ingresoscancelados .ToString(), 0, 0, 0);

            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);            
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Egresos ", " $" + totalEgresos.ToString(), 0, 0, 0);

            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Egresos cancelados ", " $" + egresoscancelados.ToString(), 0, 0, 0);

            decimal saldoActual = (saldoInicial + (totalIngresos-ingresoscancelados)) - (totalEgresos-egresoscancelados);            

            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);            
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Saldo actual ", " $" + saldoActual.ToString(), 0, 0, 0);

            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);            
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "En caja ", " $" + saldoInicial.ToString(), 0, 0, 0);

            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);            
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "___________________________", 0, 0, 0);

            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);            
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Total entregado ", " $" + saldoActual.ToString(), 0, 0, 0);

            CeldaVacio(tablaTotalesEgreso, 3);

            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);            
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "___________________________", 0, 0, 0);

            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);            
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Entrega", "", 0, 0, 0);

            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);            
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", usuario, 0, 0, 0);

            CeldaVacio(tablaTotalesEgreso, 3);

            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);            
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "___________________________", 0, 0, 0);

            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "", "", 0, 0, 0);            
            CeldaConChunk(tablaTotalesEgreso, FuenteNegritaAux, FuenteNormalAux, "Recibe", "", 0, 0, 0);            

            documentoPdf.Add(tabla);            
            documentoPdf.Add(tablaEgresos);
            documentoPdf.Add(tablaTotalesEgreso);                        
            documentoPdf.NewPage();
            tabla.DeleteBodyRows();            
            tablaTotalesEgreso.DeleteBodyRows();            
            tablaEgresos.DeleteBodyRows();            
        }

        public static void CeldaConChunk(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, int bordo, int horizontal, int vertical)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk(valor, FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = horizontal;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }

        #endregion
    }
}
