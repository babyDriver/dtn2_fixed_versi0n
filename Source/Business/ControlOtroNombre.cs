﻿using DataAccess.OtroNombre;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlOtroNombre
    {
        private static ServicioOtroNombre servicio = new ServicioOtroNombre("SQLConnectionString");

        public static int Guardar(Entity.Otro_Nombre alias)
        {
            return servicio.Guardar(alias);
        }

        public static void Actualizar(Entity.Otro_Nombre general)
        {
            servicio.Actualizar(general);
        }

        public static Entity.Otro_Nombre ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.Otro_Nombre> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Otro_Nombre ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.Otro_Nombre ObtenerPorDetenidoId(int id)
        {
            return servicio.ObtenerByDetenidoId(id);
        }

        public static Entity.Otro_Nombre ObtenerPorParametros(string[] datos)
        {
            return servicio.ObtenerByParametros(datos);
        }
    }
}
