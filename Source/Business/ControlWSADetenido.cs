﻿using DataAccess.WSADetenido;

namespace Business
{
    public class ControlWSADetenido
    {
        private static ServicioWSADetenido servicio = new ServicioWSADetenido("SQLConnectionString");

        public static int Guardar(Entity.WSADetenido item)
        {
            return servicio.Guardar(item);
        }
    }
}
