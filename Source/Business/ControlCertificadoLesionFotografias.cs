﻿using DataAccess.CertificadoLesionFotografias;

namespace Business
{
    public class ControlCertificadoLesionFotografias
    {
        private static ServicioCertificadoLesionFotografias servicio = new ServicioCertificadoLesionFotografias("SQLConnectionString");
        public static int Guardar(Entity.CertificadoLesionFotografias certificadoLesionFotografias)
        {
            return servicio.Guardar(certificadoLesionFotografias);
        }

        public static Entity.CertificadoLesionFotografias ObtenerPorId(int Id)
        {
            return servicio.ObtenerPorId(Id);
        }
    }
}
