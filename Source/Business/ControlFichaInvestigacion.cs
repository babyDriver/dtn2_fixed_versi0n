﻿using DataAccess.FichaInvestigacion;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlFichaInvestigacion
    {
        public static ServicioFichaInvestigacion servicio = new ServicioFichaInvestigacion("SQLConnectionString");

        public static int Guardar(Entity.FichaInvestigacion Ficha)
        {
            return servicio.Guardar(Ficha);
        }
    }
}
