﻿using DataAccess.Egreso;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlEgreso
    {
        private static ServicioEgreso servicio = new ServicioEgreso("SQLConnectionString");

        public static int Guardar(Entity.Egreso egreso)
        {
            return servicio.Guardar(egreso);
        }

        public static void Actualizar(Entity.Egreso egreso)
        {
            servicio.Actualizar(egreso);
        }

        public static Entity.Egreso ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.Egreso> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Egreso ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.Egreso ObtenerPorEstatusInternoId(int id)
        {
            return servicio.ObtenerByEstatusInternoId(id);
        }
    }
}
