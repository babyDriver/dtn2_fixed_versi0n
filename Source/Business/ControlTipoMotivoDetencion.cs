﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Catalogo;
using DataAccess.TipoMotivoDetencion;
namespace Business
{
  public static  class ControlTipoMotivoDetencion
    {
        private static ServicioTipoMotivoDetencion servicio = new ServicioTipoMotivoDetencion("SQLConnectionString");
        public static List<Entity.TipoMotivoDetencion> ObtenerTipos()
        {

            return servicio.ObtenerTpoMotivoDetencion();
        }

    }
}
