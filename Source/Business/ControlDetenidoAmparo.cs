﻿using DataAccess.DetenidoAmparo;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlDetenidoAmparo
    {
        private static ServicioDetenidoAmparo servicio = new ServicioDetenidoAmparo("SQLConnectionString");

        public static int Guardar(Entity.DetenidoAmparo item)
        {
            return servicio.Guardar(item);

        }
        public static void Actualizar(Entity.DetenidoAmparo item)
        {
            servicio.Actualizar(item);
        }
        public static Entity.DetenidoAmparo ObtenerPorDetalleDetencionId( int DetalleDetencionId)
        {
            return servicio.ObtenerPorDetalleDetencionId(DetalleDetencionId);
        }

        public static Entity.DetenidoAmparo ObtenerPorId(int Id)
        {
            return servicio.ObtenerPorId(Id);
        }
        public static Entity.DetenidoAmparo ObtenerPorTrackingId(Guid TrackingId)
        {
            return servicio.ObtenerPorTrackingId(TrackingId);
        }
    }
}
