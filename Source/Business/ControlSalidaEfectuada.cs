﻿using DataAccess.SalidaEfectuada;
using System.Collections.Generic;
using System;
namespace Business
{
   public class ControlSalidaEfectuada
    {
        private static ServicioSalidaEfectuada servicio = new ServicioSalidaEfectuada("SQLConnectionString");

        public static int Guardar(Entity.SalidaEfectuada salidaEfectuada)
        {
            return servicio.Guardar(salidaEfectuada);
        }
        
        public static Entity.SalidaEfectuada ObtenerByDetalledetencionId( int DetalleDetencionId)
        {
            return servicio.ObtenerByDetalleDetencionId(DetalleDetencionId);
        }
    }
}
