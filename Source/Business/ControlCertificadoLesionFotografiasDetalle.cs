﻿using DataAccess.CertificadoLesionFotografiasDetalle;
using System.Collections.Generic;

namespace Business
{
    public class ControlCertificadoLesionFotografiasDetalle
    {
        private static ServicioCertificadoLesionFotografiasDetalle servicio = new ServicioCertificadoLesionFotografiasDetalle("SQLConnectionString");

        public static int Guardar(Entity.CertificadoLesionFotografiasDetalle certificadoLesionFotografiasDetalle)
        {
            return servicio.Guardar(certificadoLesionFotografiasDetalle);
        }

        public static void Actualizar(Entity.CertificadoLesionFotografiasDetalle certificadoLesionFotografiasDetalle)
        {
            servicio.Actualizar(certificadoLesionFotografiasDetalle);
        }

        public static List<Entity.CertificadoLesionFotografiasDetalle> ObtenerPorCertificadoLesionFotografiaId(int Id)
        {
            return servicio.ObtenerPorCertificadoLesionFotografiaId(Id);
        }

        public static Entity.CertificadoLesionFotografiasDetalle ObtenerPorId(int Id)
        {
            return servicio.ObtenerPorId(Id);
        }
    }
}
