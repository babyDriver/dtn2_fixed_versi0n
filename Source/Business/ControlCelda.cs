﻿using DataAccess.Celda;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlCelda
    {
        private static ServicioCelda servicio = new ServicioCelda("SQLConnectionString");

        public static int Guardar(Entity.Celda Celda)
        {
            return servicio.Guardar(Celda);
        }

        public static void Actualizar(Entity.Celda Celda)
        {
            servicio.Actualizar(Celda);
        }

        public static Entity.Celda ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.Celda> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Celda ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.Celda ObtenerPorPeligrosidad(int id)
        {
            return servicio.ObtenerByPeligrosidad(id);
        }

        public static Entity.Celda ObtenerPorNombre(string nombre)
        {
            return servicio.ObtenerByNombre(nombre);
        }

    }

}