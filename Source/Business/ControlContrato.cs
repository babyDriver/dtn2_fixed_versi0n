﻿using DataAccess.Contrato;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlContrato
    {
        private static ServicioContrato servicio = new ServicioContrato("SQLConnectionString");

        public static List<Entity.Contrato> ObtenerPorCliente(int id)
        {
            return servicio.ObtenerByCliente(id);
        }        

        public static Entity.Contrato ObtenerPorTrackingId(Guid guid)
        {
            return servicio.ObtenerByTrackingId(guid);
        }        

        public static List<Entity.Contrato> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Contrato ObtenerPorClienteYContrato(object[] data)
        {
            return servicio.ObtenerByClienteAndContrato(data);
        }

        public static int Guardar(Entity.Contrato contrato)
        {
            return servicio.Guardar(contrato);
        }

        public static void Actualizar(Entity.Contrato contrato)
        {
            servicio.Actualizar(contrato);
        }

        public static Entity.Contrato ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }
        public static Entity.Contrato Obtenerporsubcontrato(int subcontratoid)
        {
            return servicio.ObtenerbySubcontrato(subcontratoid);

        }
        public static List<Entity.Contrato> ObetenerPorUsuarioId(int UsuarioId)
        {
            return servicio.ObtenerporUsuarioId(UsuarioId);
        }
    }
}
