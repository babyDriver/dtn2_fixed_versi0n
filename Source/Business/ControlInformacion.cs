﻿using DataAccess.Informacion;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlInformacion
    {
        private static ServicioInformacion servicio = new ServicioInformacion("SQLConnectionString");

        public static int Guardar(Entity.Informacion informacion)
        {
            return servicio.Guardar(informacion);
        }

        public static void Actualizar(Entity.Informacion informacion)
        {
            servicio.Actualizar(informacion);
        }

        public static Entity.Informacion ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.Informacion> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Informacion ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.Informacion ObtenerPorDetenidoId(int id)
        {
            return servicio.ObtenerByDetenidoId(id);
        }

        public static Entity.Informacion ObtenerPorProcesoId(int id)
        {
            return servicio.ObtenerByProcesoId(id);
        }

    }
}
