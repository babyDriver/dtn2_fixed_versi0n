﻿using DataAccess.ReporteExtravioList;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlReporteExtravioList
    {
        private static ServicioReporteExtravioList servicio = new ServicioReporteExtravioList("SQLConnectionString");

        public static List<Entity.ReporteExtravioList> ObtenerTodos(object[] data)
        {
            return servicio.ObtenerTodos(data);
        }
    }
}
