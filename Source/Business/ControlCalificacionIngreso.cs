﻿using DataAccess.CalificacionIngreso;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlCalificacionIngreso
    {
        private static ServicioCalificacionIngreso servicio = new ServicioCalificacionIngreso("SQLConnectionString");

        public static int Guardar(Entity.CalificacionIngreso item)
        {
            return servicio.Guardar(item);
        }

        public static Entity.CalificacionIngreso ObtenerPorTrackingId(Guid guid)
        {
            return servicio.ObtenerByTrackingId(guid);
        }

        public static Entity.CalificacionIngreso ObtenerPorCalificacionId(int id)
        {
            return servicio.ObtenerByCalificacionId(id);
        }

        public static Entity.CalificacionIngreso ObtenerFolio(object[] parametros)
        {
            return servicio.ObtenerFolio(parametros);
        }
        public static void Actualizar(Entity.CalificacionIngreso item)
        {
            servicio.Actualizar(item);

        }
    }
}
