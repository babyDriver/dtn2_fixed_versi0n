﻿using DataAccess.OrejaHelix;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlOrejaHelix
    {
        private static ServicioOrejaHelix servicio = new ServicioOrejaHelix("SQLConnectionString");

        public static int Guardar(Entity.OrejaHelix oreja)
        {
            return servicio.Guardar(oreja);
        }

        public static void Actualizar(Entity.OrejaHelix oreja)
        {
            servicio.Actualizar(oreja);
        }

        public static Entity.OrejaHelix ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.OrejaHelix> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.OrejaHelix ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.OrejaHelix ObtenerPorAntropometriaId(int id)
        {
            return servicio.ObtenerByAntropometriaId(id);
        }
    }
}
