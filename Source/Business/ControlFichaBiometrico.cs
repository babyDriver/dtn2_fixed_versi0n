﻿using DataAccess.FichaBiometrico;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlFichaBiometrico
    {
        public static ServicioFichaBiometrico servicio = new ServicioFichaBiometrico("SQLConnectionString");

        public static Entity.Fichabiometrico ObtenerFichaBiometrico(int DetenidoId)
        {

            return servicio.GetFichaBiometrico(DetenidoId);
        }


    }
}
