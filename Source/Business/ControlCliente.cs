﻿using DataAccess.Cliente;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlCliente
    {
        private static ServicioCliente servicio = new ServicioCliente("SQLConnectionString");

        public static List<Entity.Cliente> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Cliente ObtenerPorNombreYRfc(object[] datos)
        {
            return servicio.ObtenerPorNombreYRfc(datos);
        }

        public static int Guardar(Entity.Cliente cliente)
        {
            return servicio.Guardar(cliente);
        }

        public static Entity.Cliente ObtenerPorTrackingId(Guid tracking)
        {
            return servicio.ObtenerByTrackingId(tracking);
        }

        public static Entity.Cliente ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static void Actualizar(Entity.Cliente cliente)
        {
            servicio.Actualizar(cliente);
        }
    }
}
