﻿using DataAccess.ServicioMedico;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlServicoMedico
    {
        private static ServicioServicioMedico servicio = new ServicioServicioMedico("SQLConnectionString");

        public static void Actualizar(Entity.ServicioMedico servicioMedico)
        {
            servicio.Actualizar(servicioMedico);
        }

        public static int Guardar(Entity.ServicioMedico servicioMedico)
        {
            return servicio.Guardar(servicioMedico);
        }

        public static List<Entity.ServicioMedico> ObtenerTodo()
        {
            return servicio.ObtenerTodos();
        }
        public static Entity.ServicioMedico ObtenerPorId(int Id)
        {
            return servicio.ObtenerById(Id);
        }

        public static Entity.ServicioMedico ObtenerPorTrackingId(string TrackingId)
        {

            return servicio.ObtenerByTrackingId(TrackingId);
        }
        public static List<Entity.ServicioMedico>ObtenerPorDetalleDetencionId(int DetalledetencionId)
        {
            return servicio.ObtenerPorDetalleDetencionId(DetalledetencionId);
        }

    }
}
