﻿using System;
using DataAccess.Link;
using System.Collections.Generic;

namespace Business
{
    public class ControlLink
    {
        private static ServicioLink servicio = new ServicioLink("SQLConnectionString");

        public static Entity.Link ObtenerPorSubcontatoId(int id)
        {
            return servicio.ObtenerPorSubcontrato(id);
        }
    }
}
