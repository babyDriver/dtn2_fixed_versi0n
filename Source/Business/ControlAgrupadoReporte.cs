﻿using DataAccess.AgrupadoReporte;
using System.Collections.Generic;
using System;

namespace Business
{
   
    public class ControlAgrupadoReporte
    {
        private static ServicioAgrupado servicio = new ServicioAgrupado("SQLConnectionString");

        public static List<Entity.AgrupadoReporte>GetByRangoFechas(DateTime[] Fecha)
        {

            return servicio.GetByrangoFechas(Fecha);
        }

    }
}
