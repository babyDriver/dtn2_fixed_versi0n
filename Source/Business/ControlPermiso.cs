﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using DataAccess.PermisoDefault;

namespace Business
{
    public static class ControlPermiso
    {
        private static ServicioPermisoDefault servicio = new ServicioPermisoDefault("SQLConnectionString");

        public static Entity.Permiso ObtenerPermisos(string rol)
        {
            var permisos = new Entity.Permiso();
            int permisoId = 0;
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

            try {
                SqlCommand command = new SqlCommand("[PermisoRol_GetByRol_SP]", sqlConnection);
                command.CommandTimeout = 0;
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Rol", rol);
                          
                SqlDataReader dr;
                sqlConnection.Open();
                dr = command.ExecuteReader();
                
                while (dr.Read())
                {
                    permisoId = Convert.ToInt32(dr["PermisoId"]);
                    switch (permisoId)
                    {
                        case 1:
                            permisos.AgregarContrato = Convert.ToBoolean(dr["Permiso"]);
                            break;
                        case 2:
                            permisos.Agregar = Convert.ToBoolean(dr["Permiso"]);
                            break;
                        case 3:
                            permisos.Modificar = Convert.ToBoolean(dr["Permiso"]);
                            break;
                        case 4:
                            permisos.Eliminar = Convert.ToBoolean(dr["Permiso"]);
                            break;
                    }
                }
                
                return permisos;
                
            }
            catch (Exception ex) { return null; }
            finally { if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close(); }
        }

        public static Entity.Permiso ObtenerPermisosPorPantalla(string rol, int pantallaid)
        {
            var permisos = new Entity.Permiso();
            int permisoId = 0;
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

            try
            {
                SqlCommand command = new SqlCommand("[PermisoPantalla_GetByRolPantallaId_SP]", sqlConnection);
                command.CommandTimeout = 0;
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Rol", rol);
                command.Parameters.AddWithValue("@PantallaId", pantallaid);

                SqlDataReader dr;
                sqlConnection.Open();
                dr = command.ExecuteReader();
               
                while (dr.Read())
                {
                    permisoId = Convert.ToInt32(dr["PermisoId"]);

                    switch (permisoId)
                    {
                        case 1:
                            permisos.AgregarContrato = Convert.ToBoolean(dr["Permiso"]);
                            break;
                        case 2:
                            permisos.Agregar = Convert.ToBoolean(dr["Permiso"]);
                            break;
                        case 3:
                            permisos.Modificar = Convert.ToBoolean(dr["Permiso"]);
                            break;
                        case 4:
                            permisos.Eliminar = Convert.ToBoolean(dr["Permiso"]);
                            break;
                    }
                }
               
               return permisos;

            }
            catch (Exception ex) { return null; }
            finally { if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close(); }
        }

        public static Entity.Permiso ObtenerPermisos(string rol, int pantallaid)
        {
            var permisos = new Entity.Permiso();
            permisos.Rol = rol;
            var permisosrol = ControlPermiso.ObtenerPermisos(rol);
            var permisospantalla = ControlPermiso.ObtenerPermisosPorPantalla(rol, pantallaid);

            if (permisospantalla != null)
            {
                if (permisospantalla.AgregarContrato) permisos.AgregarContrato = permisospantalla.AgregarContrato;
                if (permisospantalla.Agregar) permisos.Agregar = permisospantalla.Agregar;
                if (permisospantalla.Modificar) permisos.Modificar = permisospantalla.Modificar;
                if (permisospantalla.Eliminar) permisos.Eliminar = permisospantalla.Eliminar;
            }

            if (permisosrol != null)
            {
                if (permisosrol.AgregarContrato) permisos.AgregarContrato = permisosrol.AgregarContrato;
                if (permisosrol.Agregar) permisos.Agregar = permisosrol.Agregar;
                if (permisosrol.Modificar) permisos.Modificar = permisosrol.Modificar;
                if (permisosrol.Eliminar) permisos.Eliminar = permisosrol.Eliminar;
            }

            return permisos;
        }

        public static List<Entity.PermisoDefault> ObtenerPermisosPorDefault(int rolId)
        {
            return servicio.ObtenerPermisoByRol(rolId);
        }

        //public static List<Entity.PermisoDefault> ObtenerPermisosPorDefault(int rolId)
        //{
        //    var list_perm = new List<Entity.PermisoDefault>();
        //    MySqlConnection sqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

        //    try
        //    {
        //        MySqlCommand command = new MySqlCommand("rol_permisodefault_GetByRol_SP", sqlConnection);
        //        command.CommandTimeout = 0;
        //        command.CommandType = System.Data.CommandType.StoredProcedure;
        //        command.Parameters.AddWithValue("_rolId", rolId);

        //        MySqlDataReader dr;
        //        sqlConnection.Open();
        //        dr = command.ExecuteReader();
        //        List<Object> list = new List<Object>();

        //        while (dr.Read())
        //        {
        //            var perm = new Entity.PermisoDefault();
        //            perm.Activo = true;
        //            perm.Habilitado = true;
        //            perm.PantallaId = Convert.ToInt32(dr["PantallaId"]);
        //            perm.PermisoId = Convert.ToInt32(dr["PermisoId"]);
        //            list_perm.Add(perm);
        //        }

        //        return list_perm;
        //    }
        //    catch (Exception ex) { return null; }
        //    finally { if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close(); }
        //}
    }
}
