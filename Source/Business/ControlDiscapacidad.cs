﻿using DataAccess.Discapacidad;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlDiscapacidad
    {
        private static ServicioDiscapacidad servicio = new ServicioDiscapacidad("SQLConnectionString");

        public static int Guardar(Entity.Discapacidad item)
        {
            return servicio.Guardar(item);
        }

        public static void Actualizar(Entity.Discapacidad item)
        {
            servicio.Actualizar(item);
        }

        public static Entity.Discapacidad ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static Entity.Discapacidad ObtenerPorNombre(string nombre, int tipoDiscapacidadId)
        {
            object[] param = new object[2];
            param[0] = nombre;
            param[1] = tipoDiscapacidadId;
            return servicio.ObtenerByNombreTipo(param);
        }

        public static List<Entity.Discapacidad> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Discapacidad ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }
    }
}
