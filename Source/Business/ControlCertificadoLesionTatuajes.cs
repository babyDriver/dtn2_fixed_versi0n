﻿using DataAccess.CertificadoLesionTatuaje;
using System.Collections.Generic;
using System;


namespace Business
{
    public class ControlCertificadoLesionTatuajes
    {
        private static ServicioCertificadoLesionTatuaje servicio = new ServicioCertificadoLesionTatuaje("SQLConnectionString");
        public static int Guardar(Entity.CertificadoLesionTatuajes certificadoLesionTatuajes)
        {
            return servicio.Guardar(certificadoLesionTatuajes);
        }
        public static Entity.CertificadoLesionTatuajes ObtenerPorId(int Id)
        {
            return servicio.ObtenerPorId(Id);

        }
    }
}
