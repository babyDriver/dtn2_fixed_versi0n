﻿using DataAccess.CeldaDetalle;
using System.Collections.Generic;
using System;
namespace Business
{
    public  class ControlCeldaDetalle
    {
        private static ServicioCeldaDetalle servicio = new ServicioCeldaDetalle("SQLConnectionString");

        public static List<Entity.DetalleCelda> GetDetall(int Id)
        {
            return servicio.ObtenerDetallesById(Id);
        }
    }
    
}
