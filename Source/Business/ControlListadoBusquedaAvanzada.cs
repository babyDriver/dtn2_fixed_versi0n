﻿using DataAccess.ListadoBusquedaAvanzada;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlListadoBusquedaAvanzada
    {
        private static ServicioListadoBusquedaAvanzada servicio = new ServicioListadoBusquedaAvanzada("SQLConnectionString");

        public static List<Entity.ListadoBusquedaAvanzada> ObtenerTodos(object[] data)
        {
            return servicio.ObtenerTodos(data);
        }
    }
}
