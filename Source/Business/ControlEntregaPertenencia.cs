﻿using DataAccess.EntregaPertenencia;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlEntregaPertenencia
    {
        private static ServicioEntregaPertenencia servicio = new ServicioEntregaPertenencia("SQLConnectionString");

        public static List<Entity.EntregaPertenencia> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }
    }
}
