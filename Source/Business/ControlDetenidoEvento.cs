﻿using DataAccess.DetenidoEvento;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlDetenidoEvento
    {
        private static ServicioDetenidoEvento servicio = new ServicioDetenidoEvento("SQLConnectionString");

        public static void Actualizar(Entity.DetenidoEvento detenido)
        {
            servicio.Actualizar(detenido);
        }

        public static int Guardar(Entity.DetenidoEvento detenido)
        {
          return  servicio.Guardar(detenido);
        }

        public static List<Entity.DetenidoEvento> ObtenerPorEventoId(int id)
        {
            return servicio.ObtenerByEventoId(id);
        }

        public static List<Entity.DetenidoEvento> ObtenersiregistroPorEventoId(int id)
        {
            return servicio.ObtenersinregistroBByEventoId(id);
        }

        public static Entity.DetenidoEvento ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }
    }
}
