﻿using DataAccess.Biometrico;
using System.Collections.Generic;
using System.Configuration;
using System;
using System.Web.Security;
using System.Web;

namespace Business
{
    public class ControlBiometricos
    {
        private static ServicioBiometrico servicio = new ServicioBiometrico("SQLConnectionString");

        public static int Guardar(Entity.Biometrico biometrico)
        {
            return servicio.Guardar(biometrico);
        }

        public static void Actualizar(Entity.Biometrico biometrico)
        {
            servicio.Actualizar(biometrico);

        }

        public static List<Entity.Biometrico> GetAll()
        {
            return servicio.GetAll();
        }
        public static Entity.Biometrico GetById(int Id)
        {
            return servicio.GetById(Id);
        }

        public static List<Entity.Biometrico> GetByDetenidoId(int DetenidoId)
        {
            return servicio.GetByDetenidoId(DetenidoId);
        }


        public static string GuardarRutasBiometrico(int DetenidoId)
        {
            string ruta = "";

            //if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
            ///Faciales
            bool Valido = true;
            var detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(new string[] { DetenidoId.ToString(), "true" });
            if (detalleDetencion == null) throw new Exception("No puede modificar la información del detenido");
            ruta = string.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/biometrias/"+detalleDetencion.ContratoId+"/" + detalleDetencion.DetenidoId + "/");
            string pathfoto = ruta;//System.Web.HttpContext.Current.Server.MapPath(ruta);
            Valido = ValidaRutas(System.Web.HttpContext.Current.Server.MapPath(ruta));
            List <Entity.Biometrico> ListaBiometricos = new List<Entity.Biometrico>();
            ListaBiometricos = GetListaTosave(pathfoto, DetenidoId);

            if (Valido)
            {
                foreach (var item in ListaBiometricos)
                {
                    ControlBiometricos.Guardar(item);
                }
            }
            else
            {              

               return "No hay biométricos registrados para este detenido";
            }

            return "OK";
        }
        
        private static List<Entity.Biometrico> GetListaTosave(string pathfoto,int DetenidoId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

            List<Entity.Biometrico> ListaBiometricos = new List<Entity.Biometrico>();

            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Facial ", usId, "Imagen facial frontal", 1, "Face_0.jpg", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Facial ", usId, "Imagen facial perfil derecho", 1, "Face_1.jpg", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Facial ", usId, "Imagen facial perfil izquierdo", 1, "Face_2.jpg", DetenidoId));

            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Iris ", usId, "Imagen iris izquierdo", 2, "Iris_1.jpg", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Iris ", usId, "Imagen iris derecho", 2, "Iris_2.jpg", DetenidoId));

            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella ", usId, "Pulgar derecho plana", 3, "Finger_0_1.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella ", usId, "Indice derecho plana", 3, "Finger_0_2.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella ", usId, "Medio derecho plana", 3, "Finger_0_3.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella ", usId, "Anular derecho plana", 3, "Finger_0_4.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella ", usId, "Meñique derecho plana", 3, "Finger_0_5.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella ", usId, "Pulgar izquierdo plana", 3, "Finger_0_6.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella ", usId, "Indice izquierdo plana", 3, "Finger_0_7.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella ", usId, "Medio izquierdo plana", 3, "Finger_0_8.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella ", usId, "Anular izquierdo plana", 3, "Finger_0_9.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella ", usId, "Meñique izquierdo plana", 3, "Finger_0_10.bmp", DetenidoId));


            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella rolada", usId, "Pulgar derecho rolada",    4, "Finger_1_1.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella rolada", usId, "Indice derecho rolada",    4, "Finger_1_2.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella rolada", usId, "Medio derecho rolada",     4, "Finger_1_3.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella rolada", usId, "Anular derecho rolada",    4, "Finger_1_4.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella rolada", usId, "Meñique derecho rolada",   4, "Finger_1_5.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella rolada", usId, "Pulgar izquierdo rolada",  4, "Finger_1_6.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella rolada", usId, "Indice izquierdo rolada",  4, "Finger_1_7.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella rolada", usId, "Medio izquierdo rolada",   4, "Finger_1_8.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella rolada", usId, "Anular izquierdo rolada",  4, "Finger_1_9.bmp", DetenidoId));
            ListaBiometricos.Add(GeneraBiometrico(pathfoto, "Huella rolada", usId, "Meñique izquierdo rolada", 4, "Finger_1_10.bmp", DetenidoId));

            return ListaBiometricos;
        }

        private static Entity.Biometrico GeneraBiometrico(string Pathfoto,string Fotoname,int usuarioId,string Descripcion,int TipoBiometricoId,string Clave,int DetenidoId)
            {
                Entity.Biometrico biometrico = new Entity.Biometrico();
            biometrico.TrackingId = Guid.NewGuid().ToString();
            biometrico.Nombre = Fotoname;
            biometrico.Descripcion = Descripcion;
            biometrico.TipobiometricoId = TipoBiometricoId;
            biometrico.Clave = Clave;
            biometrico.Rutaimagen = Pathfoto+Clave;
            biometrico.DetenidoId = DetenidoId;
            biometrico.Activo = 1;
            biometrico.Habilitado = 1;
            biometrico.Creadopor = usuarioId;
            biometrico.FechaHora = DateTime.Now;
            return biometrico;







            }


        private static Boolean ValidaRutas(string pathfoto)
        {
            int contador = 0;
            if (System.IO.File.Exists(pathfoto + "Face_0.jpg"))
            {
                contador++;

            }

            if (System.IO.File.Exists(pathfoto + "Face_1.jpg"))
            {
                contador++;

            }

            if (System.IO.File.Exists(pathfoto + "Face_2.jpg"))
            {
                contador++;

            }

            ///
            ///Iris

            if (System.IO.File.Exists(pathfoto + "Iris_1.jpg"))
            {
                contador++;

            }

            if (System.IO.File.Exists(pathfoto + "Iris_2.jpg"))
            {
                contador++;

            }

            /// 

            /// huellas planas

            if (System.IO.File.Exists(pathfoto + "Finger_0_1.bmp"))
            {
                contador++;

            }

            if (System.IO.File.Exists(pathfoto + "Finger_0_2.bmp"))
            {
                contador++;

            }

            if (System.IO.File.Exists(pathfoto + "Finger_0_3.bmp"))
            {
                contador++;

            }
            if (System.IO.File.Exists(pathfoto + "Finger_0_4.bmp"))
            {
                contador++;

            }
            if (System.IO.File.Exists(pathfoto + "Finger_0_5.bmp"))
            {
                contador++;

            }
            if (System.IO.File.Exists(pathfoto + "Finger_0_6.bmp"))
            {
                contador++;

            }
            if (System.IO.File.Exists(pathfoto + "Finger_0_7.bmp"))
            {
                contador++;

            }
            if (System.IO.File.Exists(pathfoto + "Finger_0_8.bmp"))
            {
                contador++;

            }
            if (System.IO.File.Exists(pathfoto + "Finger_0_9.bmp"))
            {
                contador++;

            }
            if (System.IO.File.Exists(pathfoto + "Finger_0_10.bmp"))
            {
                contador++;

            }

            /// 

            /// huellas roladas

            if (System.IO.File.Exists(pathfoto + "Finger_1_1.bmp"))
            {
                contador++;

            }

            if (System.IO.File.Exists(pathfoto + "Finger_1_2.bmp"))
            {
                contador++;

            }

            if (System.IO.File.Exists(pathfoto + "Finger_1_3.bmp"))
            {
                contador++;

            }
            if (System.IO.File.Exists(pathfoto + "Finger_1_4.bmp"))
            {
                contador++;

            }
            if (System.IO.File.Exists(pathfoto + "Finger_1_5.bmp"))
            {
                contador++;

            }
            if (System.IO.File.Exists(pathfoto + "Finger_1_6.bmp"))
            {
                contador++;

            }
            if (System.IO.File.Exists(pathfoto + "Finger_1_7.bmp"))
            {
                contador++;

            }
            if (System.IO.File.Exists(pathfoto + "Finger_1_8.bmp"))
            {
                contador++;

            }
            if (System.IO.File.Exists(pathfoto + "Finger_1_9.bmp"))
            {
                contador++;

            }
            if (System.IO.File.Exists(pathfoto + "Finger_1_10.bmp"))
            {
                contador++;

            }

            if (contador == 0)
            {
                return false;
            }
            return true;
            /// 

        }



    }
}
