﻿using DataAccess.InformeUsoFuerza;
using System.Collections.Generic;
using System.Configuration;
using System;
using System.Web.Security;
using System.Web;
namespace Business
{
   public class ControlInformeUsoFuerza
    {
        private static ServicioInformeUsofuerza servicio = new ServicioInformeUsofuerza("SQLConnectionString");
        public static int Guardar(Entity.InformeUsoFuerza informeUsoFuerza)
        {
            return servicio.Guardar(informeUsoFuerza);
        }

        public static void Actualizar(Entity.InformeUsoFuerza informeUsoFuerza)
        {
            servicio.Actualizar(informeUsoFuerza);

        }

        public static List<Entity.InformeUsoFuerza> GetAll()
        {
            return servicio.GetAll();
        }
        public static Entity.InformeUsoFuerza GetById(int Id)
        {
            return servicio.GetbyId(Id);
        }

    }
}
