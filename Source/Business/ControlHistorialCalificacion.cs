﻿using DataAccess.HistorialCalificacion;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlHistorialCalificacion
    {
        private static ServicioHistorialCalificacion servicio = new ServicioHistorialCalificacion("SQLConnectionString");

        public static int Guardar(Entity.HistorialCalificacion item)
        {
            return servicio.Guardar(item);
        }

        public static Entity.HistorialCalificacion ObtenerPorId(int id)
        {
            return servicio.ObtenerPorId(id);
        }

        public static List<Entity.HistorialCalificacion> ObtenerTodosPorInternoId(int id)
        {
            return servicio.ObtenerTodosPorInternoId(id);
        }
        public static void Actulizar(Entity.HistorialCalificacion item)
        {

             servicio.Actualizar(item);
        }
    }
}
