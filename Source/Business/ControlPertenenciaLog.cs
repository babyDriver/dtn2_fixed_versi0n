﻿using System.Collections.Generic;
using System;
using DataAccess.PertenenciaLog;

namespace Business
{
    public class ControlPertenenciaLog
    {
        private static ServicioPertenenciaLog servicio = new ServicioPertenenciaLog("SQLConnectionString");

        public static Entity.PertenenciaLog ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }
    }
}
