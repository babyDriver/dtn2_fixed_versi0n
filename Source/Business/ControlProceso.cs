﻿using DataAccess.Proceso;
using System.Collections.Generic;
using System;

namespace Business
{
   public class ControlProceso
    {
        private static ServicioProceso servicio = new ServicioProceso("SQLConnectionString");

        public static List<Entity.Proceso>GetAll()
            {
            return servicio.ObtenerTodo();
        }
    }
}
