﻿using DataAccess.ListadoInformacionDetencion;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlListadoInformacionDetencion
    {
        private static ServicioListadoInformacionDetencion servicio = new ServicioListadoInformacionDetencion("SQLConnectionString");

        public static List<Entity.ListadoInformacionDetencion> ObtenerTodos(object[] data)
        {
            return servicio.ObtenerTodos(data);
        }
    }
}
