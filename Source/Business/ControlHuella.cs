﻿using DataAccess.Huella;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlHuella
    {
        private static ServicioHuella servicio = new ServicioHuella("SQLConnectionString");

        public static int Guardar(Entity.Huella huella)
        {
            return servicio.Guardar(huella);
        }

        public static void Actualizar(Entity.Huella huella)
        {
            servicio.Actualizar(huella);
        }

        public static List<Entity.Huella> ObtenerPorDetenidoId(int id)
        {
            return servicio.ObtenerByDetenidoId(id);
        }

        public static Entity.Huella ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }
    }
}
