﻿using DataAccess.Observacion;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlObservacion
    {
        private static ServicioObservacion servicio = new ServicioObservacion("SQLConnectionString");

        public static List<Entity.Observacion> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static int Guardar(Entity.Observacion item)
        {
            return servicio.Guardar(item);
        }

        public static Entity.Observacion ObtenerPorTrackingId(Guid tracking)
        {
            return servicio.ObtenerByTrackingId(tracking);
        }

        public static Entity.Observacion ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static void Actualizar(Entity.Observacion item)
        {
            servicio.Actualizar(item);
        }
    }
}
