﻿using DataAccess.WSAMunicipio;
using System.Collections.Generic;

namespace Business
{
    public static class ControlWSAMunicipio
    {
        private static ServicioWSAMunicipio servicio = new ServicioWSAMunicipio("SQLConnectionString");

        public static Entity.WSAMunicipio ObtenerPorId(int id)
        {
            return servicio.ObtenerPorId(id);
        }

        public static List<Entity.WSAMunicipio> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static int Guardar(Entity.WSAMunicipio municipio)
        {
            return servicio.Guardar(municipio);
        }

        public static void Actualizar(Entity.WSAMunicipio municipio)
        {
            servicio.Actualizar(municipio);
        }
    }
}
