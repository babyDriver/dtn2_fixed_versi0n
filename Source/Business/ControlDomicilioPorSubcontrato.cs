﻿using System;
using System.Collections.Generic;
using DataAccess.DomicilioPorSubcontrato;

namespace Business
{
  public  class ControlDomicilioPorSubcontrato
    {
        private static ServicioDireccionPorSubcontrato servicio = new ServicioDireccionPorSubcontrato("SQLConnectionString");
        public static Entity.DomicilioPorSubcontrato ObtenerporId (int Id)
        {
            return servicio.GetById(Id);
        }
    }
}
