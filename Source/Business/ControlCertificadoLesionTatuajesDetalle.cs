﻿using DataAccess.CertificadoLesionTatuajeDetalle;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlCertificadoLesionTatuajesDetalle
    {
        private static ServicioCertificadoLesionTatuajeDetalle servicio = new ServicioCertificadoLesionTatuajeDetalle("SQLConnectionString");


        public static int Guardar(Entity.CertificadoLesionTatuajesDetalle certificadoAntecedentesDetalle)
        {

            return servicio.Guardar(certificadoAntecedentesDetalle);
        }
        public static List<Entity.CertificadoLesionTatuajesDetalle> ObteneroPortipoAntecedenteIdn(int Id)
        {
            return servicio.ObtenerPorId(Id);
        }

    }
}
