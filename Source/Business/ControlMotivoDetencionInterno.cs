﻿using DataAccess.MotivoDetencionInterno;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlMotivoDetencionInterno
    {
        private static  ServicioMotivoDetencionInterno servicio = new ServicioMotivoDetencionInterno("SQLConnectionString");

        public static int Guardar(Entity.MotivoDetencionInterno motivoDetencion)
        {
            return servicio.Guardar(motivoDetencion);
        }

        public static void Actualizar(Entity.MotivoDetencionInterno motivoDetencion)
        {
            servicio.Actualizar(motivoDetencion);
        }

        public static Entity.MotivoDetencionInterno GetMotivoDetencionInterno(Entity.MotivoDetencionInterno motivoDetencionInterno)
        {
            return servicio.GetMotivoDentencionInterno(motivoDetencionInterno);
        }

        public static List<Entity.MotivoDetencionInterno> GetListaMotivosDetencionByInternoId(Entity.MotivoDetencionInterno motivoDetencionInterno)
        {
            return servicio.GetMotivoDetencionInternosByDetalleDetencionId(motivoDetencionInterno);
        }

        public static Entity.MotivoDetencionInterno ObtenerByTrackingId(Guid guid)
        {
            return servicio.ObtenerByTrackingId(guid);
        }

        //public static List<Entity.MotivoDetencion> ObtenerTodos()
        //{
        //    return servicio.ObtenerTodos();
        //}

        //public static Entity.MotivoDetencion ObtenerPorTrackingId(Guid trackingId)
        //{
        //    return servicio.ObtenerByTrackingId(trackingId);
        //}
        //public static List<Entity.MotivoDetencion> ObtenerHabilitados(int tipoCatalogo)
        //{
        //    return servicio.ObtenerHabilitados(tipoCatalogo);
        //}
    }
}
