﻿using DataAccess.WSALugar;

namespace Business
{
    public class ControlWSALugar
    {
        private static ServicioWSALugar servicio = new ServicioWSALugar("SQLConnectionString");

        public static int Guardar(Entity.WSALugar item)
        {
            return servicio.Guardar(item);
        }

        public static Entity.WSALugar ObtenerPorId(int id)
        {
            return servicio.ObtenerPorId(id);
        }
    }
}
