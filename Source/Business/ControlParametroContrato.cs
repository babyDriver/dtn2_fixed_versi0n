﻿using DataAccess.ParametroContrato;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public class ControlParametroContrato
    {
        private static ServicioParametroContrato servicio = new ServicioParametroContrato("SQLConnectionString");

        public static int Guardar(Entity.ParametroContrato parametroContrato )
        {
            return servicio.Guardar(parametroContrato);
        }
        public static void Actualizar(Entity.ParametroContrato parametroContrato)
        {

            servicio.Actualizar(parametroContrato);
        }
        public static List<Entity.ParametroContrato> TraerTodos()
        {
            return servicio.GetAll();

        }
        public static void InsertaParametroContratiSiNoExsite(int ParametroId, int ContratoId,int UsuarioId, int Folio)
        {
            var listaparametroscontrato = servicio.GetAll();

            var listafiltrada = listaparametroscontrato.Where(x => x.ContratoId == ContratoId && x.Parametroid == ParametroId);
            if (listafiltrada==null || listafiltrada.Count()==0)
            {
                Entity.ParametroContrato parametro = new Entity.ParametroContrato();
                parametro.ContratoId = ContratoId;
                parametro.CreadoPor = UsuarioId;
                parametro.FechaHora = DateTime.Now;
                parametro.Parametroid = ParametroId;
                parametro.TrackingId = Guid.NewGuid().ToString();
                parametro.Valor = Folio.ToString();

                ControlParametroContrato.Guardar(parametro);

            }
        }


        

    }
}
