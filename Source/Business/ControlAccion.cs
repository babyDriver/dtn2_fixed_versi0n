﻿using DataAccess.Accion;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlAccion
    {
        private static ServicioAccion servicio = new ServicioAccion("SQLConnectionString");

        public static int Guardar(Entity.Accion accion)
        {
            return servicio.Guardar(accion);
        }
    }
}
