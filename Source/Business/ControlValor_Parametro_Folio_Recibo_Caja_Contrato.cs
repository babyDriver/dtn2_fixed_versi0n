﻿using DataAccess.Valor_Parametro_Folio_Recibo_Caja_Contrato;
using System.Collections.Generic;
using System;

namespace Business
{
     public class ControlValor_Parametro_Folio_Recibo_Caja_Contrato
    {
        private static ServicioValor_Parametro_Folio_Recibo_Caja_Contrato servicio = new ServicioValor_Parametro_Folio_Recibo_Caja_Contrato("SQLConnectionString");

        public static Entity.Valor_Parametro_Folio_Recibo_Caja_Contrato ObtenerValorByConstratoId(int ContratoId)
        {
            return servicio.GetValorparametrofoliocaja(ContratoId);
        }
    }
}
