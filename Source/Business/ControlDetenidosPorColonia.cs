﻿using DataAccess.DetenidosPorColonia;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlDetenidosPorColonia
    {
        private static ServicioDetenidosPorColonia servicio = new ServicioDetenidosPorColonia("SQLConnectionString");
        public static List<Entity.Detenidosporcolonia> GetDetenidosporcolonias(string[] filtros)
        {
            return servicio.GetDetenidosPorColonia(filtros);

        }
        public static List<Entity.Detenidosporcolonia> GetDetenidosMayoresporcolonias(string[] filtros)
        {
            return servicio.GetDetenidosMayorEdadPorColonia(filtros);

        }
        public static List<Entity.Detenidosporcolonia> GetDetenidosMenoresporcolonias(string[] filtros)
        {
            return servicio.GetDetenidosMenorEdadPorColonia(filtros);

        }
    }
}