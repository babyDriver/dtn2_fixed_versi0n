﻿using DataAccess.CertificadoPsicoFisiologicoConclusion;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlCertificadoPsicoFisologicoConclusion
    {
        private static ServicioCertificadoPsicoFisiologicoConclusion servicio = new ServicioCertificadoPsicoFisiologicoConclusion("SQLConnectionString");

        public static int Guardar(Entity.CertificadoPsicoFisilogicoConclusion entifadad)
        {
            return servicio.Guardar(entifadad);
        }

        public static Entity.CertificadoPsicoFisilogicoConclusion obtenerPorId(int Id)
        {
            return servicio.ObtenerPorId(Id);
        }

    }
}
