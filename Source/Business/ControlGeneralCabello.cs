﻿using DataAccess.GeneralCabello;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlGeneralCabello
    {
        private static ServicioGeneralCabello servicio = new ServicioGeneralCabello("SQLConnectionString");

        public static int Guardar(Entity.GeneralCabello general)
        {
            return servicio.Guardar(general);
        }

        public static void Actualizar(Entity.GeneralCabello general)
        {
            servicio.Actualizar(general);
        }

        public static Entity.GeneralCabello ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.GeneralCabello> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.GeneralCabello ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.GeneralCabello ObtenerPorAntropometriaId(int id)
        {
            return servicio.ObtenerByAntropometriaId(id);
        }
    }
}
