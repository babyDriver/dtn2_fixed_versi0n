﻿using DataAccess.CertificadoLesionAlergias;
using System.Collections.Generic;
using System;


namespace Business
{
   public  class ControlCertificadoLesionAlergia
    {
        private static ServicioCertificadoLesionAlergias servicio = new ServicioCertificadoLesionAlergias("SQLConnectionString");
        public static int Guardar(Entity.CertificadoLesionAlergia  certificadoLesionAlergia)
        {
            return servicio.Guardar(certificadoLesionAlergia);
        }
        public static Entity.CertificadoLesionAlergia ObtenerPorId(int Id)
        {
            return servicio.ObtenerPorId(Id);

        }
    }
}
