﻿using DataAccess.FichaTecnica;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlFichaTecnica
    {
        private static ServicioFichaTecnica servicio = new ServicioFichaTecnica("SQLConnectionString");

        public static int Guardar(Entity.FichaTecnica Ficha)
        {
            return servicio.Guardar(Ficha);
        }
    }
}
