﻿using DataAccess.Rol;
using System.Collections.Generic;

namespace Business
{
    public class ControlRol
    {
        private static ServicioRol servicio = new ServicioRol("SQLConnectionString");

        public static int Guardar(Entity.Rol rol)
        {
            return servicio.Guardar(rol);
        }

        public static void Actualizar(Entity.Rol rol)
        {
            servicio.Actualizar(rol);
        }

        public static Entity.Rol ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static Entity.Rol ObtenerPorNombre(string nombre)
        {
            return servicio.ObtenerByNombre(nombre);
        }

        public static List<Entity.Rol> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }
    }
}
