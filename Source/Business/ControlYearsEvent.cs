﻿using DataAccess.YearsEvent;
using System.Collections.Generic;

namespace Business
{
    public class ControlYearsEvent
    {
        private static ServicioYearsEvent servicio = new ServicioYearsEvent("SQLConnectionString");

        public static List<Entity.YearsEvent> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }
    }
}
