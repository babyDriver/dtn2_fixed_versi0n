﻿using DataAccess.Modelo;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlModelo
    {
        private static ServicioModelo servicio = new ServicioModelo("SQLConnectionString");

        public static int Guardar(Entity.Modelo item)
        {
            return servicio.Guardar(item);
        }

        public static void Actualizar(Entity.Modelo item)
        {
            servicio.Actualizar(item);
        }

        public static Entity.Modelo ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.Modelo> ObtenerPorMarcaId(int id)
        {
            return servicio.ObtenerByMarcaId(id);
        }

        public static Entity.Modelo ObtenerPorNombreMarca(Entity.Modelo modelo)
        {
            return servicio.ObtenerByNombreMarca(modelo);
        }

        public static List<Entity.Modelo> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Modelo ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }
    }
}
