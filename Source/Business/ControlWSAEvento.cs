﻿using DataAccess.WSAEvento;

namespace Business
{
    public class ControlWSAEvento
    {
        private static ServicioWSAEvento servicio = new ServicioWSAEvento("SQLConnectionString");

        public static int Guardar(Entity.WSAEvento item)
        {
            return servicio.Guardar(item);
        }

        public static Entity.WSAEvento ObtenerPorId(int id)
        {
            return servicio.ObtenerPorId(id);
        }

        public static Entity.WSAEvento ObtenerPorFolio(string folio)
        {
            return servicio.ObtenerPorFolio(folio);
        }
    }
}
