﻿using DataAccess.Traslado;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlTraslado
    {
        private static ServicioTraslado servicio = new ServicioTraslado("SQLConnectionString");

        public static int Guardar(Entity.Traslado traslado)
        {
            return servicio.Guardar(traslado);
        }
        public static Entity.Traslado GetTrasladoNyKeys(Entity.Traslado traslado)
        {
            return servicio.GetTrasladoByKeys(traslado);
        }
        public static Entity.Traslado GetTrasladoByDetalleDetencion(Entity.Traslado traslado)
        {
            return servicio.GetTrasladoByDetalledetencion(traslado);
        }
    }
}
