﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;

using System.Configuration;
using Entity;
using System.Globalization;
using System.Web.Security;
using System.Linq;
using DataAccess.Observacion;

namespace Business
{
    
    public static class ControlPDFEM
    {
        // Excepciones y casos especiales
        static Boolean bolHermosillo = true;
         
        //para crear el Header y Footer
        public partial class HeaderFooter : PdfPageEventHelper
        {
            PdfContentByte cb;
            PdfTemplate headerTemplate;
            BaseFont bf = null;
            float tamanofuente = 8f;

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                }
                catch (DocumentException de) { }
                catch (System.IO.IOException ioe) { }
            }

            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                //Encabezado de Página
                Rectangle pageSize = doc.PageSize;
                PdfPTable footerTbl = new PdfPTable(1);
                BaseColor blau = new BaseColor(0, 61, 122);

                float[] TablaAncho = new float[] { 100 };
                footerTbl.SetWidths(TablaAncho);
                footerTbl.TotalWidth = 550;
                Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                PdfPCell cell;

                string pagina = "Página " + writer.PageNumber + " de ";
                //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
                {
                    float margenbottom = 15f;
                    float margenright = 280f;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, tamanofuente);
                    cb.SetTextMatrix(doc.PageSize.GetBottom(margenright), doc.PageSize.GetBottom(margenbottom));
                    cb.ShowText(pagina);
                    cb.EndText();
                    float len = bf.GetWidthPoint(pagina, tamanofuente);
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));
                }

                PdfPTable footerTbl2 = new PdfPTable(2);


                float[] TablaAncho2 = new float[] { 100, 100 };
                footerTbl2.SetWidths(TablaAncho2);
                footerTbl2.TotalWidth = 600;
                footerTbl.HorizontalAlignment = Element.ALIGN_CENTER;
                Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                string dia = String.Format("{0:dd}", DateTime.Now);
                string anio = String.Format("{0:yyyy}", DateTime.Now);
                string hora = String.Format("{0:HH:mm}", DateTime.Now);
                string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
                string fechahora = dia + " de " + mestexto + " de " + anio + " " + hora;
                Chunk chkFecha = new Chunk(fechahora, FuenteCelda);
                Phrase phrfecha = new Phrase(chkFecha);

                //Phrase promad = new Phrase("Promad Soluciones Computarizadas " + anio, FuenteCelda2);
                Phrase promad = new Phrase("", FuenteCelda2);
                cell = new PdfPCell(promad);
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);

                cell = new PdfPCell(phrfecha);
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);
                footerTbl2.WriteSelectedRows(0, -1, 5, 25, writer.DirectContent);
            }

            private static string ObtenerMesTexto(string mes)
            {
                string mestexto = "";
                switch (mes)
                {
                    case "01":
                        mestexto = "Enero";
                        break;
                    case "02":
                        mestexto = "Febrero";
                        break;
                    case "03":
                        mestexto = "Marzo";
                        break;
                    case "04":
                        mestexto = "Abril";
                        break;
                    case "05":
                        mestexto = "Mayo";
                        break;
                    case "06":
                        mestexto = "Junio";
                        break;
                    case "07":
                        mestexto = "Julio";
                        break;
                    case "08":
                        mestexto = "Agosto";
                        break;
                    case "09":
                        mestexto = "Septiembre";
                        break;
                    case "10":
                        mestexto = "Octubre";
                        break;
                    case "11":
                        mestexto = "Noviembre";
                        break;
                    case "12":
                        mestexto = "Diciembre";
                        break;
                    default:
                        mestexto = " ";
                        break;
                }
                return mestexto;
            }



            //Necesario para incluir pagina x de y al header
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);
                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, tamanofuente);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

            }
        }

        private static string CrearDirectorio(string nombre)
        {
            try
            {


                //Verificar si existe el directorio principal del repositorio de documentos
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio de la obra existe
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio para el grupo documental existe, sino crearlo
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre)));



                return string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs//" + nombre);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("No fue posible crear el repositorio para crear el archivo. ", ex.Message));
            }
        }

        //ch3ck 0ut
        public static object ReporteEMAux(int detenidoId, Guid tracking, int examenMedicoId = 0)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(10); //milimetros para los margenes
            var space = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentpdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                //Nombre y ubicación archivo
                string nombrearchivo = "Examen_Médico_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorio("Examen Médico"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string pathfont = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont calibrifont = BaseFont.CreateFont(pathfont, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font titlefont = new Font(calibrifont, 12, Font.BOLD, blau);
                Font boldfont = new Font(calibrifont, 10f, Font.BOLD, blau);
                Font negritabold = new Font(calibrifont, 10f, Font.BOLD);
                Font normalfont = new Font(calibrifont, 10f, Font.NORMAL);
                Font bigfont = new Font(calibrifont, 10, Font.BOLD, blau);
                Font smallfont = new Font(calibrifont, 8, Font.NORMAL);
                Font bigtitlefont = new Font(calibrifont, 14, Font.BOLD, blau);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentpdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return obj = new { success = false, file = "", message = ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();

                documentpdf.Open();

                TituloExamen(documentpdf, titlefont, boldfont, normalfont, space, bigfont, detenidoId, bigtitlefont);
                GetDatosExamen(documentpdf, boldfont, normalfont, space, titlefont, detenidoId, tracking, negritabold, examenMedicoId);

                obj = new { success = true, file = ubicacionarchivo, message = "" };
                return obj;
            }
            catch (Exception ex)
            {
                return obj = new { success = false, file = "", message = ex.Message };
            }
            finally
            {
                if (documentpdf.IsOpen())
                {
                    documentpdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        //ch3ck 0ut
        public static object ReporteEM(int detenidoId, Guid tracking, int examenMedicoId = 0)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(10); //milimetros para los margenes
            var space = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentpdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                //Nombre y ubicación archivo
                string nombrearchivo = "Examen_Médico_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorio("Examen Médico"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string pathfont = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont calibrifont = BaseFont.CreateFont(pathfont, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font titlefont = new Font(calibrifont, 12, Font.BOLD, blau);
                Font boldfont = new Font(calibrifont, 10f, Font.BOLD, blau);
                Font negritabold = new Font(calibrifont, 10f, Font.BOLD);
                Font normalfont = new Font(calibrifont, 10f, Font.NORMAL);
                Font bigfont = new Font(calibrifont, 10, Font.BOLD, blau);
                Font smallfont = new Font(calibrifont, 8, Font.NORMAL);
                Font bigtitlefont = new Font(calibrifont, 14, Font.BOLD, blau);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentpdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return obj = new { success = false, file = "", message = ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();

                documentpdf.Open();

                TituloExamen(documentpdf, titlefont, boldfont, normalfont, space, bigfont, detenidoId, bigtitlefont);
                GetDatosExamenAux(documentpdf, boldfont, normalfont, space, titlefont, detenidoId, tracking, negritabold, examenMedicoId);

                obj = new { success = true, file = ubicacionarchivo, message = "" };
                return obj;
            }
            catch (Exception ex)
            {
                return obj = new { success = false, file = "", message = ex.Message };
            }
            finally
            {
                if (documentpdf.IsOpen())
                {
                    documentpdf.Close();
                    PdfWriter.Close();
                }
            }
        }

        private static void TituloExamen(Document documentpdf, Font titlefont, Font boldfont, Font normalfont, float space, Font bigfont, int detenidoId, Font bigtitle)
        {
            int centroId = 0;
            var ei = ControlDetalleDetencion.ObtenerPorDetenidoId(detenidoId);
                foreach (var val in ei)
            {
                centroId = val.CentroId;
            }
                if(centroId==0)
            {
                var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                var contrato = ControlContrato.ObtenerPorId(contratoUsuarioK.IdContrato);
                centroId = contrato.InstitucionId;
            }

            Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(centroId);
            Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            var arr = logo.Split('/');
            var str = "../../";
            for (var i = 0; i < arr.Length; i++)
            {
                if (arr[i] == "Content")
                {
                    for (int x = i; x < arr.Length; x++)
                    {
                        str = str + arr[x] + "/";
                    }
                    str = str.Remove(str.Length - 1);
                }

            }

            if (str != "undefined" && str != "")
            {
                logotipo = str;                
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);

            iTextSharp.text.Image image = null;
            try
            {

                image = iTextSharp.text.Image.GetInstance(pathfoto);
           
            }
            catch (Exception)
            {

                image = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png"));
            }
            string nombreEstado = "Estado";
            string nombreMunicipio = "Municipio";
            string nombreCentro = institucion.Nombre;

            if (domicilio != null)
            {
                Entity.Estado estado = ControlEstado.ObtenerPorId(Convert.ToInt32(domicilio.EstadoId));

                if (estado != null)
                {
                    nombreEstado = estado.Nombre;
                    Entity.Municipio municipio = ControlMunicipio.Obtener(Convert.ToInt32(domicilio.MunicipioId));
                    if (municipio != null) nombreMunicipio = municipio.Nombre;
                }
            }
            
            string lugar = nombreCentro;            

            int[] width = new int[] { 2, 2, 8 };
            PdfPTable table = new PdfPTable(3);
            table.SetWidths(width);
            table.TotalWidth = 550;
            table.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell cell;
            
            CeldaVacio(table, 3);

            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            table.AddCell(cellWithRowspan);

            cell = new PdfPCell(new Phrase("", normalfont));
            // The first cell spans 5 rows  
            cell.Rowspan = 5;
            cell.Border = 0;
            table.AddCell(cell);

          

            // Cell 2,1 does not exist  
            cell = new PdfPCell(new Phrase("Ayuntamiento de " + nombreMunicipio + ", " + nombreEstado, titlefont));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);
            // Cell 3,1 does not exist  
            cell = new PdfPCell(new Phrase(nombreMunicipio.ToUpper() + ", " + nombreEstado.ToUpper(), bigtitle));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);
            // Cell 4,1 does not exist  
            cell = new PdfPCell(new Phrase("Examen Médico", bigtitle));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);
            // Cell 5,1 does not exist  
            cell = new PdfPCell(new Phrase("Delegación: " + lugar, boldfont));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);

            CeldaVacio(table, 5);

            documentpdf.Add(table);
        }

        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }

            return edad;
        }

        private static void GetDatosExamenAux(Document documentpdf, Font boldfont, Font normalfont, float space, Font titlefont, int detenidoId, Guid tracking, Font negritabold, int examenMedicoId)
        {
            //rep4ir
            //var ei = ControlDetalleDetencion.ObtenerPorId(detenidoId);
            var ei = ControlDetalleDetencion.ObtenerPorDetenidoId(detenidoId).FirstOrDefault();
            var detenido = ei != null ? ControlDetenido.ObtenerPorId(ei.DetenidoId) : null;
            //detenido = ControlDetenido.ObtenerPorId(detenidoId);
            string[] Id = { detenido.Id.ToString(), true.ToString() };
            ei = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(Id);
             
            CultureInfo culture = new CultureInfo("en-US");
            int detalleDetId = 0;
            string remision = "";
            var fechaReg = DateTime.MinValue;
            if (ei != null)
            {
                detalleDetId = ei.Id;
                remision = ei.Expediente;
                if (ei.Fecha != null)
                {
                    var datum = ei.Fecha;
                    fechaReg = Convert.ToDateTime(datum, culture);
                }
            }

            Entity.ServicioMedico em = new Entity.ServicioMedico();
            em = ControlServicoMedico.ObtenerPorDetalleDetencionId(detalleDetId).FirstOrDefault();

            //if (examenMedicoId > 0)
            //{
            //    em = new Entity.ExamenMedico();
            //    em = ControlExamenMedico.ObtenerPorId(examenMedicoId);
            //}
            //var pruebaEM = ControlPruebaExamenMedico.ObtenerPorExamenMedicoId(em.Id);

            //var lesiones = new AdicionalExamenMedico();
            //lesiones.ExamenMedicoId = em.Id;
            //lesiones.Clasificacion = "lesion";
            //var listaLesion = ControlAdicionalExamenMedico.ObtenerPorExamenMedicoId(lesiones);

            var intoxicaciones = new AdicionalExamenMedico();
            intoxicaciones.ExamenMedicoId = em.Id;
            intoxicaciones.Clasificacion = "intoxicacion";
            var listaIntoxicacion = ControlAdicionalExamenMedico.ObtenerPorExamenMedicoId(intoxicaciones);

            float[] width = new float[] { 100 };
            PdfPTable table = new PdfPTable(1);
            table.SetWidths(width);
            table.TotalWidth = 550;
            table.DefaultCell.Border = Rectangle.NO_BORDER;
            table.HorizontalAlignment = Element.ALIGN_CENTER;

            float[] w2 = new float[] { 90, 90, 90 };
            PdfPTable tabla = new PdfPTable(3);
            tabla.SetWidths(w2);
            tabla.TotalWidth = 550;
            tabla.DefaultCell.Border = Rectangle.NO_BORDER;
            PdfPCell cell;

            Entity.General general = ControlGeneral.ObtenerPorDetenidoId(detenidoId);
            string sexoTexto = "Sin dato";
            int edad = 0;
            if (general != null)
            {
                if (general.FechaNacimineto != DateTime.MinValue)
                {
                    edad = CalcularEdad(general.FechaNacimineto);
                    edad = general.Edaddetenido;
                }

                else
                {
                    var detalleDetencion2 = ControlInformacionDeDetencion.ObtenerPorInternoId(detenido.Id);
                    var evento = ControlEvento.ObtenerById(detalleDetencion2.IdEvento);
                    var detenidosEvento = ControlDetenidoEvento.ObtenerPorEventoId(evento.Id);
                    Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
                    foreach (var item in detenidosEvento)
                    {
                        if (item.Nombre == detenido.Nombre && item.Paterno == detenido.Paterno && item.Materno == detenido.Materno)
                            detenidoEvento = item;
                    }

                    if (detenidoEvento == null) edad = 0;
                    else edad = detenidoEvento.Edad;
                }

                sexoTexto = ControlCatalogo.Obtener(general.SexoId, 4).Nombre;
            }

            CeldaConChunk(tabla, boldfont, normalfont, "Remisión: ", remision, 0, 0, 0);
            CeldaConChunk(tabla, boldfont, normalfont, "Detenido: ", detenido.Nombre + " " + detenido.Paterno + " " + detenido.Materno, 0, 0, 0);
            CeldaConChunk(tabla, boldfont, normalfont, "Examen médico: ", em.Id.ToString(), 0, 0, 0);
            CeldaVacio(tabla, 3);
            CeldaConChunk(tabla, boldfont, normalfont, "Fecha de registro: ", em.FechaRegistro.ToString("dd/MM/yyyy HH:mm:ss"), 0, 0, 0);
            CeldaConChunk(tabla, boldfont, normalfont, "Edad: ", edad != 0 ? edad.ToString() : "Sin dato", 0, 0, 0);
            CeldaConChunk(tabla, boldfont, normalfont, "Sexo: ", sexoTexto, 0, 0, 0);
            CeldaVacio(tabla, 3);
            CeldaConChunk(tabla, boldfont, normalfont, "Se aplicó examen: ", em.FechaRegistro.ToString("dd/MM/yyyy HH:mm:ss"), 0, 0, 0);

            //stats on pr0gre$$
            //CeldaConChunk(tabla, boldfont, normalfont, "Presenta lesiones visibles: ", ((em.Lesion_visible) ? "Si" : "No"), 0, 0, 0);
            CeldaConChunk(tabla, boldfont, normalfont, "Presenta lesiones visibles: ", ((Convert.ToBoolean(0)) ? "Si" : "No"), 0, 0, 0);
            CeldaVacio(tabla, 2);

            documentpdf.Add(table);
            documentpdf.Add(tabla);

            PdfPTable t2 = new PdfPTable(1);
            t2.SetWidths(width);
            t2.TotalWidth = 550;
            t2.DefaultCell.Border = Rectangle.NO_BORDER;
            t2.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPTable tt2 = new PdfPTable(3);
            tt2.SetWidths(w2);
            tt2.TotalWidth = 550;
            tt2.DefaultCell.Border = Rectangle.NO_BORDER;


            if (!string.IsNullOrEmpty(em.CertificadoLesionId.ToString()))
            {
                int lesionId = em.CertificadoLesionId;
                Entity.CertificadoLesion cl = new Entity.CertificadoLesion();
                cl = ControlCertificadoLesiones.ObtenerPorId(lesionId);
                int idTattoo = cl.Tipo_tatuajeId;
                int idLesion = cl.Tipo_lesionId;
                Entity.CertificadoLesionTatuajesDetalle listaTatuaje = new Entity.CertificadoLesionTatuajesDetalle();
                Entity.CertificadoLesionTipoLesionDetalle listaLesion = new Entity.CertificadoLesionTipoLesionDetalle();
                string tipo, observacion, lugar;

                //stats on pr0gre$$
                //if (listaIntoxicacion.Count != 0)
                //{
                //    cell = new PdfPCell(new Phrase("INTOXICACIONES: ", titlefont));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.BackgroundColor = new BaseColor(255, 255, 255);
                //    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //    cell.Border = Rectangle.NO_BORDER;
                //    tt2.AddCell(cell);

                //    CeldaVacio(tt2, 2);

                //    cell = new PdfPCell(new Phrase("Tipo:", boldfont));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.BackgroundColor = new BaseColor(255, 255, 255);
                //    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //    cell.Border = Rectangle.NO_BORDER;
                //    tt2.AddCell(cell);

                //    cell = new PdfPCell(new Phrase("Observación:", boldfont));
                //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //    cell.BackgroundColor = new BaseColor(255, 255, 255);
                //    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //    cell.Border = Rectangle.NO_BORDER;
                //    tt2.AddCell(cell);

                //    CeldaVacio(tt2, 1);


                //    if (listaIntoxicacion.Count == 0)
                //    {
                //        tipo = "Ninguna";
                //        observacion = "Ninguna";
                //    }
                //    else
                //    {
                //        foreach (var intoxicacion in listaIntoxicacion)
                //        {
                //            if (intoxicacion.Habilitado != false)
                //            {
                //                tipo = string.Empty;
                //                observacion = string.Empty;

                //                tipo = intoxicacion.Tipo.Nombre;
                //                observacion = intoxicacion.Observacion;

                //                cell = new PdfPCell(new Phrase(tipo, normalfont));
                //                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //                cell.BackgroundColor = new BaseColor(255, 255, 255);
                //                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //                cell.Border = Rectangle.NO_BORDER;
                //                tt2.AddCell(cell);

                //                cell = new PdfPCell(new Phrase(observacion, normalfont));
                //                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //                cell.BackgroundColor = new BaseColor(255, 255, 255);
                //                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //                cell.Border = Rectangle.NO_BORDER;
                //                tt2.AddCell(cell);

                //                CeldaVacio(tt2, 4);
                //            }
                //        }
                //        CeldaVacio(tt2, 3);
                //    }
                //}

                if (idLesion != 0)
                {
                    cell = new PdfPCell(new Phrase("LESIONES: ", titlefont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BackgroundColor = new BaseColor(255, 255, 255);
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    tt2.AddCell(cell);

                    CeldaVacio(tt2, 2);

                    cell = new PdfPCell(new Phrase("Tipo:", boldfont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BackgroundColor = new BaseColor(255, 255, 255);
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    tt2.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Lugar:", boldfont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BackgroundColor = new BaseColor(255, 255, 255);
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    tt2.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Observación:", boldfont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BackgroundColor = new BaseColor(255, 255, 255);
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    tt2.AddCell(cell);


                    if (lesionId == 0)
                    {
                        tipo = "Ninguna";
                        lugar = "Ninguno";
                        observacion = "Ninguna";
                    }
                    else
                    {
                        //foreach (var lesion in listaLesion)
                        //{
                        listaLesion = ControlCertificadoLesionTipoLesionDetalle.ObteneroPortipoLesion(idLesion).FirstOrDefault();
                        int lesion = Convert.ToInt16(Entity.TipoDeCatalogo.tipo_tatuaje);
                        Entity.Catalogo tipoLesion = ControlCatalogo.Obtener(listaLesion.TipoLesionId, lesion);

                        //if (string.IsNullOrEmpty(tipoTatuje.Nombre))
                        tipo = tipoLesion.Nombre != null ? tipoLesion.Nombre : "Ninguna";
                        observacion = cl.Observacion_tatuje;
                        //lugar = lesion.Lugar != null ? ControlCatalogo.Obtener(Convert.ToInt32(lesion.Lugar), Convert.ToInt32(Entity.TipoDeCatalogo.lugar)).Nombre : "";

                        cell = new PdfPCell(new Phrase(tipo, normalfont));
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.BackgroundColor = new BaseColor(255, 255, 255);
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.Border = Rectangle.NO_BORDER;
                        tt2.AddCell(cell);

                        cell = new PdfPCell(new Phrase("No especificado", normalfont));
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.BackgroundColor = new BaseColor(255, 255, 255);
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.Border = Rectangle.NO_BORDER;
                        tt2.AddCell(cell);

                        cell = new PdfPCell(new Phrase(observacion, normalfont));
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.BackgroundColor = new BaseColor(255, 255, 255);
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.Border = Rectangle.NO_BORDER;
                        tt2.AddCell(cell);

                        CeldaVacio(tt2, 3);
                    }
                }

                if (idTattoo != 0)
                {
                    cell = new PdfPCell(new Phrase("TATUAJES: ", titlefont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BackgroundColor = new BaseColor(255, 255, 255);
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    tt2.AddCell(cell);

                    CeldaVacio(tt2, 2);

                    cell = new PdfPCell(new Phrase("Tipo:", boldfont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BackgroundColor = new BaseColor(255, 255, 255);
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    tt2.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Lugar:", boldfont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BackgroundColor = new BaseColor(255, 255, 255);
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    tt2.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Observación:", boldfont));
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.BackgroundColor = new BaseColor(255, 255, 255);
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.Border = Rectangle.NO_BORDER;
                    tt2.AddCell(cell);

                    if (idTattoo == 0)
                    {
                        tipo = "Ninguna";
                        lugar = "Ninguno";
                        observacion = "Ninguna";
                    }
                    else
                    {
                        //foreach (var tatuaje in listaTatuaje)
                        //{
                        listaTatuaje = ControlCertificadoLesionTatuajesDetalle.ObteneroPortipoAntecedenteIdn(idTattoo).FirstOrDefault();
                        int tatuaje = Convert.ToInt16(Entity.TipoDeCatalogo.tipo_tatuaje);
                        Entity.Catalogo tipoTatuaje = ControlCatalogo.Obtener(listaTatuaje.TatuajeId, tatuaje);

                        //if (string.IsNullOrEmpty(tipoTatuje.Nombre))
                        tipo = tipoTatuaje.Nombre != null ? tipoTatuaje.Nombre : "Ninguna";
                        observacion = cl.Observacion_tatuje;
                        //lugar = tatuaje.Lugar != null ? ControlCatalogo.Obtener(Convert.ToInt32(tatuaje.Lugar), Convert.ToInt32(Entity.TipoDeCatalogo.lugar)).Nombre : "";

                        cell = new PdfPCell(new Phrase(tipo, normalfont));
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.BackgroundColor = new BaseColor(255, 255, 255);
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.Border = Rectangle.NO_BORDER;
                        tt2.AddCell(cell);

                        cell = new PdfPCell(new Phrase("No especificado", normalfont));
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.BackgroundColor = new BaseColor(255, 255, 255);
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.Border = Rectangle.NO_BORDER;
                        tt2.AddCell(cell);

                        cell = new PdfPCell(new Phrase(observacion, normalfont));
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        cell.BackgroundColor = new BaseColor(255, 255, 255);
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.Border = Rectangle.NO_BORDER;
                        tt2.AddCell(cell);

                        CeldaVacio(tt2, 3);
                        CeldaVacio(tt2, 3);

                        documentpdf.Add(t2);
                        documentpdf.Add(tt2);
                    }
                }

                PdfPTable t3 = new PdfPTable(1);
                table.SetWidths(width);
                table.TotalWidth = 550;
                table.DefaultCell.Border = Rectangle.NO_BORDER;

                PdfPTable tt3 = new PdfPTable(1);
                tabla.SetWidths(w2);
                tabla.TotalWidth = 550;
                tabla.DefaultCell.Border = Rectangle.NO_BORDER;

                //cell = new PdfPCell(new Phrase("Inspección ocular / Interrogatorio:", boldfont));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt3.AddCell(cell);

                //CeldaConChunk(tt3, boldfont, normalfont, "Inspección ocular / Interrogatorio:                                              " +
                //            "Tipo de examen: ", em.TipoExamen.Nombre, 0, 0, 0);

                //cell = new PdfPCell(new Phrase(em.InspeccionOcular, normalfont));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt3.AddCell(cell);

                cell = new PdfPCell(new Phrase("Indicaciones médicas:", boldfont));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt3.AddCell(cell);

                //cell = new PdfPCell(new Phrase(em.IndicacionesMedicas, normalfont));
                //cell.HorizontalAlignment = Element.ALIGN_LEFT;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt3.AddCell(cell);

                cell = new PdfPCell(new Phrase("Observación:", boldfont));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt3.AddCell(cell);

                cell = new PdfPCell(new Phrase(cl.Observacion_general, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt3.AddCell(cell);

                string medLegal = Convert.ToBoolean(cl.Consecuencias_medico_legales) ? "Sí" : "No";
                string fallecio = Convert.ToBoolean(cl.Fallecimiento) ? "Sí" : "No";
                CeldaConChunk2(tt3, boldfont, normalfont, "Las lesiones dejan consecuencias médico legales: ", medLegal, 0, 0, 0, "              Fallecimiento: ", fallecio);

                CeldaVacio(tt3, 1);

                documentpdf.Add(t3);
                documentpdf.Add(tt3);

            }
            
            //Fin apartado inspecciones

            //Inicia apartado EXÁMENES
            //if (pruebaEM != null)
            if (string.IsNullOrEmpty(em.CertificadoMedicoPsicofisiologicoId.ToString()))
            {
                int psicoId = em.CertificadoMedicoPsicofisiologicoId;
                Entity.CertificadoMedicoPsicoFisiologico cpf = new Entity.CertificadoMedicoPsicoFisiologico();
                cpf = ControlCertificadoPsicoFisiologico.ObtenerPorId(psicoId);
                //creat3 ps1co rep0rt

                float[] f = new float[] { 100 };
                PdfPTable encabezado = new PdfPTable(1);
                encabezado.SetWidths(f);
                encabezado.TotalWidth = 550;
                encabezado.HorizontalAlignment = Element.ALIGN_CENTER;

                cell = new PdfPCell(new Phrase("EXÁMENES", titlefont));
                cell.BorderWidth = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Padding = space;
                encabezado.AddCell(cell);

                documentpdf.Add(encabezado);

                PdfPTable t4 = new PdfPTable(1);
                t4.SetWidths(width);
                t4.TotalWidth = 550;
                t4.DefaultCell.Border = Rectangle.NO_BORDER;
                t4.HorizontalAlignment = Element.ALIGN_CENTER;

                float[] wd40 = new float[] { 90, 90, 90, 90, 90, 90 };
                PdfPTable tt4 = new PdfPTable(6);
                tt4.SetWidths(wd40);
                tt4.TotalWidth = 550;
                tt4.DefaultCell.Border = Rectangle.NO_BORDER;
                 
                //cell = new PdfPCell(new Phrase("Mucosas", titlefont));
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Aliento", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Ex. Neurológico", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Alcoholímetro", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Disartía", titlefont));
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Conjuntivas", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                //var tipoX = ControlCatalogo.Obtener(pruebaEM.MucosasId, 102);
                //cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt4.AddCell(cell);

                var tipoX = ControlCatalogo.Obtener(cpf.AlientoId, 103);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                //tipoX = ControlCatalogo.Obtener(pruebaEM.Examen_neurologicoId, 104);
                //cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt4.AddCell(cell);

                //stats on pr0gre$$
                Entity.CertificadoQuimico cq = ControlCertificadoQuimico.ObtenerPorId(em.CertificadoQuimicoId);
                cell = new PdfPCell(new Phrase("Ebrio", normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                //string disartiaTexto;
                //if (pruebaEM.Disartia)
                //    disartiaTexto = (pruebaEM.Disartia) ? "Sí" : "No";
                //else
                //    disartiaTexto = "No especificado";

                //cell = new PdfPCell(new Phrase(disartiaTexto, normalfont));
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(cpf.ConjuntivaId, 105);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Marcha", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Pupilas", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Coordinación", titlefont));
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Reflejos Pupilares", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Reflejos Osteo - Tendinosos", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Romberg", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(cpf.MarchaId, 106);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(cpf.PupilaId, 117);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                //tipoX = ControlCatalogo.Obtener(pruebaEM.CoordinacionId, 118);
                //cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(cpf.Reflejo_pupilarId, 107);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                //tipoX = ControlCatalogo.Obtener(pruebaEM.TendinososId, 108);
                //cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(cpf.RombergId, 110);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Conducta", titlefont));
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Lenguaje", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Atención", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Orientación", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Diadococinesia", titlefont));
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt4.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Dedo-Nariz", titlefont));
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt4.AddCell(cell);

                //tipoX = ControlCatalogo.Obtener(pruebaEM.ConductaId, 109);
                //cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(cpf.LenguajeId, 111);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(cpf.AtencionId, 112);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(cpf.OrientacionId, 113);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                //tipoX = ControlCatalogo.Obtener(pruebaEM.DiadococinenciaId, 114);
                //cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt4.AddCell(cell);

                //tipoX = ControlCatalogo.Obtener(pruebaEM.DedoId, 115);
                //cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt4.AddCell(cell);

                //cell = new PdfPCell(new Phrase("Talón-Rodilla", titlefont));
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("T/A", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("F/C", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("F/R", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Pulso", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                CeldaVacio(tt4, 1);

                //tipoX = ControlCatalogo.Obtener(pruebaEM.TalonId, 116);
                //cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.BackgroundColor = new BaseColor(255, 255, 255);
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.Border = Rectangle.NO_BORDER;
                //tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase(cpf.TA, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase(cpf.FC, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase(cpf.FR, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase(cpf.Pulso, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                CeldaVacio(tt4, 1);

                documentpdf.Add(t4);
                documentpdf.Add(tt4);
            }
            else
            {
                PdfPTable noExamen = new PdfPTable(1);
                table.SetWidths(width);
                table.TotalWidth = 550;
                table.DefaultCell.Border = Rectangle.NO_BORDER;
                noExamen.HorizontalAlignment = Element.ALIGN_CENTER;

                CeldaVacio(noExamen, 6);
                cell = new PdfPCell(new Phrase("¡EXAMEN MÉDICO NO EFECTUADO!", negritabold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                noExamen.AddCell(cell);

                CeldaVacio(noExamen, 1);

                //documentpdf.Add(noExamen);

            }

        }

        //stats on pr0gre$$
        private static void GetDatosExamen(Document documentpdf, Font boldfont, Font normalfont, float space, Font titlefont, int detenidoId, Guid tracking, Font negritabold, int examenMedicoId)
        {
            var ei = ControlDetalleDetencion.ObtenerPorId(detenidoId); 
            var detenido = ei != null ? ControlDetenido.ObtenerPorId(ei.DetenidoId) : null;
            detenido = ControlDetenido.ObtenerPorId(detenidoId);
            string[] Id = { detenido.Id.ToString(), true.ToString() };
            ei = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(Id);
           
            CultureInfo culture = new CultureInfo("en-US");
            int detalleDetId = 0;
            string remision = "";
            var fechaReg = DateTime.MinValue;
            if (ei != null)
            {
                detalleDetId = ei.Id;
                remision = ei.Expediente;
                if (ei.Fecha != null)
                {
                    var datum = ei.Fecha;
                    fechaReg = Convert.ToDateTime(datum, culture);
                }
            }

            Entity.ExamenMedico em = new Entity.ExamenMedico();
            em = ControlExamenMedico.ObtenerPorDetalleDetencionId(detalleDetId);

            if (examenMedicoId > 0)
            {
                em = new Entity.ExamenMedico();
                em = ControlExamenMedico.ObtenerPorId(examenMedicoId);
            }

            var pruebaEM = ControlPruebaExamenMedico.ObtenerPorExamenMedicoId(em.Id);

            var tatuajes = new AdicionalExamenMedico();
            tatuajes.ExamenMedicoId = em.Id;
            tatuajes.Clasificacion = "tatuaje";
            var listaTatuaje = ControlAdicionalExamenMedico.ObtenerPorExamenMedicoId(tatuajes);

            var lesiones = new AdicionalExamenMedico();
            lesiones.ExamenMedicoId = em.Id;
            lesiones.Clasificacion = "lesion";
            var listaLesion = ControlAdicionalExamenMedico.ObtenerPorExamenMedicoId(lesiones);

            var intoxicaciones = new AdicionalExamenMedico();
            intoxicaciones.ExamenMedicoId = em.Id;
            intoxicaciones.Clasificacion = "intoxicacion";
            var listaIntoxicacion = ControlAdicionalExamenMedico.ObtenerPorExamenMedicoId(intoxicaciones);

            float[] width = new float[] { 100 };
            PdfPTable table = new PdfPTable(1);
            table.SetWidths(width);
            table.TotalWidth = 550;
            table.DefaultCell.Border = Rectangle.NO_BORDER;
            table.HorizontalAlignment = Element.ALIGN_CENTER;

            float[] w2 = new float[] { 90, 90, 90 };
            PdfPTable tabla = new PdfPTable(3);
            tabla.SetWidths(w2);
            tabla.TotalWidth = 550;
            tabla.DefaultCell.Border = Rectangle.NO_BORDER;
            PdfPCell cell;

            Entity.General general = ControlGeneral.ObtenerPorDetenidoId(detenidoId);
            string sexoTexto = "Sin dato";
            int edad = 0;
            if (general != null)
            {
                if (general.FechaNacimineto != DateTime.MinValue)
                {
                    edad = CalcularEdad(general.FechaNacimineto);
                    edad = general.Edaddetenido;
                }
                    
                else
                {
                    var detalleDetencion2 = ControlInformacionDeDetencion.ObtenerPorInternoId(detenido.Id);
                    var evento = ControlEvento.ObtenerById(detalleDetencion2.IdEvento);
                    var detenidosEvento = ControlDetenidoEvento.ObtenerPorEventoId(evento.Id);
                    Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
                    foreach (var item in detenidosEvento)
                    {
                        if (item.Nombre == detenido.Nombre && item.Paterno == detenido.Paterno && item.Materno == detenido.Materno)
                            detenidoEvento = item;
                    }

                    if (detenidoEvento == null) edad = 0;
                    else edad = detenidoEvento.Edad;
                }

                sexoTexto = ControlCatalogo.Obtener(general.SexoId, 4).Nombre;
            }

            CeldaConChunk(tabla, boldfont, normalfont, "Remisión: ", remision, 0, 0, 0);
            CeldaConChunk(tabla, boldfont, normalfont, "Detenido: ", detenido.Nombre + " " + detenido.Paterno + " " + detenido.Materno, 0, 0, 0);
            CeldaConChunk(tabla, boldfont, normalfont, "Examen médico: ", em.Id.ToString(), 0, 0, 0);
            CeldaVacio(tabla, 3);
            CeldaConChunk(tabla, boldfont, normalfont, "Fecha de registro: ", em.FechaRegistro.ToString("dd/MM/yyyy HH:mm:ss"), 0, 0, 0);
            CeldaConChunk(tabla, boldfont, normalfont, "Edad: ", edad != 0 ? edad.ToString() : "Sin dato", 0, 0, 0);
            CeldaConChunk(tabla, boldfont, normalfont, "Sexo: ", sexoTexto, 0, 0, 0);
            CeldaVacio(tabla, 3);
            CeldaConChunk(tabla, boldfont, normalfont, "Se aplicó examen: " , em.Fecha.ToString("dd/MM/yyyy HH:mm:ss"), 0, 0, 0) ;
            var uss = ControlUsuario.Obtener(em.RegistradoPor);
            var nombremedico = "";
            var ceulda = "Sin dato";
            nombremedico = uss.Nombre + " " + uss.ApellidoPaterno + " " + uss.ApellidoMaterno;
            if (uss.RolId == 6) ceulda = uss.CUIP;
            CeldaConChunk(tabla, boldfont, normalfont, "Médico: ", nombremedico, 0, 0, 0);
            CeldaConChunk(tabla, boldfont, normalfont, "Cédula: ", ceulda, 0, 0, 0);
            CeldaVacio(tabla, 2);                        

            documentpdf.Add(table);
            documentpdf.Add(tabla);

            PdfPTable t2 = new PdfPTable(1);
            t2.SetWidths(width);
            t2.TotalWidth = 550;
            t2.DefaultCell.Border = Rectangle.NO_BORDER;
            t2.HorizontalAlignment = Element.ALIGN_CENTER;
            string tipo;
            string observacion;
            PdfPTable tt2 = new PdfPTable(3);
            tt2.SetWidths(w2);
            tt2.TotalWidth = 550;
            tt2.DefaultCell.Border = Rectangle.NO_BORDER;
            
            if (listaIntoxicacion.Count != 0)
            {
                cell = new PdfPCell(new Phrase("INTOXICACIONES: ", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt2.AddCell(cell);

                CeldaVacio(tt2, 2);

                cell = new PdfPCell(new Phrase("Tipo:", boldfont));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Observación:", boldfont));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt2.AddCell(cell);

                CeldaVacio(tt2, 1);

                
                if (listaIntoxicacion.Count == 0)
                {
                    tipo = "Ninguna";
                    observacion = "Ninguna";
                }
                else
                {
                    foreach (var intoxicacion in listaIntoxicacion)
                    {
                        if (intoxicacion.Habilitado != false)
                        {
                            tipo = string.Empty;
                            observacion = string.Empty;

                            tipo = intoxicacion.Tipo.Nombre;
                            observacion = intoxicacion.Observacion;

                            cell = new PdfPCell(new Phrase(tipo, normalfont));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.BackgroundColor = new BaseColor(255, 255, 255);
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.Border = Rectangle.NO_BORDER;
                            tt2.AddCell(cell);

                            cell = new PdfPCell(new Phrase(observacion, normalfont));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.BackgroundColor = new BaseColor(255, 255, 255);
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.Border = Rectangle.NO_BORDER;
                            tt2.AddCell(cell);

                            CeldaVacio(tt2, 4);
                        }
                    }
                    CeldaVacio(tt2, 3);
                }
            }
            string lugar;
            if (listaLesion.Count!=0)
            {
                cell = new PdfPCell(new Phrase("LESIONES: ", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt2.AddCell(cell);

                CeldaVacio(tt2, 2);

                cell = new PdfPCell(new Phrase("Tipo:", boldfont));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Lugar:", boldfont));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Observación:", boldfont));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt2.AddCell(cell);

                
                if (listaLesion.Count == 0)
                {
                    tipo = "Ninguna";
                    lugar = "Ninguno";
                    observacion = "Ninguna";
                }
                else
                {
                    foreach (var lesion in listaLesion)
                    {
                        if (lesion.Habilitado != false)
                        {
                            tipo = lesion.Tipo.Nombre;
                            observacion = lesion.Observacion;
                            lugar = lesion.Lugar != null ? ControlCatalogo.Obtener(Convert.ToInt32(lesion.Lugar),Convert.ToInt32(Entity.TipoDeCatalogo.lugar)).Nombre : "";

                            cell = new PdfPCell(new Phrase(tipo, normalfont));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.BackgroundColor = new BaseColor(255, 255, 255);
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.Border = Rectangle.NO_BORDER;
                            tt2.AddCell(cell);

                            cell = new PdfPCell(new Phrase(lugar, normalfont));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.BackgroundColor = new BaseColor(255, 255, 255);
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.Border = Rectangle.NO_BORDER;
                            tt2.AddCell(cell);

                            cell = new PdfPCell(new Phrase(observacion, normalfont));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.BackgroundColor = new BaseColor(255, 255, 255);
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.Border = Rectangle.NO_BORDER;
                            tt2.AddCell(cell);

                        }

                    }
                    CeldaVacio(tt2, 3);
                }
            }

            if(listaTatuaje.Count!=0)
            {
                cell = new PdfPCell(new Phrase("TATUAJES: ", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt2.AddCell(cell);

                CeldaVacio(tt2, 2);

                cell = new PdfPCell(new Phrase("Tipo:", boldfont));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Lugar:", boldfont));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Observación:", boldfont));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt2.AddCell(cell);

                if (listaTatuaje == null)
                {
                    tipo = "Ninguna";
                    lugar = "Ninguno";
                    observacion = "Ninguna";
                }
                else
                {
                    foreach (var tatuaje in listaTatuaje)
                    {
                        if (tatuaje.Habilitado != false)
                        {
                            tipo = tatuaje.Tipo != null ? tatuaje.Tipo.Nombre : "Ninguna";
                            observacion = tatuaje.Observacion;
                            lugar = tatuaje.Lugar != null ? ControlCatalogo.Obtener(Convert.ToInt32(tatuaje.Lugar),Convert.ToInt32( Entity.TipoDeCatalogo.lugar)).Nombre : "";

                            cell = new PdfPCell(new Phrase(tipo, normalfont));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.BackgroundColor = new BaseColor(255, 255, 255);
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.Border = Rectangle.NO_BORDER;
                            tt2.AddCell(cell);

                            cell = new PdfPCell(new Phrase(lugar, normalfont));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.BackgroundColor = new BaseColor(255, 255, 255);
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.Border = Rectangle.NO_BORDER;
                            tt2.AddCell(cell);

                            cell = new PdfPCell(new Phrase(observacion, normalfont));
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.BackgroundColor = new BaseColor(255, 255, 255);
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.Border = Rectangle.NO_BORDER;
                            tt2.AddCell(cell);

                            CeldaVacio(tt2, 3);
                        }
                    }
                    CeldaVacio(tt2, 3);
                }
            }

            

            documentpdf.Add(t2);
            documentpdf.Add(tt2);

            PdfPTable t3 = new PdfPTable(1);
            table.SetWidths(width);
            table.TotalWidth = 550;
            table.DefaultCell.Border = Rectangle.NO_BORDER;

            PdfPTable tt3 = new PdfPTable(1);
            tabla.SetWidths(w2);
            tabla.TotalWidth = 550;
            tabla.DefaultCell.Border = Rectangle.NO_BORDER;

            //cell = new PdfPCell(new Phrase("Inspección ocular / Interrogatorio:", boldfont));
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //cell.BackgroundColor = new BaseColor(255, 255, 255);
            //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            //cell.Border = Rectangle.NO_BORDER;
            //tt3.AddCell(cell);

            CeldaConChunk(tt3, boldfont, normalfont, "Inspección ocular / Interrogatorio:                                              " +
                        "Tipo de examen: ", em.TipoExamen.Nombre, 0, 0, 0);

            cell = new PdfPCell(new Phrase(em.InspeccionOcular, normalfont));
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = new BaseColor(255, 255, 255);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Border = Rectangle.NO_BORDER;
            tt3.AddCell(cell);

            cell = new PdfPCell(new Phrase("Indicaciones médicas:", boldfont));
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = new BaseColor(255, 255, 255);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Border = Rectangle.NO_BORDER;
            tt3.AddCell(cell);

            cell = new PdfPCell(new Phrase(em.IndicacionesMedicas, normalfont));
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = new BaseColor(255, 255, 255);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Border = Rectangle.NO_BORDER;
            tt3.AddCell(cell);
             
            cell = new PdfPCell(new Phrase("Observación:", boldfont));
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = new BaseColor(255, 255, 255);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Border = Rectangle.NO_BORDER;
            tt3.AddCell(cell);

            cell = new PdfPCell(new Phrase(em.Observacion, normalfont));
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.BackgroundColor = new BaseColor(255, 255, 255);
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Border = Rectangle.NO_BORDER;
            tt3.AddCell(cell);
            
            string medLegal = (em.ConsecuenciasLesiones) ? "Sí" : "No";
            string fallecio = (em.Fallecimiento) ? "Sí":"No";

            if (bolHermosillo && !em.Fallecimiento) {
                CeldaConChunk2(tt3, boldfont, normalfont, "Las lesiones dejan consecuencias médico legales: ", medLegal, 0, 0, 0, "", "");
            } else {
                CeldaConChunk2(tt3, boldfont, normalfont, "Las lesiones dejan consecuencias médico legales: ", medLegal, 0, 0, 0, "              Fallecimiento: ", fallecio);
            }
             
            CeldaConChunk(tt3, boldfont, normalfont, "Presenta lesiones visibles: ", ((em.Lesion_visible) ? "Si" : "No"), 0, 0, 0);
           // CeldaVacio(tt3, 1);

            documentpdf.Add(t3);
            documentpdf.Add(tt3);
            //Fin apartado inspecciones

            //Inicia apartado EXÁMENES

            if (pruebaEM != null)
            {
                float[] f = new float[] { 100 };
                PdfPTable encabezado = new PdfPTable(1);
                encabezado.SetWidths(f);
                encabezado.TotalWidth = 550;
                encabezado.HorizontalAlignment = Element.ALIGN_CENTER;

                cell = new PdfPCell(new Phrase("EXÁMENES", titlefont));
                cell.BorderWidth = 0;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Padding = space;
                encabezado.AddCell(cell);

                documentpdf.Add(encabezado);

                PdfPTable t4 = new PdfPTable(1);
                t4.SetWidths(width);
                t4.TotalWidth = 550;
                t4.DefaultCell.Border = Rectangle.NO_BORDER;
                t4.HorizontalAlignment = Element.ALIGN_CENTER;

                float[] wd40 = new float[] { 90, 90, 90, 90, 90, 90 };
                PdfPTable tt4 = new PdfPTable(6);
                tt4.SetWidths(wd40);
                tt4.TotalWidth = 550;
                tt4.DefaultCell.Border = Rectangle.NO_BORDER;
                
                cell = new PdfPCell(new Phrase("Mucosas", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Aliento", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Ex. Neurológico", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Alcoholímetro", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Disartía", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Conjuntivas", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                var tipoX = ControlCatalogo.Obtener(pruebaEM.MucosasId, 102);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(pruebaEM.AlientoId, 103);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(pruebaEM.Examen_neurologicoId, 104);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase(pruebaEM.Alcoholimetro, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                string disartiaTexto;
                if (pruebaEM.Disartia)
                    disartiaTexto = (pruebaEM.Disartia) ? "Sí" : "No";
                else
                    disartiaTexto = "No especificado";

                cell = new PdfPCell(new Phrase(disartiaTexto, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(pruebaEM.ConjuntivasId, 105);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Marcha", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Pupilas", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Coordinación", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Reflejos Pupilares", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Reflejos Osteo - Tendinosos", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Romberg", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(pruebaEM.MarchaId, 106);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(pruebaEM.PupilasId, 117);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(pruebaEM.CoordinacionId, 118);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(pruebaEM.Reflejos_pupilaresId, 107);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);
                tipoX = ControlCatalogo.Obtener(pruebaEM.TendinososId, 108);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(pruebaEM.RomberqId, 110);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Conducta", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Lenguaje", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Atención", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Orientación", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Diadococinesia", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Dedo-Nariz", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(pruebaEM.ConductaId, 109);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(pruebaEM.LenguajeId, 111);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(pruebaEM.AtencionId, 112);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(pruebaEM.OrientacionId, 113);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(pruebaEM.DiadococinenciaId, 114);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                tipoX = ControlCatalogo.Obtener(pruebaEM.DedoId, 115);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Talón-Rodilla", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("T/A", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("F/C", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("F/R", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Pulso", titlefont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                CeldaVacio(tt4, 1);

                tipoX = ControlCatalogo.Obtener(pruebaEM.TalonId, 116);
                cell = new PdfPCell(new Phrase(tipoX.Descripcion, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase(pruebaEM.TA, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase(pruebaEM.FC, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase(pruebaEM.FR, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                cell = new PdfPCell(new Phrase(pruebaEM.Pulso, normalfont));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                tt4.AddCell(cell);

                CeldaVacio(tt4, 1);

                documentpdf.Add(t4);
                documentpdf.Add(tt4);
            }
            else
            {
                PdfPTable noExamen = new PdfPTable(1);
                table.SetWidths(width);
                table.TotalWidth = 550;
                table.DefaultCell.Border = Rectangle.NO_BORDER;
                noExamen.HorizontalAlignment = Element.ALIGN_CENTER;

                CeldaVacio(noExamen, 6);
                cell = new PdfPCell(new Phrase("¡EXAMEN MÉDICO NO EFECTUADO!", negritabold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BackgroundColor = new BaseColor(255, 255, 255);
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.Border = Rectangle.NO_BORDER;
                noExamen.AddCell(cell);

                CeldaVacio(noExamen, 1);

                //documentpdf.Add(noExamen);
                
            }            

        }

        private static void CeldaConChunk(PdfPTable tabla, Font boldfont, Font normalfont, string v)
        {
            throw new NotImplementedException();
        }

        //Crear celdas
        public static void Celda(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                tabla.AddCell(celda);
            }

        }

        public static void CeldaConChunk(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, int bordo, int horizontal, int vertical)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk(valor, FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = horizontal;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }

        public static void CeldaConChunk2(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, int bordo, int horizontal, int vertical,string titulo2,string valor2)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk(valor, FuenteValor);
            Chunk chunk3 = new Chunk(titulo2, FuenteTitulo);
            Chunk chunk4 = new Chunk(valor2, FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            elements.Add(chunk3);
            elements.Add(chunk4);
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = horizontal;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }

        //Crear celdas para tablas
        public static void CeldaParaTablas(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, BaseColor colorrelleno, float espacio, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                celda.BackgroundColor = colorrelleno;
                celda.Padding = espacio;
                celda.UseAscender = true;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas vacias
        public static void CeldaVacio(PdfPTable tabla, int cantidad)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                celda.BorderWidth = 0;
                tabla.AddCell(celda);
            }

        }
    }

    public class examenAux
    {
        public int Id { get; set; }
        public string nombre { get; set; }
        public string paterno { get; set; }
        public string materno { get; set; }
        public int sexoId { get; set; }
        public int edad { get; set; }
        public bool riesgo { get; set; }
        public bool consecuencias { get; set; }
        public DateTime fechaExamen { get; set; }
        public DateTime fechaRegistro { get; set; }
        public int tipoExamen { get; set; }
        public string inspeccion { get; set; }
        public string indicaciones { get; set; }
        public string observacion { get; set; }
        public int mucosasId { get; set; }
        public int alientoId { get; set; }
        public string alcoholimetro { get; set; }
        public int neuroId { get; internal set; }
        public bool disartiaId { get; set; }
        public int conjuntivasId { get; set; }
        public int marchaId { get; set; }
        public int pupilasId { get; set; }
        public int coordinacionId { get; set; }
        public int reflejosPupiId { get; set; }
        public int reflejosTendId { get; set; }
        public int RombergId { get; set; }
        public int conductaId { get; set; }
        public int orientacionId { get; set; }
        public int talonRodillaId { get; set; }
        public string ta { get; set; }
        public int diadococinesiaId { get; set; }
        public string fc { get; set; }
        public int lenguajeId { get; set; }
        public string fr { get; set; }
        public int atencionId { get; set; }
        public int dedoNarizId { get; set; }
        public string pulso { get; set; }
        public string tipoIntox { get; set; }
        public string obsIntox { get; set; }
        public string tipoLesion { get; set; }
        public string obsLesion { get; set; }
        public string tipoTatu { get; set; }
        public string obsTatu { get; set; }
        public string numRemision { get; set; }
    }
}
