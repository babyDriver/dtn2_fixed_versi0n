﻿using DataAccess.PagoMulta;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlPagoMulta
    {
        private static ServicioPagoMulta servicio = new ServicioPagoMulta("SQLConnectionString");

        public static List<Entity.PagoMulta> ObtenerByInternoId(int id)
        {
            return servicio.ObtenerByInternoId(id);
        }
    }
}
