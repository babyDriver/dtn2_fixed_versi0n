﻿using DataAccess.WSAInstitucion;
using System.Collections.Generic;

namespace Business
{
    public class ControlWSAInstitucion
    {
        private static ServicioWSAInstitucion servicio = new ServicioWSAInstitucion("SQLConnectionString");

        public static List<Entity.WSAInstitucion> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.WSAInstitucion ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static int Guardar(Entity.WSAInstitucion institucion)
        {
            return servicio.Guardar(institucion);
        }

        public static void Actualizar(Entity.WSAInstitucion institucion)
        {
            servicio.Actualizar(institucion);
        }
    }
}
