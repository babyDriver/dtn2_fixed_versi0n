﻿using DataAccess.DashboardMotivoDetencion;
using System.Collections.Generic;
using System;


namespace Business
{
    public class ControlDashboardMotivosDetencion
    {
        private static ServicioDashboardMotivoDetencion servicio = new ServicioDashboardMotivoDetencion("SQLConnectionString");

        public static List<Entity.DashboardMotivoDetencion> Get_List_By_ContratoId(int ContratoId)
        {

            return servicio.Get_List_By_ContratoId(ContratoId);
        }

    }
}
